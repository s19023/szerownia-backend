package com.example.szerowniabackend.Email;

import com.example.szerowniabackend.Models.Entities.ResetToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@Async
public class EmailSender {

    @Value("${spring.mail.username}")
    public static String email;

    private static String SZEROWNIA_EMAIL;


    private static final String LINK_TO_RESET = "http://localhost:3000/passwordreset/";
    private static final String LINK_TO_ACCOUNT = "http://localhost:3000/my-profile/ads";

    @Value("${spring.mail.username}")
    public void setSzerowniaEmail(String email) {
        EmailSender.SZEROWNIA_EMAIL = email;
    }

    private static JavaMailSender emailSender;
    private static TemplateEngine templateEngine;


    @Autowired
    public EmailSender(JavaMailSender emailSender, TemplateEngine templateEngine) {
        EmailSender.emailSender = emailSender;
        EmailSender.templateEngine = templateEngine;
    }

    @Async
    public void sendMessageWhenCreateRentalAd(String userEmail) {

        Context context = new Context();
        context.setVariable("title", "Właśnie utworzyłeś nowe ogłoszenie wypożyczenia!");
        context.setVariable("link", LINK_TO_ACCOUNT);
        String body = templateEngine.process("template", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(userEmail);
            mess.setSubject("Utworzyłeś nowe ogłoszenie wypożyczenia!");
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }

    @Async
    public void sendMessageWhenCreateSearchAd(String userEmail) {

        Context context = new Context();
        context.setVariable("title", "Właśnie utworzyłeś nowe ogłoszenie poszukiwania!");
        context.setVariable("link", LINK_TO_ACCOUNT);
        String body = templateEngine.process("template", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(userEmail);
            mess.setSubject("Utworzyłeś nowe ogłoszenie poszukiwania!");
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }

    @Async
    public void sendMessageWhenUserGetOpinion(String userEmail) {

        Context context = new Context();
        context.setVariable("title", "Właśnie otrzymałeś komentarz!");
        context.setVariable("link", LINK_TO_ACCOUNT);
        String body = templateEngine.process("template", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(userEmail);
            mess.setSubject("Otrzymałeś nowy komentarz!");
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }

    @Async
    public void sendMessageWhenRentalAdGoToModerator(String userEmail) {

        Context context = new Context();
        context.setVariable("title", "Twoje ogłoszenie trafiło do sprawdzenia przez moderację");
        context.setVariable("link", LINK_TO_ACCOUNT);
        String body = templateEngine.process("template", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(userEmail);
            mess.setSubject("Ogłoszenie trafiło do moderacji");
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }

    @Async
    public void sendMessageWhenUserRegistered(String userEmail) {

        Context context = new Context();
        context.setVariable("title", "Utworzyłeś nowe konto na Szerownia \n Witamy w zespole :)");
        context.setVariable("link", LINK_TO_ACCOUNT);
        String body = templateEngine.process("template", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(userEmail);
            mess.setSubject("Zarejestrowałeś się na Szerownia!");
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }

    @Async
    public void sendMessageToSomeoneWhenBorrowProduct(String userEmail, String userName, String product) {

        Context context = new Context();
        context.setVariable("title", "Użtkownik " + userName + " wypożyczył twój/e produkt/y: " + product);
        context.setVariable("link", LINK_TO_ACCOUNT);
        String body = templateEngine.process("template", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(userEmail);
            mess.setSubject("Twoje przedmioty zostały wypożyczone!");
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }

    @Async
    public void sendMessageToYourSelfWhenBorrowProduct(String userEmail, String userName, String product) {

        Context context = new Context();
        context.setVariable("title", "Wypożyczyłeś produkt/y " + product + " od użytkownika " + userName);
        context.setVariable("link", LINK_TO_ACCOUNT);
        String body = templateEngine.process("template", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(userEmail);
            mess.setSubject("Wypozyczyłeś przedmiot/y!");
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }

    @Async
    public void sendMessageWhenSearchedProductFound(String userEmail, String userName, String product) {

        Context context = new Context();
        context.setVariable("title", "Użytkownik " + userName + " posiada produkt/y " + product + " którego poszukujesz!");
        context.setVariable("link", LINK_TO_ACCOUNT);
        String body = templateEngine.process("template", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(userEmail);
            mess.setSubject("Znaleziono poszukiwany produkt/y!");
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }

    @Async
    public void sendMessageWithResetPasswordToken(ResetToken token) {
        Context context = new Context();
        context.setVariable("title", "Możesz zresetować hasło klikając poniżej");
        context.setVariable("link", LINK_TO_RESET + token.getToken());
        String body = templateEngine.process("template-reset", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(token.getEmail());
            mess.setSubject("Link do resetowania hasła.");
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }

    @Async
    public void sendMessageWhenUserBlocked(String userEmail, String message) {

        Context context = new Context();
        context.setVariable("title", "Twoje konto na portalu szerownia.pl zostalo zablokowane");
        context.setVariable("comment", message);

        String body = templateEngine.process("template-blocked", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(userEmail);
            mess.setSubject("Konto zablokowane");
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }

    @Async
    public void sendMessageWithShipmentNumber(String userEmail, String shipmentNumber, String adTitle) {
        Context context = new Context();
        context.setVariable("title", "Nadano przesyłkę o numerze: " + shipmentNumber);
        context.setVariable("link", LINK_TO_ACCOUNT);
        String body = templateEngine.process("template", context);

        MimeMessage mail = emailSender.createMimeMessage();
        MimeMessageHelper mess = null;
        try {
            mess = new MimeMessageHelper(mail, true);

            mess.setFrom(SZEROWNIA_EMAIL);
            mess.setTo(userEmail);
            mess.setSubject("Nadano nową przesyłkę! - " + adTitle);
            mess.setText(body, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(mail);
    }
}
