package com.example.szerowniabackend.Models.Builders;

import com.example.szerowniabackend.Models.Entities.Product;

import javax.persistence.Column;

public class ProductBuilder {

    private String name;
    private String make;
    private String model;
    //private String description;
    private int condition;
    private int estimatedValue;

    public ProductBuilder(String name, int condition, int estimatedValue) {
        this.name = name;
        this.condition = condition;
        this.estimatedValue = estimatedValue;
    }

    public Product build() {
        return new Product(name, make, model, condition, estimatedValue);
    }

    public ProductBuilder withModel(String model) {
        this.model = model;
        return this;
    }

    public ProductBuilder withMake(String make) {
        this.make = make;
        return this;
    }
}
