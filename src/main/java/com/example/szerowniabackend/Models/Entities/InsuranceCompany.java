package com.example.szerowniabackend.Models.Entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class InsuranceCompany {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String nip;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private Double commissionRate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "location_id")
    private Location location;

    @OneToMany(mappedBy = "insuranceCompany")
    private List<Policy> policies = new ArrayList<>();

    @OneToMany(mappedBy = "insuranceCompany", cascade = CascadeType.ALL)
    private List<User> insuranceAdjusters = new ArrayList<>();

    @Column(nullable = false)
    private boolean isVisible = true;

    public InsuranceCompany() {
    }

    public InsuranceCompany(String name, String nip, String email, Double commissionRate, Location location) {
        this.name = name;
        this.nip = nip;
        this.email = email;
        this.commissionRate = commissionRate;
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Double commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Policy> getPolicies() {
        return policies;
    }

    public void setPolicies(List<Policy> policies) {
        this.policies = policies;
    }

    public List<User> getInsuranceAdjusters() {
        return insuranceAdjusters;
    }

    public void setInsuranceAdjusters(List<User> insuranceAdjusters) {
        this.insuranceAdjusters = insuranceAdjusters;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }
}
