package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.NotAcceptableException;
import com.example.szerowniabackend.Models.Entities.Enums.ModTicketResult;
import com.example.szerowniabackend.Models.Entities.Enums.ModTicketSubject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.time.LocalDateTime;


@Entity
public class ModTicket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime reportedDate = LocalDateTime.now();

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ModTicketSubject subject;

    @Column(nullable = false)
    private String description;

    @Column(nullable = true)
    private LocalDate moderationDate;

    @Column(nullable = true)
    @Enumerated(EnumType.STRING)
    private ModTicketResult result;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("modRequests")
    @JoinColumn(name = "notifier_id", referencedColumnName = "id")
    private User notifier;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("modRequests")
    @JoinColumn(name = "reported_id", referencedColumnName = "id")
    private User reported;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("modRequests")
    @JoinColumn(name = "opinion_id", referencedColumnName = "id")
    private Opinion opinion;

    @SuppressWarnings("JpaDataSourceORMInspection")
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("modRequests")
    @JoinColumn(name = "ad_id", referencedColumnName = "id")
    private Ad ad;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("modRequests")
    @JoinColumn(name = "complaint_id", referencedColumnName = "id")
    private Complaint complaint;

    @OneToMany(mappedBy = "modTicket", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<Thread> threads;

    public ModTicket() {
    }

    public ModTicket(ModTicketSubject subject, String description, User author, User taker) {
        this.subject = subject;
        this.description = description;
        this.notifier = author;
        author.getModRequestsSent().add(this);
        this.reported = taker;
        taker.getModRequestsRecived().add(this);
    }

    public ModTicket(ModTicketSubject subject, String description, User author, Opinion opinion) {
        this.subject = subject;
        this.description = description;
        this.notifier = author;
        author.getModRequestsSent().add(this);
        this.opinion = opinion;
        opinion.getModTickets().add(this);
    }

    public ModTicket(ModTicketSubject subject, String description, User author, Ad ad) {
        this.subject = subject;
        this.description = description;
        this.notifier = author;
        author.getModRequestsSent().add(this);
        this.ad = ad;
    }

    public ModTicket(ModTicketSubject subject, String description, User author, Complaint complaint) {
        this.subject = subject;
        this.description = description;
        this.notifier = author;
        author.getModRequestsSent().add(this);
        this.complaint = complaint;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ModTicketSubject getSubject() {
        return subject;
    }

    public void setSubject(ModTicketSubject subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(LocalDateTime reportedDate) {
        this.reportedDate = reportedDate;
    }

    public LocalDate getModerationDate() {
        return moderationDate;
    }

    public void setModerationDate(LocalDate moderationDate) {
        this.moderationDate = moderationDate;
    }

    public ModTicketResult getResult() {
        return result;
    }

    public void setResult(ModTicketResult result) {
        this.result = result;
    }

    public User getNotifier() {
        return notifier;
    }

    public void setNotifier(User author) {
        this.notifier = author;
    }

    public User getReported() {
        return reported;
    }

    public void setReported(User reported) {
        this.reported = reported;
    }

    public Opinion getOpinion() {
        return opinion;
    }

    public void setOpinion(Opinion opinion) {
        this.opinion = opinion;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }

    public List<Thread> getThreads() {
        return threads;
    }

    public void setThreads(List<Thread> threads) {
        this.threads = threads;
    }

    public Complaint getComplaint() {
        return complaint;
    }

    public void setComplaint(Complaint complaint) {
        this.complaint = complaint;
    }


    public static class ParameterNotFound extends BadRequestException {
        public ParameterNotFound(String param) {
            super("Nie podano " + param);
        }
    }

    public static class AuthorMissing extends BadRequestException {
        public AuthorMissing() {
            super("Nie podano autora zgłoszenia");
        }
    }

    public static class BadCommand extends NotAcceptableException {
        public BadCommand(String command) {
            super("Niepoprawny wynik moderacji zgłoszenia: " + command);
        }
    }

    public static class ReportedYourself extends BadRequestException {
        public ReportedYourself() {
            super("Nie można zgłosić samego siebie");
        }
    }

    public static class NotYours extends BadRequestException {
        public NotYours() {
            super("Nie możesz edytować cudzego zgłoszenia");
        }
    }
}
