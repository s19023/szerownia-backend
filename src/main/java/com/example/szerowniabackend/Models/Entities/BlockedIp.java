package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.NotFoundException;

import javax.persistence.*;

@Entity
public class BlockedIp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private String ip;

    public BlockedIp() {
    }

    public BlockedIp(Long id, String ip) {
        this.id = id;
        this.ip = ip;
    }

    public BlockedIp(String ip) {
        this.ip = ip;
    }

    public Long getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public static class IpCannotBeNull extends BadRequestException{
        public IpCannotBeNull() {  super("Nie podano adresu IP"); }
    }

    public static class IpExists extends BadRequestException {
        public IpExists() {
            super("Podany adres IP już istnieje");
        }
    }

    public static class NotFound extends NotFoundException {
        public NotFound() {  super("Nie znaleziono takiego adresu IP");  }
    }

}
