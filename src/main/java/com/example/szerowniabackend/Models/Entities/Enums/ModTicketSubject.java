package com.example.szerowniabackend.Models.Entities.Enums;

public enum ModTicketSubject {

    FRAUD {
        public String toString() {
            return "Oszustwo";
        }
    },
    OFFENDED {
        public String toString() {
            return "Obraźliwe treści";
        }
    },
    INDECENT {
        public String toString() {
            return "Niecenzuralne treści";
        }
    },
    ILLEGAL {
        public String toString() {
            return "Nielegalne treści";
        }
    },
    OTHER {
        public String toString() {
            return "Inny powód";
        }
    }



}
