package com.example.szerowniabackend.Models.Entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private Double latitude;

    @Column(nullable = false)
    private Double longitude;

    @Column(nullable = false)
    private String name;

    //TODO: Incorrect mapped association, it should be OneToMany according to diagram
    @OneToOne(mappedBy = "location")
    @JsonIgnoreProperties("location")
    private Ad ad;

    @OneToMany(mappedBy = "location")
    private List<Claim> claims = new ArrayList<>();

    @OneToMany(mappedBy = "location")
    private List<InsuranceCompany> insuranceCompanies = new ArrayList<>();

    public Location() {
    }

    public Location(double latitude, double longitude, String name) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }

    public List<Claim> getClaims() {
        return claims;
    }

    public void setClaims(List<Claim> claims) {
        this.claims = claims;
    }

    public List<InsuranceCompany> getInsuranceCompanies() {
        return insuranceCompanies;
    }

    public void setInsuranceCompanies(List<InsuranceCompany> insuranceCompanies) {
        this.insuranceCompanies = insuranceCompanies;
    }

    @Override
    public String toString() {
        return name;
    }
}
