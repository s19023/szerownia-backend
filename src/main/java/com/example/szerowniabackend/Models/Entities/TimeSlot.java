package com.example.szerowniabackend.Models.Entities;

import javax.persistence.Embeddable;
import java.time.LocalDate;

@Embeddable
public class TimeSlot {

    private LocalDate startDate;
    private LocalDate endDate;

    public TimeSlot() {
    }

    public TimeSlot(LocalDate startDate) {
        this.startDate = startDate;
    }

    public TimeSlot(LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
