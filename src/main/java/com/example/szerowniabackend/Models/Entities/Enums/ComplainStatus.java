package com.example.szerowniabackend.Models.Entities.Enums;

public enum ComplainStatus {

    SUBMITTED{
        public String toString() { return "Zgłoszona"; }
    },
    ACCEPTED{
        public String toString() { return "Przyjęta"; }
    },
    REJECTED{
        public String toString() { return "Odrzucona"; }
    }
}
