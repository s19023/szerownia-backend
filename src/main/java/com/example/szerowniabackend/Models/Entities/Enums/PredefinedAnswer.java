package com.example.szerowniabackend.Models.Entities.Enums;

public enum PredefinedAnswer {

    EDIT {
        public String toString() { return "Tresc do zmodyfikowania"; }
    },
    HIDE {
        public String toString() {
            return "Tresc zostala ukryta";
        }
    },
    MODIFIED {
        public String toString() {
            return "Tresc podmiotu zglosznenia zostala zaakceptowana";
        }
    }


}
