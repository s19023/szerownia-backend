package com.example.szerowniabackend.Models.Entities;

import javax.persistence.*;

@Entity
public class CustomRequirement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long idCustomRequirement;

    @Column(nullable = false)
    private String message;

    @ManyToOne()
    private RentalAd rentalAd;

    public CustomRequirement() {
    }

    public CustomRequirement(Long idCustomRequirement, String message) {
        this.idCustomRequirement = idCustomRequirement;
        this.message = message;
    }

    public Long getIdCustomRequirement() {
        return idCustomRequirement;
    }

    public void setIdCustomRequirement(Long idCustomRequirement) {
        this.idCustomRequirement = idCustomRequirement;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RentalAd getRentalAd() {
        return rentalAd;
    }

    public void setRentalAd(RentalAd rentalAd) {
        this.rentalAd = rentalAd;
    }

    @Override
    public String toString() {
        return "CustomRequirement{" +
                "idCustomRequirement=" + idCustomRequirement +
                ", message='" + message + '\'' +
                ", rentalAd=" + rentalAd +
                '}';
    }
}
