package com.example.szerowniabackend.Models.Entities;


import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.NotFoundException;

import javax.persistence.*;

@Entity
public class VulgarWords {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false, unique = true)
    private String vulgarWord;

    public VulgarWords(Long id, String vulgarWord) {
        this.id = id;
        this.vulgarWord = vulgarWord;
    }

    public VulgarWords(String vulgarWord) {
        this.vulgarWord = vulgarWord;
    }

    public VulgarWords() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVulgarWord() {
        return vulgarWord;
    }

    public void setVulgarWord(String vulgarWord) {
        this.vulgarWord = vulgarWord;
    }


    public static class RentalAdContainsVulgarWords extends BadRequestException {
        public RentalAdContainsVulgarWords() {
            super("Twoje ogłoszenie może zawierać wulgarne słowa, zostało one wysłane do moderacji");
        }
    }

    public static class NotFound extends NotFoundException {
        public NotFound(String vulgarWord) {
            super("Podane słowo '" + vulgarWord + "' nie istnieje");
        }
    }

    public static class VulgarWordExist extends BadRequestException {
        public VulgarWordExist() {
            super("Podane słowo już znajduje się w słowniku");
        }
    }
}



