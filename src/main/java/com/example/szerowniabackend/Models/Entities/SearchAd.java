package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.List;

@Entity
@DiscriminatorValue(value = SearchAd.DISCRIMINATOR)
public class SearchAd extends Ad {

    public static final String DISCRIMINATOR = "SEARCHAD";

    public SearchAd() {
    }

    public SearchAd(String title, List<TimeSlot> timeSlotList, Location location,
                    List<PickupMethod> pickupMethod, String description) {
        super(DISCRIMINATOR,title, timeSlotList, location, pickupMethod, description);
    }

    @OneToMany(mappedBy = "searchAd", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<RentalAd> rentalAds;

    @Override
    public String toString() {
        return "SearchAd{}";
    }

    public static class ProductIsNotSearchProduct extends BadRequestException {
        public ProductIsNotSearchProduct() {
            super("Dany przedmiot nie jest przedmiotem poszukiwania");
        }
    }
}
