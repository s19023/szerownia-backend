package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Models.Entities.Enums.AdStatus;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@DiscriminatorColumn(name = "ad_type", discriminatorType = DiscriminatorType.STRING)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Ad {

    private String discriminator;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = true)
    private String title;

    @Column(nullable = true)
    private LocalDate creationDate = LocalDate.now();

    @Column(nullable = false)
    @Size(min = 1)
    @ElementCollection
    private List<TimeSlot> timeSlotList;

    @Column
    private LocalDate endDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "location_id", referencedColumnName = "id")
    @JsonIgnoreProperties("ad")
    private Location location;

    @Column(nullable = true)
    @Enumerated(EnumType.STRING)
    @ElementCollection
    private List<PickupMethod> pickupMethod;

    @Column(nullable = true)
    private String description;

    @Column(nullable = false)
    private boolean isVisible = true;


    @SuppressWarnings("JpaDataSourceORMInspection")
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("ads")
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @ManyToMany(mappedBy = "includeAds", cascade = {CascadeType.ALL})
    @JsonIgnoreProperties("includeAds")
    private Set<Product> products = new HashSet<>();

    @OneToMany(mappedBy = "ad", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("ad")
    private Set<Hire> hire = new HashSet<>();

    @OneToMany(mappedBy = "ad", cascade = {CascadeType.ALL})
    @JsonIgnoreProperties("ad")
    private List<ModTicket> modTickets;

    @OneToMany(mappedBy = "ad", cascade = {CascadeType.ALL})
    @JsonIgnoreProperties("ad")
    private Set<ImageEntity> imageEntity;

    @OneToMany(mappedBy = "ad", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<Thread> threads;

    public Ad() {
    }

    public Ad(String discriminator, String title, List<TimeSlot> timeSlotList,Location location,
              List<PickupMethod> pickupMethod, String description, User user, LocalDate endDate) {
        this.discriminator = discriminator;
        this.title = title;
        this.timeSlotList = timeSlotList;
        this.location = location;
        this.pickupMethod = pickupMethod;
        this.description = description;
        this.user = user;
        this.endDate = endDate;

    }

    public Ad(String discriminator, String title, List<TimeSlot> timeSlotList,
              Location location, List<PickupMethod> pickupMethod, String description) {
        this.discriminator = discriminator;
        this.title = title;
        this.timeSlotList = timeSlotList;
        this.location = location;
        this.pickupMethod = pickupMethod;
        this.description = description;
    }

    public String getDiscriminator() {
        return discriminator;
    }

    public void setDiscriminator(String discriminator) {
        this.discriminator = discriminator;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public List<TimeSlot> getTimeSlotList() {
        return timeSlotList;
    }

    public void setTimeSlotList(List<TimeSlot> timeSlotList) {
        this.timeSlotList = timeSlotList;
    }

    public List<PickupMethod> getPickupMethod() {
        return pickupMethod;
    }

    public void setPickupMethod(List<PickupMethod> pickupMethodList) {
        this.pickupMethod = pickupMethodList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Set<ImageEntity> getImageEntity() {
        return imageEntity;
    }

    public void setImageEntity(Set<ImageEntity> imageEntity) {
        this.imageEntity = imageEntity;
    }

    public Set<Hire> getHire() {
        return hire;
    }

    public void setHire(Set<Hire> hire) {
        this.hire = hire;
    }

    public List<Thread> getThreads() {
        return threads;
    }

    public void setThreads(List<Thread> threads) {
        this.threads = threads;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Ad{" +
                "discriminator='" + discriminator + '\'' +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", creationDate=" + creationDate +
                ", timeSlotList=" + timeSlotList +
                ", endDate=" + endDate +
                ", location=" + location +
                ", pickupMethod=" + pickupMethod +
                ", description='" + description + '\'' +
                ", isVisible=" + isVisible +
                ", user=" + user +
                ", products=" + products +
                ", hire=" + hire +
                ", modTickets=" + modTickets +
                ", imageEntity=" + imageEntity +
                ", threads=" + threads +
                '}';
    }
}
