package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long idCategory;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private boolean isAbstract;

    @Column(nullable = true)
    private String iconName;

    @Column(nullable = true)
    private String iconColor;

    @ManyToOne()
    @JsonIgnoreProperties("subCategories")
    private Category parentCategory;

    @OneToMany(mappedBy = "parentCategory")
    @JsonIgnoreProperties("parentCategory")
    private List<Category> subCategories = new ArrayList<>();

    @OneToMany(mappedBy = "category")
    @JsonIgnoreProperties("category")
    private List<Product> products = new ArrayList<>();

    @OneToMany(mappedBy = "category", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("category")
    private List<FeatureCategory> featureCategories = new ArrayList<>();

    public Category() {
    }

    public Category(String name, boolean isAbstract, Category parentCategory) {
        this.name = name;
        this.isAbstract = isAbstract;
        this.parentCategory = parentCategory;
    }

    public Long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAbstract() {
        return this.isAbstract;
    }

    public void setAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public List<Category> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<Category> subCategory) {
        this.subCategories = subCategory;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<FeatureCategory> getFeatureCategories() {
        return featureCategories;
    }

    public void setFeatureCategories(List<FeatureCategory> featureCategories) {
        this.featureCategories = featureCategories;
    }

    @Override
    public String toString() {

        return "Category{" +
                "id=" + idCategory +
                ", name='" + name + '\'' +
                ", isAbstract=" + isAbstract +
                ", parentCategory=" + (parentCategory == null ? "none" : parentCategory.name) +
                ", subCategory=" + subCategories.stream().map(Category::getName).collect(Collectors.joining(",")) +
                '}';
    }

    public static class SubcategoryCannotHaveProducts extends ForbiddenException {
        public SubcategoryCannotHaveProducts(String name) {
            super("Kategoria " + name + " ma przypisane produkty stąd nie może mieć podkategorii");
        }
    }

    public static class AbstractCategoryHasNoFeatures extends BadRequestException {
        public AbstractCategoryHasNoFeatures(String name) {
            super("Kategoria " + name + " jest abstrackyjna i nie ma przypisanych cech");
        }
    }

    public static class CannotDeleteCategoryWithProducts extends ForbiddenException {
        public CannotDeleteCategoryWithProducts(String name) {
            super("Kategoria " + name + " ma przypisane produkty i nie może być usunięta");
        }
    }

    public static class ValidSubcategories extends ForbiddenException {
        public ValidSubcategories(String name) {
            super("Kategoria " + name + " ma podkategorie i nie może być usunięta");
        }
    }
}
