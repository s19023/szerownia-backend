package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.NotAcceptableException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Opinion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    @Min(1)
    @Max(5)
    private int rating;

    @Column(nullable = true)
    private String comment;

    @Column(nullable = false)
    private LocalDate date;

    @Column(nullable = false)
    private boolean isVisible;

    @Column(nullable = false)
    private boolean isAboutSharer;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    @JsonIgnoreProperties("given")
    private User author;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "taker_id", referencedColumnName = "id")
    @JsonIgnoreProperties("taken")
    private User taker;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "hire_id", referencedColumnName = "id")
    @JsonIgnoreProperties("hire")
    private Hire hire;

    @OneToMany(mappedBy = "opinion", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("opinion")
    private Set<ModTicket> modTickets = new HashSet<>();


    public Opinion() {
    }

    public Opinion(int rating, String comment, boolean isAboutSharer, User author, User taker, Hire hire) {
        this.rating = rating;
        this.comment = comment;
        this.isVisible = true;
        this.isAboutSharer = isAboutSharer;
        this.author = author;
        this.taker = taker;
        this.hire = hire;
        date = LocalDate.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isAboutSharer() {
        return isAboutSharer;
    }

    public void setAboutSharer(boolean aboutSharer) {
        isAboutSharer = aboutSharer;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public User getTaker() {
        return taker;
    }

    public void setTaker(User taker) {
        this.taker = taker;
    }

    public Hire getHire() { return hire; }

    public void setHire(Hire hire) { this.hire = hire; }

    public Set<ModTicket> getModTickets() {
        return modTickets;
    }

    public void setModTickets(Set<ModTicket> modTickets) {
        this.modTickets = modTickets;
    }

    public static class ContainsVulgar extends NotAcceptableException {
        public ContainsVulgar() { super("Twoja opinia może zawierać wulagrne słowa, " +
                "opinia została wysłana do moderacji"); }
    }
}
