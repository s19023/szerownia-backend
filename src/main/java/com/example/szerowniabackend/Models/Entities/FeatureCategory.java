package com.example.szerowniabackend.Models.Entities;

import javax.persistence.*;

@Entity
public class FeatureCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private boolean isFeatureRequired;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "feature_id")
    private Feature feature;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "category_id")
    private Category category;

    public FeatureCategory() {
    }

    public FeatureCategory(boolean isFeatureRequired) {
        this.isFeatureRequired = isFeatureRequired;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isFeatureRequired() {
        return isFeatureRequired;
    }

    public void setFeatureRequired(boolean featureRequired) {
        isFeatureRequired = featureRequired;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "FeatureCategory{" +
                "id=" + id +
                ", isFeatureRequired=" + isFeatureRequired +
                '}';
    }
}
