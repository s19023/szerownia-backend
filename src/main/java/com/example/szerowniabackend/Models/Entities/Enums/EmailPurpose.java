package com.example.szerowniabackend.Models.Entities.Enums;

public enum EmailPurpose {

    COMMENT_RECEIVED, AD_WENT_TO_MODERATOR, RENTAL_AD_CREATED, SEARCH_AD_CREATED,
    WELCOME_MAIL
}
