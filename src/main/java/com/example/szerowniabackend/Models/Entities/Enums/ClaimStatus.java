package com.example.szerowniabackend.Models.Entities.Enums;

public enum ClaimStatus {
    SUBMITTED{
        public String toString() { return "Zgłoszona"; }
    },
    IN_PROGRESS_BEFORE_INSPECTION{
        public String toString() { return "Oczekiwanie na oględziny"; }
    },
    IN_PROGRESS_AFTER_INSPECTION{
        public String toString() {
            return "W trakcie";
        }
    },
    ACCEPTED{
        public String toString() { return "Przyjęta"; }
    },
    REJECTED{
        public String toString() { return "Odrzucona"; }
    }
}
