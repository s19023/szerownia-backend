package com.example.szerowniabackend.Models.Entities;

public interface AdModTicket {
    Long getAdId();
    Long getModTicketId();
}
