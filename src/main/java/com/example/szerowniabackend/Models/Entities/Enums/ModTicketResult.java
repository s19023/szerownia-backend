package com.example.szerowniabackend.Models.Entities.Enums;

public enum ModTicketResult {

    HIDDEN, TO_MODIFY, MODIFIED, TO_VERIFY
}
