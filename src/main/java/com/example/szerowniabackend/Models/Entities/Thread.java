package com.example.szerowniabackend.Models.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Thread {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    private String topic;

    @OneToMany(mappedBy = "thread", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("thread")
    private List<Message> messages = new ArrayList<>();

    @ManyToOne()
    @JoinColumn(name = "rentalAd_id", referencedColumnName = "id")
    private Ad ad;

    @ManyToOne()
    @JoinColumn(name = "modeTicket_id", referencedColumnName = "id")
    private ModTicket modTicket;

    public Thread() {
    }

    public Thread(String topic) {
        this.topic = topic;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }

    public ModTicket getModTicket() {
        return modTicket;
    }

    public void setModTicket(ModTicket modTicket) {
        this.modTicket = modTicket;
    }

    @Override
    public String toString() {
        return "Thread{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                '}';
    }
}
