package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.NotFoundException;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class ResetToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String token;

    @Column(nullable = false)
    private LocalDateTime valiDate;


    public ResetToken() {
    }

    public ResetToken(String email) {
        this.email = email;
        token = generateToken();
        valiDate = LocalDateTime.now().plusMinutes(30);
    }

    private String generateToken() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getValiDate() {
        return valiDate;
    }

    public void setValiDate(LocalDateTime valiDate) {
        this.valiDate = valiDate;
    }

    public boolean isValid() {
        return LocalDateTime.now().isBefore(valiDate);
    }

    public static class NotFound extends NotFoundException {
        public NotFound(String token) {
            super("Nie podano resetującego tokena");
        }
    }
}
