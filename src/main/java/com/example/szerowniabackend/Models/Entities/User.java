package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;

@Entity
public class User extends Person {

    @Column(nullable = false)
    private String telephoneNumber;

    @Column(nullable = false)
    private Boolean isBlocked;

    private LocalDate premiumFrom;
    private LocalDate premiumTo;

    @OneToMany(mappedBy = "user")
    private Set<Ad> ads = new HashSet<>();

    @OneToMany(mappedBy = "borrower", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private Set<Hire> borrows = new HashSet<>();

    @OneToMany(mappedBy = "sharer", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private Set<Hire> shares = new HashSet<>();

    @OneToMany(mappedBy = "author", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("author")
    private Set<Opinion> given = new HashSet<>();

    @OneToMany(mappedBy = "taker", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("taker")
    private Set<Opinion> taken = new HashSet<>();

    @OneToMany(mappedBy = "user")
    private Set<Product> productsUser = new HashSet<>();

    @OneToMany(mappedBy = "sender", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("sender")
    private List<Message> messagesSent;

    @OneToMany(mappedBy = "receiver", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("receiver")
    private List<Message> messagesReceived;

    @OneToMany(mappedBy = "notifier", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("notifier")
    private List<ModTicket> modRequestsSent = new ArrayList<>();

    @OneToMany(mappedBy = "reported", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("reported")
    private List<ModTicket> modRequestsRecived = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<Claim> claims = new ArrayList<>();

    @OneToOne(mappedBy = "user", cascade = {CascadeType.ALL})
    @JsonIgnoreProperties("user")
    private ImageEntity userImage;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "insuranceCompany_id")
    private InsuranceCompany insuranceCompany;

    @OneToMany(mappedBy = "insuranceAdjuster", cascade = CascadeType.ALL)
    private List<Claim> adjusterClaims = new ArrayList<>();

    @OneToMany(mappedBy = "appraiser")
    private List<ClaimInspection> appraisersInspections = new ArrayList<>();

    @OneToMany(mappedBy = "tempAppraiser")
    private List<Claim> tempClaims = new ArrayList<>();

    @OneToMany(mappedBy = "currentUser")
    private List<Notification> notificationsForCurrentUser = new ArrayList<>();

    @OneToMany(mappedBy = "otherUser")
    private List<Notification> notificationsForOtherUser = new ArrayList<>();

    public User() {
    }

    public User(String email, String firstName, String lastName,
                String telephoneNumber, String password, String salt, Role role) {
        super(email, firstName, lastName, true, password, salt, role);
        this.telephoneNumber = telephoneNumber;
        this.isBlocked = false;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !getBlocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public Double getBorrowerRate() {
        Double d = 0.;
        int l = 0;
        if (taken.isEmpty()) return d;
        for (Opinion o : taken) {
            if (!o.isAboutSharer()) {
                d += o.getRating();
                l++;
            }
        }
        return d == 0 ? 0 : d / l;
    }

    public Double getSharerRate() {
        Double d = 0.;
        int l = 0;
        if (taken.isEmpty()) return d;
        for (Opinion o : taken) {
            if (o.isAboutSharer()) {
                d += o.getRating();
                l++;
            }
        }
        return d == 0 ? 0 : d / l;
    }

    public Double getBorrowerAverageRentalPrice() {

        double sum = 0.;

        for (Hire hire : borrows) {
            sum += hire.getCost();
        }

        return borrows.isEmpty() ? 0 : sum / borrows.size();
    }

    public Integer getBorrowerHistoryCount() {
        return borrows.size();
    }

    public Boolean getBlocked() {
        return isBlocked;
    }

    public void setBlocked(Boolean blocked) {
        isBlocked = blocked;
    }

    @Override
    public Boolean getPremium() {
        return super.getPremium();
    }

    public void setPremium(Boolean premium) {
        super.setPremium(premium);
        if (premium) {
            premiumFrom = LocalDate.now();
            premiumTo = LocalDate.now().plusDays(30);
        } else {
            premiumTo = LocalDate.now();
        }

    }

    public LocalDate getPremiumFrom() {
        return premiumFrom;
    }

    public void setPremiumFrom(LocalDate premiumFrom) {
        this.premiumFrom = premiumFrom;
    }

    public LocalDate getPremiumTo() {
        return premiumTo;
    }

    public void setPremiumTo(LocalDate premiumTo) {
        this.premiumTo = premiumTo;
    }

    public Set<Ad> getAds() {
        return ads;
    }

    public void setAds(Set<Ad> ads) {
        this.ads = ads;
    }

    public Set<Opinion> getTaken() {
        return taken;
    }

    public void setGiven(Set<Opinion> given) {
        this.given = given;
    }

    public void setTaken(Set<Opinion> taken) {
        this.taken = taken;
    }

    public List<ModTicket> getModRequestsSent() {
        return modRequestsSent;
    }

    public void setModRequestsSent(List<ModTicket> modRequestsSent) {
        this.modRequestsSent = modRequestsSent;
    }

    public List<ModTicket> getModRequestsRecived() {
        return modRequestsRecived;
    }

    public void setModRequestsRecived(List<ModTicket> modRequestsRecived) {
        this.modRequestsRecived = modRequestsRecived;
    }

    public ImageEntity getUserImage() {
        return userImage;
    }

    public void setUserImage(ImageEntity userImage) {
        this.userImage = userImage;
    }

    public List<ClaimInspection> getAppraisersInspections() {
        return appraisersInspections;
    }

    public void setAppraisersInspections(List<ClaimInspection> appraisersInspections) {
        this.appraisersInspections = appraisersInspections;
    }

    public Set<Product> getProductsUser() {
        return productsUser;
    }

    public void setProductsUser(Set<Product> productsUser) {
        this.productsUser = productsUser;
    }

    public InsuranceCompany getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public List<Claim> getAdjusterClaims() {
        return adjusterClaims;
    }

    public void setAdjusterClaims(List<Claim> adjusterClaims) {
        this.adjusterClaims = adjusterClaims;
    }

    public List<Claim> getTempClaims() {
        return tempClaims;
    }

    public void setTempClaims(List<Claim> tempClaims) {
        this.tempClaims = tempClaims;
    }

    public List<Notification> getNotificationsForCurrentUser() {
        return notificationsForCurrentUser;
    }

    public void setNotificationsForCurrentUser(List<Notification> notificationsForCurrentUser) {
        this.notificationsForCurrentUser = notificationsForCurrentUser;
    }

    public List<Notification> getNotificationsForOtherUser() {
        return notificationsForOtherUser;
    }

    public void setNotificationsForOtherUser(List<Notification> notificationsForOtherUser) {
        this.notificationsForOtherUser = notificationsForOtherUser;
    }

    public static class NotFound extends NotFoundException {
        public NotFound() {
            super("Nie znaleziono użytkownika o podanym adresie email");
        }
    }

    public static class InvalidValue extends BadRequestException {
        public InvalidValue() {
            super("Dana szkoda powinna mieć status - W trakcie");
        }
    }

    public static class InterruptedClaimWorkflow extends BadRequestException {
        public InterruptedClaimWorkflow() {
            super("Dana szkoda powinna mieć status - W trakcie");
        }
    }

    public static class InsuranceAdjusterNotRunningThisClaim extends BadRequestException {
        public InsuranceAdjusterNotRunningThisClaim() {
            super("Nie zajmujesz się tą szkodą");
        }
    }

    public static class ClaimAlreadyHaveCaseId extends BadRequestException {
        public ClaimAlreadyHaveCaseId() {
            super("Dana szkoda już została zgłoszona do TU");
        }
    }

    public static class PermissionDenied extends ForbiddenException {
        public PermissionDenied() {
            super("Nie posiadasz uprawień do tego zasobu");
        }
    }
}
