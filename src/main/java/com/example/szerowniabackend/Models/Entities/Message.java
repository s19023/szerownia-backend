package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.NotAcceptableException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private LocalDateTime sentDate;

    @Column
    private LocalDateTime readDate;

    private boolean isVisible = true;

    @ManyToOne()
    @JoinColumn(name = "sender_id", referencedColumnName = "id")
    @JsonIgnoreProperties("messagesSent")
    private User sender;

    @ManyToOne()
    @JoinColumn(name = "receiver_id", referencedColumnName = "id")
    @JsonIgnoreProperties("messagesReceived")
    private User receiver;

    @ManyToOne
    @JoinColumn(name = "thread_id", referencedColumnName = "id")
    @JsonIgnoreProperties("messages")
    private Thread thread;

    public Message() {
    }

    public Message(String content, LocalDateTime sentDate) {
        this.content = content;
        this.sentDate = sentDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getSentDate() {
        return sentDate;
    }

    public void setSentDate(LocalDateTime sentDate) {
        this.sentDate = sentDate;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public LocalDateTime getReadDate() {
        return readDate;
    }

    public void setReadDate(LocalDateTime readDate) {
        this.readDate = readDate;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", sentDate=" + sentDate +
                ", isVisible=" + isVisible +
                ", sender=" + sender +
                ", receiver=" + receiver +
                '}';
    }

    public static class AdAndModTicketCannotBeBothNull extends BadRequestException {
        public AdAndModTicketCannotBeBothNull() {
            super("Ogłoszenie jak i zgłoszenie do moderacji nie istnieją");
        }
    }

    public static class OnlyModeratorCanCreateNewThreadRelatedToModTicket extends BadRequestException {
        public OnlyModeratorCanCreateNewThreadRelatedToModTicket() {
            super("Tylko moderator może stworzyć nowy wątek dotyczący zgłoszenia do moderacji");
        }
    }

    public static class ThreadHaveNoRelationWithLoggedUser extends BadRequestException {
        public ThreadHaveNoRelationWithLoggedUser() {
            super("Nie jesteś uczestnikiem tej konwersacji");
        }
    }

}
