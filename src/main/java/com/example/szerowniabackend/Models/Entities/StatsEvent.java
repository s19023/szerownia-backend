package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Models.Entities.Enums.EventType;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class StatsEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long idStatsEvent;

    @Column(nullable = false)
    private LocalDate occurredOn;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private EventType eventType;

    private Long relatedItemId;

    public StatsEvent() {
    }

    public StatsEvent(EventType eventType, LocalDate occurredOn, Long relatedItemId) {
        this.occurredOn = occurredOn;
        this.eventType = eventType;
        this.relatedItemId = relatedItemId;
    }

    public Long getIdStatsEvent() {
        return idStatsEvent;
    }

    public void setIdStatsEvent(Long idStatsEvent) {
        this.idStatsEvent = idStatsEvent;
    }

    public LocalDate getOccurredOn() {
        return occurredOn;
    }

    public void setOccurredOn(LocalDate occurredOn) {
        this.occurredOn = occurredOn;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Long getRelatedItemId() {
        return relatedItemId;
    }

    public void setRelatedItemId(Long relatedItemId) {
        this.relatedItemId = relatedItemId;
    }
}
