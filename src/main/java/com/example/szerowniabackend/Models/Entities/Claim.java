package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Models.Entities.Enums.ClaimStatus;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Claim {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private LocalDate claimDate;

    @Column(nullable = false)
    private LocalDateTime reportDate = LocalDateTime.now();

    private LocalDateTime claimAdjusterSendDate;

    private LocalDateTime dateOfAssignToTheAppraiser;

    @Column(nullable = true)
    private String damageDescription;

    @Column(nullable = true)
    private String circumstances;

    @Column(nullable = true)
    private String justification;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "policy_id")
    private Policy policy;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "location_id")
    private Location location;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ClaimStatus status = ClaimStatus.SUBMITTED;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "insuranceAdjuster_id")
    private User insuranceAdjuster;

    @Column(unique = true)
    private UUID insuranceCaseId;

    @OneToMany(mappedBy = "claim", cascade = CascadeType.ALL)
    private List<ClaimInspection> claimInspections = new ArrayList<>();

    @ManyToOne
    private User tempAppraiser;


    public Claim() {
    }

    public Claim(LocalDate claimDate,
                 LocalDateTime claimAdjusterSendDate,
                 String damageDescription, String circumstances, Location location, ClaimStatus status) {
        this.claimDate = claimDate;
        this.claimAdjusterSendDate = claimAdjusterSendDate;
        this.damageDescription = damageDescription;
        this.circumstances = circumstances;
        this.location = location;
        this.status = status;
    }

    public Claim(LocalDate claimDate,
                 LocalDateTime claimAdjusterSendDate,
                 String damageDescription, String circumstances, Location location,
                 ClaimStatus status, LocalDateTime dateOfAssignToTheAppraiser) {
        this.claimDate = claimDate;
        this.claimAdjusterSendDate = claimAdjusterSendDate;
        this.damageDescription = damageDescription;
        this.circumstances = circumstances;
        this.location = location;
        this.status = status;
        this.dateOfAssignToTheAppraiser = dateOfAssignToTheAppraiser;
    }


    public Long getId() {
        return id;
    }

    public LocalDate getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(LocalDate claimDate) {
        this.claimDate = claimDate;
    }

    public LocalDateTime getReportDate() {
        return reportDate;
    }

    public void setReportDate(LocalDateTime reportDate) {
        this.reportDate = reportDate;
    }

    public LocalDateTime getClaimAdjusterSendDate() {
        return claimAdjusterSendDate;
    }

    public void setClaimAdjusterSendDate(LocalDateTime claimAdjusterSendDate) {
        this.claimAdjusterSendDate = claimAdjusterSendDate;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public void setDamageDescription(String damageDescription) {
        this.damageDescription = damageDescription;
    }

    public String getCircumstances() {
        return circumstances;
    }

    public void setCircumstances(String circumstances) {
        this.circumstances = circumstances;
    }

    public String getJustification() {
        return justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Policy getPolicy() {
        return policy;
    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ClaimStatus getStatus() {
        return status;
    }

    public void setStatus(ClaimStatus status) {
        this.status = status;
    }

    public User getInsuranceAdjuster() {
        return insuranceAdjuster;
    }

    public void setInsuranceAdjuster(User insuranceAdjuster) {
        this.insuranceAdjuster = insuranceAdjuster;
    }

    public UUID getInsuranceCaseId() {
        return insuranceCaseId;
    }

    public void setInsuranceCaseId(UUID insuranceCaseId) {
        this.insuranceCaseId = insuranceCaseId;
    }

    public List<ClaimInspection> getClaimInspections() {
        return claimInspections;
    }

    public void setClaimInspections(List<ClaimInspection> claimInspections) {
        this.claimInspections = claimInspections;
    }

    public User getTempAppraiser() {
        return tempAppraiser;
    }

    public void setTempAppraiser(User tempAppraiser) {
        this.tempAppraiser = tempAppraiser;
    }

    public LocalDateTime getDateOfAssignToTheAppraiser() {
        return dateOfAssignToTheAppraiser;
    }

    public void setDateOfAssignToTheAppraiser(LocalDateTime transferDateToAppraiser) {
        this.dateOfAssignToTheAppraiser = transferDateToAppraiser;
    }

    public static class InvalidWorkflow extends BadRequestException {
        public InvalidWorkflow(String msg) {
            super(msg);
        }
    }

    public static class YouDontHavePermissionToClaim extends ForbiddenException {
        public YouDontHavePermissionToClaim() {
            super("Nie ma posiadzasz uprawnień do tej szkody");
        }
    }

    public static class GivenClaimDateIsNotInHireRange extends BadRequestException {
        public GivenClaimDateIsNotInHireRange() {
            super("Podana data szkody nie mieści się w okresie wypożyczenia");
        }
    }

    public static class ClaimRequiresJustification extends BadRequestException {
        public ClaimRequiresJustification() {
            super("Nie podano uzasadnienia szkody");
        }
    }

}
