package com.example.szerowniabackend.Models.Entities.Enums;

public enum NotificationStatus {

    READ, UNREAD

}
