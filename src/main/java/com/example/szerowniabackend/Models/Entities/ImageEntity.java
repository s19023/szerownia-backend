package com.example.szerowniabackend.Models.Entities;



import com.example.szerowniabackend.Exceptions.NotAcceptableException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(indexes = {
        @Index(name = "NAME_index", columnList = "NAME"),
        @Index(name = "AD_ID_index", columnList = "AD_ID"),
        @Index(name = "USER_ID_index", columnList = "USER_ID"),

})
public class ImageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    Long id;

    @Column(unique = true)
    String name;

    String location;

    LocalDateTime dateAddedImage = LocalDateTime.now();

    @ManyToOne(cascade ={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("imageEntity")
    @JoinColumn(name = "ad_id", referencedColumnName = "id")
    private Ad ad;

    @OneToOne(cascade ={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("userImage")
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @OneToOne(cascade ={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("imageEntity")
    @JoinColumn(name = "image_thumbnail_id", referencedColumnName = "id")
    private ImageThumbnail imageThumbnail;

    @ManyToOne(cascade ={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("modRequests")
    @JoinColumn(name = "complaint_id", referencedColumnName = "id")
    private Complaint complaint;

    @ManyToOne
    private ClaimInspection claimInspection;

    public ImageEntity(Long id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }

    public ImageEntity(String name, String location) {
        this.name = name;
        this.location = location;
    }

    public ImageEntity(String name, String location, Ad ad) {
        this.name = name;
        this.location = location;
        this.ad = ad;
    }

    public ImageEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }

    public LocalDateTime getDateAddedImage() {
        return dateAddedImage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ImageThumbnail getImageThumbnail() {
        return imageThumbnail;
    }

    public void setImageThumbnail(ImageThumbnail imageThumbnail) {
        this.imageThumbnail = imageThumbnail;
    }

    public Complaint getComplaint() {
        return complaint;
    }

    public void setComplaint(Complaint complaint) {
        this.complaint = complaint;
    }

    public ClaimInspection getClaimInspection() {
        return claimInspection;
    }

    public void setClaimInspection(ClaimInspection claimInspection) {
        this.claimInspection = claimInspection;
    }


    public static class BadTypeExtension extends NotAcceptableException {
        public BadTypeExtension() {
            super("Niepoprawne rozszerzenie pliku");
        }
    }

}
