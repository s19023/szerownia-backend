package com.example.szerowniabackend.Models.Entities.Enums;

public enum AdStatus {

    ACTIVE, EXPIRED, IN_MODERATION, NEEDS_CORRECTION, HIDDEN_BY_MODERATOR, DELETED
}
