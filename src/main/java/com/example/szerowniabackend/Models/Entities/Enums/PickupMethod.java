package com.example.szerowniabackend.Models.Entities.Enums;

public enum PickupMethod {

    PERSONAL_PICKUP, PARCEL_LOCKER, COURIER

}
