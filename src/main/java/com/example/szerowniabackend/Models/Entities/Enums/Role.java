package com.example.szerowniabackend.Models.Entities.Enums;

public enum Role {

    ADMIN,
    USER,
    MODERATOR,
    APPRAISER,
    INSURANCE_ADJUSTER
}
