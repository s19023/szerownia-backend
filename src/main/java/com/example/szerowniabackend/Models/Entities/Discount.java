package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;

import javax.persistence.*;

@Entity
public class Discount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private Integer value;

    @Column(nullable = false)
    private Integer minNumDays;

    @Column
    private boolean isVisible = true;

    @SuppressWarnings("JpaDataSourceORMInspection")
    @ManyToOne()
    @JoinColumn(name = "rentalAd_id", referencedColumnName = "id")
    private RentalAd rentalAd;

    public Discount() {
        value = 0;
    }

    public Discount(Integer value, Integer minNumDays) {
        if (value >= 100) {
            throw new InvalidDiscountValue(100);
        }

        if (minNumDays < 1) {
            throw new InvalidMinNumDaysValue(1);
        }

        this.value = value;
        this.minNumDays = minNumDays;
    }

    public Long getId() {
        return id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getMinNumDays() {
        return minNumDays;
    }

    public void setMinNumDays(Integer minNumDays) {
        this.minNumDays = minNumDays;
    }

    public RentalAd getRentalAd() {
        return rentalAd;
    }

    public void setRentalAd(RentalAd rentalAd) {
        this.rentalAd = rentalAd;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    @Override
    public String toString() {
        return "Discount{" +
                "id=" + id +
                ", value=" + value +
                ", minNumDays=" + minNumDays +
                '}';
    }

    public static class InvalidMinNumDaysValue extends BadRequestException {
        public InvalidMinNumDaysValue(int minValue) {
            super("Wartość minimalej liczby dni nie może być mniejsza niż " + minValue);
        }
    }

    public static class InvalidDiscountValue extends BadRequestException {
        public InvalidDiscountValue(int maxValue) {
            super("Wartość zniżki nie może przekraczać " + maxValue);
        }
    }
}
