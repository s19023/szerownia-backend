package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Exceptions.NotAcceptableException;
import com.example.szerowniabackend.Models.Entities.Enums.HireStatus;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Hire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateFrom;

    @Column(nullable = false)
    private LocalDate dateToPlanned;

    private LocalDate dateToActual;

    private Double cost;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private PickupMethod selectedPickupMethod;

    @Column(nullable = false)
    private Double commission;

    private String outgoingShipmentNumber;

    private String incomingShipmentNumber;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private HireStatus hireStatus = HireStatus.WAITING_FOR_SHIPMENT;

    private boolean isDelivered;

    private boolean isReturned;

    @SuppressWarnings("JpaDataSourceORMInspection")
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "ad_id", referencedColumnName = "id")
    @JsonIgnoreProperties("hire")
    private RentalAd ad;

    @SuppressWarnings("JpaDataSourceORMInspection")
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "borrower_id", referencedColumnName = "id")
    @JsonIgnoreProperties("borrows")
    private User borrower;

    @SuppressWarnings("JpaDataSourceORMInspection")
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "sharer_id", referencedColumnName = "id")
    @JsonIgnoreProperties("hire")
    private User sharer;

    @OneToMany(mappedBy = "hire")
    private Set<Opinion> opinions;

    @OneToMany(mappedBy = "hire")
    private Set<Complaint> complaints;

    @OneToOne(mappedBy = "hire")
    @JoinColumn(name = "hire_id")
    @JsonIgnoreProperties("policy")
    private Policy policy;

    public Hire() {
    }

    public Hire(LocalDate dateFrom,
                LocalDate dateToPlanned,
                Double cost,
                PickupMethod selectedPickupMethod,
                Double commission) {
        this.dateFrom = dateFrom;
        this.dateToPlanned = dateToPlanned;
        this.cost = cost;
        this.selectedPickupMethod = selectedPickupMethod;
        this.commission = commission;
        this.opinions = new HashSet<>();
        this.complaints = new HashSet<>();

    }

    public Hire(LocalDate dateFrom,
                LocalDate dateToPlanned,
                PickupMethod selectedPickupMethod,
                Double commission) {
        this.dateFrom = dateFrom;
        this.dateToPlanned = dateToPlanned;
        this.selectedPickupMethod = selectedPickupMethod;
        this.commission = commission;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateToPlanned() {
        return dateToPlanned;
    }

    public void setDateToPlanned(LocalDate dateToPlanned) {
        this.dateToPlanned = dateToPlanned;
    }

    public LocalDate getDateToActual() {
        return dateToActual;
    }

    public void setDateToActual(LocalDate dateToActual) {
        this.dateToActual = dateToActual;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public PickupMethod getSelectedPickupMethod() {
        return selectedPickupMethod;
    }

    public void setSelectedPickupMethod(PickupMethod selectedPickupMethod) {
        this.selectedPickupMethod = selectedPickupMethod;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public RentalAd getAd() {
        return ad;
    }

    public void setAd(RentalAd ad) {
        this.ad = ad;
    }

    public User getBorrower() {
        return borrower;
    }

    public void setBorrower(User borrower) {
        this.borrower = borrower;
    }

    public User getSharer() {
        return sharer;
    }

    public void setSharer(User sharer) {
        this.sharer = sharer;
    }

    public Set<Opinion> getOpinions() {
        return opinions;
    }

    public void setOpinions(Set<Opinion> opinions) {
        this.opinions = opinions;
    }

    public Policy getPolicy() {
        return policy;
    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

    public String getOutgoingShipmentNumber() {
        return outgoingShipmentNumber;
    }

    public void setOutgoingShipmentNumber(String outgoingShipmentNumber) {
        this.outgoingShipmentNumber = outgoingShipmentNumber;
    }

    public String getIncomingShipmentNumber() {
        return incomingShipmentNumber;
    }

    public void setIncomingShipmentNumber(String incomingShipmentNumber) {
        this.incomingShipmentNumber = incomingShipmentNumber;
    }

    public Set<Complaint> getComplaints() {
        return complaints;
    }

    public void setComplaints(Set<Complaint> complaints) {
        this.complaints = complaints;
    }

    public HireStatus getHireStatus() {
        return hireStatus;
    }

    public void setHireStatus(HireStatus hireStatus) {
        this.hireStatus = hireStatus;
    }

    public boolean isDelivered() {
        return isDelivered;
    }

    public void setDelivered(boolean delivered) {
        isDelivered = delivered;
    }

    public boolean isReturned() {
        return isReturned;
    }

    public void setReturned(boolean returned) {
        isReturned = returned;
    }

    @Override
    public String toString() {
        return "Hire{" +
                "id=" + id +
                ", dateFrom=" + dateFrom +
                ", dateToPlanned=" + dateToPlanned +
                ", dateToActual=" + dateToActual +
                ", cost=" + cost +
                ", selectedPickupMethod=" + selectedPickupMethod +
                ", commision=" + commission +
                '}';
    }

    public static class InvalidDate extends NotAcceptableException {
        public InvalidDate(String message) {
            super(message);
        }
    }

    public static class InvalidUser extends ForbiddenException {
        public InvalidUser(String message) {
            super(message);
        }
    }

    public static class ViolatedRequirement extends NotAcceptableException {
        public ViolatedRequirement(String message) {
            super("Nie spełniasz kryterium " + message);
        }
    }

    public static class MissingDates extends BadRequestException {
        public MissingDates() {
            super("Nie podano okresu wypożyczenia");
        }
    }
}
