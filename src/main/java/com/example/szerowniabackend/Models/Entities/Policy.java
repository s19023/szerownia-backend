package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Policy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(nullable = false)
    private Double insuranceTotal;

    @OneToOne()
    private Hire hire;

    @OneToMany(mappedBy = "policy", cascade = CascadeType.ALL)
    private List<Claim> claims = new ArrayList<>();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "insuranceCompany_id")
    private InsuranceCompany insuranceCompany;

    public Policy() {
    }

    public Policy(Double insuranceTotal) {
        this.insuranceTotal = insuranceTotal;
    }

    public Long getId() {
        return id;
    }

    public Double getInsuranceTotal() {
        return insuranceTotal;
    }

    public void setInsuranceTotal(Double insuranceTotal) {
        this.insuranceTotal = insuranceTotal;
    }

    public Hire getHire() {
        return hire;
    }

    public void setHire(Hire hire) {
        this.hire = hire;
    }

    public LocalDate getDateFrom() {
        if(hire == null) throw new Policy.MissingRelation();

        return hire.getDateFrom();
    }

    public LocalDate getDateTo() {
        if(hire == null) throw new Policy.MissingRelation();

        return hire.getDateToPlanned();
    }

    public Integer getFee() {
        if(hire == null)  throw new Policy.MissingRelation();

        int result = 0;
        int numberOfDays = Period.between(hire.getDateFrom(), hire.getDateToPlanned()).getDays();

        for (Product product: hire.getAd().getProducts()) {
            int basicFee = countBasicFee(product.getEstimatedValue());
            double multiplier = countMultiplier(numberOfDays);
            result += basicFee*multiplier;
        }

        return result;
    }

    public List<Claim> getClaims() {
        return claims;
    }

    public void setClaims(List<Claim> claims) {
        this.claims = claims;
    }

    public InsuranceCompany getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    private boolean between(int myValue, int minValue, int maxValue) {
        if (myValue >= minValue && myValue <= maxValue)
            return true;
        else
            return false;
    }

    private int countBasicFee(int productValue) {
        if(productValue < 0)
            throw new Policy.WrongArgument("Wartość produktu nie może być ujemna");

        if(between(productValue, 0, 300)) {
            return 15;
        } else if(between(productValue, 301, 500)) {
            return 20;
        } else if(between(productValue, 501, 1000)) {
            return 50;
        } else if(between(productValue, 1001, 2500)) {
            return 90;
        } else {
            return 130;
        }
    }

    private double countMultiplier(int numberOfDays) {
        if(numberOfDays <= 0 )
            throw new Policy.WrongArgument("Liczba dni nie może być mniejsza niż 1");

        if(between(numberOfDays, 0, 7)) {
            return 1;
        } else if(between(numberOfDays, 8, 14)) {
            return 2;
        } else if(between(numberOfDays, 15, 30)) {
            return 2.5;
        } else {
            return 3;
        }
    }


    public static class WrongArgument extends BadRequestException {
        public WrongArgument(String message) { super(message); }
    }

    private static class MissingRelation extends BadRequestException {
        public MissingRelation() { super("Polisa nie jest powiązana z jakimkolwiek wypożyczeniem"); }
    }
}
