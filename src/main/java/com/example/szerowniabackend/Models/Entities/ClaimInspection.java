package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ClaimInspection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private LocalDate inspectionDate;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private double damageAssessment;

    @ManyToOne(cascade = CascadeType.ALL)
    private User appraiser;

    @ManyToOne(cascade = CascadeType.ALL)
    private Claim claim;

    @OneToMany(mappedBy = "claimInspection", cascade = CascadeType.ALL)
    private List<ImageEntity> images = new ArrayList<>();

    public ClaimInspection() {
    }

    public ClaimInspection(LocalDate inspectionDate, String description, double damageAssessment) {
        this.inspectionDate = inspectionDate;
        this.description = description;
        this.damageAssessment = damageAssessment;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(LocalDate inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getDamageAssessment() {
        return damageAssessment;
    }

    public void setDamageAssessment(double damageAssessment) {
        this.damageAssessment = damageAssessment;
    }

    public User getAppraiser() {
        return appraiser;
    }

    public void setAppraiser(User appraiser) {
        this.appraiser = appraiser;
    }

    public Claim getClaim() {
        return claim;
    }

    public void setClaim(Claim claim) {
        this.claim = claim;
    }

    public List<ImageEntity> getImages() {
        return images;
    }

    public void setImages(List<ImageEntity> images) {
        this.images = images;
    }

    public static class InvalidDateValue extends BadRequestException {
        public InvalidDateValue() { super("Niepoprawna data oględzin"); }
    }

    public static class InvalidDamageAssessmentValue extends BadRequestException {
        public InvalidDamageAssessmentValue() { super("Szacowana wartość zniszczeń nie może być większa od całkowitej kwoty z polisy"); }
    }
}
