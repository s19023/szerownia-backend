package com.example.szerowniabackend.Models.Entities.Enums;

public enum HireStatus {

    WAITING_FOR_SHIPMENT {
        @Override
        public String toString() {
            return "Oczekuje na wysłanie";
        }
    },
    SHIPPED {
        @Override
        public String toString() {
            return "Wysłane do wypożyczającego";
        }
    },
    PENDING_HIRE {
        @Override
        public String toString() {
            return "W trakcie wypożyczenia";
        }
    },
    SHIPPED_BACK {
        @Override
        public String toString() {
            return "Wysłane do użyczającego";
        }
    },
    RETURNED {
        @Override
        public String toString() {
            return "Zakończone";
        }
    }

}
