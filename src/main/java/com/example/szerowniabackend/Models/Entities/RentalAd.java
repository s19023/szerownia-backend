package com.example.szerowniabackend.Models.Entities;


import com.example.szerowniabackend.Exceptions.NotAcceptableException;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@DiscriminatorValue(value = RentalAd.DISCRIMINATOR)
public class RentalAd extends Ad {

    public static final String DISCRIMINATOR = "RENTALAD";

    @Column(nullable = true)
    private Double pricePerDay;

    @Column(nullable = true)
    private Double depositAmount;

    @Column(nullable = true)
    private Double penaltyForEachDayOfDelayInReturns;

    @Column(nullable = true)
    private Double averageOpinion;

    @Column(nullable = true)
    private Double averageRentalPrice;

    @Column(nullable = true)
    private Integer minRentalHistory;

    @Column(nullable = true)
    private String customRequirement;

    @Column(nullable = true)
    private boolean isPromoted;

    @OneToMany(mappedBy = "rentalAd")
    Set<Discount> discounts = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("rentalAds")
    @JoinColumn(name = "searchAd_id", referencedColumnName = "id")
    private SearchAd searchAd;


    public RentalAd() {
    }

    public RentalAd(User user,String title, List<TimeSlot> timeSlotList, Location location,
                    List<PickupMethod> pickupMethodList, String description,
                    Double pricePerDay, Double depositAmount, Double penaltyForEachDayOfDelayInReturns,
                    Double averageOpinion,Double averageRentalPrice, Integer minRentalHistory,
                    String customRequirement, LocalDate maxDate) {
        super(DISCRIMINATOR, title, timeSlotList, location, pickupMethodList, description, user, maxDate);
        this.pricePerDay = pricePerDay;
        this.depositAmount = depositAmount;
        this.penaltyForEachDayOfDelayInReturns = penaltyForEachDayOfDelayInReturns;
        this.averageOpinion = averageOpinion;
        this.averageRentalPrice = averageRentalPrice;
        this.minRentalHistory = minRentalHistory;
        this.customRequirement = customRequirement;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Double getPenaltyForEachDayOfDelayInReturns() {
        return penaltyForEachDayOfDelayInReturns;
    }

    public void setPenaltyForEachDayOfDelayInReturns(Double penaltyForEachDayOfDelayInReturns) {
        this.penaltyForEachDayOfDelayInReturns = penaltyForEachDayOfDelayInReturns;
    }

    public Set<Discount> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(Set<Discount> discounts) {
        this.discounts = discounts;
    }

    public Double getAverageOpinion() {
        return averageOpinion;
    }

    public void setAverageOpinion(Double averageOpinion) {
        this.averageOpinion = averageOpinion;
    }

    public Double getAverageRentalPrice() {
        return averageRentalPrice;
    }

    public void setAverageRentalPrice(Double averageRentalPrice) {
        this.averageRentalPrice = averageRentalPrice;
    }

    public Integer getMinRentalHistory() {
        return minRentalHistory;
    }

    public void setMinRentalHistory(Integer minRentalHistory) {
        this.minRentalHistory = minRentalHistory;
    }

    public String getCustomRequirement() {
        return customRequirement;
    }

    public void setCustomRequirement(String customRequirement) {
        this.customRequirement = customRequirement;
    }

//    public List<Thread> getThreads() {
//        return threads;
//    }
//
//    public void setThreads(List<Thread> threads) {
//        this.threads = threads;
//    }

    public SearchAd getSearchAd() {
        return searchAd;
    }

    public void setSearchAd(SearchAd searchAd) {
        this.searchAd = searchAd;
    }

    public boolean isPromoted() {
        return isPromoted;
    }

    public void setPromoted(boolean promoted) {
        isPromoted = promoted;
    }

    @Override
    public String toString() {
        return "RentalAd{" +
                "pricePerDay=" + pricePerDay +
                ", depositAmount=" + depositAmount +
                ", penaltyForEachDayOfDelayInReturns=" + penaltyForEachDayOfDelayInReturns +
                "} " + super.toString();
    }

    public static class WrongImagesNumber extends NotAcceptableException {
        public WrongImagesNumber() {
            super("Maksymalny limit zdjęć w ogłoszeniu wynosi 8");
        }
    }

    public static class EmptyPickupMethodListException extends NotAcceptableException {
        public EmptyPickupMethodListException() {
            super("Nie wybrano sposobu dostawy");
        }
    }
}
