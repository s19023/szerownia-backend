package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Models.Entities.Enums.ComplainStatus;
import com.example.szerowniabackend.Models.Entities.Enums.ComplaintReason;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Complaint {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    public Long authorId;

    @Column(nullable = false)
    private ComplaintReason reason;

    @Column(nullable = false)
    private String details;

    @Column(nullable = false)
    private LocalDateTime reportDate = LocalDateTime.now();

    @ManyToOne
    @JoinColumn(name = "hire_id")
    private Hire hire;

    @OneToMany(mappedBy = "complaint", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JsonIgnoreProperties("complaint")
    private Set<ModTicket> modTickets = new HashSet<>();

    @OneToMany(mappedBy = "complaint", cascade = {CascadeType.ALL})
    @JsonIgnoreProperties("complaint")
    private Set<ImageEntity> imageEntity;

    @Enumerated(EnumType.STRING)
    ComplainStatus status;

    public Complaint() {
    }

    public Complaint(Long authorId, ComplaintReason reason, String details, Hire hire, ComplainStatus status) {
        this.authorId = authorId;
        this.reason = reason;
        this.details = details;
        this.hire = hire;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public ComplaintReason getReason() {
        return reason;
    }

    public void setReason(ComplaintReason reason) {
        this.reason = reason;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public LocalDateTime getReportDate() {
        return reportDate;
    }

    public void setReportDate(LocalDateTime reportDate) {
        this.reportDate = reportDate;
    }

    public Hire getHire() {
        return hire;
    }

    public void setHire(Hire hire) {
        this.hire = hire;
    }

    public Set<ModTicket> getModTickets() {
        return modTickets;
    }

    public void setModTickets(Set<ModTicket> modTickets) {
        this.modTickets = modTickets;
    }

    public Set<ImageEntity> getImageEntity() {
        return imageEntity;
    }

    public void setImageEntity(Set<ImageEntity> imageEntity) {
        this.imageEntity = imageEntity;
    }

    public ComplainStatus getStatus() {
        return status;
    }

    public void setStatus(ComplainStatus status) {
        this.status = status;
    }

    public static class NotYourHire extends ForbiddenException {
        public NotYourHire() {
            super("Nie możesz zgłosić tego wypożycznia");
        }
    }
}
