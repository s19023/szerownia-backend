package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Models.Entities.Enums.FeatureType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Feature {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long idFeature;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private FeatureType type;

    @OneToMany(mappedBy = "feature", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
    private List<FeatureCategory> featureCategories = new ArrayList<>();

    @OneToMany(mappedBy = "feature", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
    @JsonIgnoreProperties("feature")
    private List<FeatureOccurrence> featureOccurrences = new ArrayList<>();

    public Feature() {
    }

    public Feature(String name, FeatureType type) {
        this.name = name;
        this.type = type;
    }

    public Long getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Long idFeature) {
        this.idFeature = idFeature;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FeatureType getType() {
        return type;
    }

    public void setType(FeatureType type) {
        this.type = type;
    }

    public List<FeatureCategory> getFeatureCategories() {
        return featureCategories;
    }

    public void setFeatureCategories(List<FeatureCategory> featureCategories) {
        this.featureCategories = featureCategories;
    }

    public List<FeatureOccurrence> getFeatureOccurrences() {
        return featureOccurrences;
    }

    public void setFeatureOccurrences(List<FeatureOccurrence> featureOccurrences) {
        this.featureOccurrences = featureOccurrences;
    }

    @Override
    public String toString() {
        return "Feature{" +
                "id=" + idFeature +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
