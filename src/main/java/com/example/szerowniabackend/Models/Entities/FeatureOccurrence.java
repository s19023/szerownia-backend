package com.example.szerowniabackend.Models.Entities;

import com.example.szerowniabackend.Exceptions.BadRequestException;

import javax.persistence.*;

@Entity
public class FeatureOccurrence {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = true)
    private String textValue;

    @Column(nullable = true)
    private Integer decimalNumberValue;

    @Column(nullable = true)
    private Double floatingNumberValue;

    @Column(nullable = true)
    private Boolean booleanValue;

    @ManyToOne
    private Feature feature;

    @ManyToOne
    private Product product;

    public FeatureOccurrence() {
    }

    public FeatureOccurrence(String textValue, Feature feature, Product product) {
        this.textValue = textValue;
        this.feature = feature;
        this.product = product;
    }

    public FeatureOccurrence(Integer decimalNumberValue, Feature feature, Product product) {
        this.decimalNumberValue = decimalNumberValue;
        this.feature = feature;
        this.product = product;
    }

    public FeatureOccurrence(Double floatingNumberValue, Feature feature, Product product) {
        this.floatingNumberValue = floatingNumberValue;
        this.feature = feature;
        this.product = product;
    }

    public FeatureOccurrence(Boolean booleanValue, Feature feature, Product product) {
        this.booleanValue = booleanValue;
        this.feature = feature;
        this.product = product;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public Integer getDecimalNumberValue() {
        return decimalNumberValue;
    }

    public void setDecimalNumberValue(Integer decimalNumberValue) {
        this.decimalNumberValue = decimalNumberValue;
    }

    public Double getFloatingNumberValue() {
        return floatingNumberValue;
    }

    public void setFloatingNumberValue(Double floatingNumberValue) {
        this.floatingNumberValue = floatingNumberValue;
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(Boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "FeatureOccurrence{" +
                "id=" + id +
                ", textValue='" + textValue + '\'' +
                ", decimalNumberValue=" + decimalNumberValue +
                ", floatingNumberValue=" + floatingNumberValue +
                ", booleanValue=" + booleanValue +
                ", feature=" + feature +
                '}';
    }

    public static class OnlyOneValueAllowed extends BadRequestException {
        public OnlyOneValueAllowed() {
            super("Każda cecha może mieć tylko jedną wartość");
        }
    }

    public static class RequiresValue extends BadRequestException {
        public RequiresValue(String message) {
            super("Ta cecha wymaga wartości " + message);
        }
    }
}
