package com.example.szerowniabackend.Models.Entities.Enums;

public enum EventType {

    AD_CREATION, RENT, ACCOUNT_CREATION, VIEWING_AD

}
