package com.example.szerowniabackend.Models.Entities;


import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Models.DTOs.Responses.SuggestProductCategoryResponseDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NamedNativeQuery(name = "Product.suggestProductCategory",
        query = "select top(3) c.name as \"categoryName\", c.id_category as \"categoryId\", count(*) as \"occurrenceNumber\" " +
                "from product p join category c on p.category_id_category = c.id_category " +
                "where upper(p.name) like upper(concat('%', :inputName,'%')) " +
                "group by p.name, c.name " +
                "order by count(*) desc",
        resultSetMapping = "Mapping.SuggestProductCategoryResponseDTO")
@SqlResultSetMapping(name = "Mapping.SuggestProductCategoryResponseDTO",
        classes = @ConstructorResult(targetClass = SuggestProductCategoryResponseDTO.class,
                columns = {
                        @ColumnResult(name = "categoryName", type = String.class),
                        @ColumnResult(name = "categoryId", type = Long.class),
                        @ColumnResult(name = "occurrenceNumber", type = Long.class)
                }))
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = true)
    private String make;

    @Column(nullable = true)
    private String model;

    @Column(nullable = false)
    @Min(1)
    @Max(5)
    private int condition;

    @Column(nullable = true)
    private int estimatedValue;

    @Column(nullable = false)
    private boolean isSearchProduct = false;

    @Column
    private boolean isVisible = true;

    @SuppressWarnings("JpaDataSourceORMInspection")
    @ManyToMany()
    @JsonIgnoreProperties("products")
    @JsonIgnore
    @JoinTable(
            name = "product_ad",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "ad_id")
    )
    private List<Ad> includeAds = new ArrayList<>();

    @ManyToOne()
    private Category category;

    @OneToMany(mappedBy = "product")
    private List<FeatureOccurrence> featureOccurrences = new ArrayList<>();

    @ManyToOne()
    @JsonIgnoreProperties("productsUser")
    private User user;

    public Product() {
    }

    public Product(String name, String make, String model, int condition, int estimatedValue) {
        this.name = name;
        this.make = make;
        this.model = model;
        this.condition = condition;
        this.estimatedValue = estimatedValue;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }

    public int getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(int estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Ad> getIncludeAds() {
        return includeAds;
    }

    public void setIncludeAds(List<Ad> includeRentalAds) {
        this.includeAds = includeRentalAds;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<FeatureOccurrence> getFeatureOccurrences() {
        return featureOccurrences;
    }

    public void setFeatureOccurrences(List<FeatureOccurrence> featureOccurrences) {
        this.featureOccurrences = featureOccurrences;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isSearchProduct() {
        return isSearchProduct;
    }

    public void setSearchProduct(boolean searchProduct) {
        isSearchProduct = searchProduct;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", condition=" + condition +
                ", estimatedValue=" + estimatedValue +
                '}';
    }

    public static class CannotBeAssignedToAbstractCategory extends BadRequestException {
        public CannotBeAssignedToAbstractCategory() {
            super("Nie można przypisać przedmiotu do abstrakcyjnej kategorii");
        }
    }

    public static class IsASearchProduct extends ForbiddenException {
        public IsASearchProduct() {
            super("Dany przedmiot nie jest przedmiotem poszukiwania");
        }
    }
}
