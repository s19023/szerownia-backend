package com.example.szerowniabackend.Models.Entities.Enums;

public enum ComplaintReason {

    NOT_AS_DESCRIBED{
        public String toString() { return "Przedmiot niezgodny z opisem"; }
    },
    DEFECTIVE{
        public String toString() { return "Wadliwy przedmiot"; }
    },
    INCOMPLETE{
        public String toString() { return "Niekompletne wypożyczenie"; }
    },
    NOT_RECIVED{
        public String toString() { return "Brak otrzymania przesyłki"; }
    },
    PACKAGE_BROKEN{
        public String toString() { return "Uszkodzona przesyłka"; }
    },
    OTHER{
        public String toString() { return "Inne"; }
    }

}
