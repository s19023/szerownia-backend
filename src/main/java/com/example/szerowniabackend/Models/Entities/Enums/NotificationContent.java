package com.example.szerowniabackend.Models.Entities.Enums;

public enum NotificationContent {

    HIRE_SHIPPED("Przesyłka została wysłana"), BORROWED_HIRE("Wypożyczono przedmiot") ;

    private  String polishMessages;

    NotificationContent(String polishMessages) {
        this.polishMessages = polishMessages;
    }

    public String getPolishMessages() {
        return polishMessages;
    }

    public void setPolishMessages(String polishMessages) {
        this.polishMessages = polishMessages;
    }
}
