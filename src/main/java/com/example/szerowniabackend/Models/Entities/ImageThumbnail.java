package com.example.szerowniabackend.Models.Entities;

import javax.persistence.*;

@Entity
public class ImageThumbnail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(unique = true)
    private String name;

    private String location;

    @OneToOne(mappedBy = "imageThumbnail")
    private ImageEntity imageEntity;

    public ImageThumbnail() {
    }

    public ImageThumbnail(Long id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }

    public ImageThumbnail(String name, String location) {
        this.name = name;
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ImageEntity getImageEntity() {
        return imageEntity;
    }

    public void setImageEntity(ImageEntity imageEntity) {
        this.imageEntity = imageEntity;
    }
}
