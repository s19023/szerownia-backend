package com.example.szerowniabackend.Models.ProfanityFilter;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component
public class ProfanityFilter {

    private static int largestWordLength = 0;

    private static Map<String, String[]> allBadWords = new HashMap<String, String[]>();

    private static boolean isVulgar = false;

    private static final String VULGAR_WORDS_LIST_PATH = "src/main/resources/static/profanity_list.txt";


    public static String getCensoredText(final String input) {
        if(allBadWords.isEmpty()){
            ProfanityFilterHelper.loadBadWordsToMapFromDatabase();
        }
        setIsVulgar(false);
        if (input == null) {
            return "";
        }

        String modifiedInput = input;

        modifiedInput = modifiedInput.replaceAll("1", "i").replaceAll("!", "i").replaceAll("3", "e").replaceAll("4", "a")
                .replaceAll("@", "a").replaceAll("5", "s").replaceAll("7", "t").replaceAll("0", "o").replaceAll("9", "g");
        modifiedInput = modifiedInput.toLowerCase();

        modifiedInput = removeRepeatedLetters(modifiedInput);


        ArrayList<String> badWordsFound = new ArrayList<>();

        for (int start = 0; start < modifiedInput.length(); start++) {
            for (int offset = 1; offset < (modifiedInput.length() + 1 - start) && offset < largestWordLength; offset++) {
                String wordToCheck = modifiedInput.substring(start, start + offset);
                if (allBadWords.containsKey(wordToCheck)) {
                    String[] ignoreCheck = allBadWords.get(wordToCheck);
                    boolean ignore = false;
                    for (int stringIndex = 0; stringIndex < ignoreCheck.length; stringIndex++) {
                        if (modifiedInput.contains(ignoreCheck[stringIndex])) {
                            ignore = true;
                            break;
                        }
                    }

                    if (!ignore) {
                        badWordsFound.add(wordToCheck);
                    }
                }
            }
        }

        String inputToReturn = modifiedInput;

        if (badWordsFound.isEmpty()) {
            setIsVulgar(false);
            return input;
        }

        for (int i = badWordsFound.size() - 1; i >= 0; i--) {
            inputToReturn = inputToReturn.replaceAll("(?i)" + badWordsFound.get(i), "&@#!");

            setIsVulgar(true);
        }


        return inputToReturn;
    }



    public static String removeRepeatedLetters(String word) {
        if (word == null) {
            throw new IllegalArgumentException("Sorry, can't check null word.");
        }

        if (word.length() == 1 || word.equals("")) {
            return word;
        }

        StringBuilder result = new StringBuilder();


        for (int i = 0; i < word.length() - 1; i++) {
            char current = word.charAt(i);
            if (current != word.charAt(i + 1)) {
                result.append(current);
            }
        }

        return result.toString() + word.charAt(word.length() - 1);
    }


    public static int getLargestWordLength() {
        return largestWordLength;
    }

    public static Map<String, String[]> getAllBadWords() {
        return allBadWords;
    }

    public static void setLargestWordLength(int largestWordLength) {
        ProfanityFilter.largestWordLength = largestWordLength;
    }

    public static void clearMapProfanityList(){
        allBadWords.clear();
    }

    public static void setAllBadWords(Map<String, String[]> allBadWords) {
        ProfanityFilter.allBadWords = allBadWords;
    }

    public static boolean getIsVulgar() {
        return isVulgar;
    }

    public static void setIsVulgar(boolean isVulgar) {
        ProfanityFilter.isVulgar = isVulgar;
    }

    public static String getVulgarWordsListPath() {
        return VULGAR_WORDS_LIST_PATH;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    @Async
    @EventListener(ApplicationReadyEvent.class)
    public void loadBadWordsToMapFromDatabaseOnStartApp() {
        ProfanityFilterHelper.loadBadWordsToMapFromDatabase();
    }

}
