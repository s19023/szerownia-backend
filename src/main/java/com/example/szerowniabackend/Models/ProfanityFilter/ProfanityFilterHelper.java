package com.example.szerowniabackend.Models.ProfanityFilter;

import com.example.szerowniabackend.Models.Repositories.VulgarWordsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Transactional
public class ProfanityFilterHelper {

    public static VulgarWordsRepo vulgarWordsRepo;

    @Autowired
    public ProfanityFilterHelper(VulgarWordsRepo vulgarWordsRepo) {
        ProfanityFilterHelper.vulgarWordsRepo = vulgarWordsRepo;
    }


    public static void loadBadWordsToMapFromDatabase() {
        List<String> vulgarWordsList = vulgarWordsRepo.findAllVulgarWords();
        ProfanityFilter.clearMapProfanityList();
        String[] content = null;
        Map<String, String[]> badWordsMap = new HashMap<>();

        for (String s : vulgarWordsList) {
            content = s.split(" ");

            final String word = content[0];

            if (word.length() > ProfanityFilter.getLargestWordLength()) {
                ProfanityFilter.setLargestWordLength(word.length());
            }

            String[] ignore_in_combination_with_words = new String[]{};
            if (content.length > 1) {
                ignore_in_combination_with_words = content[1].split("_");
            }
            badWordsMap.put(word.replaceAll(" ", "").toLowerCase(), ignore_in_combination_with_words);

        }

        ProfanityFilter.setAllBadWords(badWordsMap);


    }




}

