package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Claim;
import com.example.szerowniabackend.Models.Entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ClaimRepo extends JpaRepository<Claim, Long> {


    @Query(value = "SELECT * " +
            "FROM CLAIM " +
            "WHERE (STATUS ='IN_PROGRESS_BEFORE_INSPECTION' AND TEMP_APPRAISER_ID = :tempAppraiserId)", nativeQuery = true)
    Set<Claim> findClaimsWaitingForInspection(Long tempAppraiserId);

    Page<Claim> findAllByUser(User user, Pageable pageable);

}
