package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    Optional<User> findUserByEmail(String email);

    @Query(value = "SELECT * FROM user WHERE role = 'INSURANCE_ADJUSTER'", nativeQuery = true)
    List<User> getAllInsuranceAdjusters();

    List<User> getAllByRole(Role role);

    @Query(value = "SELECT * FROM USER u WHERE u.FIRST_NAME LIKE '%a'", nativeQuery = true)
    List<User> findAllWomenByLastCharacterToDbSeeder();

    @Query(value = "SELECT * FROM USER u WHERE u.FIRST_NAME NOT LIKE '%a'", nativeQuery = true)
    List<User> findAllMenByLastCharacterToDbSeeder();
}
