package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.ImageThumbnail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageThumbnailRepo extends JpaRepository<ImageThumbnail, Long> {
}
