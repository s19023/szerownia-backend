package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Claim;
import com.example.szerowniabackend.Models.Entities.ClaimInspection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClaimInspectionRepo extends JpaRepository<ClaimInspection, Long> {

    @Query(value = "SELECT CLAIM_ID FROM CLAIM_INSPECTION where APPRAISER_ID = :appraiserId", nativeQuery = true)
    List<Long> findClaimsDuringInspection(Long appraiserId);


}
