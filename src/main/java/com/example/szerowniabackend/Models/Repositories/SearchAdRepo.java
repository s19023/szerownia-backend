package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.AdModTicket;
import com.example.szerowniabackend.Models.Entities.SearchAd;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchAdRepo extends JpaRepository<SearchAd, Long> {

    @Query(value = "SELECT * FROM AD a " +
            "WHERE a.AD_TYPE = 'SEARCHAD' AND a.user_id = :userId AND a.IS_VISIBLE = TRUE " +
            "ORDER BY a.CREATION_DATE DESC", nativeQuery = true)
    Page<SearchAd> findUsersSearchAds(@Param("userId") Long userId, Pageable pageable);


    @Query(value = "SELECT * FROM AD a " +
            "where a.AD_TYPE = 'SEARCHAD' and a.user_id = :userId and a.is_visible = TRUE " +
            "Order by a.CREATION_DATE DESC", nativeQuery = true)
    Page<SearchAd> findAllByUserIdOrderDescByStartDate(@Param("userId") Long userId, Pageable pageable);

    @Query(value = "SELECT ad.id as adId, t.id as modTicketId from AD join MOD_TICKET t on t.ad_id = ad.id WHERE AD_TYPE = 'SEARCHAD' and ad.user_id = :userId and t.result = :result", nativeQuery = true)
    Page<AdModTicket> findAdsByResult(Long userId, String result, Pageable pageable);

}
