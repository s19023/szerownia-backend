package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.ResetToken;
import com.example.szerowniabackend.Models.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RTokenRepo extends JpaRepository<ResetToken,Long> {

    Optional<ResetToken> findResetTokenByToken(String token);
}
