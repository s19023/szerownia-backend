package com.example.szerowniabackend.Models.Repositories;


import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository
public class FileSystemRepo {

    String fileSeparator = FileSystems.getDefault().getSeparator();
    String RESOURCES_DIR = System.getProperty("user.dir") + fileSeparator + "IMAGES" + fileSeparator;


    public String save(byte[] content, String imageName) {
        Path newFile = Paths.get(RESOURCES_DIR + imageName);
        try {
            Files.createDirectories(newFile.getParent());
            Files.write(newFile, content);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return newFile.toAbsolutePath()
                .toString();
    }

    public FileSystemResource findInFileSystem(String location) {
        try {
            return new FileSystemResource(Paths.get(location));
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    public void deleteFile (String location){
        try {
            Files.delete(Path.of(location));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}