package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Enums.HireStatus;
import com.example.szerowniabackend.Models.Entities.Hire;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HireRepo extends JpaRepository<Hire, Long> {

    @Modifying
    @Query(value = "DELETE FROM Hire WHERE id = :id", nativeQuery = true)
    void deleteHire(@Param("id") Long id);

    List<Hire> findAllByBorrowerId(Long id);

    List<Hire> findAllBySharerId(Long id);

    @Query(value = "SELECT * from Hire where ad_id = :id " +
            "and year(DATE_TO_PLANNED) = :year " +
            "or year(DATE_TO_PLANNED) = :year-1 " +
            "or year(DATE_TO_PLANNED) = :year+1 " +
            "order by DATE_TO_PLANNED", nativeQuery = true)
    List<Hire> findAllByAdIdWhereYearIsOrderByDateFrom(@Param("id") Long id, @Param("year") int year);

    @Query(value = "SELECT * from Hire where ad_id = :id " +
            "order by DATE_TO_PLANNED", nativeQuery = true)
    List<Hire> findAllByAdIdOrderByDateFrom(@Param("id") Long id);

    List<Hire> findAllByBorrowerIdAndHireStatus(Long borrowerId, HireStatus hireStatus);

    List<Hire> findAllBySharerIdAndHireStatus(Long idSharer, HireStatus hireStatus);

    @Query(value = "SELECT * FROM HIRE WHERE BORROWER_ID = :idBorrower ORDER BY DATE_FROM DESC", nativeQuery = true)
    Page<Hire> findAllHireBorrowsByIdUser(Long idBorrower, Pageable pageable);

    @Query(value = "SELECT * FROM HIRE WHERE SHARER_ID = :idSharer ORDER BY DATE_FROM DESC", nativeQuery = true)
    Page<Hire> findAllHireSharesByIdUser(Long idSharer, Pageable pageable);
}
