package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Complaint;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ComplaintRepo extends JpaRepository<Complaint, Long> {

    @Query(value = "SELECT * FROM COMPLAINT", nativeQuery = true)
    Page<Complaint> findAllComplaints(Pageable pageable);

    @Query(value = "SELECT * FROM COMPLAINT WHERE AUTHOR_ID = :idUser", nativeQuery = true)
    Page<Complaint> findMyComplaints(@Param("idUser") Long idUser, Pageable pageable);
}
