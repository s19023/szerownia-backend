package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepo extends JpaRepository<Category,Long> {


    String FIND_MAIN_CATEGORY_BY_NAME = "SELECT * FROM CATEGORY WHERE PARENT_CATEGORY_ID_CATEGORY IS NULL AND NAME = :categoryName";
    String GET_ALL_MAIN_CATEGORIES = "SELECT * FROM CATEGORY WHERE PARENT_CATEGORY_ID_CATEGORY IS NULL";

    Optional<Category> findByName(String nameCategory);

    @Query(value=FIND_MAIN_CATEGORY_BY_NAME, nativeQuery=true)
    Optional<Category> findMainCategoryByName(@Param("categoryName") String categoryName);

    @Modifying
    @Query(value = "DELETE FROM FEATURE_CATEGORY WHERE CATEGORY_ID = :categoryId", nativeQuery = true)
    void removeFeaturesFromCategory(@Param("categoryId") Long categoryId);
}
