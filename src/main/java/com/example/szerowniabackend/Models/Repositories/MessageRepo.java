package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MessageRepo extends JpaRepository<Message, Long> {

    String IS_EXIST_THREAD_BETWEEN_USERS = "SELECT DISTINCT thread_id FROM MESSAGE m WHERE (m.RECEIVER_ID = :user1Id AND m.SENDER_ID = :user2Id)  OR (m.RECEIVER_ID = :user2Id  AND m.SENDER_ID = :user1Id)";

    @Query(value = IS_EXIST_THREAD_BETWEEN_USERS, nativeQuery = true)
    Optional<Long> isExistThreadBetweenUsers(@Param("user1Id") Long user1Id, @Param("user2Id") Long user2Id);
}
