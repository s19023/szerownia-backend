package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.VulgarWords;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VulgarWordsRepo extends JpaRepository<VulgarWords, Long> {

    @Query(value = "SELECT VULGAR_WORD FROM VULGAR_WORDS", nativeQuery = true)
    List<String> findAllVulgarWords();

    @Query(value = "SELECT * FROM VULGAR_WORDS WHERE upper(VULGAR_WORD) like upper(concat('%', :word,'%')) ",
            nativeQuery = true)
    List<VulgarWords> findAllVulgarWordsPageable(Pageable page, @Param("word") String word);


    Optional<VulgarWords> findByVulgarWord(String vulgarWord);

    Long countByVulgarWordContaining(String vulgarWord);
}
