package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.FeatureCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeatureCategoryRepo extends JpaRepository<FeatureCategory, Long> {
}
