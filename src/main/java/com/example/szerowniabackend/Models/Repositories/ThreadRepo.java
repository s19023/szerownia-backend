package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Thread;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThreadRepo extends JpaRepository<Thread, Long> {
}
