package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.AdModTicket;
import com.example.szerowniabackend.Models.Entities.Enums.ModTicketResult;
import com.example.szerowniabackend.Models.Entities.RentalAd;
import com.example.szerowniabackend.Models.Entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface RentalAdRepo extends JpaRepository<RentalAd, Long> {

    @Query(value = "SELECT * FROM PRODUCT_AD pra " +
            "join product p on p.ID = pra.PRODUCT_ID " +
            "join ad ra on ra.ID = pra.AD_ID " +
            "join Category c on p.CATEGORY_ID_CATEGORY  = c.ID_CATEGORY where " +
            "ra.IS_VISIBLE = true AND ra.END_DATE > CURRENT_DATE()+1 AND " +
            "((UPPER( ra.description) like upper(concat('%', :keyWord,'%')) " +
            "or upper(ra.title) like upper(concat('%', :keyWord,'%')) " +
            "or upper(p.name) like upper(concat('%', :keyWord,'%'))) " +
            "and upper(c.name) like upper(:category)) or " +
            "(upper(c.name) like upper(:category))", nativeQuery = true)
    List<RentalAd> findRentalAdByCategoryAndProduct(@Param("keyWord") String keyWord, @Param("category") String category);


    @Query(value = "SELECT TOP 5 * FROM AD a " +
            "join AD_TIME_SLOT_LIST at on a.ID = at.AD_ID " +
            "where a.AD_TYPE = 'RENTALAD' and a.user_id = :id and a.END_DATE > CURRENT_DATE()+1 " +
            "Order by at.START_DATE DESC", nativeQuery = true)
    List<RentalAd> findAllByUserIdOrderDescByStartDateTopFiveRecords(@Param("id") Long id);


    @Query(value = "SELECT * FROM AD a " +
            "where a.AD_TYPE = 'RENTALAD' and a.user_id = :userId and a.is_visible = TRUE and a.END_DATE > CURRENT_DATE()+1" +
            "Order by a.CREATION_DATE DESC", nativeQuery = true)
    Page<RentalAd> findAllByUserIdOrderDescByStartDate(@Param("userId") Long userId, Pageable pageable);

    @Query(value = "SELECT * FROM AD where USER_ID = :userId and ID = :rentalAdId and END_DATE > CURRENT_DATE()+1", nativeQuery = true)
    Optional<RentalAd> findRentalAdByUserIdAndRentalAdId(@Param("userId") Long userId, @Param("rentalAdId") Long rentalAdId);


    @Query(value = "SELECT * FROM AD a " +
            "join PRODUCT_AD pa on a.ID = pa.AD_ID " +
            "join PRODUCT p on p.ID = pa.PRODUCT_ID " +
            "join CATEGORY c on c.ID_CATEGORY = p.CATEGORY_ID_CATEGORY " +
            "where a.AD_TYPE = 'RENTALAD' and c.NAME= :categoryName and a.END_DATE > CURRENT_DATE()+1 " +
            "and upper(a.title) like upper(concat('%', :titleContains,'%'))", nativeQuery = true)
    List<RentalAd> forDbSeederImageConnectToRentalAd(@Param("categoryName") String categoryName, @Param("titleContains") String titleContains);

    @Query(value = "SELECT * from ad where AD_TYPE = 'RENTALAD' and IS_VISIBLE = true and END_DATE > CURRENT_DATE()+1 order by IS_PROMOTED desc, CREATION_DATE desc", nativeQuery = true)
    Page<RentalAd> findAllRentalAdWhereIsVisibleIsTrueOrderedByIsPromoted(Pageable pageable);

    @Query(value = "select * from ad where AD_TYPE = 'RENTALAD' and IS_VISIBLE = true and IS_PROMOTED = true and END_DATE > CURRENT_DATE()+1 order by CREATION_DATE desc limit 20", nativeQuery = true)
    List<RentalAd> findAllRentalAdForHomePage();

    List<RentalAd> findAllByUserAndEndDateAfter(User user, LocalDate localDate);

    @Query(value = "SELECT ad.id as adId, t.id as modTicketId FROM ad join MOD_TICKET t on t.ad_id = ad.id WHERE AD_TYPE = 'RENTALAD' and ad.user_id = :userId and t.result = :result", nativeQuery = true)
    Page<AdModTicket> findAdsByResult(Long userId, String result, Pageable pageable);

    @Query(value = "SELECT distinct a.ID, a.CREATION_DATE, a.DESCRIPTION, a.DISCRIMINATOR, a.END_DATE, " +
            "a.IS_VISIBLE, a.TITLE, a.AVERAGE_OPINION, a.AVERAGE_RENTAL_PRICE, a.CUSTOM_REQUIREMENT,  " +
            "a.DEPOSIT_AMOUNT, a.IS_PROMOTED, a.MIN_RENTAL_HISTORY, a.PENALTY_FOR_EACH_DAY_OF_DELAY_IN_RETURNS, " +
            "a.PRICE_PER_DAY, a.LOCATION_ID, a.USER_ID, a.SEARCH_AD_ID  FROM AD a " +
            "join MOD_TICKET m on m.AD_ID = a.ID " +
            "where a.USER_ID  =  :userId and a.IS_VISIBLE  = FALSE ", nativeQuery = true)
    List<RentalAd> findAllByUserAndVisibleFalse(@Param("userId") Long userId);
}