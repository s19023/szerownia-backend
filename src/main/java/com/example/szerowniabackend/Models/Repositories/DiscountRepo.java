package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Discount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface DiscountRepo extends JpaRepository<Discount, Long> {

    String GET_BEST_AVAILABLE_DISCOUNT = "SELECT MAX(VALUE) FROM DISCOUNT WHERE RENTAL_AD_ID=:rentalAdId AND MIN_NUM_DAYS <= :elapsedDays";

    @Query(value=GET_BEST_AVAILABLE_DISCOUNT, nativeQuery=true)
    Optional<Integer> getBestAvailableDiscount(Long rentalAdId, Integer elapsedDays);

    Discount getFirstByValueAndRentalAdId(Integer value, Long rentalAdId);
}
