package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.ModTicket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ModTicketRepo extends JpaRepository<ModTicket, Long> {

    String GET_LATEST_MOD_TICKET_OF_RENTAL_AD = "SELECT * FROM MOD_TICKET WHERE AD_ID = :idRentalAd ORDER BY REPORTED_DATE DESC LIMIT 1";

    @Query(value = GET_LATEST_MOD_TICKET_OF_RENTAL_AD, nativeQuery = true)
    ModTicket getLatestModTicketOfRentalAd(@Param("idRentalAd") Long idRentalAd);


    @Query(value = "SELECT * FROM MOD_TICKET", nativeQuery = true)
    Page<ModTicket> findAllModTickets(Pageable pageable);

    @Query(value = "SELECT * FROM MOD_TICKET WHERE RESULT = 'MODIFIED'", nativeQuery = true)
    Page<ModTicket> findArchiveModTickets(Pageable pageable);

    Page<ModTicket> getAllByAdNotNull(Pageable pageable);

    Page<ModTicket> getAllByOpinionNotNull(Pageable pageable);

    Page<ModTicket> getAllByReportedNotNull(Pageable pageable);

    String GET_LATEST_MOD_TICKET_OF_SEARCH_AD = "SELECT * FROM MOD_TICKET WHERE AD_ID = :idSearchAd ORDER BY REPORTED_DATE DESC LIMIT 1";

    @Query(value = GET_LATEST_MOD_TICKET_OF_SEARCH_AD, nativeQuery = true)
    ModTicket getLatestModTicketOfSearchAd(Long idSearchAd);
}
