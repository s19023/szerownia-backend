package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepo extends JpaRepository<Location,Long> {
}
