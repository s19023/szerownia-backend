package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.InsuranceCompany;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceCompanyRepo extends JpaRepository<InsuranceCompany, Long> {
}
