package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegionRepo extends JpaRepository<Region,String> {
}
