package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.FeatureOccurrence;
import com.example.szerowniabackend.Models.Entities.RentalAd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface FeatureOccurrenceRepo extends JpaRepository<FeatureOccurrence,Long> {


    @Query(value = "SELECT TOP 1 * FROM FEATURE_OCCURRENCE where FEATURE_ID_FEATURE = :id",nativeQuery = true)
    Optional<FeatureOccurrence> findFeatureOccurrenceByFeatureId(@Param("id") Long id);
}
