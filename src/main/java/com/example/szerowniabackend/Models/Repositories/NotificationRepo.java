package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Enums.NotificationStatus;
import com.example.szerowniabackend.Models.Entities.Notification;
import com.example.szerowniabackend.Models.Entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepo extends JpaRepository<Notification, Long> {

    Page<Notification> findAllByCurrentUserOrderByCreateTimeDesc (User currentUser, Pageable pageable);

    List<Notification> findAllByCurrentUserAndNotificationStatusOrderByCreateTimeDesc (User currentUser, NotificationStatus notificationStatus);
}
