package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Enums.EventType;
import com.example.szerowniabackend.Models.Entities.StatsEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.time.LocalDate;
import java.util.List;

public interface StatisticsRepo extends JpaRepository<StatsEvent, Long> {

    String GET_RECENT_EVENTS_OF_TYPE = "SELECT occurredOn FROM StatsEvent WHERE eventType = :eventType and occurredOn > :sinceWhen";
    String COUNT_RECENT_EVENTS_FOR_ITEM = "SELECT count(*) FROM StatsEvent WHERE eventType = :eventType and occurredOn > :sinceWhen and relatedItemId = :relatedItemId";

    @Query(value = GET_RECENT_EVENTS_OF_TYPE)
    List<LocalDate> getRecentEventsOfType(@Param("eventType") EventType eventType, @Param("sinceWhen") LocalDate sinceWhen);

    @Query(value = COUNT_RECENT_EVENTS_FOR_ITEM)
    Long countRecentEventsForItem(@Param("eventType") EventType eventType, @Param("sinceWhen") LocalDate sinceWhen, @Param("relatedItemId") Long relatedItemId);
}

