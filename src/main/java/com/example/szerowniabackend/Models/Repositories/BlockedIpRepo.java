package com.example.szerowniabackend.Models.Repositories;


import com.example.szerowniabackend.Models.Entities.BlockedIp;
import com.example.szerowniabackend.Models.Entities.VulgarWords;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BlockedIpRepo extends JpaRepository<BlockedIp, Long> {

    @Query(value = "SELECT IP FROM BLOCKED_IP ", nativeQuery = true)
    List<String> findAllBlockedIp ();

    @Query(value = "SELECT * FROM BLOCKED_IP WHERE upper(IP) like upper(concat('%', :word,'%')) ",
            nativeQuery = true)
    List<BlockedIp> findAllBlockedIp(Pageable page, @Param("word") String word);

    Optional<BlockedIp> findByIp(String ip);

    Optional<BlockedIp> deleteByIp(String ip);

    Long countByIpContaining(String ip);

}
