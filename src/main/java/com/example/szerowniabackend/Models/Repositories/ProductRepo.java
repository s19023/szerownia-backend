package com.example.szerowniabackend.Models.Repositories;


import com.example.szerowniabackend.Models.DTOs.Responses.SuggestProductCategoryResponseDTO;
import com.example.szerowniabackend.Models.Entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepo  extends JpaRepository<Product, Long> {


    List<Product> findAllByUserIdAndIsSearchProduct(Long id, boolean isSearchProduct);

    Optional<Product> findByName(String nameProduct);

    @Query(nativeQuery = true)
    List<SuggestProductCategoryResponseDTO> suggestProductCategory(@Param("inputName") String name);
}
