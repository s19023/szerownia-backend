package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.ImageEntity;
import com.example.szerowniabackend.Models.Entities.RentalAd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface ImageRepo extends JpaRepository<ImageEntity, Long> {


    @Query(value = "SELECT TOP 1 * FROM IMAGE_ENTITY " +
            "where ad_id = :rentalAdId "  +
            "order by ID ", nativeQuery = true)
    ImageEntity findFirstImageForRentalAd(@Param("rentalAdId") Long rentalAdId);

    @Query(value = "SELECT ID FROM IMAGE_ENTITY " +
            "where ad_id = :rentalAdId "  +
            "order by ID ", nativeQuery = true)
    List<Long> findAllOrderByIdReturnListOfId(@Param("rentalAdId") Long rentalAdId);

    @Modifying
    @Query(value = "DELETE FROM IMAGE_ENTITY WHERE ID = :imageId", nativeQuery = true)
    void deleteImage(@Param("imageId") Long imageId);

}
