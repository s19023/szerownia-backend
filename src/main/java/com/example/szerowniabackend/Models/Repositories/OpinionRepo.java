package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Opinion;
import com.example.szerowniabackend.Models.Entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OpinionRepo extends JpaRepository<Opinion, Long> {

//    List<Opinion> getAllByAboutSharer(Optional<User> user);

    List<Opinion> getAllByTaker(User user);

    @Query(value = "SELECT * FROM OPINION WHERE TAKER_ID = :userId and IS_VISIBLE = true", nativeQuery = true)
    Page<Opinion> findUserOpinions(@Param("userId") Long userId, Pageable pageable);
}
