package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Policy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PolicyRepo extends JpaRepository<Policy, Long> {

}
