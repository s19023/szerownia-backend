package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Feature;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface FeatureRepo extends JpaRepository<Feature, Long> {

    String GET_MIN_NUMERIC = "SELECT MIN(DECIMAL_NUMBER_VALUE) " +
            "FROM FEATURE_OCCURRENCE " +
            "JOIN PRODUCT ON PRODUCT.ID = PRODUCT_ID " +
            "WHERE PRODUCT.CATEGORY_ID_CATEGORY = :categoryId AND FEATURE_ID_FEATURE = :featureId";

    String GET_MAX_NUMERIC = "SELECT MAX(DECIMAL_NUMBER_VALUE) " +
            "FROM FEATURE_OCCURRENCE " +
            "JOIN PRODUCT ON PRODUCT.ID = PRODUCT_ID " +
            "WHERE PRODUCT.CATEGORY_ID_CATEGORY = :categoryId AND FEATURE_ID_FEATURE = :featureId";

    String GET_MIN_FLOAT = "SELECT MIN(FLOATING_NUMBER_VALUE) " +
            "FROM FEATURE_OCCURRENCE " +
            "JOIN PRODUCT ON PRODUCT.ID = PRODUCT_ID " +
            "WHERE PRODUCT.CATEGORY_ID_CATEGORY = :categoryId AND FEATURE_ID_FEATURE = :featureId";

    String GET_MAX_FLOAT = "SELECT MAX(FLOATING_NUMBER_VALUE) " +
            "FROM FEATURE_OCCURRENCE " +
            "JOIN PRODUCT ON PRODUCT.ID = PRODUCT_ID " +
            "WHERE PRODUCT.CATEGORY_ID_CATEGORY = :categoryId AND FEATURE_ID_FEATURE = :featureId";

    @Query(value = GET_MIN_NUMERIC, nativeQuery = true)
    Optional<Integer> getMinValueNumeric(@Param("categoryId") long categoryId, @Param("featureId") long featureId);

    @Query(value = GET_MAX_NUMERIC, nativeQuery = true)
    Optional<Integer> getMaxValueNumeric(@Param("categoryId") long categoryId, @Param("featureId") long featureId);

    @Query(value = GET_MIN_FLOAT, nativeQuery = true)
    Optional<Double> getMinValueFloat(@Param("categoryId") long categoryId, @Param("featureId") long featureId);

    @Query(value = GET_MAX_FLOAT, nativeQuery = true)
    Optional<Double> getMaxValueFloat(@Param("categoryId") long categoryId, @Param("featureId") long featureId);
}
