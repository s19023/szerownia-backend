package com.example.szerowniabackend.Models.Consts;

public interface Role {
    String ADMIN = "ADMIN";
    String MODERATOR = "MODERATOR";
    String APPRAISER = "APPRAISER";
    String INSURANCE_ADJUSTER = "INSURANCE_ADJUSTER";
}