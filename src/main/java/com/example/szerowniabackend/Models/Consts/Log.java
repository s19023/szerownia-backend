package com.example.szerowniabackend.Models.Consts;

public interface Log {
    String BAD_TOKEN = "An authorization attempt with bad token at: ";
    String BAD_HEADER = "An authorization attempt without header or token at: ";
    String AUTHORIZATION_ATTEMPT = "An authorization attempt found: ";
    String AUTHENTICATION_FAILED = "An authentication attempt have failed.";
    String AUTHENTICATION_SUCCESS = "An authentication attempt success! ";
    String AUTHENTICATION_ATTEMPT = "An authentication attempt was discovered. Email: ";
}