package com.example.szerowniabackend.Models.DTOs.Requests;

public class InsuranceAdjusterDecisionDTO {
    private String decision;
    private Long claimId;
    private String justification;

    public InsuranceAdjusterDecisionDTO(String decision, Long claimId, String justification) {
        this.decision = decision;
        this.claimId = claimId;
        this.justification = justification;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public Long getClaimId() {
        return claimId;
    }

    public void setClaimId(Long claimId) {
        this.claimId = claimId;
    }

    public String getJustification() {
        return justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }
}
