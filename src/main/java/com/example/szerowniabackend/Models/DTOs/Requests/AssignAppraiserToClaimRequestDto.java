package com.example.szerowniabackend.Models.DTOs.Requests;

public class AssignAppraiserToClaimRequestDto {
    private long claimId;
    private long appraiserId;

    public AssignAppraiserToClaimRequestDto() {
    }

    public AssignAppraiserToClaimRequestDto(long claimId, long appraiserId) {
        this.claimId = claimId;
        this.appraiserId = appraiserId;
    }

    public long getClaimId() {
        return claimId;
    }

    public void setClaimId(long claimId) {
        this.claimId = claimId;
    }

    public long getAppraiserId() {
        return appraiserId;
    }

    public void setAppraiserId(long appraiserId) {
        this.appraiserId = appraiserId;
    }
}
