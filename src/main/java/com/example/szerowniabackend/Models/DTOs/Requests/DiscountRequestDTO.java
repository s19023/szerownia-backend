package com.example.szerowniabackend.Models.DTOs.Requests;

public class DiscountRequestDTO {
    private Integer value;
    private Integer minNumDays;
    private Long rentalAdId;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getMinNumDays() {
        return minNumDays;
    }

    public void setMinNumDays(Integer minNumDays) {
        this.minNumDays = minNumDays;
    }

    public Long getRentalAdId() {
        return rentalAdId;
    }

    public void setRentalAdId(Long rentalAdId) {
        this.rentalAdId = rentalAdId;
    }
}
