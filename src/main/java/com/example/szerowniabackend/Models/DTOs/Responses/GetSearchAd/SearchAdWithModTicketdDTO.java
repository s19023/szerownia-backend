package com.example.szerowniabackend.Models.DTOs.Responses.GetSearchAd;

import com.example.szerowniabackend.Models.DTOs.Responses.GetHire.UserDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.LocationDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ProductByIdResponseDTO;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Models.Entities.TimeSlot;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public class SearchAdWithModTicketdDTO {
    private Long id;

    private String title;

    private LocalDate creationDate;

    private List<TimeSlot> timeSlotList;

    private LocationDTO location;

    private List<PickupMethod> pickupMethod;

    private UserDTO user;

    private String description;

    private Double averageOpinion;

    private Double averageRentalPrice;

    private Integer minRentalHistory;

    private String customRequirement;

    private Set<ProductByIdResponseDTO> products;

    private Long imageUserId;

    private LocalDate endDate;

    private Long modTicketId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TimeSlot> getTimeSlotList() {
        return timeSlotList;
    }

    public void setTimeSlotList(List<TimeSlot> timeSlotList) {
        this.timeSlotList = timeSlotList;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }

    public List<PickupMethod> getPickupMethod() {
        return pickupMethod;
    }

    public void setPickupMethod(List<PickupMethod> pickupMethod) {
        this.pickupMethod = pickupMethod;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAverageOpinion() {
        return averageOpinion;
    }

    public void setAverageOpinion(Double averageOpinion) {
        this.averageOpinion = averageOpinion;
    }

    public Double getAverageRentalPrice() {
        return averageRentalPrice;
    }

    public void setAverageRentalPrice(Double averageRentalPrice) {
        this.averageRentalPrice = averageRentalPrice;
    }

    public Integer getMinRentalHistory() {
        return minRentalHistory;
    }

    public void setMinRentalHistory(Integer minRentalHistory) {
        this.minRentalHistory = minRentalHistory;
    }

    public String getCustomRequirement() {
        return customRequirement;
    }

    public void setCustomRequirement(String customRequirement) {
        this.customRequirement = customRequirement;
    }

    public Set<ProductByIdResponseDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductByIdResponseDTO> products) {
        this.products = products;
    }

    public Long getImageUserId() {
        return imageUserId;
    }

    public void setImageUserId(Long imageUserId) {
        this.imageUserId = imageUserId;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Long getModTicketId() {
        return modTicketId;
    }

    public void setModTicketId(Long modTicketId) {
        this.modTicketId = modTicketId;
    }
}
