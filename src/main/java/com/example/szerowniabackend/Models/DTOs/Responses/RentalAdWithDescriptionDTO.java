package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.Entities.RentalAd;
import com.example.szerowniabackend.Models.Entities.TimeSlot;

import java.time.LocalDate;
import java.util.List;

public class RentalAdWithDescriptionDTO {
    private Long id;

    private String title;

    private List<TimeSlot> timeSlotList;

    private String location;

    private String description;

    private Double pricePerDay;

    private Double averageOpinion;

    private Double averageRentalPrice;

    private Integer minRentalHistory;

    private String customRequirement;

    private List<Long> imagesIdList;

    private Long imageUserId;

    private List<Long> thumbnailIdList;

    private LocalDate endDate;


    private String descriminator = "RENTALAD";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TimeSlot> getTimeSlotList() {
        return timeSlotList;
    }

    public void setTimeSlotList(List<TimeSlot> timeSlotList) {
        this.timeSlotList = timeSlotList;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Double getAverageOpinion() {
        return averageOpinion;
    }

    public void setAverageOpinion(Double averageOpinion) {
        this.averageOpinion = averageOpinion;
    }

    public Double getAverageRentalPrice() {
        return averageRentalPrice;
    }

    public void setAverageRentalPrice(Double averageRentalPrice) {
        this.averageRentalPrice = averageRentalPrice;
    }

    public Integer getMinRentalHistory() {
        return minRentalHistory;
    }

    public void setMinRentalHistory(Integer minRentalHistory) {
        this.minRentalHistory = minRentalHistory;
    }

    public String getCustomRequirement() {
        return customRequirement;
    }

    public void setCustomRequirement(String customRequirement) {
        this.customRequirement = customRequirement;
    }

    public List<Long> getImagesIdList() {
        return imagesIdList;
    }

    public void setImagesIdList(List<Long> imagesIdList) {
        this.imagesIdList = imagesIdList;
    }

    public Long getImageUserId() {
        return imageUserId;
    }

    public void setImageUserId(Long imageUserId) {
        this.imageUserId = imageUserId;
    }

    public String getDescriminator() {
        return descriminator;
    }

    public void setDescriminator(String descriminator) {
        this.descriminator = descriminator;
    }

    public List getThumbnailIdList() {
        return thumbnailIdList;
    }

    public void setThumbnailIdList(List thumbnailIdList) {
        this.thumbnailIdList = thumbnailIdList;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "RentalAdResponseDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", location='" + location + '\'' +
                ", pricePerDay=" + pricePerDay +
                '}';
    }
}
