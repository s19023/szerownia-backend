package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.Entities.Enums.FeatureType;

import java.util.List;

public class FeatureCreateRequestDTO {

    private String name;
    private FeatureType type;

    public FeatureCreateRequestDTO() {
    }

    public FeatureCreateRequestDTO(String name, FeatureType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FeatureType getType() {
        return type;
    }

    public void setType(FeatureType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "FeatureCreateRequestDTO{" +
                "name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}