package com.example.szerowniabackend.Models.DTOs.Responses.GetSearchAd;

import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Models.Entities.TimeSlot;

import java.time.LocalDate;
import java.util.List;

public class SearchAdResponseDTO {

    private Long id;

    private String title;

    private List<TimeSlot> timeSlotList;

    private String location;

    private List<PickupMethod> pickupMethod;

    private Long userId;

    private String descriminator = "SEARCHAD";

    private LocalDate endDate;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TimeSlot> getTimeSlotList() {
        return timeSlotList;
    }

    public void setTimeSlotList(List<TimeSlot> timeSlotList) {
        this.timeSlotList = timeSlotList;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<PickupMethod> getPickupMethod() {
        return pickupMethod;
    }

    public void setPickupMethod(List<PickupMethod> pickupMethod) {
        this.pickupMethod = pickupMethod;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDescriminator() {
        return descriminator;
    }

    public void setDescriminator(String descriminator) {
        this.descriminator = descriminator;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
