package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.LocationDTO;

public class InsuranceCompanyResponseDto {

    private Long id;
    private String name;
    private String nip;
    private String email;
    private Double commissionRate;
    private LocationDTO location;

    public InsuranceCompanyResponseDto() {
    }

    public InsuranceCompanyResponseDto(Long id, String name, String nip, String email, Double commissionRate, LocationDTO location) {
        this.id = id;
        this.name = name;
        this.nip = nip;
        this.email = email;
        this.commissionRate = commissionRate;
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Double commissionRate) {
        this.commissionRate = commissionRate;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }
}
