package com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById;

import com.example.szerowniabackend.Models.DTOs.Responses.ProductByIdResponseDTO;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Models.Entities.TimeSlot;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public class GetRentalAdByIdResponseDTO {
    
    private Long id;

    private String title;

    private LocalDate creationDate;

    private List<TimeSlot> timeSlotList;

    private LocationDTO location;

    private Set<DiscountDTO> discounts;

    private List<PickupMethod> pickupMethodList;

    private String description;

    private boolean isVisible;

    private Double pricePerDay;

    private Double depositAmount;

    private Double penaltyForEachDayOfDelayInReturns;

    private Double averageOpinion;

    private Double averageRentalPrice;

    private Integer minRentalHistory;

    private String customRequirement;

    private GetRentalAdByIdUserResponseDTO user;

    private Set<ProductByIdResponseDTO> products;

    private List<Long> imagesIdList;

    private Long imageUserId;

    private boolean isPromoted;

    private Long viewsCount;

    private LocalDate endDate;



    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public void setTimeSlotList(List<TimeSlot> timeSlotList) {
        this.timeSlotList = timeSlotList;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }

    public Set<DiscountDTO> getDiscounts() {
        return discounts;
    }

    public void setPickupMethodList(List<PickupMethod> pickupMethodList) {
        this.pickupMethodList = pickupMethodList;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public void setPenaltyForEachDayOfDelayInReturns(Double penaltyForEachDayOfDelayInReturns) {
        this.penaltyForEachDayOfDelayInReturns = penaltyForEachDayOfDelayInReturns;
    }

    public void setAverageOpinion(Double averageOpinion) {
        this.averageOpinion = averageOpinion;
    }

    public void setAverageRentalPrice(Double averageRentalPrice) {
        this.averageRentalPrice = averageRentalPrice;
    }

    public void setMinRentalHistory(Integer minRentalHistory) {
        this.minRentalHistory = minRentalHistory;
    }

    public void setCustomRequirement(String customRequirement) {
        this.customRequirement = customRequirement;
    }

    public void setProducts(Set<ProductByIdResponseDTO> products) {
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public List<TimeSlot> getTimeSlotList() {
        return timeSlotList;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setDiscounts(Set<DiscountDTO> discounts) {
        this.discounts = discounts;
    }

    public List<PickupMethod> getPickupMethodList() {
        return pickupMethodList;
    }

    public String getDescription() {
        return description;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public Double getPenaltyForEachDayOfDelayInReturns() {
        return penaltyForEachDayOfDelayInReturns;
    }

    public Integer getMinRentalHistory() {
        return minRentalHistory;
    }

    public Double getAverageRentalPrice() {
        return averageRentalPrice;
    }

    public Double getAverageOpinion() {
        return averageOpinion;
    }

    public String getCustomRequirement() {
        return customRequirement;
    }

    public GetRentalAdByIdUserResponseDTO getUser() {
        return user;
    }

    public void setUser(GetRentalAdByIdUserResponseDTO user) {
        this.user = user;
    }

    public Set<ProductByIdResponseDTO> getProducts() {
        return products;
    }

    public Long getImageUserId() {
        return imageUserId;
    }

    public void setImageUserId(Long imageUserId) {
        this.imageUserId = imageUserId;
    }

    public List<Long> getImagesIdList() {
        return imagesIdList;
    }

    public void setImagesIdList(List<Long> imagesIdList) {
        this.imagesIdList = imagesIdList;
    }

    public boolean isPromoted() {
        return isPromoted;
    }

    public void setPromoted(boolean promoted) {
        isPromoted = promoted;
    }

    public Long getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(Long viewsCount) {
        this.viewsCount = viewsCount;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
