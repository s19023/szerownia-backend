package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.LocationDTO;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Models.Entities.TimeSlot;


import java.time.LocalDate;
import java.util.List;

public class SearchAdCreateRequestDTO {
    private String title;

    private List<TimeSlot> timeSlotList;

    private LocationDTO location;

    private List<PickupMethod> pickupMethod;

    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TimeSlot> getTimeSlotList() {
        return timeSlotList;
    }

    public void setTimeSlotList(List<TimeSlot> timeSlotList) {
        this.timeSlotList = timeSlotList;
    }

    public List<PickupMethod> getPickupMethod() {
        return pickupMethod;
    }

    public void setPickupMethod(List<PickupMethod> pickupMethod) {
        this.pickupMethod = pickupMethod;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }
}
