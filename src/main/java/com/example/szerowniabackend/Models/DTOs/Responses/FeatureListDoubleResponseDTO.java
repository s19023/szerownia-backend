package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.Entities.Enums.FeatureType;

public class FeatureListDoubleResponseDTO extends FeatureListResponseDTO {
    private Double min;
    private Double max;

    public FeatureListDoubleResponseDTO() {
    }

    public FeatureListDoubleResponseDTO(Long idFeature, String name, FeatureType type, boolean required, Double min, Double max) {
        super(idFeature, name, type, required);
        this.min = min;
        this.max = max;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }
}
