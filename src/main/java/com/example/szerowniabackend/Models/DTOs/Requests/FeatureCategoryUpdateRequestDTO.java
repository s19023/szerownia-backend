package com.example.szerowniabackend.Models.DTOs.Requests;

public class FeatureCategoryUpdateRequestDTO {
    private Long idFeature;
    private Boolean isRequired;

    public FeatureCategoryUpdateRequestDTO() {
    }

    public FeatureCategoryUpdateRequestDTO(Long idFeature, Boolean isRequired) {
        this.idFeature = idFeature;
        this.isRequired = isRequired;
    }

    public Long getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Long idFeature) {
        this.idFeature = idFeature;
    }

    public Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Boolean required) {
        isRequired = required;
    }
}
