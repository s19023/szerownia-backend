package com.example.szerowniabackend.Models.DTOs.Responses;

public class ImageDTO {

    private Long imageId;

    private Long thumbnailId;

    public ImageDTO() {
    }

    public ImageDTO(Long imageId, Long thumbnailId) {
        this.imageId = imageId;
        this.thumbnailId = thumbnailId;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public Long getThumbnailId() {
        return thumbnailId;
    }

    public void setThumbnailId(Long thumbnailId) {
        this.thumbnailId = thumbnailId;
    }
}
