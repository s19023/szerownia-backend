package com.example.szerowniabackend.Models.DTOs.Requests;

import java.util.List;

public class SearchAdFindRequestDTO {

    private Long idCategory;
    private String keyWord;
    List<ParameterReferenceDTO> parameters;


    public Long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public List<ParameterReferenceDTO> getParameters() {
        return parameters;
    }

    public void setParameters(List<ParameterReferenceDTO> parameters) {
        this.parameters = parameters;
    }
}
