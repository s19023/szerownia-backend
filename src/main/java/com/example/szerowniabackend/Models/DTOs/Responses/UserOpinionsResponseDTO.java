package com.example.szerowniabackend.Models.DTOs.Responses;

import java.time.LocalDate;

public class UserOpinionsResponseDTO {

    private Long idOpinion;
    private int rating;
    private String comment;
    private Boolean isAboutSharer;
    private Long idAd;
    private Long idAdThumbnail;
    private String adTitle;
    private Long idAuthor;
    private Long idAuthorAvatar;
    private String authorFirstName;
    private String authorLastName;
    private LocalDate date;

    public Long getIdOpinion() {
        return idOpinion;
    }

    public void setIdOpinion(Long idOpinion) {
        this.idOpinion = idOpinion;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getAboutSharer() {
        return isAboutSharer;
    }

    public void setAboutSharer(Boolean aboutSharer) {
        isAboutSharer = aboutSharer;
    }

    public Long getIdAd() {
        return idAd;
    }

    public void setIdAd(Long idAd) {
        this.idAd = idAd;
    }

    public Long getIdAdThumbnail() {
        return idAdThumbnail;
    }

    public void setIdAdThumbnail(Long idAdThumbnail) {
        this.idAdThumbnail = idAdThumbnail;
    }

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
    }

    public Long getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(Long idAuthor) {
        this.idAuthor = idAuthor;
    }

    public Long getIdAuthorAvatar() {
        return idAuthorAvatar;
    }

    public void setIdAuthorAvatar(Long idAuthorAvatar) {
        this.idAuthorAvatar = idAuthorAvatar;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }
}
