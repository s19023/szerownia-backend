package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.Entities.VulgarWords;

import java.util.List;

public class PaginatedDTO <T> {
    private Long count;
    private int totalPages;
    private List<T> results;

    public PaginatedDTO(Long count, List<T> results) {
        this.count = count;
        this.results = results;
    }

    public PaginatedDTO(Long count, int totalPages, List<T> results) {
        this.count = count;
        this.totalPages = totalPages;
        this.results = results;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }

    public Long getCount() {
        return count;
    }

    public List<T> getResults() {
        return results;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
