package com.example.szerowniabackend.Models.DTOs.Responses.GetHire;

import com.example.szerowniabackend.Models.Entities.Enums.HireStatus;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;

import java.time.LocalDate;

public class HireResponseDTO {

    private Long id;
    private LocalDate dateFrom;
    private LocalDate dateToPlanned;
    private LocalDate dateToActual;
    private Double cost;
    private String outgoingShipmentNumber;
    private String incomingShipmentNumber;
    private PickupMethod selectedPickupMethod;
    private Float commission;
    private RentalAdResponseDTO ad;
    private UserDTO borrower;
    private UserDTO sharer;
    private HireStatus hireStatus;
    private boolean isDelivered;
    private boolean isReturned;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateToPlanned() {
        return dateToPlanned;
    }

    public void setDateToPlanned(LocalDate dateToPlanned) {
        this.dateToPlanned = dateToPlanned;
    }

    public LocalDate getDateToActual() {
        return dateToActual;
    }

    public void setDateToActual(LocalDate dateToActual) {
        this.dateToActual = dateToActual;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getOutgoingShipmentNumber() {
        return outgoingShipmentNumber;
    }

    public void setOutgoingShipmentNumber(String outgoingShipmentNumber) {
        this.outgoingShipmentNumber = outgoingShipmentNumber;
    }

    public String getIncomingShipmentNumber() {
        return incomingShipmentNumber;
    }

    public void setIncomingShipmentNumber(String incomingShipmentNumber) {
        this.incomingShipmentNumber = incomingShipmentNumber;
    }

    public PickupMethod getSelectedPickupMethod() {
        return selectedPickupMethod;
    }

    public void setSelectedPickupMethod(PickupMethod selectedPickupMethod) {
        this.selectedPickupMethod = selectedPickupMethod;
    }

    public Float getCommission() {
        return commission;
    }

    public void setCommission(Float commission) {
        this.commission = commission;
    }

    public RentalAdResponseDTO getAd() {
        return ad;
    }

    public void setAd(RentalAdResponseDTO ad) {
        this.ad = ad;
    }

    public UserDTO getBorrower() {
        return borrower;
    }

    public void setBorrower(UserDTO borrower) {
        this.borrower = borrower;
    }

    public UserDTO getSharer() {
        return sharer;
    }

    public void setSharer(UserDTO sharer) {
        this.sharer = sharer;
    }

    public HireStatus getHireStatus() {
        return hireStatus;
    }

    public void setHireStatus(HireStatus hireStatus) {
        this.hireStatus = hireStatus;
    }

    public boolean isDelivered() {
        return isDelivered;
    }

    public void setDelivered(boolean delivered) {
        isDelivered = delivered;
    }

    public boolean isReturned() {
        return isReturned;
    }

    public void setReturned(boolean returned) {
        isReturned = returned;
    }
}
