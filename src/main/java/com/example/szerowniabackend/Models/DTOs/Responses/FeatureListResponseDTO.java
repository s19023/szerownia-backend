package com.example.szerowniabackend.Models.DTOs.Responses;


import com.example.szerowniabackend.Models.Entities.Enums.FeatureType;

public class FeatureListResponseDTO {

    private Long idFeature;
    private String name;
    private FeatureType type;
    private boolean required;

    public FeatureListResponseDTO() {
    }

    public FeatureListResponseDTO(Long idFeature, String name, FeatureType type, boolean required) {
        this.idFeature = idFeature;
        this.name = name;
        this.type = type;
        this.required = required;
    }

    public Long getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Long idFeature) {
        this.idFeature = idFeature;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FeatureType getType() {
        return type;
    }

    public void setType(FeatureType type) {
        this.type = type;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public String toString() {
        return "FeatureListResponseDTO{" +
                "idFeature=" + idFeature +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", required=" + required +
                '}';
    }
}
