package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Models.Entities.TimeSlot;

import java.time.LocalDate;
import java.util.List;

public class RentalAdCreateRequestDTO {

    private String title;

    private List<TimeSlot> timeSlotList;

    private LocationForRentalAdRequestDTO location;

    private List<PickupMethod> pickupMethod;

    private String description;

    private Double pricePerDay;

    private Double depositAmount;

    private Double penaltyForEachDayOfDelayInReturns;

    private Double averageOpinion;

    private Double averageRentalPrice;

    private Integer minRentalHistory;

    private String customRequirement;

    private Long searchAdId;

    private List<String> imageNames;

    private boolean isPromoted;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TimeSlot> getTimeSlotList() {
        return timeSlotList;
    }

    public void setTimeSlotList(List<TimeSlot> timeSlotList) {
        this.timeSlotList = timeSlotList;
    }

    public LocationForRentalAdRequestDTO getLocation() {
        return location;
    }

    public void setLocation(LocationForRentalAdRequestDTO location) {
        this.location = location;
    }

    public List<PickupMethod> getPickupMethod() {
        return pickupMethod;
    }

    public void setPickupMethod(List<PickupMethod> pickupMethod) {
        this.pickupMethod = pickupMethod;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Double getPenaltyForEachDayOfDelayInReturns() {
        return penaltyForEachDayOfDelayInReturns;
    }

    public void setPenaltyForEachDayOfDelayInReturns(Double penaltyForEachDayOfDelayInReturns) {
        this.penaltyForEachDayOfDelayInReturns = penaltyForEachDayOfDelayInReturns;
    }

    public Double getAverageOpinion() {
        return averageOpinion;
    }

    public void setAverageOpinion(Double averageOpinion) {
        this.averageOpinion = averageOpinion;
    }

    public Double getAverageRentalPrice() {
        return averageRentalPrice;
    }

    public void setAverageRentalPrice(Double averageRentalPrice) {
        this.averageRentalPrice = averageRentalPrice;
    }

    public Integer getMinRentalHistory() {
        return minRentalHistory;
    }

    public void setMinRentalHistory(Integer minRentalHistory) {
        this.minRentalHistory = minRentalHistory;
    }

    public String getCustomRequirement() {
        return customRequirement;
    }

    public void setCustomRequirement(String customRequirement) {
        this.customRequirement = customRequirement;
    }

    public Long getSearchAdId() {
        return searchAdId;
    }

    public void setSearchAdId(Long searchAdId) {
        this.searchAdId = searchAdId;
    }

    public List<String> getImageNames() {
        return imageNames;
    }

    public void setImageNames(List<String> imageNames) {
        this.imageNames = imageNames;
    }

    public boolean isPromoted() {
        return isPromoted;
    }

    public void setPromoted(boolean promoted) {
        isPromoted = promoted;
    }
}
