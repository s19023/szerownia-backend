package com.example.szerowniabackend.Models.DTOs.Requests;

import java.util.List;

public class CategoryUpdateRequestDTO {

    private String name;
    private Long idParentCategory;
    private List<FeatureCategoryUpdateRequestDTO> features;

    public CategoryUpdateRequestDTO() {
    }

    public CategoryUpdateRequestDTO(String name, Long idParentCategory, List<FeatureCategoryUpdateRequestDTO> features) {
        this.name = name;
        this.idParentCategory = idParentCategory;
        this.features = features;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdParentCategory() {
        return idParentCategory;
    }

    public void setIdParentCategory(Long idParentCategory) {
        this.idParentCategory = idParentCategory;
    }

    public List<FeatureCategoryUpdateRequestDTO> getFeatures() {
        return features;
    }

    public void setFeatures(List<FeatureCategoryUpdateRequestDTO> features) {
        this.features = features;
    }

    @Override
    public String toString() {
        return "CategoryUpdateRequestDTO{" +
                "name='" + name + '\'' +
                ", idParentCategory=" + idParentCategory +
                '}';
    }
}
