package com.example.szerowniabackend.Models.DTOs.Requests;

import java.util.ArrayList;
import java.util.List;

public class ProductCreateRequestDTO {

    private String name;

    private String make;

    private String model;

    private int condition;

    private int estimatedValue;

    private Long idCategory;

    private List<FeatureOccurrenceAssignProductRequestDTO> productFeatures = new ArrayList<>();

    private boolean isSearchProduct;

    public ProductCreateRequestDTO() {
    }

    public ProductCreateRequestDTO(String name, String make, String model, int condition, int estimatedValue, Long idCategory) {
        this.name = name;
        this.make = make;
        this.model = model;
        this.condition = condition;
        this.estimatedValue = estimatedValue;
        this.idCategory = idCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }

    public int getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(int estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public Long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }

    public List<FeatureOccurrenceAssignProductRequestDTO> getProductFeatures() {
        return productFeatures;
    }

    public void setProductFeatures(List<FeatureOccurrenceAssignProductRequestDTO> productFeatures) {
        this.productFeatures = productFeatures;
    }

    public boolean isSearchProduct() {
        return isSearchProduct;
    }

    public void setSearchProduct(boolean searchProduct) {
        isSearchProduct = searchProduct;
    }

    @Override
    public String toString() {
        return "ProductCreateRequestDTO{" +
                "name='" + name + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", condition=" + condition +
                ", estimatedValue=" + estimatedValue +
                '}';
    }
}
