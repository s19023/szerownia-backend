package com.example.szerowniabackend.Models.DTOs.Requests;

public class HireOutgoingRequestDto {
    private String outgoingShipmentNumber;
    private Boolean insure;
    private long insuranceCompanyId;

    public HireOutgoingRequestDto() {
    }

    public HireOutgoingRequestDto(String outgoingShipmentNumber, Boolean insure, long insuranceCompanyId) {
        this.outgoingShipmentNumber = outgoingShipmentNumber;
        this.insure = insure;
        this.insuranceCompanyId = insuranceCompanyId;
    }

    public String getOutgoingShipmentNumber() {
        return outgoingShipmentNumber;
    }

    public void setOutgoingShipmentNumber(String outgoingShipmentNumber) {
        this.outgoingShipmentNumber = outgoingShipmentNumber;
    }

    public Boolean getInsure() {
        return insure;
    }

    public void setInsure(Boolean insure) {
        this.insure = insure;
    }

    public long getInsuranceCompanyId() {
        return insuranceCompanyId;
    }

    public void setInsuranceCompanyId(long insuranceCompanyId) {
        this.insuranceCompanyId = insuranceCompanyId;
    }


}
