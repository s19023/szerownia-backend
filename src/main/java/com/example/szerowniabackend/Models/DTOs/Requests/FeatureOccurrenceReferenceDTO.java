package com.example.szerowniabackend.Models.DTOs.Requests;

public class FeatureOccurrenceReferenceDTO {

    private Long idFeature;
    private Long idFeatureOccurrence;
    private String featureName;
    private String textValue;
    private Integer decimalNumberValue;
    private Double floatingNumberValue;
    private Boolean booleanValue;

    public FeatureOccurrenceReferenceDTO() {
    }

    public FeatureOccurrenceReferenceDTO(Long idFeatureOccurrence, String featureName, String textValue, Integer decimalNumberValue, Double floatingNumberValue, Boolean booleanValue) {
        this.idFeatureOccurrence = idFeatureOccurrence;
        this.featureName = featureName;
        this.textValue = textValue;
        this.decimalNumberValue = decimalNumberValue;
        this.floatingNumberValue = floatingNumberValue;
        this.booleanValue = booleanValue;
    }

    public Long getIdFeatureOccurrence() {
        return idFeatureOccurrence;
    }

    public void setIdFeatureOccurrence(Long idFeatureOccurrence) {
        this.idFeatureOccurrence = idFeatureOccurrence;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public Integer getDecimalNumberValue() {
        return decimalNumberValue;
    }

    public void setDecimalNumberValue(Integer decimalNumberValue) {
        this.decimalNumberValue = decimalNumberValue;
    }

    public Double getFloatingNumberValue() {
        return floatingNumberValue;
    }

    public void setFloatingNumberValue(Double floatingNumberValue) {
        this.floatingNumberValue = floatingNumberValue;
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(Boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public Long getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Long idFeature) {
        this.idFeature = idFeature;
    }

    @Override
    public String toString() {
        return "FeatureOccurrenceReferenceDTO{" +
                "featureName='" + featureName + '\'' +
                ", textValue='" + textValue + '\'' +
                ", decimalNumberValue=" + decimalNumberValue +
                ", floatingNumberValue=" + floatingNumberValue +
                ", booleanValue=" + booleanValue +
                '}';
    }
}
