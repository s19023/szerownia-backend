package com.example.szerowniabackend.Models.DTOs.Responses;

public class VulgarWordResponseDTO {

    private String vulgarWord;

    public String getVulgarWord() {
        return vulgarWord;
    }

    public void setVulgarWord(String vulgarWord) {
        this.vulgarWord = vulgarWord;
    }
}
