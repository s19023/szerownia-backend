package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.LocationDTO;

import java.time.LocalDate;

public class ClaimRequestDTO {
    private Long idPolicy;
    private LocalDate claimDate;
    private String damageDescription;
    private String circumstances;
    private LocationDTO location;

    public Long getIdPolicy() {
        return idPolicy;
    }

    public void setIdPolicy(Long idPolicy) {
        this.idPolicy = idPolicy;
    }

    public LocalDate getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(LocalDate claimDate) {
        this.claimDate = claimDate;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public void setDamageDescription(String damageDescription) {
        this.damageDescription = damageDescription;
    }

    public String getCircumstances() {
        return circumstances;
    }

    public void setCircumstances(String circumstances) {
        this.circumstances = circumstances;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }
}
