package com.example.szerowniabackend.Models.DTOs.Responses;

public class UserRespDto {

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private Double borrowerRate;
    private Double sharerRate;
    private String telephoneNumber;
    private Long userImageId;

    public UserRespDto() {
    }

    public UserRespDto(Long id, String email, String firstName, String lastName, Double borrowerRate, Double sharerRate, String telephoneNumber, Long userImageId) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.borrowerRate = borrowerRate;
        this.sharerRate = sharerRate;
        this.telephoneNumber = telephoneNumber;
        this.userImageId = userImageId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getBorrowerRate() {
        return borrowerRate;
    }

    public void setBorrowerRate(Double borrowerRate) {
        this.borrowerRate = borrowerRate;
    }

    public Double getSharerRate() {
        return sharerRate;
    }

    public void setSharerRate(Double sharerRate) {
        this.sharerRate = sharerRate;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public Long getUserImageId() {
        return userImageId;
    }

    public void setUserImageId(Long userImageId) {
        this.userImageId = userImageId;
    }
}
