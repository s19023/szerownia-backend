package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.Entities.Enums.FeatureType;

public class FeatureListIntegerResponseDTO extends FeatureListResponseDTO {
    private Integer min;
    private Integer max;

    public FeatureListIntegerResponseDTO() {
    }

    public FeatureListIntegerResponseDTO(Long idFeature, String name, FeatureType type, boolean required, Integer min, Integer max) {
        super(idFeature, name, type, required);
        this.min = min;
        this.max = max;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }


}
