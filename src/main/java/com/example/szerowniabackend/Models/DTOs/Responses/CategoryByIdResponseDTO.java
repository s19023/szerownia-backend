package com.example.szerowniabackend.Models.DTOs.Responses;

import java.util.ArrayList;
import java.util.List;

public class CategoryByIdResponseDTO {

    private String name;
    private boolean isAbstract;
    private CategoryReferenceDTO parentCategory;
    private List<CategoryReferenceDTO> subCategories = new ArrayList<>();

    public CategoryByIdResponseDTO() {
    }

    public CategoryByIdResponseDTO(String name, boolean isAbstract, CategoryReferenceDTO parentCategory, List<CategoryReferenceDTO> subCategories) {
        this.name = name;
        this.isAbstract = isAbstract;
        this.parentCategory = parentCategory;
        this.subCategories = subCategories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public void setAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public CategoryReferenceDTO getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(CategoryReferenceDTO parentCategory) {
        this.parentCategory = parentCategory;
    }

    public List<CategoryReferenceDTO> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<CategoryReferenceDTO> subCategories) {
        this.subCategories = subCategories;
    }

    @Override
    public String toString() {
        return "CategoryCreateRequestDTO{" +
                "name='" + name + '\'' +
                ", isAbstract=" + isAbstract +
                ", parentCategory=" + parentCategory +
                ", subCategory=" + subCategories +
                '}';
    }
}
