package com.example.szerowniabackend.Models.DTOs.Responses;

import java.util.List;

public class RegionResponseDTO {

    private String id;

    private String name;

    private List<DistrictResponseDTO> districts;

    public RegionResponseDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DistrictResponseDTO> getDistricts() {
        return districts;
    }

    public void setDistricts(List<DistrictResponseDTO> districts) {
        this.districts = districts;
    }
}
