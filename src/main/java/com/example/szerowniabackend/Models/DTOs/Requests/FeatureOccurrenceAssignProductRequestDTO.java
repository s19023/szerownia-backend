package com.example.szerowniabackend.Models.DTOs.Requests;


public class FeatureOccurrenceAssignProductRequestDTO {


    private String textValue;
    private Integer decimalNumberValue;
    private Double floatingNumberValue;
    private Boolean booleanValue;
    private Long idFeature;

    public FeatureOccurrenceAssignProductRequestDTO() {
    }

    public FeatureOccurrenceAssignProductRequestDTO(String textValue, Integer decimalNumberValue, Double floatingNumberValue, Boolean booleanValue, Long idFeature) {
        this.textValue = textValue;
        this.decimalNumberValue = decimalNumberValue;
        this.floatingNumberValue = floatingNumberValue;
        this.booleanValue = booleanValue;
        this.idFeature = idFeature;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public Integer getDecimalNumberValue() {
        return decimalNumberValue;
    }

    public void setDecimalNumberValue(Integer decimalNumberValue) {
        this.decimalNumberValue = decimalNumberValue;
    }

    public Double getFloatingNumberValue() {
        return floatingNumberValue;
    }

    public void setFloatingNumberValue(Double floatingNumberValue) {
        this.floatingNumberValue = floatingNumberValue;
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(Boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public Long getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Long idFeature) {
        this.idFeature = idFeature;
    }
}
