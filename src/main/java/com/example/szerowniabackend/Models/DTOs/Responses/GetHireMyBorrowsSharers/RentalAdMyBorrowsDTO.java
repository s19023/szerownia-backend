package com.example.szerowniabackend.Models.DTOs.Responses.GetHireMyBorrowsSharers;

public class RentalAdMyBorrowsDTO {

    private Long id;
    private String title;
    private Long adThumbnailId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getAdThumbnailId() {
        return adThumbnailId;
    }

    public void setAdThumbnailId(Long adThumbnailId) {
        this.adThumbnailId = adThumbnailId;
    }
}
