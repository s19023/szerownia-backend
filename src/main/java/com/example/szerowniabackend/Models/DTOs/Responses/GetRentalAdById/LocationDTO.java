package com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById;

public class LocationDTO {
    private String name;
    private Double longitude;
    private Double latitude;

    public LocationDTO(String location, Double longitude, Double latitude) {
        this.name = location;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public LocationDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
