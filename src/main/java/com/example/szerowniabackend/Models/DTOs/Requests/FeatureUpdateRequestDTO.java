package com.example.szerowniabackend.Models.DTOs.Requests;

public class FeatureUpdateRequestDTO {

    private String name;
    private String type;

    public FeatureUpdateRequestDTO() {
    }

    public FeatureUpdateRequestDTO(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FeatureByIdResponseDTO{" +
                "name='" + name + '\'' +
                '}';
    }
}