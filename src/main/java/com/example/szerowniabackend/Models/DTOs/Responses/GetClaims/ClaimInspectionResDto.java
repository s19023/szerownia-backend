package com.example.szerowniabackend.Models.DTOs.Responses.GetClaims;

import com.example.szerowniabackend.Models.DTOs.Responses.ImageDTO;

import java.time.LocalDate;
import java.util.List;

public class ClaimInspectionResDto {
    private Long id;
    private LocalDate inspectionDate;
    private String description;
    private double damageAssessment;
    private UserAppraiserDTO appraiser;
    private List<ImageDTO> images;

    public ClaimInspectionResDto() {
    }

    public ClaimInspectionResDto(Long id, LocalDate inspectionDate, String description, double damageAssessment, UserAppraiserDTO appraiser) {
        this.id = id;
        this.inspectionDate = inspectionDate;
        this.description = description;
        this.damageAssessment = damageAssessment;
        this.appraiser = appraiser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(LocalDate inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getDamageAssessment() {
        return damageAssessment;
    }

    public void setDamageAssessment(double damageAssessment) {
        this.damageAssessment = damageAssessment;
    }

    public UserAppraiserDTO getAppraiser() {
        return appraiser;
    }

    public void setAppraiser(UserAppraiserDTO appraiser) {
        this.appraiser = appraiser;
    }

    public List<ImageDTO> getImages() {
        return images;
    }

    public void setImages(List<ImageDTO> images) {
        this.images = images;
    }
}
