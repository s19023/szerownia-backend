package com.example.szerowniabackend.Models.DTOs.Responses;

public class CalculateHireCostDTO {
    private Double cost;
    private Integer period;
    private Double depositAmount;
    private Double pricePerDay;
    private Double commission;
    private DiscountDTO usedDiscount;

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public DiscountDTO getUsedDiscount() {
        return usedDiscount;
    }

    public void setUsedDiscount(DiscountDTO usedDiscount) {
        this.usedDiscount = usedDiscount;
    }
}
