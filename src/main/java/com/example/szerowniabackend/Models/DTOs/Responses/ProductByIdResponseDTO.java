package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.DTOs.Requests.FeatureOccurrenceReferenceDTO;

import java.util.List;

public class ProductByIdResponseDTO {

    private Long id;

    private String name;

    private String make;

    private String model;

    private int condition;

    private int estimatedValue;

    private boolean isModifiable;

    private CategoryReferenceDTO category;

    private List<FeatureOccurrenceReferenceDTO> features;

    public ProductByIdResponseDTO() {
    }

    public ProductByIdResponseDTO(String name, String make, String model, int condition, int estimatedValue, CategoryReferenceDTO categoryReferenceDTO) {
        this.name = name;
        this.make = make;
        this.model = model;
        this.condition = condition;
        this.estimatedValue = estimatedValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }

    public int getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(int estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public boolean isModifiable() {
        return isModifiable;
    }

    public void setModifiable(boolean modifiable) {
        isModifiable = modifiable;
    }

    public CategoryReferenceDTO getCategoryReferenceDTO() {
        return category;
    }

    public void setCategoryReferenceDTO(CategoryReferenceDTO categoryReferenceDTO) {
        this.category = categoryReferenceDTO;
    }

    public List<FeatureOccurrenceReferenceDTO> getFeatureOccurrences() {
        return features;
    }

    public void setFeatureOccurrences(List<FeatureOccurrenceReferenceDTO> featureOccurrences) {
        this.features = featureOccurrences;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ProductCreateRequestDTO{" +
                "name='" + name + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", condition=" + condition +
                ", estimatedValue=" + estimatedValue +
                '}';
    }
}