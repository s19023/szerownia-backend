package com.example.szerowniabackend.Models.DTOs.Requests;

public class BlockedIpRequestDTO {

    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
