package com.example.szerowniabackend.Models.DTOs.Responses.GetClaims;

import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.LocationDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.PolicyResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.UserRespDto;
import com.example.szerowniabackend.Models.Entities.Enums.ClaimStatus;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class ClaimDetailsResponseDTO {
    private Long id;
    private LocalDate claimDate;
    private LocalDateTime reportDate;
    private LocalDateTime claimAdjusterSendDate;
    private LocalDateTime dateOfAssignToTheAppraiser;
    private String damageDescription;
    private String circumstances;
    private String justification;
    private UserRespDto user;
    private PolicyResponseDTO policy;
    private LocationDTO location;
    private ClaimStatus status;
    private String title;
    private UserAppraiserDTO appraiser;
    private UserRespDto borrower;
    private List<ClaimInspectionResDto> claimInspections;
    private UUID insuranceCaseId;

    public ClaimDetailsResponseDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(LocalDate claimDate) {
        this.claimDate = claimDate;
    }

    public LocalDateTime getReportDate() {
        return reportDate;
    }

    public void setReportDate(LocalDateTime reportDate) {
        this.reportDate = reportDate;
    }

    public LocalDateTime getClaimAdjusterSendDate() {
        return claimAdjusterSendDate;
    }

    public void setClaimAdjusterSendDate(LocalDateTime claimAdjusterSendDate) {
        this.claimAdjusterSendDate = claimAdjusterSendDate;
    }

    public LocalDateTime getDateOfAssignToTheAppraiser() {
        return dateOfAssignToTheAppraiser;
    }

    public void setDateOfAssignToTheAppraiser(LocalDateTime dateOfAssignToTheAppraiser) {
        this.dateOfAssignToTheAppraiser = dateOfAssignToTheAppraiser;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public void setDamageDescription(String damageDescription) {
        this.damageDescription = damageDescription;
    }

    public String getCircumstances() {
        return circumstances;
    }

    public void setCircumstances(String circumstances) {
        this.circumstances = circumstances;
    }

    public String getJustification() {
        return justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }

    public UserRespDto getUser() {
        return user;
    }

    public void setUser(UserRespDto user) {
        this.user = user;
    }

    public PolicyResponseDTO getPolicy() {
        return policy;
    }

    public void setPolicy(PolicyResponseDTO policy) {
        this.policy = policy;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }

    public ClaimStatus getStatus() {
        return status;
    }

    public void setStatus(ClaimStatus status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UserAppraiserDTO getAppraiser() {
        return appraiser;
    }

    public void setAppraiser(UserAppraiserDTO appraiser) {
        this.appraiser = appraiser;
    }

    public UserRespDto getBorrower() {
        return borrower;
    }

    public void setBorrower(UserRespDto borrower) {
        this.borrower = borrower;
    }

    public List<ClaimInspectionResDto> getClaimInspections() {
        return claimInspections;
    }

    public void setClaimInspections(List<ClaimInspectionResDto> claimInspections) {
        this.claimInspections = claimInspections;
    }

    public UUID getInsuranceCaseId() {
        return insuranceCaseId;
    }

    public void setInsuranceCaseId(UUID insuranceCaseId) {
        this.insuranceCaseId = insuranceCaseId;
    }
}
