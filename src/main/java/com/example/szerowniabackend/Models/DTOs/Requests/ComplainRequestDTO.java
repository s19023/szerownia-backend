package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.Entities.Enums.ComplaintReason;

public class ComplainRequestDTO {

    private Long hireId;

    private ComplaintReason reason;

    private String details;

    public ComplaintReason getReason() {
        return reason;
    }

    public void setReason(ComplaintReason reason) {
        this.reason = reason;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Long getHireId() {
        return hireId;
    }

    public void setHireId(Long hireId) {
        this.hireId = hireId;
    }
}
