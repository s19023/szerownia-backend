package com.example.szerowniabackend.Models.DTOs.Responses;




public class AdReferenceDTO {

    private Long id;
    private String title;
    private String locationName;
    private Double pricePerDay;
    private Long adThumbnailId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Long getAdThumbnailId() {
        return adThumbnailId;
    }

    public void setAdThumbnailId(Long adThumbnailId) {
        this.adThumbnailId = adThumbnailId;
    }
}
