package com.example.szerowniabackend.Models.DTOs.Responses.GetHire;

import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;

import java.time.LocalDate;

public class CreateHireResponseDTO {
    private Long id;
    private LocalDate dateFrom;
    private LocalDate dateToPlanned;
    private LocalDate dateToActual;
    private Double cost;
    private PickupMethod selectedPickupMethod;
    private Float commission;
    private Long adId;
    private UserDTO borrower;
    private UserDTO sharer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateToPlanned() {
        return dateToPlanned;
    }

    public void setDateToPlanned(LocalDate dateToPlanned) {
        this.dateToPlanned = dateToPlanned;
    }

    public LocalDate getDateToActual() {
        return dateToActual;
    }

    public void setDateToActual(LocalDate dateToActual) {
        this.dateToActual = dateToActual;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public PickupMethod getSelectedPickupMethod() {
        return selectedPickupMethod;
    }

    public void setSelectedPickupMethod(PickupMethod selectedPickupMethod) {
        this.selectedPickupMethod = selectedPickupMethod;
    }

    public Float getCommission() {
        return commission;
    }

    public void setCommission(Float commission) {
        this.commission = commission;
    }

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }

    public UserDTO getBorrower() {
        return borrower;
    }

    public void setBorrower(UserDTO borrower) {
        this.borrower = borrower;
    }

    public UserDTO getSharer() {
        return sharer;
    }

    public void setSharer(UserDTO sharer) {
        this.sharer = sharer;
    }
}
