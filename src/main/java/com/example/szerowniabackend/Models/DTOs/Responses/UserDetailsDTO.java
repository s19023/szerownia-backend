package com.example.szerowniabackend.Models.DTOs.Responses;

import java.time.LocalDate;

public class UserDetailsDTO {

    private String email;

    private String firstName;

    private String lastName;

    private LocalDate registrationDate;

    private String telephoneNumber;

    private Double averageRateBorrower;

    private Double averageRate2Sharer;

    private Boolean isBlocked;

    private Boolean isPremium;

    private LocalDate premiumFrom;

    private LocalDate premiumTo;

    private Long userImageId;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public Double getAverageRateBorrower() {
        return averageRateBorrower;
    }

    public void setAverageRateBorrower(Double averageRateBorrower) {
        this.averageRateBorrower = averageRateBorrower;
    }

    public Double getAverageRate2Sharer() {
        return averageRate2Sharer;
    }

    public void setAverageRate2Sharer(Double averageRate2Sharer) {
        this.averageRate2Sharer = averageRate2Sharer;
    }

    public Boolean getBlocked() {
        return isBlocked;
    }

    public void setBlocked(Boolean blocked) {
        isBlocked = blocked;
    }

    public Boolean getPremium() {
        return isPremium;
    }

    public void setPremium(Boolean premium) {
        isPremium = premium;
    }

    public LocalDate getPremiumFrom() {
        return premiumFrom;
    }

    public void setPremiumFrom(LocalDate premiumFrom) {
        this.premiumFrom = premiumFrom;
    }

    public LocalDate getPremiumTo() {
        return premiumTo;
    }

    public void setPremiumTo(LocalDate premiumTo) {
        this.premiumTo = premiumTo;
    }

    public Long getUserImageId() {
        return userImageId;
    }

    public void setUserImageId(Long userImageId) {
        this.userImageId = userImageId;
    }
}
