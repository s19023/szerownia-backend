package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.Entities.Location;
import com.example.szerowniabackend.Models.Entities.RentalAd;
import com.example.szerowniabackend.Models.Entities.TimeSlot;

import java.time.LocalDate;
import java.util.List;

public class RentalAdResponseDTO {

    private Long id;

    private String title;

    private List<TimeSlot> timeSlotList;

    private String location;

    private Double pricePerDay;

    private Double averageOpinion;

    private Double averageRentalPrice;

    private Integer minRentalHistory;

    private String customRequirement;

    private List<Long> imagesIdList;

    private List<Long> thumbnailIdList;

    private LocalDate endDate;

    private boolean isPromoted;
    private String descriminator = "RENTALAD";


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TimeSlot> getTimeSlotList() {
        return timeSlotList;
    }

    public void setTimeSlotList(List<TimeSlot> timeSlotList) {
        this.timeSlotList = timeSlotList;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Double getAverageOpinion() {
        return averageOpinion;
    }

    public void setAverageOpinion(Double averageOpinion) {
        this.averageOpinion = averageOpinion;
    }

    public Double getAverageRentalPrice() {
        return averageRentalPrice;
    }

    public void setAverageRentalPrice(Double averageRentalPrice) {
        this.averageRentalPrice = averageRentalPrice;
    }

    public Integer getMinRentalHistory() {
        return minRentalHistory;
    }

    public void setMinRentalHistory(Integer minRentalHistory) {
        this.minRentalHistory = minRentalHistory;
    }

    public String getCustomRequirement() {
        return customRequirement;
    }

    public void setCustomRequirement(String customRequirement) {
        this.customRequirement = customRequirement;
    }

    public List<Long> getImagesIdList() {
        return imagesIdList;
    }

    public void setImagesIdList(List<Long> imagesIdList) {
        this.imagesIdList = imagesIdList;
    }

    public boolean isPromoted() {
        return isPromoted;
    }

    public void setPromoted(boolean promoted) {
        isPromoted = promoted;
    }

    public String getDescriminator() {
        return descriminator;
    }

    public void setDescriminator(String descriminator) {
        this.descriminator = descriminator;
    }

    public List<Long> getThumbnailIdList() {
        return thumbnailIdList;
    }

    public void setThumbnailIdList(List<Long> thumbnailIdList) {
        this.thumbnailIdList = thumbnailIdList;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
