package com.example.szerowniabackend.Models.DTOs.Requests;

import java.time.LocalDate;
import java.util.List;

public class ClaimInspectionRequestDTO {
    private double damageAssessment;
    private String description;
    private LocalDate inspectionDate;
    private long claimId;
    private List<Long> imagesId;

    public ClaimInspectionRequestDTO() {
    }

    public ClaimInspectionRequestDTO(double damageAssessment, String description, LocalDate inspectionDate, long claimId) {
        this.damageAssessment = damageAssessment;
        this.description = description;
        this.inspectionDate = inspectionDate;
        this.claimId = claimId;
    }

    public double getDamageAssessment() {
        return damageAssessment;
    }

    public void setDamageAssessment(double damageAssessment) {
        this.damageAssessment = damageAssessment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(LocalDate inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public long getClaimId() {
        return claimId;
    }

    public void setClaimId(long claimId) {
        this.claimId = claimId;
    }

    public List<Long> getImagesId() {
        return imagesId;
    }

    public void setImagesNames(List<Long> imagesId) {
        this.imagesId = imagesId;
    }

    @Override
    public String toString() {
        return "ClaimInspectionRequestDTO{" +
                "damageAssessment=" + damageAssessment +
                ", description='" + description + '\'' +
                ", inspectionDate=" + inspectionDate +
                ", claimId=" + claimId +
                '}';
    }
}
