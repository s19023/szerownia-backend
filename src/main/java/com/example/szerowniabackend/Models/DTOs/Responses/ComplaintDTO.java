package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.DTOs.Responses.GetHireMyBorrowsSharers.HireMyBorrowsResponseDTO;
import com.example.szerowniabackend.Models.Entities.Enums.ComplainStatus;
import com.example.szerowniabackend.Models.Entities.Enums.ComplaintReason;

import java.time.LocalDateTime;
import java.util.Set;

public class ComplaintDTO {

    private Long id;

    public Long authorId;

    private ComplaintReason reason;

    private String details;

    private LocalDateTime reportDate;

    private HireMyBorrowsResponseDTO hire;

    private ComplainStatus status;

    public ComplaintDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public ComplaintReason getReason() {
        return reason;
    }

    public void setReason(ComplaintReason reason) {
        this.reason = reason;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public LocalDateTime getReportDate() {
        return reportDate;
    }

    public void setReportDate(LocalDateTime reportDate) {
        this.reportDate = reportDate;
    }

    public HireMyBorrowsResponseDTO getHire() {
        return hire;
    }

    public void setHire(HireMyBorrowsResponseDTO hire) {
        this.hire = hire;
    }

    public ComplainStatus getStatus() {
        return status;
    }

    public void setStatus(ComplainStatus status) {
        this.status = status;
    }
}
