package com.example.szerowniabackend.Models.DTOs.Requests;

import java.util.List;

public class ProductUpdateRequestDTO {

    private String name;

    private String make;

    private String model;

    private Integer condition;

    private Integer estimatedValue;

    private Long idCategory;

    private List<FeatureOccurrenceUpdateRequestDTO> featureOccurrenceUpdateRequestDTO;

    public ProductUpdateRequestDTO() {
    }

    public ProductUpdateRequestDTO(String name, String make, String model, Integer condition, Integer estimatedValue, Long idCategory,
                                   List<FeatureOccurrenceUpdateRequestDTO> featureOccurrenceUpdateRequestDTO) {
        this.name = name;
        this.make = make;
        this.model = model;
        this.condition = condition;
        this.estimatedValue = estimatedValue;
        this.idCategory = idCategory;
        this.featureOccurrenceUpdateRequestDTO = featureOccurrenceUpdateRequestDTO;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getCondition() {
        return condition;
    }

    public void setCondition(Integer condition) {
        this.condition = condition;
    }

    public Integer getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(Integer estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public Long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }

    public List<FeatureOccurrenceUpdateRequestDTO> getFeatureOccurrenceUpdateRequestDTO() {
        return featureOccurrenceUpdateRequestDTO;
    }

    public void setFeatureOccurrenceUpdateRequestDTO(List<FeatureOccurrenceUpdateRequestDTO> featureOccurrenceUpdateRequestDTO) {
        this.featureOccurrenceUpdateRequestDTO = featureOccurrenceUpdateRequestDTO;
    }

    @Override
    public String toString() {
        return "ProductCreateRequestDTO{" +
                "name='" + name + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", condition=" + condition +
                ", estimatedValue=" + estimatedValue +
                '}';
    }
}
