package com.example.szerowniabackend.Models.DTOs.Requests;

import java.util.List;

public class CategoryCreateRequestDTO {

    private String name;
    private Long idParentCategory;
    private List<FeatureForCategoryRequestDto> features;

    public CategoryCreateRequestDTO() {
    }

    public CategoryCreateRequestDTO(String name, Long idParentCategory, List<Long> idSubCategories) {
        this.name = name;
        this.idParentCategory = idParentCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdParentCategory() {
        return idParentCategory;
    }

    public void setIdParentCategory(Long idParentCategory) {
        this.idParentCategory = idParentCategory;
    }

    public List<FeatureForCategoryRequestDto> getFeatures() {
        return features;
    }

    public void setFeatures(List<FeatureForCategoryRequestDto> features) {
        this.features = features;
    }

    @Override
    public String toString() {
        return "CategoryCreateRequestDTO{" +
                "name='" + name + '\'' +
                ", idParentCategory=" + idParentCategory +
                ", features=" + features +
                '}';
    }
}
