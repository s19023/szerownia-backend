package com.example.szerowniabackend.Models.DTOs.Requests;

public class OpinionRequestDTO {

    private Long id;
    private int rating;
    private String comment;
    private boolean isAboutSharer;
    private Long authorId;
    private Long takerId;
    private Long hireId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isAboutSharer() {
        return isAboutSharer;
    }

    public void setAboutSharer(boolean aboutSharer) {
        isAboutSharer = aboutSharer;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long getTakerId() {
        return takerId;
    }

    public void setTakerId(Long takerId) {
        this.takerId = takerId;
    }

    public Long getHireId() { return hireId; }

    public void setHireId(Long hireId) { this.hireId = hireId; }
}
