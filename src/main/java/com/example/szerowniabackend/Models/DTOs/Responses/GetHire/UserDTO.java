package com.example.szerowniabackend.Models.DTOs.Responses.GetHire;

public class UserDTO {

    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String telephoneNumber;
    private Double borrowerRate;
    private Double sharerRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public Double getBorrowerRate() {
        return borrowerRate;
    }

    public void setBorrowerRate(Double borrowerRate) {
        this.borrowerRate = borrowerRate;
    }

    public Double getSharerRate() {
        return sharerRate;
    }

    public void setSharerRate(Double sharerRate) {
        this.sharerRate = sharerRate;
    }
}
