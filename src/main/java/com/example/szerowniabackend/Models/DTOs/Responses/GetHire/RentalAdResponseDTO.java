package com.example.szerowniabackend.Models.DTOs.Responses.GetHire;

import com.example.szerowniabackend.Models.DTOs.Responses.ProductReferenceDTO;
import com.example.szerowniabackend.Models.Entities.Product;

import javax.persistence.Column;
import java.util.Set;

public class RentalAdResponseDTO {

    private Long id;
    private String title;
    private Set<ProductReferenceDTO> products;
    private Double pricePerDay;
    private Double depositAmount;
    private Double penaltyForEachDayOfDelayInReturns;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<ProductReferenceDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductReferenceDTO> products) {
        this.products = products;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Double getPenaltyForEachDayOfDelayInReturns() {
        return penaltyForEachDayOfDelayInReturns;
    }

    public void setPenaltyForEachDayOfDelayInReturns(Double penaltyForEachDayOfDelayInReturns) {
        this.penaltyForEachDayOfDelayInReturns = penaltyForEachDayOfDelayInReturns;
    }
}
