package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.Entities.Enums.ModTicketResult;
import com.example.szerowniabackend.Models.Entities.Enums.ModTicketSubject;

import java.time.LocalDate;

public class ModTicketRequestDTO {

    private Long id;

    private ModTicketSubject subject;

    private String desc;

    private Long notifierId;

    private Long reportedId;

    private Long opinionId;

    private Long adId;

    private boolean isSearchAd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ModTicketSubject getSubject() {
        return subject;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setSubject(ModTicketSubject subject) {
        this.subject = subject;
    }

    public Long getNotifierId() {
        return notifierId;
    }

    public void setNotifierId(Long notifierId) {
        this.notifierId = notifierId;
    }

    public Long getReportedId() {
        return reportedId;
    }

    public void setReportedId(Long reportedId) {
        this.reportedId = reportedId;
    }

    public Long getOpinionId() {
        return opinionId;
    }

    public void setOpinionId(Long opinionId) {
        this.opinionId = opinionId;
    }

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }

    public boolean getIsSearchAd() {
        return isSearchAd;
    }

    public void setIsSearchAd(boolean searchAd) {
        isSearchAd = searchAd;
    }
}
