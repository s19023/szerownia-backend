package com.example.szerowniabackend.Models.DTOs.Responses;

public class ModTicketOpinionDTO {
    private Long id;
    private int rating;
    private String comment;
    private Long authorId;
    private ModTicketUserDTO author;
    private Long takerId;
    private Long hireId;
    private Boolean isAboutSharer;

    public Boolean getAboutSharer() {
        return isAboutSharer;
    }

    public void setAboutSharer(Boolean aboutSharer) {
        isAboutSharer = aboutSharer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long getTakerId() {
        return takerId;
    }

    public void setTakerId(Long takerId) {
        this.takerId = takerId;
    }

    public Long getHireId() { return hireId; }

    public void setHireId(Long hireId) { this.hireId = hireId; }

    public ModTicketUserDTO getAuthor() {
        return author;
    }

    public void setAuthor(ModTicketUserDTO author) {
        this.author = author;
    }
}
