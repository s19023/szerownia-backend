package com.example.szerowniabackend.Models.DTOs.Requests;

public class UsernameAndPasswordAuthRequest {

    private String email;
    private String password;

    public UsernameAndPasswordAuthRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}