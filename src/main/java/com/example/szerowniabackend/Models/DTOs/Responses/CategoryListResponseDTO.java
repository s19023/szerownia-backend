package com.example.szerowniabackend.Models.DTOs.Responses;

import java.util.ArrayList;
import java.util.List;

public class CategoryListResponseDTO {

    private Long idCategory;
    private String name;
    private boolean isAbstract;
    private CategoryReferenceDTO parentCategory;
    private List<CategoryReferenceDTO> subCategories = new ArrayList<>();
    private String iconName;
    private String iconColor;


    public CategoryListResponseDTO() {
    }

    public CategoryListResponseDTO(Long idCategory, String name, boolean isAbstract, CategoryReferenceDTO parentCategory,
                                   List<CategoryReferenceDTO> subCategories) {
        this.idCategory = idCategory;
        this.name = name;
        this.isAbstract = isAbstract;
        this.parentCategory = parentCategory;
        this.subCategories = subCategories;
    }

    public Long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public void setAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public CategoryReferenceDTO getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(CategoryReferenceDTO parentCategory) {
        this.parentCategory = parentCategory;
    }

    public List<CategoryReferenceDTO> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<CategoryReferenceDTO> subCategories) {
        this.subCategories = subCategories;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    @Override
    public String toString() {
        return "CategoryListResponseDTO{" +
                "idCategory=" + idCategory +
                ", name='" + name + '\'' +
                ", isAbstract=" + isAbstract +
                ", parentCategory=" + parentCategory +
                ", subCategories=" + subCategories +
                '}';
    }
}