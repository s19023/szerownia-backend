package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.Entities.Enums.NotificationStatus;

import java.time.LocalDateTime;

public class NotificationDTO {


    private Long id;

    private LocalDateTime createTime;

    private LocalDateTime readTime;

    private String content;

    private NotificationStatus notificationStatus;

    private UserRespDto currentUser;

    private UserRespDto otherUser;

    public NotificationDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getReadTime() {
        return readTime;
    }

    public void setReadTime(LocalDateTime readTime) {
        this.readTime = readTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public UserRespDto getOtherUser() {
        return otherUser;
    }

    public void setOtherUser(UserRespDto otherUser) {
        this.otherUser = otherUser;
    }

    public UserRespDto getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserRespDto currentUser) {
        this.currentUser = currentUser;
    }
}
