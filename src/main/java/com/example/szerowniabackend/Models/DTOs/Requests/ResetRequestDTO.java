package com.example.szerowniabackend.Models.DTOs.Requests;

public class ResetRequestDTO {

    private String token;
    private String newpassword;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }
}
