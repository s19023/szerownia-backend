package com.example.szerowniabackend.Models.DTOs.Requests;

import java.time.LocalDate;

public class CalculateHireCostRequestDTO {
    private Long rentalAdId;
    private LocalDate startDate;
    private LocalDate endDate;

    public long getRentalAdId() {
        return rentalAdId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }
}
