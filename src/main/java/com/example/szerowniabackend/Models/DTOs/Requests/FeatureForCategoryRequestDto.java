package com.example.szerowniabackend.Models.DTOs.Requests;

public class FeatureForCategoryRequestDto {

    private Long featureId;
    private Boolean featureRequired;

    public FeatureForCategoryRequestDto() {
    }

    public FeatureForCategoryRequestDto(Long featureId, boolean isFeatureRequired) {
        this.featureId = featureId;
        this.featureRequired = isFeatureRequired;
    }

    public Long getFeatureId() {
        return featureId;
    }

    public void setFeatureId(Long featureId) {
        this.featureId = featureId;
    }

    public Boolean getFeatureRequired() {
        return featureRequired;
    }

    public void setFeatureRequired(Boolean featureRequired) {
        this.featureRequired = featureRequired;
    }

    @Override
    public String toString() {
        return "FeatureForCategoryRequestDto{" +
                "featureId=" + featureId +
                ", isFeatureRequired=" + featureRequired +
                '}';
    }
}
