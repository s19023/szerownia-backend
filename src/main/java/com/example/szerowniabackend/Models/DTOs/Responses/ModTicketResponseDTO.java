package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.Entities.Enums.ModTicketResult;
import com.example.szerowniabackend.Models.Entities.Enums.ModTicketSubject;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class ModTicketResponseDTO {

    private Long id;

    private LocalDateTime reportedDate;

    private String subject;

    private ModTicketSubject ticketSubject;

    private LocalDate moderationDate;

    private ModTicketResult result;

    private Long reportedId;

    private Long opinionId;

    private Long adId;

    private String discriminator;

    private ModTicketUserDTO notifier;

    private Set<Long> adImagesIdList = new HashSet<>();



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(LocalDateTime reportedDate) {
        this.reportedDate = reportedDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public ModTicketSubject getTicketSubject() {
        return ticketSubject;
    }

    public void setTicketSubject(ModTicketSubject ticketSubject) {
        this.ticketSubject = ticketSubject;
    }

    public LocalDate getModerationDate() {
        return moderationDate;
    }

    public void setModerationDate(LocalDate moderationDate) {
        this.moderationDate = moderationDate;
    }

    public ModTicketResult getResult() {
        return result;
    }

    public void setResult(ModTicketResult result) {
        this.result = result;
    }

    public Long getReportedId() {
        return reportedId;
    }

    public void setReportedId(Long reportedId) {
        this.reportedId = reportedId;
    }

    public Long getOpinionId() {
        return opinionId;
    }

    public void setOpinionId(Long opinionId) {
        this.opinionId = opinionId;
    }

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }

    public String getDiscriminator() {
        return discriminator;
    }

    public void setDiscriminator(String discriminator) {
        this.discriminator = discriminator;
    }

    public ModTicketUserDTO getNotifier() {
        return notifier;
    }

    public void setNotifier(ModTicketUserDTO notifier) {
        this.notifier = notifier;
    }

    public Set<Long> getAdImagesIdList() {
        return adImagesIdList;
    }

    public void setAdImagesIdList(Set<Long> adImagesIdList) {
        this.adImagesIdList = adImagesIdList;
    }
}
