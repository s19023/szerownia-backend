package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.Entities.Enums.ModTicketResult;
import com.example.szerowniabackend.Models.Entities.Enums.ModTicketSubject;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ModTicketResponseWithObjectDTO {

    private Long id;

    private LocalDateTime reportedDate;

    private String subject;

    private String description;

    private ModTicketSubject ticketSubject;

    private LocalDate moderationDate;

    private ModTicketResult result;

    private Long adId;

    private ModTicketUserDTO reported;

    private ModTicketUserDTO notifier;

    private ModTicketOpinionDTO opinion;

    private RentalAdWithDescriptionDTO ad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(LocalDateTime reportedDate) {
        this.reportedDate = reportedDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public ModTicketSubject getTicketSubject() {
        return ticketSubject;
    }

    public void setTicketSubject(ModTicketSubject ticketSubject) {
        this.ticketSubject = ticketSubject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getModerationDate() {
        return moderationDate;
    }

    public void setModerationDate(LocalDate moderationDate) {
        this.moderationDate = moderationDate;
    }

    public ModTicketResult getResult() {
        return result;
    }

    public void setResult(ModTicketResult result) {
        this.result = result;
    }

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }

    public ModTicketUserDTO getReported() {
        return reported;
    }

    public void setReported(ModTicketUserDTO reported) {
        this.reported = reported;
    }

    public ModTicketUserDTO getNotifier() {
        return notifier;
    }

    public void setNotifier(ModTicketUserDTO notifier) {
        this.notifier = notifier;
    }

    public ModTicketOpinionDTO getOpinion() {
        return opinion;
    }

    public void setOpinion(ModTicketOpinionDTO opinion) {
        this.opinion = opinion;
    }

    public RentalAdWithDescriptionDTO getAd() {
        return ad;
    }

    public void setAd(RentalAdWithDescriptionDTO rentalAdByIdResponseDTO) {
        this.ad = rentalAdByIdResponseDTO;
    }
}
