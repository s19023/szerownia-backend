package com.example.szerowniabackend.Models.DTOs.Requests;

import io.swagger.models.auth.In;

public class RentalAdSupport {

    Long id;
    String eq;
    Double gt;
    Double lt;
    Boolean bool;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEq() {
        return eq;
    }

    public void setEq(String eq) {
        this.eq = eq;
    }

    public Double getGt() {
        return gt;
    }

    public void setGt(Double gt) {
        this.gt = gt;
    }

    public Double getLt() {
        return lt;
    }

    public void setLt(Double lt) {
        this.lt = lt;
    }

    public Boolean getBool() {
        return bool;
    }

    public void setBool(Boolean bool) {
        this.bool = bool;
    }

    @Override
    public String toString() {
        return "RentalAdSupport{" +
                "id=" + id +
                ", eq='" + eq + '\'' +
                ", gt=" + gt +
                ", lt=" + lt +
                ", bool=" + bool +
                '}';
    }
}

