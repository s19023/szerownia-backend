package com.example.szerowniabackend.Models.DTOs.Responses;

public class SuggestProductCategoryResponseDTO {
    private String categoryName;
    private long categoryId;
    private long occurrenceNumber;

    public SuggestProductCategoryResponseDTO() {
    }

    public SuggestProductCategoryResponseDTO(String categoryName, long categoryId, long occurrenceNumber) {
        this.categoryName = categoryName;
        this.categoryId = categoryId;
        this.occurrenceNumber = occurrenceNumber;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getOccurrenceNumber() {
        return occurrenceNumber;
    }

    public void setOccurrenceNumber(long occurrenceNumber) {
        this.occurrenceNumber = occurrenceNumber;
    }

    @Override
    public String toString() {
        return "SuggestProductCategoryResponseDTO{" +
                "categoryName='" + categoryName + '\'' +
                ", categoryId=" + categoryId +
                ", occurrenceNumber=" + occurrenceNumber +
                '}';
    }
}
