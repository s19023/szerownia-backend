package com.example.szerowniabackend.Models.DTOs.Responses;

public class RentalAdFindByKeyWordAndCategoryDTO {

    private String keyWords;

    private String category;


    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
