package com.example.szerowniabackend.Models.DTOs.Responses.GetHireMyBorrowsSharers;

import com.example.szerowniabackend.Models.DTOs.Responses.GetHire.UserDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.OpinionResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.UserRespDto;
import com.example.szerowniabackend.Models.Entities.Enums.HireStatus;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;

import java.time.LocalDate;
import java.util.List;

public class HireMyBorrowsResponseDTO {

    private Long id;
    private LocalDate dateFrom;
    private LocalDate dateToPlanned;
    private LocalDate dateToActual;
    private Double cost;
    private String outgoingShipmentNumber;
    private String incomingShipmentNumber;
    private PickupMethod selectedPickupMethod;
    private RentalAdMyBorrowsDTO ad;
    private List<OpinionResponseDTO> opinions;
    private UserRespDto borrower;
    private UserRespDto sharer;
    private HireStatus hireStatus;
    private boolean isDelivered;
    private boolean isReturned;
    private Long policyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateToActual() {
        return dateToActual;
    }

    public void setDateToActual(LocalDate dateToActual) {
        this.dateToActual = dateToActual;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getOutgoingShipmentNumber() {
        return outgoingShipmentNumber;
    }

    public void setOutgoingShipmentNumber(String outgoingShipmentNumber) {
        this.outgoingShipmentNumber = outgoingShipmentNumber;
    }

    public String getIncomingShipmentNumber() {
        return incomingShipmentNumber;
    }

    public void setIncomingShipmentNumber(String incomingShipmentNumber) {
        this.incomingShipmentNumber = incomingShipmentNumber;
    }

    public PickupMethod getSelectedPickupMethod() {
        return selectedPickupMethod;
    }

    public void setSelectedPickupMethod(PickupMethod selectedPickupMethod) {
        this.selectedPickupMethod = selectedPickupMethod;
    }

    public RentalAdMyBorrowsDTO getAd() {
        return ad;
    }

    public void setAd(RentalAdMyBorrowsDTO ad) {
        this.ad = ad;
    }

    public UserRespDto getBorrower() {
        return borrower;
    }

    public void setBorrower(UserRespDto borrower) {
        this.borrower = borrower;
    }

    public UserRespDto getSharer() {
        return sharer;
    }

    public void setSharer(UserRespDto sharer) {
        this.sharer = sharer;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateToPlanned() {
        return dateToPlanned;
    }

    public void setDateToPlanned(LocalDate dateToPlanned) {
        this.dateToPlanned = dateToPlanned;
    }

    public List<OpinionResponseDTO> getOpinions() {
        return opinions;
    }

    public void setOpinions(List<OpinionResponseDTO> opinions) {
        this.opinions = opinions;
    }

    public HireStatus getHireStatus() {
        return hireStatus;
    }

    public void setHireStatus(HireStatus hireStatus) {
        this.hireStatus = hireStatus;
    }

    public boolean isDelivered() {
        return isDelivered;
    }

    public void setDelivered(boolean delivered) {
        isDelivered = delivered;
    }

    public boolean isReturned() {
        return isReturned;
    }

    public void setReturned(boolean returned) {
        isReturned = returned;
    }

    public Long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }
}
