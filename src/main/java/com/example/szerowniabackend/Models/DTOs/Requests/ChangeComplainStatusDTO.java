package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.Entities.Enums.ComplainStatus;

public class ChangeComplainStatusDTO {

    private Long complaintId;
    private ComplainStatus status;


    public Long getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(Long complaintId) {
        this.complaintId = complaintId;
    }

    public ComplainStatus getStatus() {
        return status;
    }

    public void setStatus(ComplainStatus status) {
        this.status = status;
    }
}
