package com.example.szerowniabackend.Models.DTOs.Requests;

import java.time.LocalDateTime;

public class MessageExistingThreadDTO {

    private String content;
    //private List<Files> attachments;
    private Long thread;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getThread() {
        return thread;
    }

    public void setThread(Long thread) {
        this.thread = thread;
    }
}
