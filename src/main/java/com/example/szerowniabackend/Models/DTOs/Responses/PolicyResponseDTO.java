package com.example.szerowniabackend.Models.DTOs.Responses;

public class PolicyResponseDTO {
    private Long id;
    private Double insuranceTotal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getInsuranceTotal() {
        return insuranceTotal;
    }

    public void setInsuranceTotal(Double insuranceTotal) {
        this.insuranceTotal = insuranceTotal;
    }
}
