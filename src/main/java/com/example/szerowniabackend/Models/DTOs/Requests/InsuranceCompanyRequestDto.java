package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.LocationDTO;

public class InsuranceCompanyRequestDto {

    private String name;
    private String nip;
    private String email;
    private Double commissionRate;
    private LocationDTO location;

    public InsuranceCompanyRequestDto() {
    }

    public InsuranceCompanyRequestDto(String name, String nip, String email, Double commissionRate, LocationDTO location) {
        this.name = name;
        this.nip = nip;
        this.email = email;
        this.commissionRate = commissionRate;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Double commissionRate) {
        this.commissionRate = commissionRate;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }
}

