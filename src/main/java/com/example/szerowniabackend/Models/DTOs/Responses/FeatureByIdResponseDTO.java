package com.example.szerowniabackend.Models.DTOs.Responses;

import com.example.szerowniabackend.Models.Entities.Enums.FeatureType;

public class FeatureByIdResponseDTO {

    private String name;
    private FeatureType type;
    private boolean required;

    public FeatureByIdResponseDTO() {
    }

    public FeatureByIdResponseDTO(String name, FeatureType type, boolean required) {
        this.name = name;
        this.type = type;
        this.required = required;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FeatureType getType() {
        return type;
    }

    public void setType(FeatureType type) {
        this.type = type;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public String toString() {
        return "FeatureByIdResponseDTO{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", required=" + required +
                '}';
    }
}
