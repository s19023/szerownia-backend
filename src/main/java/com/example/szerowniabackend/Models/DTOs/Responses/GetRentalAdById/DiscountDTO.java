package com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById;

public class DiscountDTO {
    private Long id;
    private Integer value;
    private Integer minNumDays;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getMinNumDays() {
        return minNumDays;
    }

    public void setMinNumDays(Integer minNumDays) {
        this.minNumDays = minNumDays;
    }
}
