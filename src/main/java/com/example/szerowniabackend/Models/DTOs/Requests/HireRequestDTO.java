package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Models.Entities.RentalAd;
import com.example.szerowniabackend.Models.Entities.User;

import java.time.LocalDate;

public class HireRequestDTO {

    private LocalDate dateFrom;
    private LocalDate dateToPlanned;
    private PickupMethod selectedPickupMethod;
    private Long adId;

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateToPlanned() {
        return dateToPlanned;
    }

    public void setDateToPlanned(LocalDate dateToPlanned) {
        this.dateToPlanned = dateToPlanned;
    }

    public PickupMethod getSelectedPickupMethod() {
        return selectedPickupMethod;
    }

    public void setSelectedPickupMethod(PickupMethod selectedPickupMethod) {
        this.selectedPickupMethod = selectedPickupMethod;
    }

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }
}







