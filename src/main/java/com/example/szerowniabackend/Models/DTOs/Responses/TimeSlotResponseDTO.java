package com.example.szerowniabackend.Models.DTOs.Responses;

import java.time.LocalDate;

public class TimeSlotResponseDTO {

    public LocalDate from;
    public LocalDate to;

    public TimeSlotResponseDTO(LocalDate from, LocalDate to) {
        this.from = from;
        this.to = to;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getTo() {
        return to;
    }

    public void setTo(LocalDate to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "TimeSlotResponseDTO{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}
