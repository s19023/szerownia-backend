package com.example.szerowniabackend.Models.DTOs.Requests;

import java.util.List;

public class RentalADSearchDTO {

    String search;
    Long category;
    List<RentalAdSupport> filters;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    public List<RentalAdSupport> getFilters() {
        return filters;
    }

    public void setFilters(List<RentalAdSupport> filters) {
        this.filters = filters;
    }
}





