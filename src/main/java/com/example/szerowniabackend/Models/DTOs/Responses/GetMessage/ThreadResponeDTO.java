package com.example.szerowniabackend.Models.DTOs.Responses.GetMessage;


public class ThreadResponeDTO {
    private Long id;
    private String topic;
    private ThreadUserDTO sender;
    private ThreadUserDTO receiver;
    private RentalAdResponseDTO rentalAd;
    private MessageResponseDTO lastMessage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public ThreadUserDTO getSender() {
        return sender;
    }

    public void setSender(ThreadUserDTO sender) {
        this.sender = sender;
    }

    public ThreadUserDTO getReceiver() {
        return receiver;
    }

    public void setReceiver(ThreadUserDTO receiver) {
        this.receiver = receiver;
    }

    public RentalAdResponseDTO getRentalAd() {
        return rentalAd;
    }

    public void setRentalAd(RentalAdResponseDTO rentalAd) {
        this.rentalAd = rentalAd;
    }

    public MessageResponseDTO getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(MessageResponseDTO lastMessage) {
        this.lastMessage = lastMessage;
    }
}
