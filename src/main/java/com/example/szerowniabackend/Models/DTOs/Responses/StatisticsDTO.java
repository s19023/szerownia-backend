package com.example.szerowniabackend.Models.DTOs.Responses;

import java.time.LocalDate;

public class StatisticsDTO {

    private LocalDate[] dates;
    private Long[] counts;

    public LocalDate[] getDates() {
        return dates;
    }

    public void setDates(LocalDate[] dates) {
        this.dates = dates;
    }

    public Long[] getCounts() {
        return counts;
    }

    public void setCounts(Long[] counts) {
        this.counts = counts;
    }
}
