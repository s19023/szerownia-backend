package com.example.szerowniabackend.Models.DTOs.Requests;

import java.util.List;

public class ImageRequestDTO {

    private List<String> imagesNamesList;

    public List<String> getImagesNamesList() {
        return imagesNamesList;
    }

    public void setImagesNamesList(List<String> imagesNamesList) {
        this.imagesNamesList = imagesNamesList;
    }
}
