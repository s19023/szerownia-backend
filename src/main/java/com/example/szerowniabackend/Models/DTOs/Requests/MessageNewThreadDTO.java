package com.example.szerowniabackend.Models.DTOs.Requests;

public class MessageNewThreadDTO {
    private String content;
    //private List<Files> attachments;
    private Long adId;
    private Long modTicketId;

    public MessageNewThreadDTO() {
    }

    public MessageNewThreadDTO(String content, Long adId, Long modTicketId) {
        this.content = content;
        this.adId = adId;
        this.modTicketId = modTicketId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }

    public Long getModTicketId() {
        return modTicketId;
    }

    public void setModTicketId(Long modTicketId) {
        this.modTicketId = modTicketId;
    }
}
