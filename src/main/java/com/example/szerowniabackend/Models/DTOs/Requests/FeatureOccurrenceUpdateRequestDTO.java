package com.example.szerowniabackend.Models.DTOs.Requests;

public class FeatureOccurrenceUpdateRequestDTO {


    private Long id;
    private String textValue;
    private Integer decimalNumberValue;
    private Double floatingNumberValue;
    private Boolean booleanValue;

    public FeatureOccurrenceUpdateRequestDTO() {
    }

    public FeatureOccurrenceUpdateRequestDTO(Long id, String textValue, Integer decimalNumberValue, Double floatingNumberValue, Boolean booleanValue) {
        this.id = id;
        this.textValue = textValue;
        this.decimalNumberValue = decimalNumberValue;
        this.floatingNumberValue = floatingNumberValue;
        this.booleanValue = booleanValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public Integer getDecimalNumberValue() {
        return decimalNumberValue;
    }

    public void setDecimalNumberValue(Integer decimalNumberValue) {
        this.decimalNumberValue = decimalNumberValue;
    }

    public Double getFloatingNumberValue() {
        return floatingNumberValue;
    }

    public void setFloatingNumberValue(Double floatingNumberValue) {
        this.floatingNumberValue = floatingNumberValue;
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(Boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    @Override
    public String toString() {
        return "FeatureOccurrenceUpdateRequestDTO{" +
                "id=" + id +
                ", textValue='" + textValue + '\'' +
                ", decimalNumberValue=" + decimalNumberValue +
                ", floatingNumberValue=" + floatingNumberValue +
                ", booleanValue=" + booleanValue +
                '}';
    }

}
