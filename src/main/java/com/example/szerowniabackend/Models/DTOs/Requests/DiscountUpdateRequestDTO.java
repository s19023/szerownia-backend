package com.example.szerowniabackend.Models.DTOs.Requests;

public class DiscountUpdateRequestDTO {
    private Integer value;
    private Integer minNumDays;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getMinNumDays() {
        return minNumDays;
    }

    public void setMinNumDays(Integer minNumDays) {
        this.minNumDays = minNumDays;
    }

}
