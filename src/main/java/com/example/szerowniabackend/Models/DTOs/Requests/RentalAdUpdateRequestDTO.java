package com.example.szerowniabackend.Models.DTOs.Requests;

import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Models.Entities.Location;
import com.example.szerowniabackend.Models.Entities.TimeSlot;

import java.time.LocalDate;
import java.util.List;

public class RentalAdUpdateRequestDTO {

    private String title;

    private List<TimeSlot> timeSlotsList;

    private Location location; //TODO nie da się zupdatetować RentalAd

    private List<PickupMethod> pickupMethodList;

    private String description;

    private Double pricePerDay;

    private Double depositAmount;

    private Double penaltyForEachDayOfDelayInReturns;

    private Double averageOpinion;

    private Double averageRentalPrice;

    private Integer minRentalHistory;

    private String customRequirement;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TimeSlot> getTimeSlotsList() {
        return timeSlotsList;
    }

    public void setTimeSlotsList(List<TimeSlot> timeSlotsList) {
        this.timeSlotsList = timeSlotsList;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<PickupMethod> getPickupMethodList() {
        return pickupMethodList;
    }

    public void setPickupMethodList(List<PickupMethod> pickupMethodList) {
        this.pickupMethodList = pickupMethodList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Double getPenaltyForEachDayOfDelayInReturns() {
        return penaltyForEachDayOfDelayInReturns;
    }

    public void setPenaltyForEachDayOfDelayInReturns(Double penaltyForEachDayOfDelayInReturns) {
        this.penaltyForEachDayOfDelayInReturns = penaltyForEachDayOfDelayInReturns;
    }

    public Double getAverageOpinion() {
        return averageOpinion;
    }

    public void setAverageOpinion(Double averageOpinion) {
        this.averageOpinion = averageOpinion;
    }

    public Double getAverageRentalPrice() {
        return averageRentalPrice;
    }

    public void setAverageRentalPrice(Double averageRentalPrice) {
        this.averageRentalPrice = averageRentalPrice;
    }

    public Integer getMinRentalHistory() {
        return minRentalHistory;
    }

    public void setMinRentalHistory(Integer minRentalHistory) {
        this.minRentalHistory = minRentalHistory;
    }

    public String getCustomRequirement() {
        return customRequirement;
    }

    public void setCustomRequirement(String customRequirement) {
        this.customRequirement = customRequirement;
    }
}
