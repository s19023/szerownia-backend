package com.example.szerowniabackend.Models.DTOs.Requests;

public class ParameterReferenceDTO {

    Long idFeature;
    String equal;
    Double greaterThan;
    Double lessThan;
    Boolean bool;

    public Long getIdFeature() {
        return idFeature;
    }

    public void setIdFeature(Long idFeature) {
        this.idFeature = idFeature;
    }

    public String getEqual() {
        return equal;
    }

    public void setEqual(String equal) {
        this.equal = equal;
    }

    public Double getGreaterThan() {
        return greaterThan;
    }

    public void setGreaterThan(Double greaterThan) {
        this.greaterThan = greaterThan;
    }

    public Double getLessThan() {
        return lessThan;
    }

    public void setLessThan(Double lessThan) {
        this.lessThan = lessThan;
    }

    public Boolean getBool() {
        return bool;
    }

    public void setBool(Boolean bool) {
        this.bool = bool;
    }
}
