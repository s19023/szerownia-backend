package com.example.szerowniabackend.Models;


import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.Entities.Thread;
import com.example.szerowniabackend.Models.Entities.Enums.*;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Services.ImagesService;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Transactional
@Component
public class DbSeeder implements CommandLineRunner {


    @Autowired
    private ProductRepo productRepo;
    @Autowired
    private RentalAdRepo rentalAdRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private CategoryRepo categoryRepo;
    @Autowired
    private HireRepo hireRepo;
    @Autowired
    private FeatureRepo featureRepo;
    @Autowired
    private FeatureOccurrenceRepo featureOccurrenceRepo;
    @Autowired
    private OpinionRepo opinionRepo;
    @Autowired
    private DiscountRepo discountRepo;
    @Autowired
    private MessageRepo messageRepo;
    @Autowired
    private ThreadRepo threadRepo;
    @Autowired
    private ModTicketRepo modTicketRepo;
    @Autowired
    private FeatureCategoryRepo featureCategoryRepo;
    @Autowired
    private SearchAdRepo searchAdRepo;
    @Autowired
    private VulgarWordsRepo vulgarWordsRepo;
    @Autowired
    private StatisticsRepo statisticsRepo;
    @Autowired
    private BlockedIpRepo blockedIpRepo;
    @Autowired
    private ClaimRepo claimRepo;
    @Autowired
    private PolicyRepo policyRepo;
    @Autowired
    private FileSystemRepo fileSystemRepo;
    @Autowired
    private ImageRepo imageRepo;
    @Autowired
    private ImagesService imagesService;
    @Autowired
    private InsuranceCompanyRepo insuranceCompanyRepo;
    @Autowired
    private RegionRepo regionRepo;
    @Autowired
    private DistrictRepo districtRepo;
    @Autowired
    private ImageThumbnailRepo imageThumbnailRepo;
    @Autowired
    private ClaimInspectionRepo claimInspectionRepo;
    @Autowired
    private ComplaintRepo complaintRepo;


    private List<String> lines = new ArrayList<>();

    private List<User> userList = new ArrayList<>();
    private List<Product> prodList = new ArrayList<>();
    private List<RentalAd> rentalAds = new ArrayList<>();
    private Map<String, Feature> features = new HashMap<>();
    private Map<String, Category> categories = new HashMap<>();
    private List<Discount> discounts = new ArrayList<>();
    private List<Message> messages = new ArrayList<>();
    private List<Hire> hireList = new ArrayList<>();
    private List<Thread> threads = new ArrayList<>();
    private List<SearchAd> searchAds = new ArrayList<>();
    private List<VulgarWords> vulgarWordsList = new ArrayList<>();
    private List<BlockedIp> blockedIpList = new ArrayList<>();
    private List<Claim> claims = new ArrayList<>();
    private List<Policy> policies = new ArrayList<>();
    private List<InsuranceCompany> insuranceCompanies = new ArrayList<>();
    private List<User> insuranceAdjusters = new ArrayList<>();
    private Map<String, Region> regions = new HashMap<>();
    private Logger logger = LoggerFactory.getLogger(DbSeeder.class);


    @Override
    public void run(String... args) throws Exception {

        logger.info("---- SEED DATA STARTED ---");
        InputStream productsFile = new ClassPathResource("static/Przedmioty.txt").getInputStream();
        InputStream superUsersFile = new ClassPathResource("static/SuperUzytkownik.txt").getInputStream();
        InputStream usersFile = new ClassPathResource("static/UżytkownikWithPassword.txt").getInputStream();
        InputStream adsFile = new ClassPathResource("static/Ogloszenia.txt").getInputStream();
        InputStream categoriesFile = new ClassPathResource("static/Kategorie.txt").getInputStream();
        InputStream hireFile = new ClassPathResource("static/Wypozyczenia.txt").getInputStream();
        InputStream featuresFile = new ClassPathResource("static/Cechy.txt").getInputStream();
        InputStream discountFile = new ClassPathResource("static/Rabat.txt").getInputStream();
        InputStream opinionsFile = new ClassPathResource("static/Opinie.txt").getInputStream();
        InputStream messageFile = new ClassPathResource("static/Wiadomosci.txt").getInputStream();
        InputStream threadFile = new ClassPathResource("static/Watki.txt").getInputStream();
        InputStream searchAdFile = new ClassPathResource("static/OgloszeniaPoszukiwania.txt").getInputStream();
        InputStream vulgarWordsFile = new ClassPathResource("static/profanityListForDbSeeder.txt").getInputStream();
        InputStream iconsFile = new ClassPathResource("static/Ikonki.txt").getInputStream();
        InputStream blockIpFile = new ClassPathResource("static/blockedIpForDbSeeder.txt").getInputStream();
        InputStream claimFile = new ClassPathResource("static/Szkoda.txt").getInputStream();
        InputStream policyFile = new ClassPathResource("static/PolisaUbezpieczeniowa.txt").getInputStream();
        InputStream insuranceCompanyFile = new ClassPathResource("static/TowarzystwoUbezpieczeniowe.txt").getInputStream();
        InputStream regionsFile = new ClassPathResource("static/Wojewodztwa.txt").getInputStream();
        InputStream districtsFile = new ClassPathResource("static/Powiaty.txt").getInputStream();
        InputStream inspectionsFile = new ClassPathResource("static/Ogledziny.txt").getInputStream();
        InputStream complaintFile = new ClassPathResource("static/Reklamacje.txt").getInputStream();


        seedCategories(categoriesFile, iconsFile);

        seedFeatures(featuresFile);

        seedUsers(superUsersFile, usersFile);

        seedProducts(productsFile);

        seedAds(adsFile);

        seedHire(hireFile);

        seedDiscount(discountFile);

        seedOpinions(opinionsFile);

        seedThread(threadFile);

        seedMessage(messageFile);

        seedModTickets();

        seedSearchAds(searchAdFile);

        seedVulgarWords(vulgarWordsFile);

        seedBlockedIp(blockIpFile);

        seedPolicies(policyFile);

        seedClaims(claimFile);

        deleteOldImageForDbSeeder();

        addMoreImageToRentalAds();

        seedImagesToUserProfileImage();

        seedInsuranceCompanies(insuranceCompanyFile);

        seedRegions(regionsFile);

        seedDistricts(districtsFile);

        seedInspections(inspectionsFile);

        relateClaimWithInsuranceAdjuster();

        seedComplaints(complaintFile);

        logger.info("---- SEED DATA FINISHED ---");
    }


    private void relateClaimWithInsuranceAdjuster() {
        //Claim - InsuranceAdjusters
        for (Claim claim : claims) {
            List<User> insuranceAdjustersFromAssignedCompany = claim.getPolicy().getInsuranceCompany().getInsuranceAdjusters();
            claim.setInsuranceAdjuster(insuranceAdjustersFromAssignedCompany.get((int) (Math.random() * insuranceAdjustersFromAssignedCompany.size())));
        }
    }

    private void seedDistricts(InputStream districtsFile) {
        lines = getlines(districtsFile);

        for (String s : lines) {
            String[] row = s.split(";");

            districtRepo.save(new District(row[1], row[2], Double.valueOf(row[3]), Double.valueOf(row[4]), regions.get(row[0])));
        }
    }

    private void seedRegions(InputStream regionsFile) {
        lines = getlines(regionsFile);
        for (String s : lines) {
            String[] row = s.split(";");
            Region region = new Region(row[0], row[1]);

            regionRepo.save(region);
            regions.put(region.getId(), region);
        }
    }

    private void seedModTickets() {

        for (int i = 0; i < 5; i++) {
            modTicketRepo.save(new ModTicket(ModTicketSubject.INDECENT, "Wypozyczone przedmioty nie kompletne", userList.get(i), rentalAds.get(i)));
            modTicketRepo.save(new ModTicket(ModTicketSubject.ILLEGAL, "Wulgarne slownictwo", userList.get(i + 1), opinionRepo.findById(4L).get()));
            modTicketRepo.save(new ModTicket(ModTicketSubject.FRAUD, "Oszkukal mnie", userList.get(i + 2), userList.get(i)));
        }


    }

    private void seedOpinions(InputStream opinionsFile) {

        int i=0;
        lines = getlines(opinionsFile);

        for (Hire hire : hireList) {
            if (!hire.getHireStatus().equals(HireStatus.RETURNED))
                continue;

            String line;
            i++;

            if( i==4)
                line = lines.get(0);
            else
                line = lines.get((int) (Math.random() * lines.size()));

            String[] attributes = line.split(";");

            Opinion opinion = new Opinion(
                    Integer.valueOf(attributes[0]),
                    attributes[1],
                    Boolean.parseBoolean(attributes[2]),
                    hire.getSharer(),
                    hire.getBorrower(),
                    hire);

            if (opinion.isAboutSharer()) {
                opinion.setAuthor(hire.getBorrower());
                opinion.setTaker(hire.getSharer());
            } else {
                opinion.setAuthor(hire.getSharer());
                opinion.setTaker(hire.getBorrower());
            }

            opinionRepo.save(opinion);
        }
    }

    private void seedDiscount(InputStream discountFile) {
        lines = getlines(discountFile);
        for (
                String s : lines) {
            String[] row = s.split(";");
            Discount discount = new Discount(Integer.parseInt(row[0]), Integer.parseInt(row[1]));
            discounts.add(discount);
            discountRepo.save(discount);
        }

        // discount - rentalAd
        for (
                int i = 0; i < discounts.size(); i++) {
            Optional<Discount> discount = discountRepo.findById((long) i);
            Long random = (long) (Math.random() * rentalAds.size()) + 1;
            Optional<RentalAd> rentalAd = rentalAdRepo.findById(random);

            if (rentalAd.isPresent() && discount.isPresent()) {
                discount.get().setRentalAd(rentalAd.get());
            }
        }
    }

    private void seedHire(InputStream hireFile) {
        lines = getlines(hireFile);
        int counter = 0;
        for (String s : lines) {
            String[] row = s.split(";");

            PickupMethod pickupMethod;
            switch (row[3]) {
                case "kurier":
                    pickupMethod = PickupMethod.COURIER;
                    break;
                case "odbiór osobisty":
                    pickupMethod = PickupMethod.PERSONAL_PICKUP;
                    break;
                case "paczkomat":
                    pickupMethod = PickupMethod.PARCEL_LOCKER;
                    break;
                default:
                    throw new NoSuchElementException("This pickup method is not correct");


            }

            LocalDate dateFrom = LocalDate.parse(row[0]);
            LocalDate dateToPlanned = LocalDate.parse(row[1]);

            if (!dateFrom.isBefore(dateToPlanned)) {
                continue;
            }

            Hire hire = new Hire(LocalDate.parse(row[0]), LocalDate.parse(row[1]), Double.valueOf(row[2]), pickupMethod, Double.valueOf(row[4]));

            if (counter % 5 == 0) {
                hire.setHireStatus(HireStatus.RETURNED);
            }

            List<Integer> shippedHires = List.of(3, 8, 13, 18, 23, 28, 33, 38, 43, 48);
            if (shippedHires.contains(counter)) {
                hire.setHireStatus(HireStatus.SHIPPED);
            }

            List<Integer> shippedBackHires = List.of(4, 9, 14, 19, 24, 29, 34, 39, 44, 49);
            if (shippedBackHires.contains(counter)) {
                hire.setHireStatus(HireStatus.SHIPPED_BACK);
            }

            hire.setAd(rentalAdRepo.findById(Long.parseLong(row[5])).get());
            hireList.add(hire);
            hireRepo.save(hire);

            recordStatsEvent(EventType.RENT, hire.getId());
            counter++;
        }

        // hire-user xor relation
        for (
                int i = 0; i < hireList.size(); i++) {
            Optional<Hire> hire = hireRepo.findById((long) i);

            if (hire.isPresent()) {
                hire.get().setSharer(hire.get().getAd().getUser());
            }
        }

        for (
                int i = 0; i < hireList.size(); i++) {
            Optional<Hire> hire = hireRepo.findById((long) i);
            Long random = (long) (Math.random() * userList.size()) + 1;
            Optional<User> user = userRepo.findById(random);

            if (hire.isPresent()) {
                while (user.get().getId().equals(hire.get().getSharer().getId())) {
                    random = (long) (Math.random() * userList.size()) + 1;
                    user = userRepo.findById(random);
                }
                hire.get().setBorrower(user.get());
            }
        }
    }

    private void seedAds(InputStream adsFile) {
        lines = getlines(adsFile);

        int index = 0;
        long tmpAd = 0;
        int tmpToTimeSlot = 0;
        for (String s : lines) {
            String[] row = s.split(";");

            List<PickupMethod> pickupMethodList = new ArrayList<>();
            String[] countPickupMethod = row[6].split(",");
            for (String s1 : countPickupMethod) {
                if ((s1.replace(" ", "")).equals(("odbiór osobisty").replace(" ", ""))) {
                    pickupMethodList.add(PickupMethod.PERSONAL_PICKUP);
                }
                if ((s1.replace(" ", "")).equals("paczkomat")) {
                    pickupMethodList.add(PickupMethod.PARCEL_LOCKER);

                }
                if ((s1.replace(" ", "")).equals("kurier")) {
                    pickupMethodList.add(PickupMethod.COURIER);
                }
            }

            List<TimeSlot> timeSlotList = new LinkedList<>();

            if (tmpAd % 2 == 0) {
                tmpToTimeSlot = (int) (tmpAd * 2);
            }

            LocalDate endDate = LocalDate.now().plusDays(24 + tmpToTimeSlot);
            timeSlotList.add(new TimeSlot(LocalDate.parse(row[2]), endDate));

            RentalAd rentalAd = new RentalAd(
                    userList.get(index++),
                    row[0],
                    timeSlotList,
                    new Location(Double.valueOf(row[3]), Double.valueOf(row[4]), row[5]),
                    pickupMethodList,
                    row[7],
                    Double.valueOf(row[8]),
                    Double.valueOf(row[9]),
                    Double.valueOf(row[10]),
                    null, null, null,null,
                    endDate);

            if (tmpAd % 3 == 0) {
                rentalAd.setPromoted(true);
            }

            rentalAds.add(rentalAd);
            rentalAdRepo.save(rentalAd);
            recordStatsEvent(EventType.AD_CREATION, rentalAd.getCreationDate(), rentalAd.getId());

            tmpAd++;
        }

        Long tmp = 0L;
        for (int i = 0; i < rentalAds.size(); i++) {
            tmp++;
            Optional<Product> product = productRepo.findById(tmp);
            Optional<RentalAd> rentalAd = rentalAdRepo.findById(tmp);
            if (rentalAd.isEmpty() || product.isEmpty())
                throw new IllegalStateException("Enroll product to rentalAd fail - dbSeeder");

            Product productNew = product.get();
            RentalAd rentalAdNew = rentalAd.get();

            productNew.getIncludeAds().add(rentalAdNew);
            rentalAdNew.getProducts().add(productNew);

            productRepo.save(productNew);
            rentalAdRepo.save(rentalAdNew);
        }
    }

    private void seedUsers(InputStream superUsersFile, InputStream usersFile) {
        lines = getlines(usersFile);
        for (String s : lines) {
            String[] row = s.split(";");
            User user = new User(row[0], row[1], row[2], row[4], passwordEncoder.encode(row[7]), row[8], Role.USER);
            userList.add(user);
            userRepo.save(user);

            recordStatsEvent(EventType.ACCOUNT_CREATION, user.getId());
        }

        lines = getlines(superUsersFile);
        for (String s : lines) {
            String[] row = s.split(";");
            User user = new User(row[0], row[1], row[2], row[4], passwordEncoder.encode(row[5]), "Salt", Role.valueOf(row[6]));
            if (user.getRole().equals(Role.INSURANCE_ADJUSTER)) {
                insuranceAdjusters.add(user);
            }
            userRepo.save(user);
        }
    }

    private void recordStatsEvent(EventType eventType, long relatedItemId) {
        LocalDate occurredOn = LocalDate.now().minusDays((long) (Math.random() * 30));
        StatsEvent statsEvent = new StatsEvent(eventType, occurredOn, relatedItemId);
        statisticsRepo.save(statsEvent);
    }

    private void recordStatsEvent(EventType eventType, LocalDate occurredOn, long relatedItemId) {
        StatsEvent statsEvent = new StatsEvent(eventType, occurredOn, relatedItemId);
        statisticsRepo.save(statsEvent);
    }

    private void seedFeatures(InputStream featuresFile) {
        lines = getlines(featuresFile);
        for (String row : lines) {

            String[] attributes = row.split(";");
            //Feature feature = new Feature(attributes[0], FeatureType.valueOf(attributes[1]), Boolean.parseBoolean(attributes[2]));
            Feature feature = new Feature(attributes[0], FeatureType.valueOf(attributes[1]));
            featureRepo.save(feature);
            features.put(feature.getName(), feature);


            for (int i = 3; i < attributes.length; i++) {
                Category category = categories.get(attributes[i]);
                FeatureCategory featureCategory = new FeatureCategory(Boolean.parseBoolean(attributes[2]));
                featureCategory.setFeature(feature);
                featureCategory.setCategory(category);
                featureCategoryRepo.save(featureCategory);
            }


        }
    }

    private void seedThread(InputStream threadFile) {
        lines = getlines(threadFile);
        for (String topic : lines) {
            Thread thread = new Thread(topic);
            threads.add(thread);
            threadRepo.save(thread);
        }

        for (int i = 0; i < threads.size(); i++) {
            threads.get(i).setAd(rentalAds.get((int) (Math.random() * rentalAds.size())));
        }
    }

    private void seedMessage(InputStream messageFile) {
        lines = getlines(messageFile);
        for (String s : lines) {
            String[] row = s.split(";");
            Message message = new Message(row[0], LocalDateTime.parse(row[1]));
            message.setReadDate(LocalDateTime.parse(row[2]));
            messages.add(message);
            messageRepo.save(message);
        }

        // message - thread relation
        int y = 0;
        for (int i = 0; i < messages.size(); i++) {
            if (i % 2 == 0 && i != 0) {
                y++;
            }
            messages.get(i).setThread(threads.get(y));
            threads.get(y).getMessages().add(messages.get(i));
        }


        for (Thread thread : threads) {
            int z = (int) (Math.random() * userList.size() - 1) + 1;
            User user1 = userList.get(z - 1);
            User user2 = thread.getAd().getUser();

            for (int i = 0; i < thread.getMessages().size(); i++) {
                Message message = thread.getMessages().get(i);
                if (i % 2 == 0) {
                    message.setSender(user1);
                    message.setReceiver(user2);
                } else {
                    message.setSender(user2);
                    message.setReceiver(user1);
                }
            }
        }

    }

    private void seedCategories(InputStream categoriesFile, InputStream iconsFile) {
        lines = getlines(categoriesFile);
        List<String> iconsLines = getlines(iconsFile);

        for (String row : lines) {
            String[] attributes = row.split(";");
            Category parentCategory = new Category(attributes[0], true, null);
            categoryRepo.save(parentCategory);
            categories.put(parentCategory.getName(), parentCategory);

            for (int i = 1; i < attributes.length; i++) {
                Category subCategory = new Category(attributes[i], false, parentCategory);
                categoryRepo.save(subCategory);
                categories.put(subCategory.getName(), subCategory);
            }
        }

        for (String row : iconsLines) {
            String[] data = row.split(";");
            Optional<Category> mainCategory = categoryRepo.findMainCategoryByName(data[0]);

            if (mainCategory.isEmpty()) {
                throw new RuntimeException(String.format("Main category with name %s does not exist in db", data[0]));
            }

            mainCategory.get().setIconColor(data[1]);
            mainCategory.get().setIconName(data[2]);

        }
    }

    private void seedProducts(InputStream productsFile) {
        lines = getlines(productsFile);
        int index = 0;
        for (String s : lines) {

            String[] row = s.split(";");
            Product product = new Product(row[0], row[1], row[2], Integer.parseInt(row[3]), Integer.parseInt(row[4]));
            product.setCategory(categories.get(row[5]));
            prodList.add(product);
            for (int i = 6; i < row.length; i += 2) {

                Feature feature = features.get(row[i]);

                FeatureOccurrence featureOccurrence = null;

                switch (feature.getType()) {
                    case TEXT:
                        featureOccurrence = new FeatureOccurrence(row[i + 1], feature, product);
                        break;
                    case DOUBLE:
                        featureOccurrence = new FeatureOccurrence(Double.parseDouble(row[i + 1]), feature, product);
                        break;
                    case INTEGER:
                        featureOccurrence = new FeatureOccurrence(Integer.parseInt(row[i + 1]), feature, product);
                        break;
                    case BOOLEAN:
                        featureOccurrence = new FeatureOccurrence(Boolean.parseBoolean(row[i + 1]), feature, product);
                        break;
                }

                featureOccurrenceRepo.save(featureOccurrence);
                product.getFeatureOccurrences().add(featureOccurrence);

            }
            product.setUser(userList.get(index++));
            productRepo.save(product);
        }
    }

    private void seedSearchAds(InputStream searchAdFile) {
        lines = getlines(searchAdFile);
        long tmpAd = 0;
        int tmpToTimeSlot = 0;
        for (String s : lines) {
            String[] row = s.split(";");

            String[] pickupmethods = row[4].split(",");
            List<PickupMethod> pmList = new ArrayList<>();
            for (String str : pickupmethods) {
                if (str.contains("odbiór")) {
                    pmList.add(PickupMethod.PERSONAL_PICKUP);
                } else if (str.contains("paczkomat")) {
                    pmList.add(PickupMethod.PARCEL_LOCKER);
                } else if (str.contains("kurier")) {
                    pmList.add(PickupMethod.COURIER);
                }
            }

            if (tmpAd % 2 == 0) {
                tmpToTimeSlot = (int) (tmpAd * 2);
            }
            LocalDate endDate = LocalDate.now().plusDays(8 + tmpToTimeSlot);

            List<TimeSlot> timeSlotList = new LinkedList<>();
            timeSlotList.add(new TimeSlot(LocalDate.parse(row[2]), endDate));

            SearchAd searchAd = new SearchAd(row[0], timeSlotList, new Location(Double.valueOf(row[7]), Double.valueOf(row[8]), row[6]), pmList, row[5]);
            searchAd.setUser(userList.get((int) (Math.random() * userList.size())));
            searchAds.add(searchAd);
            searchAdRepo.save(searchAd);
            tmpAd++;
        }

        long tmpSearch = 14L;
        for (int i = 0; i < searchAds.size(); i++) {
            Optional<Product> product = productRepo.findById(tmpSearch);
            Optional<SearchAd> searchAd = searchAdRepo.findById(tmpSearch);
            if (searchAd.isEmpty() || product.isEmpty()) {
                throw new IllegalStateException("Enroll product to rentalAd fail - dbSeeder");
            }

            Product productNew = product.get();
            SearchAd searchAdNew = searchAd.get();

            productNew.setSearchProduct(true);
            productNew.getIncludeAds().add(searchAdNew);
            searchAdNew.getProducts().add(productNew);

            searchAdRepo.save(searchAdNew);
            productRepo.save(productNew);
            tmpSearch++;


        }
    }


    private void seedVulgarWords(InputStream vulgarFile) {
        lines = getlines(vulgarFile);
        for (String s : lines) {
            VulgarWords vulgarWords = new VulgarWords(s);
            vulgarWordsList.add(vulgarWords);
            vulgarWordsRepo.save(vulgarWords);
        }
    }

    private void seedBlockedIp(InputStream blockedIpFile) {
        lines = getlines(blockedIpFile);
        for (String s : lines) {

            BlockedIp blockedIp = new BlockedIp(s);
            blockedIpList.add(blockedIp);
            blockedIpRepo.save(blockedIp);

        }
    }

    private void seedClaims(InputStream claimFile) {
        lines = getlines(claimFile);
        for (String s : lines) {
            String[] rows = s.split(";");

            ClaimStatus status = null;
            switch (rows[5]) {
                case "Zgłoszona":
                    status = ClaimStatus.SUBMITTED;
                    break;
                case "Oczekiwanie na oględziny":
                    status = ClaimStatus.IN_PROGRESS_BEFORE_INSPECTION;
                    break;
                case "W trakcie":
                    status = ClaimStatus.IN_PROGRESS_AFTER_INSPECTION;
                    break;
                case "Przyjęta":
                    status = ClaimStatus.ACCEPTED;
                    break;
                case "Odrzucona":
                    status = ClaimStatus.REJECTED;
                    break;
            }
            LocalDateTime ldt = !rows[1].equals("null") ? LocalDateTime.parse(rows[1]) : null;
            Claim claim;
            if (status.equals(ClaimStatus.SUBMITTED)) {
                claim = new Claim(LocalDate.parse(rows[0]), ldt, rows[2], rows[3], new Location(52.2297700, 21.0117800, rows[4]), status);
                claim.setDateOfAssignToTheAppraiser(null);
            } else {
                claim = new Claim(LocalDate.parse(rows[0]), ldt, rows[2], rows[3], new Location(52.2297700, 21.0117800, rows[4]), status);
                claim.setDateOfAssignToTheAppraiser(LocalDateTime.parse(rows[7]));

            }

            if (status == ClaimStatus.ACCEPTED || status == ClaimStatus.REJECTED) {
                claim.setJustification(rows[6]);
            }

            Policy policy = policyRepo.findById(Long.parseLong(rows[8])).get();
            claim.setPolicy(policy);
            if (!rows[9].equals("null")) {
                claim.setInsuranceCaseId(UUID.fromString(rows[9]));
            }

            if (!rows[10].equals("null")) {
                claim.setTempAppraiser(userRepo.findById(Long.parseLong(rows[10])).get());
            }
            claim.setUser(policy.getHire().getSharer());
            claimRepo.save(claim);
            claims.add(claim);
        }
    }

    private void seedPolicies(InputStream policyFile) {
        lines = getlines(policyFile);
        for (String s : lines) {
            String[] str = s.split(";");
            Policy policy = new Policy(Double.parseDouble(str[0]));

            Hire hire = hireList.get(Integer.parseInt(str[1]) - 1);

            if (hire.getHireStatus().equals(HireStatus.RETURNED)) {
                policy.setHire(hire);
                policies.add(policy);

                policyRepo.save(policy);
            }

        }
    }


    private void deleteOldImageForDbSeeder() throws IOException {
        //TODO usunąć jak zmienimy bazę z create na update (dwie linijki pod tym)
        String fileSeparator = FileSystems.getDefault().getSeparator();
        String RESOURCES_DIR = System.getProperty("user.dir") + fileSeparator + "IMAGES" + fileSeparator;
        Files.createDirectories(Paths.get(RESOURCES_DIR));
        FileUtils.cleanDirectory(new File(RESOURCES_DIR));
    }

    private void seedImagesToUserProfileImage() throws IOException {

        final File men = new File("src/main/resources/static/zdjProfilowe/M");
        final File women = new File("src/main/resources/static/zdjProfilowe/K");

        List<User> menList = userRepo.findAllMenByLastCharacterToDbSeeder();
        List<User> womenList = userRepo.findAllWomenByLastCharacterToDbSeeder();

        int tmp = 0;
        for (final File fileEntry : women.listFiles()) {
            if (!womenList.isEmpty()) {
                if (tmp < womenList.size()) {
                    if (womenList.get(tmp).getFirstName().endsWith("a")) {
                        byte[] content = Files.readAllBytes(Path.of(fileEntry.getPath()));
                        MultipartFile multipartFile = new MockMultipartFile(fileEntry.getName(), fileEntry.getName(), Files.probeContentType(Path.of(fileEntry.getPath())), content);
                        var imageEntity = save(Collections.singletonList(multipartFile));
                        womenList.get(tmp).setUserImage(imageEntity.get(0));
                        imageEntity.get(0).setUser(womenList.get(tmp));
                    }

                }
            }

            if (tmp > userList.size()) {
                break;
            }
            tmp++;

        }

        tmp = 0;

        for (final File fileEntry : men.listFiles()) {
            if (!menList.isEmpty()) {
                if (tmp < menList.size()) {
                    if (!menList.get(tmp).getFirstName().endsWith("a")) {
                        byte[] content = Files.readAllBytes(Path.of(fileEntry.getPath()));
                        MultipartFile multipartFile = new MockMultipartFile(fileEntry.getName(), fileEntry.getName(), Files.probeContentType(Path.of(fileEntry.getPath())), content);
                        var imageEntity = save(Collections.singletonList(multipartFile));
                        menList.get(tmp).setUserImage(imageEntity.get(0));
                        imageEntity.get(0).setUser(menList.get(tmp));
                    }
                }
            }

            if (tmp > userList.size()) {
                break;
            }

            tmp++;
        }


    }


    private void addMoreImageToRentalAds() throws IOException {
        addMoreImagesToRentalAdGryIKonsoleXboxSeriesX();
        addMoreImagesToRentalAdGryIKonsoleXboxSeriesS();
        addMoreImagesToRentalAdGryIKonsolePlayStationPS4();
        addMoreImagesToRentalAdGryIKonsolePlayStationPS5();
        addMoreImagesToRentalAdKomputery();
        addMoreImagesToRentalAdTelefonyKomorkowe();
        addMoreImagesToRentalAdTelewizory();

    }

    private void addMoreImagesToRentalAdGryIKonsoleXboxSeriesX() throws IOException {

        final File folder = new File("src/main/resources/static/ZdjeciaDoOglszonMore/Gry i konsole/xbox/xboxSeriesX");
        List<RentalAd> rentalAdListWihXbox = rentalAdRepo.forDbSeederImageConnectToRentalAd("Gry i konsole", "Xbox Series X");

        int tmp = 0;
        int tmp2 = 0;
        Set<Integer> tempSet = Set.of(2, 5, 8, 11, 14, 17, 20);
        for (final File fileEntry : folder.listFiles()) {
            if (rentalAdListWihXbox.size() > tmp2) {
                byte[] content = Files.readAllBytes(Path.of(fileEntry.getPath()));
                MultipartFile multipartFile = new MockMultipartFile(fileEntry.getName(), fileEntry.getName(), Files.probeContentType(Path.of(fileEntry.getPath())), content);
                var imageEntity = save(Collections.singletonList(multipartFile));
                imageEntity.get(0).setAd(rentalAdListWihXbox.get(tmp2));
                if (tempSet.contains(tmp)) {
                    tmp2++;
                }
                tmp++;
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (rentalAdListWihXbox.size() < tmp2) {
                break;
            }

        }
    }

    private void addMoreImagesToRentalAdGryIKonsoleXboxSeriesS() throws IOException {

        final File folder = new File("src/main/resources/static/ZdjeciaDoOglszonMore/Gry i konsole/xbox/xboxSeriesS");
        List<RentalAd> rentalAdListWihXbox = rentalAdRepo.forDbSeederImageConnectToRentalAd("Gry i konsole", "Xbox Series S");

        int tmp = 0;
        int tmp2 = 0;
        Set<Integer> tempSet = Set.of(2, 5, 8, 11, 14, 17, 20);
        for (final File fileEntry : folder.listFiles()) {
            if (rentalAdListWihXbox.size() > tmp2) {
                byte[] content = Files.readAllBytes(Path.of(fileEntry.getPath()));
                MultipartFile multipartFile = new MockMultipartFile(fileEntry.getName(), fileEntry.getName(), Files.probeContentType(Path.of(fileEntry.getPath())), content);
                var imageEntity = save(Collections.singletonList(multipartFile));
                imageEntity.get(0).setAd(rentalAdListWihXbox.get(tmp2));
                if (tempSet.contains(tmp)) {
                    tmp2++;
                }
                tmp++;
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (rentalAdListWihXbox.size() < tmp2) {
                break;
            }

        }
    }


    private void addMoreImagesToRentalAdGryIKonsolePlayStationPS5() throws IOException {

        final File folder = new File("src/main/resources/static/ZdjeciaDoOglszonMore/Gry i konsole/PlayStation/ps5");
        List<RentalAd> rentalAdListWihPlayStation = rentalAdRepo.forDbSeederImageConnectToRentalAd("Gry i konsole", "PlayStation 5");

        int tmp = 0;
        int tmp2 = 0;
        Set<Integer> tempSet = Set.of(2, 5, 8, 11, 14, 17, 20);
        for (final File fileEntry : folder.listFiles()) {
            if (rentalAdListWihPlayStation.size() > tmp2) {
                byte[] content = Files.readAllBytes(Path.of(fileEntry.getPath()));
                MultipartFile multipartFile = new MockMultipartFile(fileEntry.getName(), fileEntry.getName(), Files.probeContentType(Path.of(fileEntry.getPath())), content);
                var imageEntity = save(Collections.singletonList(multipartFile));
                imageEntity.get(0).setAd(rentalAdListWihPlayStation.get(tmp2));
                if (tempSet.contains(tmp)) {
                    tmp2++;
                }
                tmp++;
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (rentalAdListWihPlayStation.size() < tmp2) {
                break;
            }

        }
    }

    private void addMoreImagesToRentalAdGryIKonsolePlayStationPS4() throws IOException {

        final File folder = new File("src/main/resources/static/ZdjeciaDoOglszonMore/Gry i konsole/PlayStation/ps4");
        List<RentalAd> rentalAdListWihPlayStation = rentalAdRepo.forDbSeederImageConnectToRentalAd("Gry i konsole", "PlayStation 4");

        int tmp = 0;
        int tmp2 = 0;
        Set<Integer> tempSet = Set.of(2, 5, 8, 11, 14, 17, 20);
        for (final File fileEntry : folder.listFiles()) {
            if (rentalAdListWihPlayStation.size() > tmp2) {
                byte[] content = Files.readAllBytes(Path.of(fileEntry.getPath()));
                MultipartFile multipartFile = new MockMultipartFile(fileEntry.getName(), fileEntry.getName(), Files.probeContentType(Path.of(fileEntry.getPath())), content);
                var imageEntity = save(Collections.singletonList(multipartFile));
                imageEntity.get(0).setAd(rentalAdListWihPlayStation.get(tmp2));
                if (tempSet.contains(tmp)) {
                    tmp2++;
                }
                tmp++;
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (rentalAdListWihPlayStation.size() < tmp2) {
                break;
            }

        }
    }

    private void addMoreImagesToRentalAdKomputery() throws IOException {

        final File folder = new File("src/main/resources/static/ZdjeciaDoOglszonMore/Komputery");
        List<RentalAd> rentalAdList = rentalAdRepo.forDbSeederImageConnectToRentalAd("Komputery", "");
        int tmp = 0;
        int tmp2 = 0;
        Set<Integer> tempSet = Set.of(2, 5, 8, 11, 14, 17, 20);
        for (final File fileEntry : folder.listFiles()) {
            if (rentalAdList.size() > tmp2) {
                byte[] content = Files.readAllBytes(Path.of(fileEntry.getPath()));
                MultipartFile multipartFile = new MockMultipartFile(fileEntry.getName(), fileEntry.getName(), Files.probeContentType(Path.of(fileEntry.getPath())), content);
                var imageEntity = save(Collections.singletonList(multipartFile));
                imageEntity.get(0).setAd(rentalAdList.get(tmp2));
                if (tempSet.contains(tmp)) {
                    tmp2++;
                }
                tmp++;
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


        }
    }

    private void addMoreImagesToRentalAdTelefonyKomorkowe() throws IOException {

        final File folder = new File("src/main/resources/static/ZdjeciaDoOglszonMore/Telefony komórkowe");
        List<RentalAd> rentalAdList = rentalAdRepo.forDbSeederImageConnectToRentalAd("Telefony komórkowe", "");
        int tmp = 0;
        int tmp2 = 0;
        Set<Integer> tempSet = Set.of(2, 5, 8, 11, 14, 17, 20);
        for (final File fileEntry : folder.listFiles()) {
            if (rentalAdList.size() > tmp2) {
                byte[] content = Files.readAllBytes(Path.of(fileEntry.getPath()));
                MultipartFile multipartFile = new MockMultipartFile(fileEntry.getName(), fileEntry.getName(), Files.probeContentType(Path.of(fileEntry.getPath())), content);
                var imageEntity = save(Collections.singletonList(multipartFile));
                imageEntity.get(0).setAd(rentalAdList.get(tmp2));
                if (tempSet.contains(tmp)) {
                    tmp2++;
                }
                tmp++;
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


        }
    }

    private void addMoreImagesToRentalAdTelewizory() throws IOException {

        final File folder = new File("src/main/resources/static/ZdjeciaDoOglszonMore/Telewizory");
        List<RentalAd> rentalAdList = rentalAdRepo.forDbSeederImageConnectToRentalAd("Telewizory", "");
        int tmp = 0;
        int tmp2 = 0;
        Set<Integer> tempSet = Set.of(2, 5, 8, 11, 14, 17, 20);
        for (final File fileEntry : folder.listFiles()) {
            if (rentalAdList.size() > tmp2) {
                byte[] content = Files.readAllBytes(Path.of(fileEntry.getPath()));
                MultipartFile multipartFile = new MockMultipartFile(fileEntry.getName(), fileEntry.getName(), Files.probeContentType(Path.of(fileEntry.getPath())), content);
                var imageEntity = save(Collections.singletonList(multipartFile));
                imageEntity.get(0).setAd(rentalAdList.get(tmp2));
                if (tempSet.contains(tmp)) {
                    tmp2++;
                }
                tmp++;
                try {
                    TimeUnit.MILLISECONDS.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }


        }
    }


    public List<ImageEntity> save(List<MultipartFile> imageList) {
        List<ImageEntity> listOfImagesId = new ArrayList<>();

        for (MultipartFile image : imageList) {
            byte[] bytes = new byte[0];
            try {
                bytes = image.getBytes();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String imageName = image.getOriginalFilename();
            checkIsExtensionIsCorrect(imageName);

            imageName.toLowerCase();

            String location = null;
            MessageDigest md = null;
            var nowDate = new Date().getTime();
            try {
                String toHash = nowDate + imageName;
                md = MessageDigest.getInstance("MD5");
                md.update(toHash.getBytes(StandardCharsets.UTF_8));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            byte[] digest = md.digest();
            String imageHash = DatatypeConverter
                    .printHexBinary(digest).toUpperCase();

            String finalNameOfImage = imageHash + "." + imageName.substring(imageName.length() - 3);

            try {

                location = fileSystemRepo.save(bytes, finalNameOfImage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ImageEntity imageEntity = new ImageEntity(finalNameOfImage, location);
            ImageThumbnail imageThumbnail = addThumbnailForExistingImage(imageEntity);
            imageThumbnail.setImageEntity(imageEntity);
            imageEntity.setImageThumbnail(imageThumbnail);
            imageRepo.save(imageEntity);
            listOfImagesId.add(imageEntity);
        }
        return listOfImagesId;
    }

    @Async
    public ImageThumbnail addThumbnailForExistingImage(ImageEntity imageDownload) {
        FileSystemResource image = fileSystemRepo.findInFileSystem(imageDownload.getLocation());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        BufferedImage bufferedImage = null;

        try {
            bufferedImage = ImageIO.read(image.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Thumbnails.of(bufferedImage)
                    .size(640, 360)
                    .outputFormat("JPEG")
                    .outputQuality(1)
                    .toOutputStream(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bytes = outputStream.toByteArray();
        String imageName = imageDownload.getName();

        return addThumbnailToDb(bytes, imageName);
    }

    private ImageThumbnail addThumbnailToDb(byte[] bytes, String imageName) {
        checkIsExtensionIsCorrect(imageName);

        imageName.toLowerCase();

        String location = null;
        MessageDigest md = null;
        var nowDate = new Date().getTime();
        try {
            String toHash = nowDate + imageName + "thumbNail";
            md = MessageDigest.getInstance("MD5");
            md.update(toHash.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] digest = md.digest();
        String imageHash = DatatypeConverter
                .printHexBinary(digest).toUpperCase();

        String finalNameOfImage = imageHash + "." + imageName.substring(imageName.length() - 3);

        try {
            location = fileSystemRepo.save(bytes, finalNameOfImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ImageThumbnail imageThumbnail = new ImageThumbnail(finalNameOfImage, location);
        imageThumbnailRepo.save(imageThumbnail);

        return imageThumbnail;
    }


    private void checkIsExtensionIsCorrect(String imageName) {
        Pattern fileExtnPtrn = Pattern.compile("([^\\s]+(\\.(?i)(jpg|png))$)");
        Matcher mtch = fileExtnPtrn.matcher(imageName);
        if (!mtch.matches())
            throw new ImageEntity.BadTypeExtension();
    }

    private void seedInsuranceCompanies(InputStream insuranceCompanyFile) {
        lines = getlines(insuranceCompanyFile);
        for (String s : lines) {
            String[] item = s.split(";");
            InsuranceCompany insuranceCompany = new InsuranceCompany(item[0], item[1], item[2], Double.parseDouble(item[3]), new Location(12.22, 12.22, item[4]));
            insuranceCompanies.add(insuranceCompany);
            insuranceCompanyRepo.save(insuranceCompany);
        }

        //InsuranceCompany - Policy relation
        for (Policy policy : policies) {
            int random = (int) (Math.random() * insuranceCompanies.size());
            policy.setInsuranceCompany(insuranceCompanies.get(random));
        }

        //InsuranceCompany - InsuranceAdjuster relation
        int counter = 0;
        for (User insuranceAdjuster : insuranceAdjusters) {
            if (counter == 5) {
                counter = 0;
            }
            insuranceAdjuster.setInsuranceCompany(insuranceCompanies.get(counter));
            insuranceCompanies.get(counter).getInsuranceAdjusters().add(insuranceAdjuster);
            userRepo.save(insuranceAdjuster);
            counter++;
        }
    }

    private void seedInspections(InputStream inspectionsFile) {
        lines = getlines(inspectionsFile);
        for (String s : lines) {
            String[] item = s.split(";");
            ClaimInspection claimInspection = new ClaimInspection(LocalDate.parse(item[0]), item[1], Double.parseDouble(item[2]));
            claimInspection.setAppraiser(userRepo.findById(Long.parseLong(item[3])).get());
            claimInspection.setClaim(claimRepo.findById(Long.parseLong(item[4])).get());
            claimInspectionRepo.save(claimInspection);
        }
    }

    private List<String> getlines(InputStream regionsFile) {
        return new BufferedReader(new InputStreamReader(regionsFile, StandardCharsets.UTF_8)).lines().collect(Collectors.toList());
    }

    private void seedComplaints(InputStream complaintFile) throws Exception {
        lines = getlines(complaintFile);

        int i = 0;
        for (String s : lines) {
            String[] item = s.split(";");
            ComplaintReason complaintReason;

            switch (item[1]) {
                case "Przedmiot niezgodny z opisem":
                    complaintReason = ComplaintReason.NOT_AS_DESCRIBED;
                    break;
                case "Wadliwy przedmiot":
                    complaintReason = ComplaintReason.DEFECTIVE;
                    break;
                case "Niekompletne wypożyczenie":
                    complaintReason = ComplaintReason.INCOMPLETE;
                    break;
                case "Brak otrzymania przesyłki":
                    complaintReason = ComplaintReason.NOT_RECIVED;
                    break;
                case "Uszkodzona przesyłka":
                    complaintReason = ComplaintReason.PACKAGE_BROKEN;
                    break;
                case "Inne":
                    complaintReason = ComplaintReason.OTHER;
                    break;
                default:
                    throw new NoSuchElementException("Blad danych w pliku Reklamacje.txt");
            }

            Hire hire = hireRepo.findById(Long.parseLong(item[3])).orElseThrow(() -> new Exception("Hire not found"));

            var status  = ComplainStatus.ACCEPTED;

            if(i % 3 == 0){
                status = ComplainStatus.SUBMITTED;
            }else if (i % 4 == 0){
                status = ComplainStatus.REJECTED;
            }

            Complaint complaint = new Complaint(Long.parseLong(item[0]), complaintReason, item[2], hire, status);

            complaintRepo.save(complaint);

            i++;
        }

    }
}
