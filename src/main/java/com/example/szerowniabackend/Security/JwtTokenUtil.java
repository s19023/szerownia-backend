package com.example.szerowniabackend.Security;

import com.example.szerowniabackend.Models.Entities.Enums.Role;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtTokenUtil implements Serializable {
    public static final long TOKEN_VALIDITY = 30 * 60 * 1000; //30 minutes


    @Value("${jwt.secret}")
    private String sec;

    private static String secret;

    @Value("${jwt.secret}")
    public void setSecretStatic(String sec) {
        JwtTokenUtil.secret = sec;
    }

    public String getClaimFromToken(String token, String key) {
        Claims claims = getAllClaimsFromToken(token);
        return claims.get(key).toString();
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }


    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public Date getExpirationDateFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getExpiration();
    }

    public Boolean validateToken(String token, String username) {
        final String tokenUsername = getClaimFromToken(token, "username");
        return (username.equals(tokenUsername) && !isTokenExpired(token));
    }

    public static String generateToken(String email, Long id, Role role, Boolean isPremium) {
        return Jwts.builder()
                .setSubject(email)
                .claim("id", id)
                .claim("role", role)
                .claim("isPremium", isPremium)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JwtTokenUtil.TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.HS256, secret.getBytes())
                .compact();

    }

    public static String generateRefreshToken() {

        byte[] bytes = new byte[64];
        try {
            SecureRandom.getInstanceStrong().nextBytes(bytes);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Base64.getEncoder().withoutPadding().encodeToString(bytes);

    }

    public static Long getIdFromToken(String tokenWithBearer) {
        String token = tokenWithBearer.substring(6, tokenWithBearer.length());
        var decodedToken = decodeToken(token);
        String idInString = decodedToken.getBody().get("id").toString();
        return Long.parseLong(idInString);

    }



    public static Boolean getIsPremiumFromToken(String tokenWithBearer) {
        String token = tokenWithBearer.substring(6, tokenWithBearer.length());
        var decodedToken = decodeToken(token);
        String idInString = decodedToken.getBody().get("isPremium").toString();
        return Boolean.parseBoolean(idInString);

    }

    public static String getEmailFromToken(String tokenWithBearer) {
        String token = tokenWithBearer;
        if (tokenWithBearer.contains("Bearer")) {
            token = tokenWithBearer.substring(6, tokenWithBearer.length());
        }
        var decodedToken = decodeToken(token);
        return decodedToken.getBody().get("sub").toString();
    }

    public static String getRoleFromToken(String tokenWithBearer) {
        String token = tokenWithBearer.substring(6, tokenWithBearer.length());
        var decodedToken = decodeToken(token);
        return decodedToken.getBody().get("role").toString();

    }

    private static Jws<Claims> decodeToken(String token) {
        return Jwts.parser()
                .setSigningKey(secret.getBytes(StandardCharsets.UTF_8))
                .parseClaimsJws(token);

    }
}
