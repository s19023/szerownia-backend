package com.example.szerowniabackend.Security;


import com.example.szerowniabackend.Models.Entities.Person;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RefreshToken {

    public static UserRepo userRepo;

    @Autowired
    public RefreshToken(UserRepo userRepo) {
        RefreshToken.userRepo = userRepo;
    }

    public static void addRefreshTokenToPerson(String refreshToken, Person person){

        person.setRefreshToken(refreshToken);
        userRepo.save((User) person);

    }
}
