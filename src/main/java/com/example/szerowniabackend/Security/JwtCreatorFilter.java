package com.example.szerowniabackend.Security;

import com.example.szerowniabackend.Models.DTOs.Requests.UsernameAndPasswordAuthRequest;
import com.example.szerowniabackend.Models.Entities.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.example.szerowniabackend.Models.Consts.Log.*;

@EnableJpaRepositories
public class JwtCreatorFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private Logger logger = LoggerFactory.getLogger(JwtCreatorFilter.class);

    public JwtCreatorFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        UsernameAndPasswordAuthRequest req;
        Authentication authentication = null;

        try {
            req = new ObjectMapper().readValue(request.getInputStream(), UsernameAndPasswordAuthRequest.class);

            authentication = authenticationManager.authenticate(
                  new UsernamePasswordAuthenticationToken(req.getEmail(), req.getPassword()));

        } catch (IOException | NullPointerException ignored) {
        } catch (LockedException e) {
            logger.info("ACCCOUNT LOCKED");
            logError(request, response, new InternalAuthenticationServiceException("Account blocked", e));
        } catch (InternalAuthenticationServiceException error) {
            logger.info("BAD CREDENTIALS");
            logError(request, response, error);
        }
        return authentication;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult) throws IOException {
        Person p = (Person) authResult.getPrincipal();
        String token = JwtTokenUtil.generateToken(authResult.getName(), p.getId(), p.getRole(), p.getPremium());
        String refreshToken = JwtTokenUtil.generateRefreshToken();

        response.getOutputStream().print("{\"token\": \"" + token + "\"");
        response.getOutputStream().print(",\"refreshToken\": \"" + refreshToken + "\"}");

        RefreshToken.addRefreshTokenToPerson(refreshToken, p);
        logger.info(AUTHENTICATION_SUCCESS);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        logger.error(AUTHENTICATION_FAILED);
        super.unsuccessfulAuthentication(request, response, failed);
    }

    private void logError(HttpServletRequest request, HttpServletResponse response, InternalAuthenticationServiceException e) {
        try {
            if(e.getCause() instanceof LockedException) {
                response.sendError(418, "USER BLOCKED");
            } else {
                unsuccessfulAuthentication(request, response, e);
            }
        } catch (IOException | ServletException e1) {
            e1.printStackTrace();
        }
    }
}
