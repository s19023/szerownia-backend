package com.example.szerowniabackend.Security;

import com.example.szerowniabackend.Security.Ip.SecurityFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import static com.example.szerowniabackend.Models.Consts.Role.*;
import static org.springframework.http.HttpMethod.*;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final String secret;

    public SecurityConfig(@Value("${jwt.secret}") String secret) {
        this.secret = secret;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
                .csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(new JwtCreatorFilter(authenticationManager()))
                .addFilterAfter(new JwtInspectorFilter(secret), JwtCreatorFilter.class)
                .addFilterAfter(new SecurityFilter(), JwtInspectorFilter.class)
                .authorizeRequests()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/api/products/").permitAll()
                .antMatchers("/api/products/myProducts").authenticated()
                .antMatchers(POST, "/api/users").permitAll()
                .antMatchers(POST, "/api/users/exists").permitAll()
                .antMatchers(PUT, "/api/users/**").authenticated()
                .antMatchers(DELETE,"/api/users/**").hasAnyAuthority(ADMIN, MODERATOR)
                .antMatchers(POST,"/api/users/super").hasAnyAuthority(ADMIN, MODERATOR)
                .antMatchers(GET, "/api/users/inspectors").hasAuthority(INSURANCE_ADJUSTER)
                .antMatchers("/api/users/details").authenticated()
                .antMatchers("/api/users/premium/**").authenticated()
                .antMatchers("/console").permitAll()
                .antMatchers("/console/**").permitAll()
                .antMatchers(GET, "/api/rentalAd").permitAll()
                .antMatchers(GET, "/api/rentalAd/getStatus/{\\d+}").authenticated()
//                .antMatchers(GET, "api/searchingAd/{\\d+}").authenticated()
                .antMatchers( "/api/rentalAd/getStatus").authenticated()
                .antMatchers("/api/vulgarWords/**").hasAnyAuthority(ADMIN, MODERATOR)
                .antMatchers("/api/blockedIp/**").hasAnyAuthority(ADMIN, MODERATOR)
                .antMatchers("/api/insuranceadjuster/**").hasAuthority(INSURANCE_ADJUSTER)
                .antMatchers("/api/inspection/**").hasAuthority(APPRAISER);
   }
}