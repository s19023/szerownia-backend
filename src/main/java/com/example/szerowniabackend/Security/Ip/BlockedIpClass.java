package com.example.szerowniabackend.Security.Ip;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BlockedIpClass {

    private static List<String> blockedIpList = new ArrayList<>();


    @Async
    @EventListener(ApplicationReadyEvent.class)
    public void loadBlockedIpOnStart(){

        BlockedIpClass.blockedIpList.clear();
        BlockedIpClass.blockedIpList = BlockedIpHelper.loadListFromDb();

    }

    public static List<String> getBlockedIpList() {
        if(blockedIpList.isEmpty()){
            BlockedIpClass.blockedIpList = BlockedIpHelper.loadListFromDb();
        }
        return blockedIpList;
    }



    public static void clearStaticListAndSynchronizeStaticBlockedListWithDbBlockedList(){
        blockedIpList.clear();
        blockedIpList = BlockedIpHelper.loadListFromDb();
    }
}
