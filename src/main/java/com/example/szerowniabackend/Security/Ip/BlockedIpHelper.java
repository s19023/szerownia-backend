package com.example.szerowniabackend.Security.Ip;

import com.example.szerowniabackend.Models.Repositories.BlockedIpRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Component
@Transactional
public class BlockedIpHelper {

    public static BlockedIpRepo blockedIpRepo;

    @Autowired
    public BlockedIpHelper(BlockedIpRepo blockedIpRepo) {
        BlockedIpHelper.blockedIpRepo = blockedIpRepo;
    }


    public static List<String> loadListFromDb(){
        return blockedIpRepo.findAllBlockedIp();
    }
}
