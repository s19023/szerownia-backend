package com.example.szerowniabackend.Security;

import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.RentalAd;
import com.example.szerowniabackend.Models.Entities.User;


public interface Authorization {


    public boolean isModerator(String token);

    public boolean isAdmin(String token);

    public boolean isCurrentUser(User user, String token);

    public boolean isPremium(String token);

    public Long getIdUserFromToken(String token);

    public Role getRoleFromToken(String token);

    public String getEmailFromToken(String token);

    public String generateToken(String email, Long id, Role role, Boolean isPremium);

    public String generateRefreshToken();

    public boolean isSuperUser(String token);


}
