package com.example.szerowniabackend.Security;


import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.User;
import org.springframework.stereotype.Service;


@Service("Authorization")
public class AuthorizationJwtImpl implements Authorization {


    @Override
    public boolean isModerator(String token) {
        return hasRole(token, Role.MODERATOR);
    }

    @Override
    public boolean isAdmin(String token) {
        return hasRole(token, Role.ADMIN);
    }

    @Override
    public boolean isCurrentUser(User user, String token) {
        Long idUser = JwtTokenUtil.getIdFromToken(token);
        return user.getId().equals(idUser);
    }

    @Override
    public boolean isPremium(String token) {
        return JwtTokenUtil.getIsPremiumFromToken(token);
    }

    @Override
    public Long getIdUserFromToken(String token) {
        return JwtTokenUtil.getIdFromToken(token);
    }

    @Override
    public Role getRoleFromToken(String token) {
        return Role.valueOf(JwtTokenUtil.getRoleFromToken(token));
    }

    @Override
    public String getEmailFromToken(String token) {
        return JwtTokenUtil.getEmailFromToken(token);
    }

    @Override
    public String generateToken(String email, Long id, Role role, Boolean isPremium) {
        return JwtTokenUtil.generateToken(email, id, role, isPremium);
    }

    @Override
    public String generateRefreshToken() {
        return JwtTokenUtil.generateRefreshToken();
    }

    @Override
    public boolean isSuperUser(String token) {
        return (hasRole(token, Role.ADMIN) || hasRole(token, Role.MODERATOR) || hasRole(token, Role.APPRAISER) || hasRole(token, Role.INSURANCE_ADJUSTER));
    }

    private boolean hasRole(String token, Role role) {

        String roleFromToken = JwtTokenUtil.getRoleFromToken(token);
        return role.toString().equals(roleFromToken);
    }

}
