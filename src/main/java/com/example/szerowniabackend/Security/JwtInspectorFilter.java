package com.example.szerowniabackend.Security;

import com.google.common.base.Strings;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

import static com.example.szerowniabackend.Models.Consts.Log.*;

public class JwtInspectorFilter extends OncePerRequestFilter {

    private final String secret;
    private Logger logger = LoggerFactory.getLogger(JwtCreatorFilter.class);

    public JwtInspectorFilter(@Value("${jwt.secret}") String secret) {
        this.secret = secret;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader("Authorization");

        if (Strings.isNullOrEmpty(header) || !header.startsWith("Bearer ")) {
            logError(req, res, chain);
            return;
        }

        logger.warn(AUTHORIZATION_ATTEMPT + req.getMethod() + " " + req.getServletPath());

        try {
            Authentication authentication = getAuthByToken(header.substring(7));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            chain.doFilter(req, res);

        } catch (JwtException e){
            chain.doFilter(req, res);
            logger.error(BAD_TOKEN + req.getServletPath() + " " + req.getMethod() + " " + res.getStatus());
        }
    }

    private UsernamePasswordAuthenticationToken getAuthByToken(String token) {
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token);

        String email = claimsJws.getBody().getSubject();
        String role = claimsJws.getBody().get("role").toString();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);

        return new UsernamePasswordAuthenticationToken(email, null, Collections.singleton(authority));
    }

    private void logError(HttpServletRequest req, HttpServletResponse res,
                          FilterChain chain) throws IOException, ServletException {
        chain.doFilter(req, res);
        if (res.getStatus() != 200)
            logger.error(BAD_HEADER + req.getServletPath() + " " + req.getMethod() + " " + res.getStatus());
    }
}
