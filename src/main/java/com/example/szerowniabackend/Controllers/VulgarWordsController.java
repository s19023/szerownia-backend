package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.VulgarWordResponseDTO;
import com.example.szerowniabackend.Models.Entities.VulgarWords;
import com.example.szerowniabackend.Services.VulgarWordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/vulgarWords")
public class VulgarWordsController {

    private VulgarWordsService vulgarWordsService;

    @Autowired
    public VulgarWordsController(VulgarWordsService vulgarWordsService) {
        this.vulgarWordsService = vulgarWordsService;
    }

    @GetMapping
    public PaginatedDTO<VulgarWords> getAllVulgarWords(@RequestParam(defaultValue = "0") Integer page,
                                          @RequestParam(defaultValue = "25") Integer howManyRecord,
                                          @RequestParam(defaultValue = "true") Boolean sortAscending,
                                          @RequestParam(required = false) String vulgarWord) {
        return vulgarWordsService.getListOfVulgarWords(page, howManyRecord, sortAscending, vulgarWord);
    }

    @GetMapping("getById/{id}")
    public VulgarWordResponseDTO getVulgarWordById(@PathVariable Long id) {
        return vulgarWordsService.getVulgarWordById(id);
    }

    @GetMapping("getByWord/{vulgarWord}")
    public VulgarWords getVulgarWordByWord(@PathVariable String vulgarWord) {
        return vulgarWordsService.getVulgarWordByWord(vulgarWord);
    }

    @PostMapping()
    public void createVulgarWord(@RequestBody VulgarWordResponseDTO vulgarWords) {
        vulgarWordsService.createVulgarWord(vulgarWords);
    }

    @PutMapping("/updateById/{vulgarWordId}")
    public void updateVulgarWordById(@PathVariable Long vulgarWordId, @RequestBody VulgarWordResponseDTO vulgarWordResponseDTO) {
        vulgarWordsService.updateVulgarWord(vulgarWordId, vulgarWordResponseDTO);
    }

    @PutMapping("/updateByWord/{word}")
    public void updateVulgarWordByWord(@PathVariable String word, @RequestBody VulgarWordResponseDTO vulgarWordResponseDTO) {
        vulgarWordsService.updateVulgarWordByWord(word, vulgarWordResponseDTO);
    }


    @DeleteMapping("/deleteById/{id}")
    public void deleteVulgarWordById(@PathVariable Long id) {
        vulgarWordsService.deleteVulgarWordById(id);

    }

    @DeleteMapping("/deleteByWord/{word}")
    public void deleteVulgarWordByVulgarWord(@PathVariable String word) {
        vulgarWordsService.deleteVulgarWordByVulgarWord(word);

    }


}
