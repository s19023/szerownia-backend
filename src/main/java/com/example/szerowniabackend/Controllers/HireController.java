package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.CalculateHireCostRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.HireOutgoingRequestDto;
import com.example.szerowniabackend.Models.DTOs.Requests.HireRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.CalculateHireCostDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetHire.HireResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetHireMyBorrowsSharers.HireMyBorrowsResponseDTO;
import com.example.szerowniabackend.Models.Entities.Enums.HireStatus;
import com.example.szerowniabackend.Services.HireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/hires")
@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
public class HireController {

    private HireService hireService;

    @Autowired
    public HireController(HireService hireService) {
        this.hireService = hireService;
    }

    @GetMapping
    public List<HireResponseDTO> getAllHires() {
        return hireService.getAllHires();
    }

    @GetMapping("/{id}")
    public HireResponseDTO getHire(@PathVariable Long id) {
        return hireService.getHire(id);
    }

    @PostMapping("/estimate")
    public CalculateHireCostDTO calculateHireCost(@RequestBody CalculateHireCostRequestDTO resquest) {
        return hireService.calculateHireCost(resquest);
    }

    @PostMapping()
    public void createHire(@RequestBody HireRequestDTO hire, @RequestHeader String authorization) {
        hireService.createHire(hire, authorization);
    }

    @PutMapping("{id}")
    public void updateHire(@PathVariable Long id, @RequestBody LocalDate dateToActual) {
        hireService.updateHire(id, dateToActual);
    }

    @DeleteMapping("{id}")
    public void deleteHire(@PathVariable Long id) {
        hireService.deleteHire(id);
    }


    @GetMapping("myBorrows")
    public List<HireMyBorrowsResponseDTO> getMyBorrows(@RequestHeader String authorization, @RequestParam(required = false) HireStatus hireStatus) {
        return hireService.getMyBorrows(authorization, hireStatus);
    }

    @GetMapping("myShares")
    public List<HireMyBorrowsResponseDTO> getMyShares(@RequestHeader String authorization, @RequestParam(required = false) HireStatus hireStatus) {
        return hireService.getMyShares(authorization, hireStatus);
    }

    @PostMapping("{id}/package/outgoing")
    public void addOutgoingShipmentNumber(@PathVariable Long id, @RequestHeader String authorization, @RequestBody HireOutgoingRequestDto request) {
        hireService.addOutgoingShipmentNumber(id, authorization, request);
    }

    @PostMapping("{id}/package/incoming")
    public void addIncomingShipmentNumber(@PathVariable Long id, @RequestHeader String authorization, @RequestBody String incomingShipmentNumber) {
        hireService.addIncomingShipmentNumber(id, authorization, incomingShipmentNumber);
    }

    @PutMapping("{id}/package/confirm")
    public void confirmReceivingShipment(@PathVariable Long id, @RequestHeader String authorization) {
        hireService.confirmReceivingShipment(id, authorization);
    }

    @GetMapping("/product/{productId}")
    public List<HireResponseDTO> productHistory(@PathVariable Long productId, @RequestHeader String authorization) {
        return hireService.productHistory(productId, authorization);
    }

    @GetMapping("/{id}/insuranceCost")
    public Double calculateCostOfInsurance(@PathVariable Long id) {
        return hireService.calculateCostOfInsurance(id);
    }

    @GetMapping("userBorrows/{idUser}")
    public PaginatedDTO<HireMyBorrowsResponseDTO> getUserBorrows(@PathVariable Long idUser, @RequestHeader String authorization,
                                                                 @RequestParam(defaultValue = "0") int page,
                                                                 @RequestParam(defaultValue = "25") int howManyRecord) {
        return hireService.getUserBorrows(idUser, authorization, page, howManyRecord);
    }

    @GetMapping("userShares/{idUser}")
    public PaginatedDTO<HireMyBorrowsResponseDTO> getUserShares(@PathVariable Long idUser, @RequestHeader String authorization,
                                                                @RequestParam(defaultValue = "0") int page,
                                                                @RequestParam(defaultValue = "25") int howManyRecord) {
        return hireService.getUserShares(idUser, authorization, page, howManyRecord);
    }

}
