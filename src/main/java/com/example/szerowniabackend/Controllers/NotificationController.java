package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.NotificationDTO;
import com.example.szerowniabackend.Services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/notification")
public class NotificationController {

    private final NotificationService notificationService;

    @Autowired
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping
    public PaginatedDTO<NotificationDTO> getNotifications(@RequestHeader String authorization,
                                                          @RequestParam(defaultValue = "0") int page,
                                                          @RequestParam(defaultValue = "25") int howManyRecord) {
        return notificationService.getUserNotifications(authorization, page, howManyRecord);
    }

    @GetMapping("/unread")
    public int getUnreadNotifications(@RequestHeader String authorization) {
        return notificationService.getUnreadUserNotifications(authorization);
    }

    @PutMapping("/{notificationId}")
    public void markNotificationAsRead(@RequestHeader String authorization, @PathVariable Long notificationId) {
        notificationService.markNotificationAsRead(authorization, notificationId);
    }


}
