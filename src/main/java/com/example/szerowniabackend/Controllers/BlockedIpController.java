package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.BlockedIpRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.Entities.BlockedIp;
import com.example.szerowniabackend.Services.BlockedIpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping(path = "api/blockedIp")
public class BlockedIpController {

    private BlockedIpService blockedIpService;

    @Autowired
    public BlockedIpController(BlockedIpService blockedIpService) {
        this.blockedIpService = blockedIpService;
    }

    @GetMapping
    public PaginatedDTO<BlockedIp> getAllBlockedIp(@RequestParam(defaultValue = "0") Integer page,
                                                   @RequestParam(defaultValue = "25") Integer howManyRecord,
                                                   @RequestParam(defaultValue = "true") Boolean sortAscending,
                                                   @RequestParam(required = false) String ip){
        return blockedIpService.getAllBlockedIp(page, howManyRecord, sortAscending, ip);

    }



    @GetMapping("/{id}")
    public BlockedIp getBlockedIpById(@PathVariable Long id) {
        return blockedIpService.getBlockedIpById(id);
    }

    @PutMapping("/{id}")
    public void updateBlockedIp(@PathVariable Long id, @RequestBody BlockedIpRequestDTO blockedIp ){
        blockedIpService.updateBlockedIp(id, blockedIp);

    }

    @PostMapping()
    public void addIdToBlock(@RequestBody BlockedIpRequestDTO blockedIp) {
        blockedIpService.addIpToBlock(blockedIp);
    }

    @DeleteMapping("/deleteByIp/{ipToUnlock}")
    public void deleteIpFromBlackListByIp(@PathVariable String ipToUnlock ){
        blockedIpService.deleteIpFromBlackList(ipToUnlock);

    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteIpFromBlackListById(@PathVariable Long id ){
        blockedIpService.deleteIpFromBlackListById(id);

    }


}
