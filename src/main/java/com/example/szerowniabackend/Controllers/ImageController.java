package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Responses.ImageDTO;
import com.example.szerowniabackend.Services.ImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/image")
class ImageController {

    ImagesService imagesService;

    @Autowired
    public ImageController(ImagesService imagesService) {
        this.imagesService = imagesService;
    }

    //w swaggerze nie testować bo nie działą
    @PostMapping()
    public List<ImageDTO> uploadImage(@RequestParam("imageList") List<MultipartFile> imageList, @RequestHeader String authorization) {
        return imagesService.save(imageList, authorization);
    }

       @GetMapping(value = "/{imageId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public FileSystemResource downloadImage(@PathVariable Long imageId) {
        return imagesService.find(imageId);
    }


    @GetMapping(value = "/thumbnail/{thumbnailId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public FileSystemResource downloadImageThumbnail(@PathVariable Long thumbnailId) {
        return imagesService.findThumbnail(thumbnailId);
    }

    @DeleteMapping(value = "/{imageId}")
    public void deleteImage(@PathVariable Long imageId, @RequestHeader String authorization) {
        imagesService.deleteImage(imageId, authorization);
    }




}