package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.*;
import com.example.szerowniabackend.Models.DTOs.Responses.*;
import com.example.szerowniabackend.Models.DTOs.Responses.GetSearchAd.SearchAdResponseDTO;
import com.example.szerowniabackend.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<UserResponseDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public UserRespDto getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @PostMapping()
    public void createUser(@RequestBody UserRegisterRequestDTO user) {
        userService.createUser(user);
    }

    @PostMapping("/update")
    public void updateUser(@RequestBody UserUpdateRequestDTO user) {
        userService.updateUser(user);
    }

    @PostMapping("/super")
    public void createSuperUser(@RequestBody SuperUserRegisterRequestDTO user) {
        userService.createSuperUser(user);
    }

    @PostMapping("/premium")
    public void changePremium() {
        userService.changePremium();
    }

    @PostMapping("/premium/more")
    public void extendPremium() {
        userService.extendPremium();
    }

    @PostMapping("/rtoken")
    public void getResetToken(@RequestBody String email) {
        userService.getResetToken(email);
    }

    @PostMapping("/resetpass")
    public void resetPassword(@RequestBody ResetRequestDTO request) {
        userService.resetPassword(request);
    }

    @GetMapping("details")
    public UserDetailsDTO getMyDetails() {
        return userService.getMyDetails();
    }

    @GetMapping("/inspectors")
    public List<UserResponseDTO> getInspectors() {
        return userService.getInspectors();
    }

    @PutMapping()
    public String changeProfileImage(@RequestParam("image") MultipartFile image) {
        return userService.changeProfileImage(image);
    }

    @DeleteMapping("{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @PostMapping("/exists")
    public boolean checkIfExists(@RequestBody String email) {
        return userService.checkIfExists(email);
    }

    @GetMapping("/{userid}/rentalAds")
    public PaginatedDTO<RentalAdWithDescriptionDTO> getUserRentalAds(@PathVariable("userid") Long userid,
                                                                     @RequestParam(defaultValue = "0") int page,
                                                                     @RequestParam(defaultValue = "25") int howManyRecord) {
        return userService.getUserRentalAds(userid, page, howManyRecord);
    }

    @GetMapping("/{userid}/searchAds")
    public PaginatedDTO<SearchAdResponseDTO> getUserSearchAds(@PathVariable("userid") Long userid,
                                                              @RequestParam(defaultValue = "0") int page,
                                                              @RequestParam(defaultValue = "25") int howManyRecord) {
        return userService.getUserSearchAds(userid, page, howManyRecord);
    }

    @GetMapping("/{userid}/opinions")
    public PaginatedDTO<UserOpinionsResponseDTO> getUserOpinions(@PathVariable("userid") Long userid,
                                                                 @RequestParam(defaultValue = "0") int page,
                                                                 @RequestParam(defaultValue = "25") int howManyRecord) {
        return userService.getUserOpinions(userid, page, howManyRecord);
    }

    @GetMapping("/validate/{idUser}")
    public String validateUser(@PathVariable Long idUser, @RequestHeader String authorization) {
       return userService.validateUser(idUser, authorization);
    }

}
