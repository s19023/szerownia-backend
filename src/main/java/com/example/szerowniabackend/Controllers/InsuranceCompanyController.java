package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.InsuranceCompanyRequestDto;
import com.example.szerowniabackend.Models.DTOs.Responses.InsuranceCompanyResponseDto;
import com.example.szerowniabackend.Services.InsuranceCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("/api/insurancecompany")
public class InsuranceCompanyController {

    private final InsuranceCompanyService insuranceCompanyService;

    @Autowired
    public InsuranceCompanyController(InsuranceCompanyService insuranceCompanyService) {
        this.insuranceCompanyService = insuranceCompanyService;
    }

    @GetMapping
    public List<InsuranceCompanyResponseDto> getInsuranceCompanies(@RequestHeader String authorization) {
        return insuranceCompanyService.getInsuranceCompanies(authorization);
    }

    @GetMapping("/{id}")
    public InsuranceCompanyResponseDto getInsuranceCompany(@RequestHeader String authorization, @PathVariable Long id) {
        return insuranceCompanyService.getInsuranceCompany(authorization, id);
    }

    @PostMapping
    public void createInsuranceCompany(@RequestHeader String authorization, @RequestBody InsuranceCompanyRequestDto dto) {
        insuranceCompanyService.createInsuranceCompany(authorization, dto);
    }

    @PutMapping("/{id}")
    public void updateInsuranceCompany(@RequestHeader String authorization, @RequestBody InsuranceCompanyRequestDto dto, @PathVariable Long id) {
        insuranceCompanyService.updateInsuranceCompany(authorization, dto, id);
    }

    @DeleteMapping("/{id}")
    public void deleteInsuranceCompany(@RequestHeader String authorization, @PathVariable Long id) {
        insuranceCompanyService.deleteInsuranceCompany(authorization, id);
    }



}
