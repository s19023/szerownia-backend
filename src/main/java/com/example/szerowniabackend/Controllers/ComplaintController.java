package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.ChangeComplainStatusDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.ComplainRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ComplaintDTO;
import com.example.szerowniabackend.Services.ComplaintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping(path = "api/complaints")
public class ComplaintController {

    private final ComplaintService complaintService;

    @Autowired
    public ComplaintController(ComplaintService complaintService) {
        this.complaintService = complaintService;
    }

    @GetMapping
    public PaginatedDTO<ComplaintDTO> getComplaints(@RequestParam(defaultValue = "0") int page,
                                                    @RequestParam(defaultValue = "25") int howManyRecord) {
        return complaintService.getComplaints(page, howManyRecord);
    }

    @GetMapping("/my")
    public PaginatedDTO<ComplaintDTO> getMyComplaints(@RequestHeader String authorization,
                                                      @RequestParam(defaultValue = "0") int page,
                                                      @RequestParam(defaultValue = "25") int howManyRecord) {
        return complaintService.getMyComplaints(authorization, page, howManyRecord);
    }

    @GetMapping("/{id}")
    public ComplaintDTO getComplaint(@PathVariable("id") Long id) {
        return complaintService.getComplaint(id);
    }

    @PostMapping()
    public void createComplaint(@RequestHeader String authorization, @RequestBody ComplainRequestDTO dto) {
        complaintService.createComplaint(dto, authorization);
    }

    @PutMapping()
    public void changeComplaintStatus(@RequestHeader String authorization, @RequestBody ChangeComplainStatusDTO changeComplainStatusDTO) {
        complaintService.changeComplaintStatus(authorization, changeComplainStatusDTO);
    }

}
