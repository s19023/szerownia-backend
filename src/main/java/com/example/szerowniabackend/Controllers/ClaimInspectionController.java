package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.ClaimInspectionRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetClaims.ClaimAppraiserResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetClaims.ClaimsResponseDTO;
import com.example.szerowniabackend.Services.ClaimInspectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/inspection")
public class ClaimInspectionController {

    private final ClaimInspectionService claimInspectionService;

    @Autowired
    public ClaimInspectionController(ClaimInspectionService claimInspectionService) {
        this.claimInspectionService = claimInspectionService;
    }

    @PostMapping
    public void createClaimInspection(@RequestBody ClaimInspectionRequestDTO dto, @RequestHeader String authorization) {
        claimInspectionService.createClaimInspection(dto, authorization);
    }

    @GetMapping("/claimsForInspection")
    public List<ClaimAppraiserResponseDTO> getClaimsWaitingForInspection(@RequestHeader String authorization) {
        return claimInspectionService.getClaimsWaitingForInspection(authorization);
    }

}