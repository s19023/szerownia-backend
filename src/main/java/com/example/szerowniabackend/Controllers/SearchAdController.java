package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.*;
import com.example.szerowniabackend.Models.DTOs.Responses.GetSearchAd.SearchAdByIdDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetSearchAd.SearchAdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetSearchAd.SearchAdWithModTicketdDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.RentalAdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.RentalAdWithDescriptionDTO;
import com.example.szerowniabackend.Services.SearchAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/searchingAd")
public class SearchAdController {

    private SearchAdService searchAdService;

    @Autowired
    public SearchAdController(SearchAdService searchAdService) {
        this.searchAdService = searchAdService;
    }

    @GetMapping
    public PaginatedDTO<SearchAdResponseDTO> getAllSearchAds(@RequestParam(defaultValue = "0") int page,
                                                             @RequestParam(defaultValue = "25") int howManyRecord) {
        return searchAdService.getAllSearchAds(page, howManyRecord);
    }

    @GetMapping("{id}")
    public SearchAdByIdDTO getSearchAd(@PathVariable Long id, @RequestHeader(required = false) String authorization) {
        return searchAdService.getSearchAd(id, authorization);
    }

    @PostMapping("/{productsId}")
    public void createSearchAd(@RequestBody SearchAdCreateRequestDTO searchAdCreateRequestDTO, @RequestHeader String authorization, @PathVariable Long[] productsId) {
        searchAdService.createSearchAd(searchAdCreateRequestDTO, authorization, productsId);
    }

    @PutMapping("{searchAdId}")
    public void updateSearchAd(@PathVariable Long searchAdId, @RequestBody UpdateSearchAdRequestDTO newSearchAd, @RequestHeader String authorization) {
        searchAdService.updateRentalAd(searchAdId, newSearchAd, authorization);
    }

    @DeleteMapping("{id}")
    public void deleteSearchAd(@PathVariable Long id, @RequestHeader String authorization) {
        searchAdService.deleteSearchAd(id, authorization);
    }

    @PostMapping("/find")
    public PaginatedDTO<SearchAdResponseDTO> findSearchAd(@RequestBody RentalADSearchDTO searchRequestDTO,
                                                          @RequestParam(defaultValue = "0")
                                                                  int page,
                                                          @RequestParam(defaultValue = "25")
                                                                  int howManyRecords) {
        return searchAdService.getSearchAdByCriteria(searchRequestDTO, page, howManyRecords);
    }

    @GetMapping("mySearchAdsToCorrect")
    public PaginatedDTO<SearchAdWithModTicketdDTO> getMySearchAdsToCorrect(@RequestHeader String authorization,
                                                                           @RequestParam(defaultValue = "0") int page,
                                                                           @RequestParam(defaultValue = "25") int howManyRecord) {
        return searchAdService.getMySearchAdsToCorrect(page, howManyRecord, authorization);
    }


}
