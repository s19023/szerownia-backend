package com.example.szerowniabackend.Controllers;


import com.example.szerowniabackend.Models.DTOs.Requests.ProductCreateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.ProductUpdateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ProductByIdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ProductGetMyProductsResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ProductListResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.SuggestProductCategoryResponseDTO;
import com.example.szerowniabackend.Services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/products")
public class ProductController {

    final private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping()
    public List<ProductListResponseDTO> getAllProducts() {
        return productService.getProductsList();
    }

    @GetMapping("/{id}")
    public ProductByIdResponseDTO getProduct(@PathVariable Long id) {
        return productService.getProductById(id);
    }

    @PostMapping()
    public void createProduct(@RequestBody ProductCreateRequestDTO productCreateRequestDTO) {
        productService.createProduct(productCreateRequestDTO);
    }

    @PutMapping("/{id}")
    public void updateProduct(@PathVariable Long id, @RequestBody ProductUpdateRequestDTO productUpdateRequestDTO) {
        productService.updateProduct(id, productUpdateRequestDTO);
    }

    @GetMapping("myProducts")
    public List<ProductGetMyProductsResponseDTO> getMyProducts(@RequestParam(name="showSearch") boolean showSearch) {
        return productService.getMyProducts(showSearch);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
    }

    @GetMapping("/suggest/{name}")
    public List<SuggestProductCategoryResponseDTO> getSuggestedProductName(@PathVariable String name) {
        return productService.suggestProductCategory(name);
    }


}
