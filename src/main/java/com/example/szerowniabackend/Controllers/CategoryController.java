package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.CategoryCreateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.CategoryUpdateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.CategoryByIdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.CategoryListResponseDTO;
import com.example.szerowniabackend.Services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/categories")
public class CategoryController {

    final private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/")
    public List<CategoryListResponseDTO> getAllCategories() {
        return categoryService.getCategoryList();
    }

    @GetMapping("/{id}")
    public CategoryByIdResponseDTO getCategory(@PathVariable Long id) {
        return categoryService.getCategoryById(id);
    }

    @PostMapping("/")
    public void createCategory(@RequestBody CategoryCreateRequestDTO categoryCreateRequestDTO) {
        categoryService.createCategory(categoryCreateRequestDTO);
    }

    @PutMapping("/{id}")
    public void updateCategory(@PathVariable Long id, @RequestBody CategoryUpdateRequestDTO categoryUpdateRequestDTO) {
        categoryService.updateCategory(id, categoryUpdateRequestDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteCategory(@PathVariable Long id) {
        categoryService.deleteCategory(id);
    }

}
