package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.OpinionRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.OpinionResponseDTO;
import com.example.szerowniabackend.Models.Entities.Opinion;
import com.example.szerowniabackend.Services.OpinionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/opinions")
public class OpinionController {

    private OpinionService opinionService;

    @Autowired
    public OpinionController(OpinionService opinionService) {
        this.opinionService = opinionService;
    }

    @GetMapping()
    public List<OpinionResponseDTO> getAllOpinions() {
        return opinionService.getAllOpinions();
    }

    @GetMapping("/{id}")
    public OpinionResponseDTO getOpinion(@PathVariable Long id) {
        return opinionService.getOpinion(id);
    }

    @PostMapping()
    public void createOpinion(@RequestBody OpinionRequestDTO opinion) {
        opinionService.createOpinion(opinion);
    }

    @GetMapping("/")
    public List<OpinionResponseDTO> getOpinionForUser(@RequestParam Long userId) {
        return opinionService.getOpinionforUser(userId);
    }

    @PutMapping("/")
    public void updateOpinion(@RequestBody OpinionRequestDTO opinion) {
        opinionService.updateOpinion(opinion);
    }

    @DeleteMapping("/{id}")
    public void deleteOpinion(@PathVariable Long id) {
        opinionService.deleteOpinion(id);
    }


}
