package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.MessageNewThreadDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.MessageExistingThreadDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetMessage.MessageResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetMessage.ThreadResponeDTO;
import com.example.szerowniabackend.Services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/messages")
public class MessageController {

    private MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    public List<ThreadResponeDTO> getAllThreads(@RequestHeader String authorization) {
        return messageService.getAllThreads(authorization);
    }

    @GetMapping({"/thread/{threadId}"})
    public List<MessageResponseDTO> getAllMessagesFromThread(@RequestHeader String authorization, @PathVariable Long threadId) {
        return messageService.getAllMessagesFromThread(authorization, threadId);
    }

    @PostMapping
    public void createMessageForExistingThread(@RequestBody MessageExistingThreadDTO message, @RequestHeader String authorization) {
        messageService.createMessageForExistingThread(message, authorization);
    }

    @PostMapping("/request")
    public void createMessageForNewThread(@RequestBody MessageNewThreadDTO message, @RequestHeader String authorization) {
        messageService.createMessageForNewThread(message, authorization);
    }

}
