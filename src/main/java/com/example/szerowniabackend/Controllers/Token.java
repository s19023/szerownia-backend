package com.example.szerowniabackend.Controllers;


import com.example.szerowniabackend.Models.DTOs.Responses.RefreshTokenDTO;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import com.example.szerowniabackend.Security.RefreshToken;
import com.example.szerowniabackend.Services.RefreshTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("refreshToken")
public class Token {

    private final RefreshTokenService refreshTokenService;

    @Autowired
    public Token(RefreshTokenService refreshTokenService) {
        this.refreshTokenService = refreshTokenService;
    }

    @PostMapping
    public RefreshTokenDTO refreshToken(@RequestBody RefreshTokenDTO refreshToken){
        return refreshTokenService.refreshToken(refreshToken);
    }

}
