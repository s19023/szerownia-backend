package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.FeatureCreateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.FeatureUpdateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.FeatureByIdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.FeatureListResponseDTO;
import com.example.szerowniabackend.Services.FeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/features")
public class FeatureController {

    final private FeatureService featureService;

    @Autowired
    public FeatureController(FeatureService featureService) {
        this.featureService = featureService;
    }

    @GetMapping("/list/{idCategory}")
    public List<FeatureListResponseDTO> getAllFeatures(@PathVariable Long idCategory) {
        return featureService.getFeatureList(idCategory);
    }

    @GetMapping
    public List<FeatureListResponseDTO> list(){
        return featureService.getAllFeatures();
    }

    @GetMapping("/{idFeature}")
    public FeatureByIdResponseDTO getFeature(@PathVariable Long idFeature) {
        return featureService.getFeatureById(idFeature);
    }

    @PostMapping("/")
    public void createFeature(@RequestBody FeatureCreateRequestDTO featureCreateRequestDTO) {
        featureService.createFeature(featureCreateRequestDTO);
    }

    @PutMapping("/{id}")
    public void updateFeature(@PathVariable Long id, @RequestBody FeatureUpdateRequestDTO featureUpdateRequestDTO) {
        featureService.updateFeature(id, featureUpdateRequestDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteFeature(@PathVariable Long id) {
        featureService.deleteFeature(id);
    }


}
