package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.AssignAppraiserToClaimRequestDto;
import com.example.szerowniabackend.Models.DTOs.Requests.InsuranceAdjusterDecisionDTO;
import com.example.szerowniabackend.Services.InsuranceAdjusterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/insuranceadjuster")
public class InsuranceAdjusterController {

    private final InsuranceAdjusterService insuranceAdjusterService;

    @Autowired
    public InsuranceAdjusterController(InsuranceAdjusterService insuranceAdjusterService) {
        this.insuranceAdjusterService = insuranceAdjusterService;
    }

    @PostMapping("/assign")
    public void assignAppraiserToClaim(@RequestHeader String authorization, @RequestBody AssignAppraiserToClaimRequestDto dto) {
        insuranceAdjusterService.assignAppraiserToClaim(authorization, dto);
    }

    @PostMapping("/endInspections/{claimId}")
    public void endInspectionPhase(@RequestHeader String authorization, @PathVariable Long claimId) {
        insuranceAdjusterService.endInspectionPhase(authorization, claimId);
    }

    @PostMapping("/{claimId}")
    public UUID sendClaimToInsuranceCompany(@RequestHeader String authorization, @PathVariable Long claimId) {
        return insuranceAdjusterService.sendClaimToInsuranceCompany(authorization, claimId);
    }

    @PostMapping("/decision")
    public void makeDecisionOnClaim(@RequestHeader String authorization, @RequestBody InsuranceAdjusterDecisionDTO decision) {
        insuranceAdjusterService.makeDecisionOnClaim(authorization, decision);
    }
}
