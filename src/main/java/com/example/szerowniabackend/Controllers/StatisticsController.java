package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Responses.StatisticsDTO;
import com.example.szerowniabackend.Services.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/stats")
public class StatisticsController {

    private final StatisticsService statisticsService;

    @Autowired
    public StatisticsController(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @GetMapping("createdRentalAdStats")
    public StatisticsDTO getCreatedRentalAdStats() {
        return statisticsService.getCreatedRentalAdStats();
    }

    @GetMapping("rentStats")
    public StatisticsDTO getRentStats() {
        return statisticsService.getRentStats();
    }

    @GetMapping("createdUserStats")
    public StatisticsDTO getCreatedUserStats() {
        return statisticsService.getCreatedUserStats();
    }

    @GetMapping("rentalAdStats/{idRentalAd}")
    public Long getViewedRentalAdStats(@PathVariable Long idRentalAd) {
        return statisticsService.getViewedRentalAdStats(idRentalAd);
    }

    @GetMapping("totalAdView")
    public StatisticsDTO getTotalAdViews() {
        return statisticsService.getTotalAdViews();
    }

}
