package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.*;
import com.example.szerowniabackend.Models.DTOs.Responses.*;
import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.GetRentalAdByIdResponseDTO;
import com.example.szerowniabackend.Models.Entities.Enums.AdStatus;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Services.RentalAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping(path = "api/rentalAd")
public class RentalAdController {


    private final RentalAdService rentalAdService;

    @Autowired
    public RentalAdController(RentalAdService rentalAdService) {
        this.rentalAdService = rentalAdService;
    }

    @GetMapping
    public PaginatedDTO<RentalAdResponseDTO> getAllRentalAds(@RequestParam(defaultValue = "0") int page,
                                                             @RequestParam(defaultValue = "25") int howManyRecord) {
        return rentalAdService.getListOfRentalAd(page, howManyRecord);

    }

    @GetMapping("/{id}")
    public GetRentalAdByIdResponseDTO getRentalAdById(@PathVariable Long id) {
        return rentalAdService.getRentalAdById(id);
    }
    @GetMapping("/name/{id}")
    public String getRentalAdNameById(@PathVariable Long id) {
        return rentalAdService.getRentalAdNameById(id);
    }

    @GetMapping("/toFix")
    public List<RentalAdResponseDTO> getRentalAdToFix(@RequestHeader String authorization) {
        return rentalAdService.getRentalAdToFix(authorization);
    }

    @GetMapping("/homePage")
    public List<RentalAdResponseDTO> getRentalAdsForHomePage() {
        return rentalAdService.getListOfRentalAdForHomePage();
    }

    @PostMapping("/{arrayProductId}")
    public Long createNewRentalAd(@RequestBody RentalAdCreateRequestDTO rentalAdCreateRequestDTO,
                                  @RequestHeader String authorization,
                                  @PathVariable Long[] arrayProductId) {
        return rentalAdService.createRentalAd(rentalAdCreateRequestDTO, authorization, arrayProductId);
    }

    @PostMapping("/find")
    public List<RentalAdResponseDTO> findRentalAdByKeyWord(@RequestBody RentalAdFindByKeyWordAndCategoryDTO rentalAdFind) {
        return rentalAdService.getRentalAdByKeyWord(rentalAdFind);
    }


    @PutMapping("{rentalAdId}")
    public void updateRentalAd(@PathVariable Long rentalAdId, @RequestBody RentalAdUpdateRequestDTO updateRentalAd) {
        rentalAdService.updateRentalAd(rentalAdId, updateRentalAd);
    }

    @GetMapping("/promotedCount")
    public Long getPromotedCount() {
        return rentalAdService.getPromotedCount();
    }


//    @GetMapping("availableDates/{idRentalAd}")
//    public List<LocalDate> getAllAvailableDatesOfHire(@PathVariable Long idRentalAd, @RequestParam Integer year, @RequestParam Integer month) {
//        return rentalAdService.getAllAvailableDatesOfHire(idRentalAd, year, month);
//    }

    @GetMapping("availableDates/{idRentalAd}")
    public List<TimeSlotResponseDTO> getAllAvailableDatesOfHire(@PathVariable Long idRentalAd) {
        return rentalAdService.getAllAvailableDatesOfHire(idRentalAd);
    }

    @DeleteMapping("/{id}")
    public void deleteRentalAd(@PathVariable Long id) {
        rentalAdService.deleteRentalAd(id);

    }

    @GetMapping("/pickups/{id}")
    public List<PickupMethod> getPickupMethods(@PathVariable Long id) {
        return rentalAdService.getPickupMethods(id);
    }

    @GetMapping("history")
    public List<RentalAdWithDescriptionDTO> getHistory() {
        return rentalAdService.getHistory();
    }

    @GetMapping("myRentalAds")
    public PaginatedDTO<RentalAdWithDescriptionDTO> getMyRentalAds(@RequestParam(defaultValue = "0") int page,
                                                                   @RequestParam(defaultValue = "25") int howManyRecord) {
        return rentalAdService.getMyRentalAds(page, howManyRecord);

    }

    @GetMapping("myRentalAdsToCorrect")
    public PaginatedDTO<RentalAdWithModTicketDTO> getMyRentalAdsToCorrect(@RequestHeader String authorization,
                                                                          @RequestParam(defaultValue = "0") int page,
                                                                          @RequestParam(defaultValue = "25") int howManyRecord) {
        return rentalAdService.getMyRentalAdsToCorrect(page, howManyRecord, authorization);
    }


    @PostMapping("/findByParams")
    public PaginatedDTO<RentalAdResponseDTO> findRentalAdWithByParams(@RequestBody RentalADSearchDTO rentalADSearchDTO,
                                                                      @RequestParam(defaultValue = "0") int page,
                                                                      @RequestParam(defaultValue = "25") int howManyRecord) {
        return rentalAdService.getRentalAdWithByParams(rentalADSearchDTO, howManyRecord, page);
    }

    @GetMapping("/getStatus/{idRentalAd}")
    public AdStatus getRentalAdStatus(@PathVariable Long idRentalAd, @RequestHeader String authorization) {
        return rentalAdService.getRentalAdStatus(idRentalAd, authorization);
    }


    @GetMapping("/product/{productId}")
    public List<RentalAdResponseDTO> productHistory(@PathVariable Long productId) {
        return rentalAdService.productHistory(productId);
    }

}
