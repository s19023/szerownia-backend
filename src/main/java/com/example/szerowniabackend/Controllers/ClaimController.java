package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.ClaimRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetClaims.ClaimDetailsResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetClaims.ClaimsResponseDTO;
import com.example.szerowniabackend.Services.ClaimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/claims")
public class ClaimController {

    private final ClaimService claimService;

    @Autowired
    public ClaimController(ClaimService claimService) {
        this.claimService = claimService;
    }

    @GetMapping
    public List<ClaimsResponseDTO> getClaims(@RequestHeader String authorization) {
        return claimService.getClaims(authorization);
    }

    @GetMapping("/{id}")
    public ClaimDetailsResponseDTO getClaim(@RequestHeader String authorization, @PathVariable("id") Long id) {
        return claimService.getClaim(authorization, id);
    }


    @GetMapping("/name/{id}")
    public String getClaimNameById(@PathVariable Long id) {
        return claimService.getClaimNameById(id);
    }

    @PostMapping
    public void createClaim(@RequestBody ClaimRequestDTO dto, @RequestHeader String authorization) {
        claimService.createClaim(dto, authorization);
    }

    @GetMapping("/myClaims")
    public List<ClaimsResponseDTO> getMyClaims(@RequestHeader String authorization) {
        return claimService.getMyClaims(authorization);
    }

    @GetMapping("allUserClaims/{userId}")
    public PaginatedDTO<ClaimsResponseDTO> getAllUserClaims(@RequestHeader String authorization,
                                                            @PathVariable("userId") Long userId,
                                                            @RequestParam(defaultValue = "0") int page,
                                                            @RequestParam(defaultValue = "25") int howManyRecord) {
        return claimService.getAllUserClaims(authorization, userId, page, howManyRecord);
    }
}
