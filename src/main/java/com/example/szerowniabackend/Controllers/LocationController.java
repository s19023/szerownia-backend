package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.LocalizeRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.DistrictResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.RegionResponseDTO;
import com.example.szerowniabackend.Models.Entities.District;
import com.example.szerowniabackend.Models.Entities.Location;
import com.example.szerowniabackend.Services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/locations")
public class LocationController {

    final private LocationService lService;
    @Autowired
    public LocationController(LocationService locationService){ this.lService = locationService; }

    @GetMapping()
    public List<Location> getAllLocatoins(){ return lService.getAll(); }

    @GetMapping("/regions")
    public List<RegionResponseDTO> getAllRegions(){ return lService.getAllRegions(); }

    @GetMapping("/{id}")
    public Optional<Location> getLocById(@PathVariable Long id){ return lService.getById(id); }

    @PostMapping("/closest")
    public DistrictResponseDTO getClosest(@RequestBody LocalizeRequestDTO l){ return lService.getClosestDistrict(l); }

    @PostMapping()
    public void createLocation(Location location){ lService.save(location); }

    @PutMapping()
    public void updateLocation(Location location){ lService.save(location); }
    
    @DeleteMapping("/{id}")
    public void deleteLocation(@PathVariable Long id){ lService.deleteById(id); }
}
