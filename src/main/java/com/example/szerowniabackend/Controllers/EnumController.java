package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.Entities.Enums.*;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/datasets")
public class EnumController {

    @GetMapping("/eventTypes")
    public ObjectNode getEventTypes() {
        return createOutput(EventType.values());
    }

    @GetMapping("/featureTypes")
    public ObjectNode getFeatureTypes() {
        return createOutput(FeatureType.values());
    }

    @GetMapping("/ticketResults")
    public ObjectNode getModTicketResult() {
        return createOutput(ModTicketResult.values());
    }

    @GetMapping("/ticketSubject")
    public ObjectNode getModTicketSubject() {
        return createOutput(ModTicketSubject.values());
    }

    @GetMapping("/pickUpMethods")
    public ObjectNode getPickUpMethods() {
        return createOutput(PickupMethod.values());
    }

    @GetMapping("/answers")
    public ObjectNode getAnswers() {
        return createOutput(PredefinedAnswer.values());
    }

    @GetMapping("/roles")
    public ObjectNode getRoles() {
        return createOutput(Role.values());
    }

    @GetMapping("/claims")
    public ObjectNode getClaims() {
        return createOutput(ClaimStatus.values());
    }

    @GetMapping("/complaints")
    public ObjectNode getComplaints() {
        return createOutput(ComplaintReason.values());
    }

    @GetMapping("/complaintStatues")
    public ObjectNode getComplaintStatuses() {
        return createOutput(ComplainStatus.values());
    }

    @GetMapping("/hireStatusTypes")
    public ObjectNode getHireStatusTypes() {
        return createOutput(HireStatus.values());
    }

    private ObjectNode createOutput(Enum[] numnum) {
        List<String> values = getEnumValues(numnum);
        List<String> strings = getEnumToStrings(numnum);
        ObjectNode jn = JsonNodeFactory.instance.objectNode();

        for (int i = 0; i < strings.size(); i++) {
            jn.put(values.get(i), strings.get(i));
        }

        return jn;
    }

    private List<String> getEnumValues(Enum[] numnum) {
        return Stream.of(numnum).map(Enum::name).collect(Collectors.toList());
    }

    private List<String> getEnumToStrings(Enum[] numnum) {
        return Stream.of(numnum).map(Enum::toString).collect(Collectors.toList());
    }


}
