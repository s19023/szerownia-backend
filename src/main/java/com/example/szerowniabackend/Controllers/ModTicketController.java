package com.example.szerowniabackend.Controllers;


import com.example.szerowniabackend.Models.DTOs.Requests.ModTicketRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ModTicketResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ModTicketResponseWithObjectDTO;
import com.example.szerowniabackend.Services.ModTicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/tickets")
public class ModTicketController {

    private ModTicketService modTicketService;

    @Autowired
    public ModTicketController(ModTicketService modTicketService) {
        this.modTicketService = modTicketService;
    }

    @GetMapping
    public PaginatedDTO<ModTicketResponseDTO> getAllModTickets(@RequestParam(defaultValue = "0") int page,
                                                               @RequestParam(defaultValue = "25") int howManyRecord) {
        return modTicketService.getAllModTickets(page, howManyRecord);
    }

    @GetMapping("/count")
    public List<Integer> getCountofWaiting() {
        return modTicketService.getCountofWaitingTickets();
    }

    @GetMapping("/opinions")
    public PaginatedDTO<ModTicketResponseDTO> getOpinionsModTickets(@RequestParam int page, @RequestParam(required = false) String sort,
                                                            @RequestParam(required = false) boolean desc) {
        return modTicketService.getOpinionsModTickets(page, sort, desc);
    }

    @GetMapping("/ads")
    public PaginatedDTO<ModTicketResponseDTO> getAdsModTickets(@RequestParam int page, @RequestParam(required = false) String sort,
                                                       @RequestParam(required = false) boolean desc) {
        return modTicketService.getAdsModTickets(page, sort, desc);
    }

    @GetMapping("/users")
    public PaginatedDTO<ModTicketResponseDTO> getUsersModTickets(@RequestParam int page, @RequestParam(required = false) String sort,
                                                         @RequestParam(required = false) boolean desc) {
        return modTicketService.getUsersModTickets(page, sort, desc);
    }

    @GetMapping("/archival")
    public PaginatedDTO<ModTicketResponseDTO> getArchivalModTickets(@RequestParam(defaultValue = "0") int page,
                                                            @RequestParam(defaultValue = "25") int howManyRecord) {
        return modTicketService.getModifiedModTickets(page,howManyRecord);
    }


    @GetMapping("/{id}")
    public ModTicketResponseWithObjectDTO getModTicket(@PathVariable Long id) {
        return modTicketService.getModTicket(id);
    }

    @GetMapping("/name/{id}")
    public String getModTicketNameById(@PathVariable Long id) {
        return modTicketService.getModTicketNameById(id);
    }

    @PostMapping()
    public void createModTicket(@RequestBody ModTicketRequestDTO ticket) {
        modTicketService.createModTicket(ticket);
    }

    @PutMapping("/{id}")
    public void updateModTicket(@PathVariable Long id, @RequestBody ModTicketRequestDTO ticketRequestDTO) {
        modTicketService.updateModTicket(id, ticketRequestDTO);
    }

    @PostMapping("/{id}")
    public void moderateModTicket(@PathVariable Long id, @RequestBody String command, @RequestHeader String authorization,
                                  @RequestParam(required = false) String message) {
        modTicketService.moderateTicket(id, command, authorization, message);

    }

    @PostMapping("/verify/{id}")
    public void sendToVerify(@PathVariable Long id) {
        modTicketService.sendToVerify(id);
    }

    @DeleteMapping("/{id}")
    public void deleteModTicket(@PathVariable Long id) {
        modTicketService.deleteModTicket(id);
    }


}