package com.example.szerowniabackend.Controllers;

import com.example.szerowniabackend.Models.DTOs.Requests.DiscountRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.DiscountUpdateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.HireRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetDiscount.DiscountResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetHire.HireResponseDTO;
import com.example.szerowniabackend.Models.Entities.Discount;
import com.example.szerowniabackend.Services.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://172.21.40.105:80"})
@RestController
@RequestMapping("api/discounts")
public class DiscountController {

    private DiscountService discountService;

    @Autowired
    public DiscountController(DiscountService discountService) {
        this.discountService = discountService;
    }

    @GetMapping
    public List<DiscountResponseDTO> getAllDiscounts() {
        return discountService.getAllDiscounts();
    }

    @GetMapping("{id}")
    public DiscountResponseDTO getDiscount(@PathVariable Long id) {
        return discountService.getDiscount(id);
    }

    @PostMapping()
    public Long createDiscount(@RequestBody DiscountRequestDTO discount) {
        return discountService.createDiscount(discount);
    }

    @PutMapping("{id}")
    public void updateDiscount(@PathVariable Long id, @RequestBody DiscountUpdateRequestDTO discount) {
        discountService.updateDiscount(id, discount);
    }

    @DeleteMapping("/{id}")
    public void deleteRentalAd(@PathVariable Long id) {
        discountService.deleteDiscount(id);
    }
}
