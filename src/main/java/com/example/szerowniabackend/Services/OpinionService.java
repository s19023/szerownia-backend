package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Email.EmailSender;
import com.example.szerowniabackend.Models.DTOs.Requests.OpinionRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.OpinionResponseDTO;
import com.example.szerowniabackend.Models.Entities.Enums.ModTicketSubject;
import com.example.szerowniabackend.Models.Entities.Hire;
import com.example.szerowniabackend.Models.Entities.ModTicket;
import com.example.szerowniabackend.Models.Entities.Opinion;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.ProfanityFilter.ProfanityFilter;
import com.example.szerowniabackend.Models.Repositories.HireRepo;
import com.example.szerowniabackend.Models.Repositories.ModTicketRepo;
import com.example.szerowniabackend.Models.Repositories.OpinionRepo;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class OpinionService {

    private final ModelMapper mapper;
    private OpinionRepo opinionRepo;
    private final UserRepo userRepo;
    private final HireRepo hireRepo;
    private final ModTicketRepo modTicketRepo;
    private final EmailSender emailSender;

    @Autowired
    public OpinionService(OpinionRepo opinionRepo, UserRepo userRepo, HireRepo hireRepo, ModTicketRepo modTicketRepo, EmailSender emailSender) {
        this.opinionRepo = opinionRepo;
        this.userRepo = userRepo;
        this.hireRepo = hireRepo;
        this.modTicketRepo = modTicketRepo;
        this.emailSender = emailSender;
        mapper = new ModelMapper();
    }

    public List<OpinionResponseDTO> getAllOpinions() {
        List<OpinionResponseDTO> list = new ArrayList<>();

        for (Opinion o : opinionRepo.findAll()) {
            ModelMapper mapper = new ModelMapper();
            OpinionResponseDTO opinionResponseDTO = mapper.map(o,OpinionResponseDTO.class);
            list.add(opinionResponseDTO);
        }
        return list;
    }

    public OpinionResponseDTO getOpinion(Long id) {
        Opinion opinion = AService.Get(Opinion.class, opinionRepo, id);
        return mapper.map(opinion, OpinionResponseDTO.class);
    }

    public List<OpinionResponseDTO> getOpinionforUser(Long id) {
        List<OpinionResponseDTO>  list = new ArrayList<>();
        User user = AService.Get(User.class, userRepo, id);

        for (Opinion o : opinionRepo.getAllByTaker(user)){
            OpinionResponseDTO responseDTO = mapper.map(o,OpinionResponseDTO.class);
            list.add(responseDTO);
        }
        return list;
    }

    public void updateOpinion(OpinionRequestDTO o) {
        if (o.getAuthorId() != o.getTakerId()) {
            Opinion opinion = AService.Get(Opinion.class, opinionRepo, o.getId());
            User taker = AService.Get(User.class, userRepo, o.getTakerId());

            opinion.setRating(o.getRating());
            opinion.setComment(o.getComment());

            opinionRepo.save(opinion);
            taker.setTaken(Collections.singleton(opinion));
        }
    }

    public void createOpinion(OpinionRequestDTO o) {
        if (o.getAuthorId() != o.getTakerId()) {

            User taker = AService.Get(User.class, userRepo, o.getTakerId());
            User author = AService.Get(User.class, userRepo, o.getAuthorId());
            Hire hire = AService.Get(Hire.class, hireRepo, o.getHireId());

            Opinion opinion = new Opinion(o.getRating(), o.getComment(), o.isAboutSharer(),
                    author, taker, hire);

            ProfanityFilter.getCensoredText(o.getComment());
            if (ProfanityFilter.getIsVulgar()) {
                opinion.setVisible(false);
                opinionRepo.save(opinion);

                ModTicket modTicket = new ModTicket();
                modTicket.setOpinion(opinion);
                modTicket.setSubject(ModTicketSubject.OFFENDED);
                modTicket.setDescription("Opinion about user contains vulgar word/s");
                modTicketRepo.save(modTicket);

                taker.setTaken(Collections.singleton(opinion));

                throw new Opinion.ContainsVulgar();
            }
            opinionRepo.save(opinion);
            taker.setTaken(Collections.singleton(opinion));
            emailSender.sendMessageWhenUserGetOpinion(taker.getEmail());
        }
    }

    public void deleteOpinion(Long id) {
        AService.Get(Opinion.class, opinionRepo, id).setVisible(false);
    }
}
