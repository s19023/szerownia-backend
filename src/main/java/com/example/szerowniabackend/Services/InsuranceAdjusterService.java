package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Models.DTOs.Requests.AssignAppraiserToClaimRequestDto;
import com.example.szerowniabackend.Models.DTOs.Requests.InsuranceAdjusterDecisionDTO;
import com.example.szerowniabackend.Models.Entities.Claim;
import com.example.szerowniabackend.Models.Entities.Enums.ClaimStatus;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.Repositories.ClaimRepo;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import com.example.szerowniabackend.Security.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
@Transactional
public class InsuranceAdjusterService {

    private UserRepo userRepo;
    private ClaimRepo claimRepo;
    private final Authorization authorization;

    @Autowired
    public InsuranceAdjusterService(UserRepo userRepo, ClaimRepo claimRepo, Authorization authorization) {
        this.userRepo = userRepo;
        this.claimRepo = claimRepo;
        this.authorization = authorization;
    }

    public void assignAppraiserToClaim(String authorization, AssignAppraiserToClaimRequestDto dto) {
        Long id = this.authorization.getIdUserFromToken(authorization);
        User insuranceAdjuster = AService.Get(User.class, userRepo, id);
        if (!insuranceAdjuster.getRole().equals(Role.INSURANCE_ADJUSTER)) {
            throw new User.PermissionDenied();
        }

        Claim claim = AService.Get(Claim.class, claimRepo, dto.getClaimId());
        if (!claim.getInsuranceAdjuster().equals(insuranceAdjuster)) {
            throw new User.PermissionDenied();
        }

        User appraiser = AService.Get(User.class, userRepo, dto.getAppraiserId());
        if (!appraiser.getRole().equals(Role.APPRAISER)) {
            throw new BadRequestException("Niepoprawny rzeczoznawca");
        }

        if (!claim.getStatus().equals(ClaimStatus.SUBMITTED) || claim.getStatus().equals(ClaimStatus.IN_PROGRESS_AFTER_INSPECTION)) {
            throw new User.InterruptedClaimWorkflow();
        }

        claim.setDateOfAssignToTheAppraiser(LocalDateTime.now());
        claim.setTempAppraiser(appraiser);
        claim.setStatus(ClaimStatus.IN_PROGRESS_BEFORE_INSPECTION);
    }

    public void endInspectionPhase(String authorization, Long claimId) {
        Long insuranceAdjusterId = this.authorization.getIdUserFromToken(authorization);
        User insuranceAdjuster = AService.Get(User.class, userRepo, insuranceAdjusterId);
        Claim claim = AService.Get(Claim.class, claimRepo, claimId);

        if (!insuranceAdjuster.equals(claim.getInsuranceAdjuster())) {
            throw new Claim.YouDontHavePermissionToClaim();
        }

        claim.setStatus(ClaimStatus.IN_PROGRESS_AFTER_INSPECTION);
    }

    public UUID sendClaimToInsuranceCompany(String authorization, Long claimId) {
        long id = this.authorization.getIdUserFromToken(authorization);
        Claim claim = AService.Get(Claim.class, claimRepo, claimId);
        User user = AService.Get(User.class, userRepo, id);

        if (!user.equals(claim.getInsuranceAdjuster()))
            throw new User.InsuranceAdjusterNotRunningThisClaim();

        if (claim.getInsuranceCaseId() != null)
            throw new User.ClaimAlreadyHaveCaseId();

        if (claim.getStatus() != ClaimStatus.IN_PROGRESS_AFTER_INSPECTION) {
            throw new Claim.InvalidWorkflow("Nieprawidłowy przepływ rozwiązywania szkody. Brak wykonanych oględzin");
        }
        claim.setClaimAdjusterSendDate(LocalDateTime.now());
        UUID insuranceCaseId = UUID.randomUUID();
        claim.setInsuranceCaseId(insuranceCaseId);

        return insuranceCaseId;
    }

    public void makeDecisionOnClaim(String authorization, InsuranceAdjusterDecisionDTO decision) {
        Long id = this.authorization.getIdUserFromToken(authorization);
        User user = AService.Get(User.class, userRepo, id);
        Claim claim = AService.Get(Claim.class, claimRepo, decision.getClaimId());

        if (!user.equals(claim.getInsuranceAdjuster()))
            throw new User.InsuranceAdjusterNotRunningThisClaim();

        ClaimStatus status = claim.getStatus();

        if (status.equals(ClaimStatus.ACCEPTED) || status.equals(ClaimStatus.REJECTED) || (claim.getInsuranceCaseId() == null && decision.getDecision().equals("accepted"))) {
            throw new User.InterruptedClaimWorkflow();
        }

        String justification = decision.getJustification();

        if (justification == null || justification.isEmpty()) {
            throw new Claim.ClaimRequiresJustification();
        }

        claim.setJustification(justification);

        switch (decision.getDecision()) {
            case "accepted":
                claim.setStatus(ClaimStatus.ACCEPTED);
                break;
            case "rejected":
                claim.setStatus(ClaimStatus.REJECTED);
                break;
            default:
                throw new User.InvalidValue();
        }
    }
}
