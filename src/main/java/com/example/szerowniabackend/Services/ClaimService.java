package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Models.DTOs.Requests.ClaimRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetClaims.ClaimDetailsResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetClaims.ClaimInspectionResDto;
import com.example.szerowniabackend.Models.DTOs.Responses.GetClaims.ClaimsResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetClaims.UserAppraiserDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.UserOpinionsResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.UserRespDto;
import com.example.szerowniabackend.Models.DTOs.Responses.UserResponseDTO;
import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Repositories.ClaimRepo;
import com.example.szerowniabackend.Models.Repositories.PolicyRepo;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import com.example.szerowniabackend.Security.Authorization;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static com.example.szerowniabackend.Services.AService.Get;

@Service
@Transactional
public class ClaimService {

    private final ClaimRepo claimRepo;
    private final UserRepo userRepo;
    private final PolicyRepo policyRepo;
    private final Authorization authorization;
    private final ModelMapper mapper;

    @Autowired
    public ClaimService(ClaimRepo claimRepo, UserRepo userRepo, PolicyRepo policyRepo, Authorization authorization) {
        this.claimRepo = claimRepo;
        this.userRepo = userRepo;
        this.policyRepo = policyRepo;
        this.authorization = authorization;
        mapper = new ModelMapper();
    }

    public List<ClaimsResponseDTO> getClaims(String authorization) {
        Long userId = this.authorization.getIdUserFromToken(authorization);
        User user = Get(User.class, userRepo, userId);

        if (!user.getRole().equals(Role.ADMIN) && !user.getRole().equals(Role.INSURANCE_ADJUSTER)) {
            throw new ForbiddenException();
        }

        List<ClaimsResponseDTO> result = new ArrayList<>();
        if (user.getRole().equals(Role.ADMIN)) {
            for (Claim claim : claimRepo.findAll()) {
                ClaimsResponseDTO claimsResponseDTO = mapper.map(claim, ClaimsResponseDTO.class);
                claimsResponseDTO.setTitle(claim.getPolicy().getHire().getAd().getTitle());
                setAppraiserToClaimDTO(claim, claimsResponseDTO);
                setBorrowerToClaimDTO(claim, claimsResponseDTO);
                result.add(claimsResponseDTO);
            }
        }

        if (user.getRole().equals(Role.INSURANCE_ADJUSTER)) {
            for (Claim claim : claimRepo.findAll()) {
                if (claim.getInsuranceAdjuster().equals(user)) {
                    ClaimsResponseDTO claimsResponseDTO = mapper.map(claim, ClaimsResponseDTO.class);
                    claimsResponseDTO.setTitle(claim.getPolicy().getHire().getAd().getTitle());
                    setAppraiserToClaimDTO(claim, claimsResponseDTO);
                    setBorrowerToClaimDTO(claim, claimsResponseDTO);
                    result.add(claimsResponseDTO);
                }
            }
        }
        return result;
    }

    public ClaimDetailsResponseDTO getClaim(String authorization, Long id) {
        Long userId = this.authorization.getIdUserFromToken(authorization);
        User user = Get(User.class, userRepo, userId);
        Claim claim = Get(Claim.class, claimRepo, id);

        if (!user.getRole().equals(Role.ADMIN) && !user.getRole().equals(Role.INSURANCE_ADJUSTER) && !user.getRole().equals(Role.APPRAISER) && !user.equals(claim.getUser())) {
            throw new Claim.YouDontHavePermissionToClaim();
        }

        if (user.getRole().equals(Role.INSURANCE_ADJUSTER) && !user.equals(claim.getInsuranceAdjuster())) {
            throw new Claim.YouDontHavePermissionToClaim();
        }

        ClaimDetailsResponseDTO claimsResponseDTO = mapper.map(claim, ClaimDetailsResponseDTO.class);
        claimsResponseDTO.setTitle(claim.getPolicy().getHire().getAd().getTitle());

        setAppraiserToClaimDetailsDTO(claim, claimsResponseDTO);
        setBorrowerToClaimDetailsDTO(claim, claimsResponseDTO);
        return claimsResponseDTO;
    }


    public void createClaim(ClaimRequestDTO dto, String authorization) {
        Long userId = this.authorization.getIdUserFromToken(authorization);
        User user = Get(User.class, userRepo, userId);
        Policy policy = Get(Policy.class, policyRepo, dto.getIdPolicy());

        if (!user.getPremium()) {
            throw new ForbiddenException("Usługa dostępna tylko dla użytkowników premium");
        }

        if (!policy.getHire().getSharer().equals(user))
            throw new ForbiddenException();

        if (!(dto.getClaimDate().isAfter(policy.getDateFrom()) && dto.getClaimDate().isBefore(policy.getDateTo())))
            throw new Claim.GivenClaimDateIsNotInHireRange();

        Location newLocation = mapper.map(dto.getLocation(), Location.class);
        Claim newClaim = mapper.map(dto, Claim.class);
        newClaim.setLocation(newLocation);
        newClaim.setUser(user);
        newClaim.setPolicy(policy);

        int randomInsuranceAdjusterFromCompanyAssignToCompany = (int) (Math.random() * policy.getInsuranceCompany().getInsuranceAdjusters().size());
        newClaim.setInsuranceAdjuster(policy.getInsuranceCompany().getInsuranceAdjusters().get(randomInsuranceAdjusterFromCompanyAssignToCompany));

        claimRepo.save(newClaim);
    }

    private void setBorrowerToClaimDTO(Claim claim, ClaimsResponseDTO claimsResponseDTO) {
        UserRespDto userResponseDTO = mapper.map(claim.getPolicy().getHire().getBorrower(), UserRespDto.class);
        claimsResponseDTO.setBorrower(userResponseDTO);
    }

    private void setAppraiserToClaimDTO(Claim claim, ClaimsResponseDTO claimsResponseDTO) {

        if (claim.getClaimInspections() == null || claim.getClaimInspections().isEmpty()) {
            if (claim.getTempAppraiser() == null) {
                claimsResponseDTO.setAppraiser(null);
            } else {
                UserAppraiserDTO userAppraiserDTO = mapper.map(claim.getTempAppraiser(), UserAppraiserDTO.class);
                claimsResponseDTO.setAppraiser(userAppraiserDTO);
            }
        } else {
            UserAppraiserDTO userAppraiserDTO = mapper.map(claim.getClaimInspections().get(0).getAppraiser(), UserAppraiserDTO.class);
            claimsResponseDTO.setAppraiser(userAppraiserDTO);
        }
    }

    private void setBorrowerToClaimDetailsDTO(Claim claim, ClaimDetailsResponseDTO claimsResponseDTO) {
        UserRespDto userResponseDTO = mapper.map(claim.getPolicy().getHire().getBorrower(), UserRespDto.class);
        claimsResponseDTO.setBorrower(userResponseDTO);
    }

    private void setAppraiserToClaimDetailsDTO(Claim claim, ClaimDetailsResponseDTO claimsResponseDTO) {

        if (claim.getClaimInspections() == null || claim.getClaimInspections().isEmpty()) {
            if (claim.getTempAppraiser() == null) {
                claimsResponseDTO.setAppraiser(null);
            } else {
                UserAppraiserDTO userAppraiserDTO = mapper.map(claim.getTempAppraiser(), UserAppraiserDTO.class);
                claimsResponseDTO.setAppraiser(userAppraiserDTO);
            }
        } else {
            UserAppraiserDTO userAppraiserDTO = mapper.map(claim.getClaimInspections().get(0).getAppraiser(), UserAppraiserDTO.class);
            claimsResponseDTO.setAppraiser(userAppraiserDTO);
        }
    }

    public List<ClaimsResponseDTO> getMyClaims(String authorization) {
        Long userId = this.authorization.getIdUserFromToken(authorization);
        User user = Get(User.class, userRepo, userId);

        if (user.getRole() != Role.USER) {
            throw new ForbiddenException();
        }

        List<ClaimsResponseDTO> result = new ArrayList<>();
        for (Claim claim : claimRepo.findAll()) {
            ClaimsResponseDTO claimsResponseDTO = mapper.map(claim, ClaimsResponseDTO.class);
            claimsResponseDTO.setTitle(claim.getPolicy().getHire().getAd().getTitle());
            setAppraiserToClaimDTO(claim, claimsResponseDTO);

            result.add(claimsResponseDTO);
        }

        return result;
    }

    public PaginatedDTO<ClaimsResponseDTO> getAllUserClaims(String authorization, Long userId, int page, int howManyRecord) {
        Long currentUserId = this.authorization.getIdUserFromToken(authorization);
        User currentUser = Get(User.class, userRepo, currentUserId);

        if (currentUser.getRole().equals(Role.USER)) {
            throw new ForbiddenException();
        }

        User user = Get(User.class, userRepo, userId);
        Page<Claim> claims =  claimRepo.findAllByUser(user, PageRequest.of(page, howManyRecord));

        List<ClaimsResponseDTO> result = new ArrayList<>();
        for (Claim claim : claims) {
            ClaimsResponseDTO claimsResponseDTO = mapper.map(claim, ClaimsResponseDTO.class);
            claimsResponseDTO.setTitle(claim.getPolicy().getHire().getAd().getTitle());
            result.add(claimsResponseDTO);
        }

        return new PaginatedDTO<>((long) result.size(), claims.getTotalPages(), result);
    }

    public String getClaimNameById(Long id) {
        return Get(Claim.class,claimRepo,id).getPolicy().getHire().getAd().getTitle();
    }
}
