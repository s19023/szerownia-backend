package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Email.EmailSender;
import com.example.szerowniabackend.Models.DTOs.Requests.MessageNewThreadDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.ModTicketRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.*;
import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.Entities.Enums.ModTicketResult;
import com.example.szerowniabackend.Models.Entities.Enums.NotificationStatus;
import com.example.szerowniabackend.Models.Entities.Enums.PredefinedAnswer;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Security.Authorization;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

import static com.example.szerowniabackend.Services.AService.Get;

@Service
public class ModTicketService {

    private static final int PAGE_SIZE = 15;

    private ModTicketRepo modTicketRepo;
    private RentalAdRepo rentalAdRepo;
    private SearchAdRepo searchAdRepo;
    private UserRepo userRepo;
    private OpinionRepo opinionRepo;
    private MessageService messageService;
    private NotificationRepo notificationRepo;
    private Authorization authorization;
    final private ModelMapper mapper;
    private EmailSender emailSender;

    @Autowired
    public ModTicketService(ModTicketRepo modTicketRepo, RentalAdRepo rentalAdRepo, SearchAdRepo searchAdRepo, UserRepo userRepo,
                            OpinionRepo opinionRepo, MessageService messageService, NotificationRepo notificationRepo, Authorization authorization, EmailSender emailSender) {
        this.modTicketRepo = modTicketRepo;
        this.rentalAdRepo = rentalAdRepo;
        this.searchAdRepo = searchAdRepo;
        this.userRepo = userRepo;
        this.opinionRepo = opinionRepo;
        this.messageService = messageService;
        this.notificationRepo = notificationRepo;
        this.authorization = authorization;
        this.emailSender = emailSender;
        this.mapper = new ModelMapper();
    }

    public PaginatedDTO<ModTicketResponseDTO> getAllModTickets(int page, int howManyRecord) {

        Page<ModTicket> modTicketPage = modTicketRepo.findAllModTickets(PageRequest.of(page, howManyRecord));
        List<ModTicketResponseDTO> list = new ArrayList<>();

        for (ModTicket t : modTicketPage) {
            list.add(mapModTicket(t));
        }

        PaginatedDTO<ModTicketResponseDTO> paginatedDTO = new PaginatedDTO<>((long) list.size(), modTicketPage.getTotalPages(), list);

        return paginatedDTO;
    }

    public List<Integer> getCountofWaitingTickets() {
        Integer[] tab = Collections.nCopies(3, 0).toArray(new Integer[0]);

        for (ModTicket t : modTicketRepo.findAll()) {
            if (t.getOpinion() != null) {
                checkIfWaiting(tab, t, 0);
            }
            if (t.getAd() != null) {
                checkIfWaiting(tab, t, 1);
            }
            if (t.getReported() != null) {
                checkIfWaiting(tab, t, 2);
            }
        }
        return Arrays.asList(tab);
    }

    public PaginatedDTO<ModTicketResponseDTO> getOpinionsModTickets(int page, String sort, boolean desc) {
        List<ModTicketResponseDTO> list = new ArrayList<>();
        Sort sorting = getSorting(sort, desc);
        Page<ModTicket> ticketPage = modTicketRepo.getAllByOpinionNotNull(PageRequest.of(page, PAGE_SIZE, sorting));

        for (ModTicket ticket : ticketPage) {
            mapAndAddModTicket(ticket, list);
        }

        return new PaginatedDTO<>((long) list.size(), ticketPage.getTotalPages(), list);
    }

    public PaginatedDTO<ModTicketResponseDTO> getAdsModTickets(int page, String sort, boolean desc) {
        List<ModTicketResponseDTO> list = new ArrayList<>();
        Sort sorting = getSorting(sort, desc);
        Page<ModTicket> ticketPage = modTicketRepo.getAllByAdNotNull(PageRequest.of(page, PAGE_SIZE, sorting));

        for (ModTicket ticket : ticketPage) {
            mapAndAddModTicket(ticket, list);
        }

        return new PaginatedDTO<>((long) list.size(), ticketPage.getTotalPages(), list);
    }

    public PaginatedDTO<ModTicketResponseDTO> getUsersModTickets(int page, String sort, boolean desc) {
        List<ModTicketResponseDTO> list = new ArrayList<>();
        Sort sorting = getSorting(sort, desc);
        Page<ModTicket> ticketPage = modTicketRepo.getAllByReportedNotNull(PageRequest.of(page, PAGE_SIZE, sorting));

        for (ModTicket ticket : ticketPage) {
            mapAndAddModTicket(ticket, list);
        }

        return new PaginatedDTO<>((long) list.size(), ticketPage.getTotalPages(), list);
    }

    public PaginatedDTO<ModTicketResponseDTO> getModifiedModTickets(int page, int howManyRecord) {

        List<ModTicketResponseDTO> list = new ArrayList<>();
        Page<ModTicket> modTicketPage = modTicketRepo.findArchiveModTickets(PageRequest.of(page, howManyRecord));

        for (ModTicket t : modTicketPage) {
            list.add(mapModTicket(t));
        }

        PaginatedDTO<ModTicketResponseDTO> paginatedDTO = new PaginatedDTO<>((long) list.size(), modTicketPage.getTotalPages(), list);

        return paginatedDTO;
    }

    public ModTicketResponseWithObjectDTO getModTicket(Long id) {
        ModTicket modTicket = Get(ModTicket.class, modTicketRepo, id);
        ModTicketResponseWithObjectDTO modTicketDto = mapper.map(modTicket, ModTicketResponseWithObjectDTO.class);
        modTicketDto.setTicketSubject(modTicket.getSubject());

        if (modTicket.getReported() != null) {
            ModTicketUserDTO user = mapper.map(modTicket.getReported(), ModTicketUserDTO.class);
            modTicketDto.setReported(user);
        }

        if (modTicket.getOpinion() != null) {
            ModTicketOpinionDTO opinion = mapper.map(modTicket.getOpinion(), ModTicketOpinionDTO.class);
            opinion.setAuthor(mapper.map(modTicket.getOpinion().getAuthor(), ModTicketUserDTO.class));
            modTicketDto.setOpinion(opinion);
        }

        if (modTicket.getAd() != null) {
            Ad rentalAd = modTicket.getAd();
            RentalAdWithDescriptionDTO mapRentalAdToDTO = mapper.map(rentalAd, RentalAdWithDescriptionDTO.class);
            List<Long> imagesIdList = new ArrayList<>();
            for (ImageEntity imageEntity : rentalAd.getImageEntity()) {
                imagesIdList.add(imageEntity.getId());
            }
            mapRentalAdToDTO.setImagesIdList(imagesIdList);
            modTicketDto.setAd(mapRentalAdToDTO);
        }
        return modTicketDto;
    }

    public void createModTicket(ModTicketRequestDTO t) {
        if (t.getNotifierId() == t.getReportedId()) throw new ModTicket.ReportedYourself();
        //TODO author from token?
        Optional<User> authorOptional = userRepo.findById(t.getNotifierId());
        if (authorOptional.isEmpty()) throw new ModTicket.AuthorMissing();

        User author = authorOptional.get();

        if (t.getAdId() != null) {

            if (t.getIsSearchAd()) {
                Optional<SearchAd> optionalSearchAd = searchAdRepo.findById(t.getAdId());
                if (optionalSearchAd.isEmpty()) {
                    throw new ModTicket.ParameterNotFound("ogłoszenia użyczenia");
                }
                modTicketRepo.save(new ModTicket(t.getSubject(), t.getDesc(), author, optionalSearchAd.get()));
            } else {
                Optional<RentalAd> optionalRentalAd = rentalAdRepo.findById(t.getAdId());
                if (optionalRentalAd.isEmpty()) {
                    throw new ModTicket.ParameterNotFound("ogłoszenia wypożyczenia");
                }
                modTicketRepo.save(new ModTicket(t.getSubject(), t.getDesc(), author, optionalRentalAd.get()));
            }

        } else if (t.getReportedId() != null) {
            Optional<User> reportedOptional = userRepo.findById(t.getReportedId());
            if (reportedOptional.isEmpty())
                throw new ModTicket.ParameterNotFound("użytkownika do zgłoszenia");

            modTicketRepo.save(new ModTicket(t.getSubject(), t.getDesc(), author, reportedOptional.get()));

        } else if (t.getOpinionId() != null) {
            Optional<Opinion> opinion = opinionRepo.findById(t.getOpinionId());
            if (opinion.isEmpty())
                throw new ModTicket.ParameterNotFound("opinii");

            modTicketRepo.save(new ModTicket(t.getSubject(), t.getDesc(), author, opinion.get()));
        }
    }

    public void updateModTicket(Long id, ModTicketRequestDTO t) {
        ModTicket modTicket = Get(ModTicket.class, modTicketRepo, id);

        modTicket.setSubject(t.getSubject());
        modTicket.setDescription(t.getDesc());
        modTicketRepo.save(modTicket);
    }

    public void moderateTicket(Long id, String command, String token, String message) {

        ModTicketResult result;
        message = message == null ? "" : message;

        try {
            result = ModTicketResult.valueOf(command);
        } catch (IllegalArgumentException e) {
            throw new ModTicket.BadCommand(command);
        }

        ModTicket ticket = Get(ModTicket.class, modTicketRepo, id);

        if (result == ModTicketResult.HIDDEN || result == ModTicketResult.TO_MODIFY) {
            hideSubject(ticket, true);

            if (result == ModTicketResult.TO_MODIFY) {
                sendNotification(ticket.getAd().getUser(), "Twoje ogłoszenie zostało odesłane do poprawy przez moderatora");
                sendNewMessage(token, ticket, PredefinedAnswer.EDIT.toString() + "\n" + message);
            } else {
                sendNewMessage(token, ticket, PredefinedAnswer.HIDE.toString() + "\n" + message);
                if (ticket.getReported() != null)
                    emailSender.sendMessageWhenUserBlocked(ticket.getReported().getEmail(),message);
            }
        }

        if (result == ModTicketResult.MODIFIED) {
            hideSubject(ticket, false);
            sendNotification(ticket.getAd().getUser(), "Twoje ogłoszenie zostało zatwierdzone przez moderatora");
//              sendMessage(token, ticket, PredefinedAnswer.MODIFIED.toString());
        }
        ticket.setModerationDate(LocalDate.now());
        saveAndSend(result, ticket);
    }

    public void sendToVerify(Long id) {
        User user = AService.logged();
        Role role = user.getRole();
        ModTicket ticket = Get(ModTicket.class, modTicketRepo, id);

        if (role == Role.USER && ticket.getResult().equals(ModTicketResult.TO_MODIFY) && isAboutUser(user, ticket)) {
            saveAndSend(ModTicketResult.TO_VERIFY, ticket);
        } else {
            throw new ModTicket.NotYours();
        }
    }

    private boolean isAboutUser(User user, ModTicket ticket) {
        Long id = user.getId();

        if(ticket.getAd() != null ) return ticket.getAd().getUser().getId() == id;
        if(ticket.getReported() != null) return  ticket.getReported().getId() == id;
        if(ticket.getOpinion() !=  null) return ticket.getOpinion().getAuthor().getId() == id;

        return false;
    }

    public void deleteModTicket(Long id) {
        modTicketRepo.deleteById(id);
    }

    private boolean onlyOneNull(ModTicketRequestDTO t) {
        int i = 0;
        if (t.getAdId() == null) i++;
        if (t.getOpinionId() == null) i++;
        if (t.getReportedId() == null) i++;
        return i == 1;
    }

    private void sendNewMessage(String auth, ModTicket ticket, String text) {
        messageService.createMessageForNewThread(new MessageNewThreadDTO(text, null, ticket.getId()), auth);
    }

    private void sendMessage(String auth, ModTicket ticket, String text) {

//        messageService.createMessageForExistingThread(new MessageExistingThreadDTO(), auth);
    }

    private void saveAndSend(ModTicketResult result, ModTicket ticket) {
        ticket.setResult(result);
        modTicketRepo.save(ticket);
    }

    private void hideSubject(ModTicket ticket, boolean b) {
        if (ticket.getAd() != null) ticket.getAd().setVisible(!b);
        if (ticket.getReported() != null) ticket.getReported().setBlocked(b);
        if (ticket.getOpinion() != null) ticket.getOpinion().setVisible(!b);
    }

    private void mapAndAddModTicket(ModTicket t, List<ModTicketResponseDTO> list) {
        ModTicketResponseDTO modTicketResponseDTO = mapModTicket(t);
        if (t.getResult() == null) {
            list.add(modTicketResponseDTO);
        } else if (t.getResult() == ModTicketResult.TO_VERIFY) {
            list.add(modTicketResponseDTO);
        }
    }

    private ModTicketResponseDTO mapModTicket(ModTicket t) {
        ModTicketResponseDTO modTicketResponseDTO = mapper.map(t, ModTicketResponseDTO.class);
        if (t.getNotifier() != null) {
            ModTicketUserDTO notifier = mapper.map(t.getNotifier(), ModTicketUserDTO.class);
            modTicketResponseDTO.setNotifier(notifier);
        }
        if (t.getAd() != null) {
            modTicketResponseDTO.setDiscriminator(t.getAd().getDiscriminator());
            for (ImageEntity imageEntity : t.getAd().getImageEntity()) {
                modTicketResponseDTO.getAdImagesIdList().add(imageEntity.getId());
            }
        }
        modTicketResponseDTO.setTicketSubject(t.getSubject());
        return modTicketResponseDTO;
    }

    private void checkIfWaiting(Integer[] tab, ModTicket t, int i) {
        if (t.getResult() == null) {
            tab[i] += 1;
        } else if (t.getResult() == ModTicketResult.TO_VERIFY) {
            tab[i] += 1;
        }
    }

    private Sort getSorting(String sort, boolean desc) {
        Sort sorting = sort == null ? Sort.unsorted() : Sort.by(sort);
        if (desc) sorting = sorting.descending();
        return sorting;
    }

    private void sendNotification(User userToNotify, String contentOfNotification) {

        Notification notification = new Notification();
        notification.setNotificationStatus(NotificationStatus.UNREAD);
        notification.setContent(contentOfNotification);
        //notification.setCurrentUser(hire.getSharer());
        notification.setOtherUser(userToNotify);
        notificationRepo.save(notification);
    }

    public String getModTicketNameById(Long id) {
        return Get(ModTicket.class,modTicketRepo,id).getSubject().toString();
    }
}
