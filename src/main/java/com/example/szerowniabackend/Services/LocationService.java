package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Models.DTOs.Requests.LocalizeRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.DistrictResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.RegionResponseDTO;
import com.example.szerowniabackend.Models.Entities.District;
import com.example.szerowniabackend.Models.Entities.Location;
import com.example.szerowniabackend.Models.Entities.Region;
import com.example.szerowniabackend.Models.Repositories.DistrictRepo;
import com.example.szerowniabackend.Models.Repositories.LocationRepo;
import com.example.szerowniabackend.Models.Repositories.RegionRepo;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LocationService {

    private final LocationRepo locationRepo;
    private final RegionRepo regionRepo;
    private final DistrictRepo districtRepo;

    public LocationService(LocationRepo locationRepo, RegionRepo regionRepo, DistrictRepo districtRepo) {
        this.locationRepo = locationRepo;
        this.regionRepo = regionRepo;
        this.districtRepo = districtRepo;
    }

    public List<Location> getAll() {
        return locationRepo.findAll();
    }

    public Optional<Location> getById(Long id) {
        return locationRepo.findById(id);
    }

    public void save(Location location) {
        locationRepo.save(location);
    }

    public void deleteById(Long id) {
        locationRepo.deleteById(id);
    }

    public List<RegionResponseDTO> getAllRegions() {
        List<RegionResponseDTO> res = new ArrayList<>();
        List<Region> regions = regionRepo.findAll();

        for (Region r : regions) {
            RegionResponseDTO rr = new RegionResponseDTO(r.getId(), r.getName());
            List<DistrictResponseDTO> districts = new ArrayList<>();
            for (District d : r.getDistricts())
                districts.add(new DistrictResponseDTO(d.getId(), d.getName()));

            rr.setDistricts(districts);
            res.add(rr);
        }
        return res;
    }

    public DistrictResponseDTO getClosestDistrict(LocalizeRequestDTO dto) {
        TreeMap<Double,String> distanceSet = new TreeMap<>();

        for (District d : districtRepo.findAll())
            distanceSet.put(distance(d,dto),d.getId());

        District closest = AService.Get(District.class, districtRepo, distanceSet.firstEntry().getValue());
        return new DistrictResponseDTO(closest.getId(),closest.getName());
    }

    private double distance(District d, LocalizeRequestDTO dto) {
        double lat1 = d.getLatitude(), lon1 = d.getLongitude();
        double lat2 = dto.getLatitude(), lon2 = dto.getLongitude();
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;

        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
