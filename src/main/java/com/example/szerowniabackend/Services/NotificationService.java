package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetHireMyBorrowsSharers.HireMyBorrowsResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.NotificationDTO;
import com.example.szerowniabackend.Models.Entities.Enums.NotificationStatus;
import com.example.szerowniabackend.Models.Entities.Notification;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.Repositories.NotificationRepo;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import com.example.szerowniabackend.Security.Authorization;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NotificationService {

    private final NotificationRepo notificationRepo;
    private final Authorization authorization;
    private final UserRepo userRepo;

    public NotificationService(NotificationRepo notificationRepo, Authorization authorization, UserRepo userRepo) {
        this.notificationRepo = notificationRepo;
        this.authorization = authorization;
        this.userRepo = userRepo;
    }

    public PaginatedDTO<NotificationDTO> getUserNotifications(String tokenWithBearer, int page, int howManyRecord) {

        Long idUser = authorization.getIdUserFromToken(tokenWithBearer);
        User user = AService.Get(User.class, userRepo, idUser);

        List<NotificationDTO> list = new ArrayList<>();
        Page<Notification> pageNotification = notificationRepo.findAllByCurrentUserOrderByCreateTimeDesc(user, PageRequest.of(page, howManyRecord));
        return getNotificationDTOPaginatedDTO(list, pageNotification);
    }

    public int getUnreadUserNotifications(String tokenWithBearer) {

        Long idUser = authorization.getIdUserFromToken(tokenWithBearer);
        User user = AService.Get(User.class, userRepo, idUser);

        List<Notification> unreadNotifications = notificationRepo.findAllByCurrentUserAndNotificationStatusOrderByCreateTimeDesc(user, NotificationStatus.UNREAD);
        return unreadNotifications.size();
    }

    private PaginatedDTO<NotificationDTO> getNotificationDTOPaginatedDTO(List<NotificationDTO> list, Page<Notification> pageNotification) {
        for (Notification notification : pageNotification) {
            ModelMapper mapper = new ModelMapper();
            NotificationDTO notificationDTO = mapper.map(notification, NotificationDTO.class);
            list.add(notificationDTO);
            if (notification.getNotificationStatus().equals(NotificationStatus.UNREAD)) {
                notification.setNotificationStatus(NotificationStatus.READ);
                notification.setReadTime(LocalDateTime.now());
                notificationRepo.save(notification);
            }
        }
        return new PaginatedDTO<>((long) list.size(), pageNotification.getTotalPages(), list);
    }

    public void markNotificationAsRead(String tokenWithBearer, Long notificationId) {
        Long idUser = authorization.getIdUserFromToken(tokenWithBearer);
        User user = AService.Get(User.class, userRepo, idUser);

        Notification notificationById = notificationRepo.findById(notificationId).orElseThrow(() -> new NotFoundException("Powiadomienie nie istnieje"));
        notificationById.setNotificationStatus(NotificationStatus.READ);
        notificationRepo.save(notificationById);
    }
}
