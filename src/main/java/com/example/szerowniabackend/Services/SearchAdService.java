package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Email.EmailSender;
import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Exceptions.NotFoundException;

import com.example.szerowniabackend.Models.DTOs.Requests.*;
import com.example.szerowniabackend.Models.DTOs.Responses.CategoryReferenceDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetSearchAd.SearchAdByIdDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetSearchAd.SearchAdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetSearchAd.SearchAdWithModTicketdDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ProductByIdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.RentalAdWithDescriptionDTO;
import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.Entities.Enums.AdStatus;
import com.example.szerowniabackend.Models.Entities.Enums.EventType;
import com.example.szerowniabackend.Models.Entities.Enums.ModTicketResult;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Security.Authorization;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.*;


@Service
public class SearchAdService {

    private SearchAdRepo searchAdRepo;
    private UserRepo userRepo;
    private ProductRepo productRepo;
    private LocationRepo locationRepo;
    private CategoryRepo categoryRepo;
    private FeatureOccurrenceRepo featureOccurrenceRepo;
    private ModTicketRepo modTicketRepo;
    private Authorization authorization;
    private final EmailSender emailSender;
    private final ModelMapper mapper;
    private final StatisticsService statisticsService;

    @PersistenceContext
    private final EntityManager entityManager;

    @Autowired
    public SearchAdService(SearchAdRepo searchAdRepo, UserRepo userRepo, ProductRepo productRepo, LocationRepo locationRepo, CategoryRepo categoryRepo, FeatureOccurrenceRepo featureOccurrenceRepo, ModTicketRepo modTicketRepo, Authorization authorization, EmailSender emailSender, StatisticsService statisticsService, EntityManager entityManager) {
        this.searchAdRepo = searchAdRepo;
        this.userRepo = userRepo;
        this.productRepo = productRepo;
        this.locationRepo = locationRepo;
        this.categoryRepo = categoryRepo;
        this.featureOccurrenceRepo = featureOccurrenceRepo;
        this.modTicketRepo = modTicketRepo;
        this.authorization = authorization;
        this.emailSender = emailSender;
        this.statisticsService = statisticsService;
        this.mapper = new ModelMapper();
        this.entityManager = entityManager;
    }

    public PaginatedDTO<SearchAdResponseDTO> getAllSearchAds(int page, int howManyRecord) {
        Page<SearchAd> searchAdRepoAll = searchAdRepo.findAll(PageRequest.of(page, howManyRecord));
        List<SearchAdResponseDTO> result = new ArrayList<>();
        ModelMapper mapper = new ModelMapper();

        for (SearchAd searchAd : searchAdRepoAll) {
            if (searchAd.isVisible()) {
                SearchAdResponseDTO searchAdResponseDTO = mapper.map(searchAd, SearchAdResponseDTO.class);
                result.add(searchAdResponseDTO);
            }
        }
        Long count = (long) result.size();
        return new PaginatedDTO<>(count, searchAdRepoAll.getTotalPages(), result);
    }

    public SearchAdByIdDTO getSearchAd(Long id, String token) {
        SearchAd searchAd = AService.Get(SearchAd.class, searchAdRepo, id);
        User searchAdOwner = searchAd.getUser();

        if (!searchAd.isVisible()) {
            if (!authorization.isModerator(token) && !authorization.isAdmin(token) && !authorization.isCurrentUser(searchAdOwner, token))
                throw new ForbiddenException();
        }

        ModelMapper mapper = new ModelMapper();
        SearchAdByIdDTO searchAdByIdDTO = mapper.map(searchAd, SearchAdByIdDTO.class);
        Set<ProductByIdResponseDTO> products = new HashSet<>();
        for (Product product : searchAd.getProducts()) {
            ProductByIdResponseDTO mapProductToDTO = mapper.map(product, ProductByIdResponseDTO.class);
            mapProductToDTO.setCategoryReferenceDTO(mapper.map(product.getCategory(), CategoryReferenceDTO.class));
            products.add(mapProductToDTO);
        }

        if (searchAd.getUser().getUserImage() != null)
            searchAdByIdDTO.setImageUserId(searchAd.getUser().getUserImage().getId());

        searchAdByIdDTO.setProducts(products);

        statisticsService.recordEvent(EventType.VIEWING_AD, id);
        return searchAdByIdDTO;
    }

    public void createSearchAd(SearchAdCreateRequestDTO searchAdCreateRequestDTO, String authorization, Long[] productsId) {
        Long creatorId = this.authorization.getIdUserFromToken(authorization);
        User user = AService.Get(User.class, userRepo, creatorId);

        //TODO walidacja time slotów
/*        if (searchAdCreateRequestDTO.getPublicationDate().isAfter(searchAdCreateRequestDTO.getEndDate())) {
            throw new RuntimeException("Publication date cannot be after end date");
        }
        if (searchAdCreateRequestDTO.getPublicationDate().isBefore(LocalDate.now())) {
            throw new RuntimeException("Publication date cannot be earlier than now");
        }*/

        Set<Product> productSet = new HashSet<>();
        for (Long productId : productsId) {
            Product product = AService.Get(Product.class, productRepo, productId);

            if (!product.getUser().getId().equals(user.getId()))
                throw new ForbiddenException();

            if (!product.isSearchProduct())
                throw new SearchAd.ProductIsNotSearchProduct();

            productSet.add(product);
        }

        SearchAd searchAd = new SearchAd();
        searchAd.setTitle(searchAdCreateRequestDTO.getTitle());
        searchAd.setTimeSlotList(searchAdCreateRequestDTO.getTimeSlotList());

        LocalDate maxDate = searchAdCreateRequestDTO.getTimeSlotList().stream().map(TimeSlot::getEndDate).max(LocalDate::compareTo).orElseThrow();
        searchAd.setEndDate(maxDate);

        Location location = new Location();
        location.setLatitude(searchAdCreateRequestDTO.getLocation().getLatitude());
        location.setLongitude(searchAdCreateRequestDTO.getLocation().getLongitude());
        location.setName(searchAdCreateRequestDTO.getLocation().getName());
        locationRepo.save(location);
        searchAd.setLocation(location);

        searchAd.setPickupMethod(searchAdCreateRequestDTO.getPickupMethod());
        searchAd.setDescription(searchAdCreateRequestDTO.getDescription());
        searchAd.setUser(user);
        //searchAd.setProducts(productSet);

        for (Product product : productSet) {
            searchAd.getProducts().add(product);
            product.getIncludeAds().add(searchAd);
        }

        emailSender.sendMessageWhenCreateSearchAd(user.getEmail());
        searchAdRepo.save(searchAd);
    }

    public void updateRentalAd(Long id, UpdateSearchAdRequestDTO newSearchAd, String authorization) {
        SearchAd searchAd = checkErrors(id, authorization);

        if (newSearchAd.getDescription() != null)
            searchAd.setDescription(newSearchAd.getDescription());

        if (newSearchAd.getTitle() != null)
            searchAd.setTitle(newSearchAd.getTitle());

        searchAdRepo.save(searchAd);
    }

    public void deleteSearchAd(Long id, String authorization) {
        SearchAd searchAd = checkErrors(id, authorization);
        searchAd.setVisible(false);
        searchAdRepo.save(searchAd);
    }

    public PaginatedDTO<SearchAdResponseDTO> getSearchAdByCriteria(RentalADSearchDTO searchRequestDTO, int page, int howManyRecord) {

        List<SearchAdResponseDTO> result = new ArrayList<>();


        String baseQuery = "SELECT * FROM AD sa " +
                "JOIN PRODUCT_AD  pra ON sa.ID = pra.AD_ID " +
                "JOIN product p ON p.ID = pra.PRODUCT_ID " +
                "WHERE sa.is_visible = true and ad_type = 'SEARCHAD' ";

        StringBuilder queryToBuild = new StringBuilder(baseQuery);


        String keyWord = searchRequestDTO.getSearch();
        if (keyWord != null && keyWord.length() != 0) {
            //Append product name or searchAd title or searchAd description
            queryToBuild.append("AND (upper(p.name) like upper(concat('%', :keyWord,'%')) ");
            queryToBuild.append(" OR upper(p.MAKE) like upper(concat('%', :keyWord,'%')) ");
            queryToBuild.append(" OR upper(sa.title) like upper(concat('%', :keyWord,'%')) ");
            queryToBuild.append(" OR upper(sa.description) like upper(concat('%', :keyWord,'%'))) ");
        }

        Long idCategory = searchRequestDTO.getCategory();

        if (idCategory != null) {

            AService.Get(Category.class, categoryRepo, idCategory);

            //Append category
            queryToBuild.append("AND CATEGORY_ID_CATEGORY = :idCategory");

            if (!searchRequestDTO.getFilters().isEmpty()) {
                queryToBuild.append(" AND p.ID IN ");
                queryToBuild.append("(");
            }

            int tmp = 0;
            int sizeListDTO = searchRequestDTO.getFilters().size();

            //Append FEATURE_OCCURRENCE
            for (RentalAdSupport parameter : searchRequestDTO.getFilters()) {

                Optional<FeatureOccurrence> optionalFO = featureOccurrenceRepo.findFeatureOccurrenceByFeatureId(parameter.getId());

                if (optionalFO.isEmpty()) {
//                    throw new NotFoundException("Brak \"wystąpienia cechy\" powiązanej z cechą o id: " + parameter.getIdFeature());
                    break;
                }

                FeatureOccurrence featureOccurrence = optionalFO.get();

                if (tmp >= 2) {
                    queryToBuild.append(" INTERSECT ");
                }

                queryToBuild.append("SELECT PRODUCT_ID FROM FEATURE_OCCURRENCE where feature_id_feature = ");
                queryToBuild.append(parameter.getId());

                if (parameter.getBool() != null) {
                    queryToBuild.append(" and BOOLEAN_VALUE = ");
                    queryToBuild.append(parameter.getBool());
                    queryToBuild.append(" ");
                }

                if (parameter.getGt() != null) {
                    if (featureOccurrence.getDecimalNumberValue() != null) {
                        queryToBuild.append(" and DECIMAL_NUMBER_VALUE >= ");
                        queryToBuild.append(parameter.getGt());
                        queryToBuild.append(" ");
                    } else {
                        queryToBuild.append(" and FLOATING_NUMBER_VALUE >= ");
                        queryToBuild.append(parameter.getGt());
                        queryToBuild.append(" ");
                    }
                }

                if (parameter.getLt() != null) {
                    if (featureOccurrence.getDecimalNumberValue() != null) {
                        queryToBuild.append(" and DECIMAL_NUMBER_VALUE <= ");
                        queryToBuild.append(parameter.getLt());
                        queryToBuild.append(" ");
                    } else {
                        queryToBuild.append(" and FLOATING_NUMBER_VALUE <= ");
                        queryToBuild.append(parameter.getLt());
                        queryToBuild.append(" ");
                    }
                }

                StringBuilder eqValue = new StringBuilder();
                eqValue.append(":eqValue");
                eqValue.append(tmp);
                if (parameter.getEq() != null) {
                    if (featureOccurrence.getDecimalNumberValue() != null) {
                        queryToBuild.append(" and DECIMAL_NUMBER_VALUE = ");
                        queryToBuild.append(eqValue);
                        queryToBuild.append(" ");
                    }
                    if (featureOccurrence.getFloatingNumberValue() != null) {
                        queryToBuild.append(" and FLOATING_NUMBER_VALUE = ");
                        queryToBuild.append(eqValue);
                        queryToBuild.append(" ");
                    }
                    if (featureOccurrence.getTextValue() != null) {
                        queryToBuild.append(" and UPPER(TEXT_VALUE) = UPPER(");
                        queryToBuild.append(eqValue);
                        queryToBuild.append(" ) ");
                    }
                }

                if (tmp == 0 && sizeListDTO != tmp + 1)
                    queryToBuild.append(" INTERSECT ");

                tmp++;
            }

            if (sizeListDTO > 0) {
                queryToBuild.append(") ");
            }
        }

        queryToBuild.append(" LIMIT :limit OFFSET :offset ;");

        Query query = entityManager.createNativeQuery(queryToBuild.toString(), SearchAd.class);

        if (keyWord != null && !keyWord.equals("")) {
            query.setParameter("keyWord", keyWord);
        }

        if (idCategory != null) {
            query.setParameter("idCategory", idCategory);
            int tmp = 0;
            for (RentalAdSupport parameter : searchRequestDTO.getFilters()) {
                if (parameter.getEq() != null) {
                    String eqValue = "eqValue" + tmp;
                    query.setParameter(eqValue, parameter.getEq());
                }
                tmp++;
            }
        }

        if (howManyRecord <= 0)
            howManyRecord = 0;

        query.setParameter("limit", howManyRecord);
        query.setParameter("offset", howManyRecord * page);

        for (Object searchAd : query.getResultList()) {
            SearchAdResponseDTO searchAdResponseDTO = mapper.map(searchAd, SearchAdResponseDTO.class);
            result.add(searchAdResponseDTO);
        }


        String countQueryString = queryToBuild.toString().replace("*", " count(*) ");
        countQueryString = countQueryString.substring(0, countQueryString.lastIndexOf("LIMIT"));

        Query countQuery = entityManager.createNativeQuery(countQueryString);

        for (Parameter<?> param : query.getParameters()) {
            if (!param.getName().equals("limit") && !param.getName().equals("offset")) {
                countQuery.setParameter(param.getName(), query.getParameterValue(param.getName()));
            }
        }

        Long totalRecords = ((BigInteger) countQuery.getResultList().get(0)).longValue();

        PaginatedDTO<SearchAdResponseDTO> paginatedDTO = new PaginatedDTO<>((long) result.size(), (int) (totalRecords / howManyRecord + 1), result);

        return paginatedDTO;
    }


    private SearchAd checkErrors(Long id, String authorization) {
        Long loggedUser = this.authorization.getIdUserFromToken(authorization);
        User user = AService.Get(User.class, userRepo, loggedUser);
        SearchAd searchAd = AService.Get(SearchAd.class, searchAdRepo, id);

        if (!searchAd.getUser().equals(user)) throw new ForbiddenException();

        return searchAd;
    }

    public PaginatedDTO<SearchAdWithModTicketdDTO> getMySearchAdsToCorrect(int page, int howManyRecord, String authorization) {
        Long idUser = this.authorization.getIdUserFromToken(authorization);
        List<SearchAdWithModTicketdDTO> result = new ArrayList<>();

        Page<AdModTicket> searchAdPage = searchAdRepo.findAdsByResult(idUser, ModTicketResult.TO_MODIFY.toString(), PageRequest.of(page, howManyRecord));

        for (AdModTicket modTicket : searchAdPage) {
            SearchAd searchAd = AService.Get(SearchAd.class, searchAdRepo, modTicket.getAdId());
            SearchAdWithModTicketdDTO obj = mapper.map(searchAd, SearchAdWithModTicketdDTO.class);
            obj.setModTicketId(modTicket.getModTicketId());
            result.add(obj);
        }

        Long count = (long) result.size();
        return new PaginatedDTO<>(count, searchAdPage.getTotalPages(), result);

    }

    private AdStatus getModTicketBasedStatus(SearchAd searchAd, ModTicket latestModTicket) {

        if (latestModTicket.getResult() == null)
            return AdStatus.IN_MODERATION;

        if (latestModTicket.getResult() == ModTicketResult.TO_MODIFY)
            return AdStatus.NEEDS_CORRECTION;

        if (latestModTicket.getResult() == ModTicketResult.HIDDEN && !searchAd.isVisible())
            return AdStatus.HIDDEN_BY_MODERATOR;

        return null;
    }
}
