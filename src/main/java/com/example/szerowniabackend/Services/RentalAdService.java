package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Email.EmailSender;
import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.DTOs.Requests.*;
import com.example.szerowniabackend.Models.DTOs.Responses.*;
import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.GetRentalAdByIdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.GetRentalAdByIdUserResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.LocationDTO;
import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.Entities.Enums.*;
import com.example.szerowniabackend.Models.ProfanityFilter.ProfanityFilter;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Security.Authorization;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static com.example.szerowniabackend.Services.AService.Get;
import static com.example.szerowniabackend.Services.AService.logged;
import static com.example.szerowniabackend.Services.ProductService.validateProductForModifications;

@Service
public class RentalAdService {

    private static final String RENTAL_AD = "RENTALAD";
    private final RentalAdRepo rentalAdRepo;
    private final ProductRepo productRepo;
    private final CategoryRepo categoryRepo;
    private final FeatureOccurrenceRepo featureOccurrenceRepo;
    private final ModTicketRepo modTicketRepo;
    private final LocationRepo locationRepo;
    private final HireRepo hireRepo;
    private final StatisticsService statisticsService;
    private final MessageRepo messageRepo;
    private final MessageService messageService;
    private final SearchAdRepo searchAdRepo;
    private final ImageRepo imageRepo;
    private final int safeReturnMargin = 5;
    private final int infiniteEndDateMargin = 2;
    private final String MESSAGE_CONTENT = "Posiadam poszukiwany przez ciebie produkt! Wejdz w moj profil i sprawdz!";
    private final Authorization authorization;
    private final EmailSender emailSender;
    private final ModelMapper mapper;
    private final UserRepo userRepo;

    @PersistenceContext
    private final EntityManager entityManager;

    @Autowired
    public RentalAdService(RentalAdRepo rentalAdRepo, ProductRepo productRepo,
                           CategoryRepo categoryRepo, FeatureOccurrenceRepo featureOccurrenceRepo,
                           ModTicketRepo modTicketRepo, LocationRepo locationRepo, HireRepo hireRepo,
                           StatisticsService statisticsService, MessageRepo messageRepo,
                           MessageService messageService, SearchAdRepo searchAdRepo, ImageRepo imageRepo,
                           EntityManager entityManager, Authorization authorization, EmailSender emailSender, UserRepo userRepo) {
        this.rentalAdRepo = rentalAdRepo;
        this.productRepo = productRepo;
        this.categoryRepo = categoryRepo;
        this.featureOccurrenceRepo = featureOccurrenceRepo;
        this.modTicketRepo = modTicketRepo;
        this.locationRepo = locationRepo;
        this.hireRepo = hireRepo;
        this.statisticsService = statisticsService;
        this.messageRepo = messageRepo;
        this.messageService = messageService;
        this.searchAdRepo = searchAdRepo;
        this.imageRepo = imageRepo;
        this.entityManager = entityManager;
        this.authorization = authorization;
        this.emailSender = emailSender;
        this.userRepo = userRepo;
        mapper = new ModelMapper();
    }

    public String getRentalAdNameById(Long id) {
        return Get(RentalAd.class, rentalAdRepo, id).getTitle();
    }

    public PaginatedDTO<RentalAdResponseDTO> getListOfRentalAd(int page, int howManyRecord) {

        List<RentalAdResponseDTO> list = new ArrayList<>();
        Page<RentalAd> rentalAdPage = rentalAdRepo.
                findAllRentalAdWhereIsVisibleIsTrueOrderedByIsPromoted(PageRequest.of(page, howManyRecord));

        for (RentalAd rentalAd : rentalAdPage) {
            if (rentalAd.isVisible()) {
                RentalAdResponseDTO mapRentalAdToDTO = setIdImageAndThumbnailToRentalAdResponseDTO(mapper, rentalAd);
                list.add(mapRentalAdToDTO);
            }

        }
        Long count = (long) list.size();
        return new PaginatedDTO<>(count, rentalAdPage.getTotalPages(), list);
    }

    private RentalAdResponseDTO setIdImageAndThumbnailToRentalAdResponseDTO(ModelMapper mapper, RentalAd rentalAd) {
        RentalAdResponseDTO mapRentalAdToDTO = mapper.map(rentalAd, RentalAdResponseDTO.class);
        ImageEntity firstImageForRentalAd = imageRepo.findFirstImageForRentalAd(rentalAd.getId());

        if (firstImageForRentalAd != null) {
            mapRentalAdToDTO.setImagesIdList(Collections.singletonList(firstImageForRentalAd.getId()));
            if (firstImageForRentalAd.getImageThumbnail() != null)
                mapRentalAdToDTO.setThumbnailIdList(Collections.singletonList(firstImageForRentalAd.getImageThumbnail().getId()));
        }
        return mapRentalAdToDTO;
    }

    public PaginatedDTO<RentalAdWithDescriptionDTO> getMyRentalAds(int page, int howManyRecord) {

        Long idUser = logged().getId();
        List<RentalAdWithDescriptionDTO> list = new ArrayList<>();
        Page<RentalAd> rentalAdPage = rentalAdRepo.
                findAllByUserIdOrderDescByStartDate(idUser, PageRequest.of(page, howManyRecord));

        for (RentalAd rentalAd : rentalAdPage) {
            RentalAdWithDescriptionDTO mapRentalAdToDTO = mapper.map(rentalAd, RentalAdWithDescriptionDTO.class);
            ImageEntity firstImageForRentalAd = imageRepo.findFirstImageForRentalAd(rentalAd.getId());
            if (firstImageForRentalAd != null) {
                mapRentalAdToDTO.setImagesIdList(Collections.singletonList(firstImageForRentalAd.getId()));
                mapRentalAdToDTO.setThumbnailIdList(Collections.singletonList(firstImageForRentalAd.getImageThumbnail().getId()));
            }
            list.add(mapRentalAdToDTO);
        }
        Long count = (long) list.size();
        return new PaginatedDTO<>(count, rentalAdPage.getTotalPages(), list);
    }

    public GetRentalAdByIdResponseDTO getRentalAdById(Long id) {
        Role role = null;
        Long userId = null;
        RentalAd rentalAd = Get(RentalAd.class, rentalAdRepo, id);

        try {
            role = logged().getRole();
            userId = logged().getId();
        } catch (User.NotFound ignored) {

        }

        if (!rentalAd.isVisible() && (role == null || userId == null || (!role.equals(Role.MODERATOR) && !rentalAd.getUser().getId().equals(userId)))) {
            throw new NotFoundException("Nie znaleziono ogłoszenia wypożyczenia");
        }

        GetRentalAdByIdResponseDTO mapRentalAdToDTO = mapper.map(rentalAd, GetRentalAdByIdResponseDTO.class);
        GetRentalAdByIdUserResponseDTO mapUserToDTO = mapper.map(rentalAd.getUser(), GetRentalAdByIdUserResponseDTO.class);
        mapRentalAdToDTO.setUser(mapUserToDTO);

        Set<ProductByIdResponseDTO> products = new HashSet<>();
        for (Product product : rentalAd.getProducts()) {
            ProductByIdResponseDTO mapProductToDTO = mapper.map(product, ProductByIdResponseDTO.class);
            mapProductToDTO.setCategoryReferenceDTO(mapper.map(product.getCategory(), CategoryReferenceDTO.class));
            mapProductToDTO.setModifiable(validateProductForModifications(product.getIncludeAds()));
            products.add(mapProductToDTO);
        }

        double faker = Math.random() * 0.01;
        Location location = rentalAd.getLocation();
        Double fakedLon = rentalAd.getLocation().getLongitude() + faker;
        Double fakedLat = rentalAd.getLocation().getLatitude() + faker;

        mapRentalAdToDTO.setLocation(new LocationDTO(location.getName(), fakedLon, fakedLat));

        mapRentalAdToDTO.setProducts(products);
        mapRentalAdToDTO.setImagesIdList(imageRepo.findAllOrderByIdReturnListOfId(rentalAd.getId()));
        if (rentalAd.getUser().getUserImage() != null)
            mapRentalAdToDTO.setImageUserId(rentalAd.getUser().getUserImage().getId());


        mapRentalAdToDTO.setViewsCount(statisticsService.getViewedRentalAdStats(id));
        statisticsService.recordEvent(EventType.VIEWING_AD, id);

        return mapRentalAdToDTO;
    }

    public List<RentalAdResponseDTO> getListOfRentalAdForHomePage() {
        List<RentalAdResponseDTO> list = new ArrayList<>();

        for (RentalAd rentalAd : rentalAdRepo.findAllRentalAdForHomePage()) {
            if (rentalAd.isVisible()) {
                RentalAdResponseDTO mapRentalAdToDTO = setIdImageAndThumbnailToRentalAdResponseDTO(mapper, rentalAd);
                list.add(mapRentalAdToDTO);
            }
        }
        return list;
    }

    public List<RentalAdResponseDTO> getRentalAdByKeyWord(RentalAdFindByKeyWordAndCategoryDTO rentalAdFind) {
        List<RentalAdResponseDTO> list = new ArrayList<>();
        String keyWord = rentalAdFind.getKeyWords();
        String category = rentalAdFind.getCategory();

        if (category.length() == 0) return list;

        for (RentalAd rentalAd : rentalAdRepo.findRentalAdByCategoryAndProduct(keyWord, category)) {
            RentalAdResponseDTO mapRentalAdToDTO = mapper.map(rentalAd, RentalAdResponseDTO.class);
            mapRentalAdToDTO.setImagesIdList(imageRepo.findAllOrderByIdReturnListOfId(rentalAd.getId()));

            list.add(mapRentalAdToDTO);
        }
        return list;
    }

    public List<TimeSlotResponseDTO> getAllAvailableDatesOfHire(Long idRentalAd) {
        RentalAd rentalAd = Get(RentalAd.class, rentalAdRepo, idRentalAd);
        TreeSet<LocalDate> availableDays = new TreeSet<>();
        boolean infiniteTimeSlot = false;

        for (TimeSlot timeSlot : rentalAd.getTimeSlotList()) {
            LocalDate endDate;

            if (timeSlot.getEndDate() == null) {
                infiniteTimeSlot = true;
                endDate = LocalDate.now().plusYears(infiniteEndDateMargin);
            } else {
                endDate = timeSlot.getEndDate().plusDays(1);
            }

            availableDays.addAll(timeSlot.getStartDate().datesUntil(endDate).collect(Collectors.toList()));
        }

        List<Hire> hireList = hireRepo.findAllByAdIdOrderByDateFrom(idRentalAd);
        LocalDate hireFrom;
        LocalDate hireTo;

        for (Hire hire : hireList) {
            hireFrom = hire.getDateFrom();
            hireTo = hire.getDateToPlanned();

            if (hire.getSelectedPickupMethod() == PickupMethod.COURIER || hire.getSelectedPickupMethod() == PickupMethod.PARCEL_LOCKER) {
                hireTo = hireTo.plusDays(safeReturnMargin);
            }

            availableDays.removeAll(hireFrom.datesUntil(hireTo.plusDays(1)).collect(Collectors.toList()));
        }

        LocalDate today = LocalDate.now();
        LocalDate firstAvailableDay = availableDays.first();

        if (today.isAfter(firstAvailableDay)) {
            availableDays.removeAll(firstAvailableDay.datesUntil(today).collect(Collectors.toList()));
        }

        List<TimeSlotResponseDTO> availablePeriods = new ArrayList<>();
        LinkedList<LocalDate> tmp = new LinkedList<>();

        while (!availableDays.isEmpty()) {
            LocalDate current = availableDays.pollFirst();
            tmp.add(current);

            for (LocalDate nextDate : availableDays) {
                if (!current.plusDays(1).isEqual(nextDate)) {
                    break;
                }
                tmp.add(nextDate);
                current = nextDate;
            }
            availablePeriods.add(new TimeSlotResponseDTO(tmp.getFirst(), tmp.getLast()));
            availableDays.removeAll(tmp);
            tmp.clear();
        }

        if (infiniteTimeSlot) {
            availablePeriods.get(availablePeriods.size() - 1).setTo(null);
        }

        return availablePeriods;

    }

    public void deleteRentalAd(Long id) {
        RentalAd rentalAd = Get(RentalAd.class, rentalAdRepo, id);

        rentalAd.setVisible(false);
        rentalAdRepo.save(rentalAd);
    }

    public Long createRentalAd(RentalAdCreateRequestDTO dto, String tokenWithBearer, Long[] arrayProductId) {
        User user = logged();

        for (Long aLong : arrayProductId) {
            Product product = Get(Product.class, productRepo, aLong);

            if (!product.getUser().equals(user))
                throw new ForbiddenException();
        }

        if (dto.getImageNames() != null && dto.getImageNames().size() > 8)
            throw new RentalAd.WrongImagesNumber();

        if (dto.getPickupMethod().isEmpty())
            throw new RentalAd.EmptyPickupMethodListException();

        LocalDate maxDate = dto.getTimeSlotList().stream().map(TimeSlot::getEndDate).max(LocalDate::compareTo).orElseThrow();

        Location location = new Location(
                dto.getLocation().getLatitude(),
                dto.getLocation().getLongitude(),
                dto.getLocation().getName());

        RentalAd ad = new RentalAd(
                user,
                dto.getTitle(),
                dto.getTimeSlotList(),
                location,
                dto.getPickupMethod(),
                dto.getDescription(),
                dto.getPricePerDay(),
                dto.getDepositAmount(),
                dto.getPenaltyForEachDayOfDelayInReturns(),
                dto.getAverageOpinion(),
                dto.getAverageRentalPrice(),
                dto.getMinRentalHistory(),
                dto.getCustomRequirement(),
                maxDate);

        if (user.getPremium() && countPromoted(user) <= 5)
            ad.setPromoted(dto.isPromoted());


        ProfanityFilter.getCensoredText(ad.getTitle());
        boolean isTitleVulgar = checkVulgar(ad, user.getEmail());
        ProfanityFilter.getCensoredText(ad.getDescription());
        boolean isVulgar = checkVulgar(ad, user.getEmail());

        locationRepo.save(location);
        location.setAd(ad);

        rentalAdRepo.save(ad);

        isVulgar = isVulgar || isTitleVulgar;
        if (!isVulgar && dto.getSearchAdId() != null) {
            SearchAd searchAd = Get(SearchAd.class, searchAdRepo, dto.getSearchAdId());
            ad.setSearchAd(searchAd);

            User searchAdCreator = searchAd.getUser();
            Optional<Long> threadId = messageRepo.isExistThreadBetweenUsers(user.getId(), searchAdCreator.getId());
            if (threadId.isPresent()) {
                MessageExistingThreadDTO newDto = new MessageExistingThreadDTO();
                newDto.setThread(threadId.get());
                newDto.setContent(MESSAGE_CONTENT);
                messageService.createMessageForExistingThread(newDto, tokenWithBearer);
            } else {
                MessageNewThreadDTO newDTO = new MessageNewThreadDTO();
                newDTO.setContent(MESSAGE_CONTENT);
                newDTO.setAdId(searchAd.getId());
                messageService.createMessageForNewThread(newDTO, tokenWithBearer);
            }

            var setProduct = ad.getProducts();
            List<String> productNameList = new ArrayList<>();

            for (Product product : setProduct)
                productNameList.add(product.getName());

            emailSender.sendMessageWhenSearchedProductFound(searchAdCreator.getEmail(), user.getEmail(), productNameList.toString());
        }

        addMultipleProduct(arrayProductId, ad);
        statisticsService.recordEvent(EventType.AD_CREATION, ad.getId());

        for (String listNameImage : dto.getImageNames()) {
            ImageEntity imageEntity = Get(ImageEntity.class, imageRepo, Long.valueOf(listNameImage));
            imageEntity.setAd(ad);
            imageRepo.save(imageEntity);
        }

        emailSender.sendMessageWhenCreateRentalAd(user.getEmail());
        if (isVulgar) {
            throw new VulgarWords.RentalAdContainsVulgarWords();
        }
        return ad.getId();
    }

    public Long getPromotedCount() {
        User user = logged();
        return countPromoted(user);
    }

    private Long countPromoted(User user) {
        return rentalAdRepo
                .findAllByUserAndEndDateAfter(user, LocalDate.now().plusDays(1)).stream()
                .filter(RentalAd::isPromoted)
                .count();
    }

    private void addMultipleProduct(Long[] arrayProductId, RentalAd rentalAd) {
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < arrayProductId.length; i++) {
            @SuppressWarnings("OptionalGetWithoutIsPresent") Product productNew = Get(Product.class, productRepo, arrayProductId[i]);
            productNew.getIncludeAds().add(rentalAd);
            productRepo.save(productNew);
            rentalAd.getProducts().add(productNew);
            rentalAdRepo.save(rentalAd);
        }
    }

    public void updateRentalAd(Long rentalAdId, RentalAdUpdateRequestDTO dto) {
        Optional<RentalAd> rentalAdOpt = rentalAdRepo.findRentalAdByUserIdAndRentalAdId(logged().getId(), rentalAdId);

        if (rentalAdOpt.isEmpty()) throw new NotFoundException(RentalAd.class, rentalAdId);

        RentalAd rentalAd = rentalAdOpt.get();

        rentalAd.setTitle(dto.getTitle());
        rentalAd.setDescription(dto.getDescription());
        rentalAd.setPenaltyForEachDayOfDelayInReturns(dto.getPenaltyForEachDayOfDelayInReturns());
        rentalAd.setPricePerDay(dto.getPricePerDay());

        if (dto.getTimeSlotsList() == null)
            dto.setTimeSlotsList(List.of());

        if (dto.getTimeSlotsList().isEmpty())
            rentalAd.setTimeSlotList(dto.getTimeSlotsList());

        rentalAd.setLocation(dto.getLocation());
        rentalAd.setDepositAmount(dto.getDepositAmount());
        rentalAd.setPickupMethod(dto.getPickupMethodList());

        if (dto.getAverageOpinion() != null)
            rentalAd.setAverageOpinion(dto.getAverageOpinion());

        if (dto.getAverageRentalPrice() != null)
            rentalAd.setAverageRentalPrice(dto.getAverageRentalPrice());

        if (dto.getMinRentalHistory() != null)
            rentalAd.setMinRentalHistory(dto.getMinRentalHistory());

        if (dto.getCustomRequirement() != null)
            rentalAd.setCustomRequirement(dto.getCustomRequirement());

        LocalDate maxDate = dto.getTimeSlotsList().stream().map(TimeSlot::getEndDate).max(LocalDate::compareTo).orElseThrow();
        rentalAd.setEndDate(maxDate);

        ProfanityFilter.getCensoredText(rentalAd.getTitle());
        checkVulgar(rentalAd, rentalAd.getUser().getEmail());

        ProfanityFilter.getCensoredText(rentalAd.getDescription());
        checkVulgar(rentalAd, rentalAd.getUser().getEmail());

        rentalAdRepo.save(rentalAd);
    }

    public List<PickupMethod> getPickupMethods(Long id) {
        RentalAd ad = Get(RentalAd.class, rentalAdRepo, id);
        return ad.getPickupMethod();
    }

    public List<RentalAdWithDescriptionDTO> getHistory() {
        List<RentalAdWithDescriptionDTO> result = new ArrayList<>();

        for (RentalAd rentalAd : rentalAdRepo.findAllByUserIdOrderDescByStartDateTopFiveRecords(logged().getId())) {

            RentalAdWithDescriptionDTO rentalAdResponseDTO = mapper.map(rentalAd, RentalAdWithDescriptionDTO.class);
            ImageEntity firstImageForRentalAd = imageRepo.findFirstImageForRentalAd(rentalAd.getId());
            if (firstImageForRentalAd != null) {
                rentalAdResponseDTO.setImagesIdList(Collections.singletonList(firstImageForRentalAd.getId()));
                rentalAdResponseDTO.setThumbnailIdList(Collections.singletonList(firstImageForRentalAd.getImageThumbnail().getId()));
            }
            result.add(rentalAdResponseDTO);
        }
        return result;
    }

    public PaginatedDTO<RentalAdResponseDTO> getRentalAdWithByParams(RentalADSearchDTO rentalADSearchDTO,
                                                                     int howManyRecord,
                                                                     int page) {
        List<RentalAdResponseDTO> returnList = new ArrayList<>();

        String starterStringQuery = "SELECT * FROM AD ra " +
                "join PRODUCT_AD  pra on ra.ID = pra.AD_ID " +
                "join product p on p.ID = pra.PRODUCT_ID ";

        StringBuilder sb = new StringBuilder();
        sb.append(starterStringQuery);

        String searchString = rentalADSearchDTO.getSearch();
        if (searchString != null && !searchString.equals("")) {
            //Append product name or rentalAa title or rentalAd description
            sb.append("WHERE (upper(p.name) like upper(concat('%', :searchedNameProduct,'%')) ");
            sb.append(" OR upper(p.MAKE) like upper(concat('%', :searchedNameProduct,'%')) ");
            sb.append(" OR upper(ra.title) like upper(concat('%', :title,'%')) ");
            sb.append(" OR upper(ra.description) like upper(concat('%', :description,'%'))) ");
        }
        //I make sure that it is visible = true
        sb.append(" AND ra.IS_VISIBLE = true and ra.ad_type = 'RENTALAD' ");

        //I make sure that end date is grater than today +1
        sb.append(" AND ra.END_DATE > CURRENT_DATE()+1 ");

        Long idCategory = rentalADSearchDTO.getCategory();

        int sizeListDTO = 0;
        List<Integer> filterIndex = new ArrayList<>();
        if (idCategory != null) {
            Category category = Get(Category.class, categoryRepo, idCategory);

            //Append category
            sb.append("AND CATEGORY_ID_CATEGORY = ");
            sb.append(category.getIdCategory());

            if (rentalADSearchDTO.getFilters().size() > 0) {
                sb.append(" AND p.ID IN ");
                sb.append("(");
            }

            int tmp = 0;
            sizeListDTO = rentalADSearchDTO.getFilters().size();

            //Append FEATURE_OCCURRENCE
            for (RentalAdSupport filter : rentalADSearchDTO.getFilters()) {

                Optional<FeatureOccurrence> optionalFO = featureOccurrenceRepo.findFeatureOccurrenceByFeatureId(filter.getId());

                if (optionalFO.isEmpty()) {
                    tmp++;
                    continue;
                }

                FeatureOccurrence featureOccurrence = optionalFO.get();

                if (tmp >= 2) {
                    sb.append(" INTERSECT ");
                }

                sb.append("SELECT PRODUCT_ID FROM FEATURE_OCCURRENCE where feature_id_feature = ");
                sb.append(filter.getId());

                if (filter.getBool() != null) {
                    sb.append(" and BOOLEAN_VALUE = ");
                    sb.append(filter.getBool());
                    sb.append(" ");
                }

                if (filter.getGt() != null) {
                    if (featureOccurrence.getDecimalNumberValue() != null) {
                        sb.append(" and DECIMAL_NUMBER_VALUE >= ");
                        sb.append(filter.getGt());
                        sb.append(" ");
                    } else {
                        sb.append(" and FLOATING_NUMBER_VALUE >= ");
                        sb.append(filter.getGt());
                        sb.append(" ");
                    }
                }

                if (filter.getLt() != null) {
                    if (featureOccurrence.getDecimalNumberValue() != null) {
                        sb.append(" and DECIMAL_NUMBER_VALUE <= ");
                        sb.append(filter.getLt());
                        sb.append(" ");
                    } else {
                        sb.append(" and FLOATING_NUMBER_VALUE <= ");
                        sb.append(filter.getLt());
                        sb.append(" ");
                    }
                }

                StringBuilder eqValue = new StringBuilder();
                eqValue.append(":eqValue");
                eqValue.append(tmp);
                if (filter.getEq() != null) {
                    if (featureOccurrence.getDecimalNumberValue() != null) {
                        sb.append(" and DECIMAL_NUMBER_VALUE = ");
                        sb.append(eqValue);

                        sb.append(" ");
                    }
                    if (featureOccurrence.getFloatingNumberValue() != null) {
                        sb.append(" and FLOATING_NUMBER_VALUE = ");
                        sb.append(eqValue);
                        sb.append(" ");
                    }
                    if (featureOccurrence.getTextValue() != null) {
                        sb.append(" and UPPER(TEXT_VALUE) = UPPER(");
                        sb.append(eqValue);
                        sb.append(" ) ");
                    }
                    filterIndex.add(tmp);
                }

                if(tmp+1 < rentalADSearchDTO.getFilters().size()) {
                    Optional<FeatureOccurrence> nextFO = featureOccurrenceRepo.findFeatureOccurrenceByFeatureId(rentalADSearchDTO.getFilters().get(tmp+1).getId());
                    if(nextFO.isEmpty()) {
                        tmp++;
                        continue;
                    }
                }

                if (tmp == 0 && sizeListDTO != tmp + 1) {
                    sb.append(" INTERSECT ");
                }

                tmp++;
            }
        }

        if (sizeListDTO > 0) {
            sb.append(") ORDER BY IS_PROMOTED DESC, CREATION_DATE DESC ");
            sb.append(" LIMIT :limit OFFSET :offset ;");
        } else {
            sb.append(" ORDER BY IS_PROMOTED DESC, CREATION_DATE DESC ");
            sb.append("  LIMIT :limit OFFSET :offset ;");
        }

        Query query = entityManager.createNativeQuery(sb.toString(), RentalAd.class);
        if (searchString != null && !searchString.equals("")) {
            query.setParameter("searchedNameProduct", searchString);
            query.setParameter("title", searchString);
            query.setParameter("description", searchString);
        }

        for (Integer index : filterIndex) {
            query.setParameter("eqValue"+index, rentalADSearchDTO.getFilters().get(index).getEq());
        }

        if (howManyRecord <= 0)
            howManyRecord = 0;

        query.setParameter("limit", howManyRecord);
        query.setParameter("offset", howManyRecord * page);

        for (Object rentalAd : query.getResultList()) {
            RentalAdResponseDTO mapRentalAdToDTO = setIdImageAndThumbnailToRentalAdResponseDTO(mapper, (RentalAd) rentalAd);
            returnList.add(mapRentalAdToDTO);
        }

        PaginatedDTO<RentalAdResponseDTO> paginatedDTO = new PaginatedDTO<>((long) returnList.size(), returnList);

        return paginatedDTO;
    }


    public List<RentalAdResponseDTO> productHistory(Long productId) {
        Product product = Get(Product.class, productRepo, productId);

        if (!product.getUser().equals(logged()))
            throw new User.PermissionDenied();

        List<RentalAdResponseDTO> rentalAdList = new ArrayList<>();

        for (Ad includeAd : product.getIncludeAds()) {
            if (includeAd.getDiscriminator().equals(RENTAL_AD))
                rentalAdList.add(mapper.map(includeAd, RentalAdResponseDTO.class));
        }

        return rentalAdList;
    }


    public AdStatus getRentalAdStatus(Long idRentalAd, String tokenWithBearer) {
        RentalAd rentalAd = Get(RentalAd.class, rentalAdRepo, idRentalAd);
        User rentalAdOwner = rentalAd.getUser();

        if (!authorization.isCurrentUser(rentalAdOwner, tokenWithBearer)) throw new ForbiddenException();

        AdStatus timeBasedStatus = getTimeBasedStatus(rentalAd);
        ModTicket latestModTicket = modTicketRepo.getLatestModTicketOfRentalAd(idRentalAd);

        if (latestModTicket != null) {
            AdStatus modTicketBasedStatus = getModTicketBasedStatus(rentalAd, latestModTicket);
            if (modTicketBasedStatus != null) {
                return modTicketBasedStatus;
            }
        }

        if (!rentalAd.isVisible())
            return AdStatus.DELETED;

        return timeBasedStatus;
    }

    private AdStatus getModTicketBasedStatus(RentalAd rentalAd, ModTicket latestModTicket) {

        if (latestModTicket.getResult() == null)
            return AdStatus.IN_MODERATION;

        if (latestModTicket.getResult() == ModTicketResult.TO_MODIFY)
            return AdStatus.NEEDS_CORRECTION;

        if (latestModTicket.getResult() == ModTicketResult.HIDDEN && !rentalAd.isVisible())
            return AdStatus.HIDDEN_BY_MODERATOR;

        return null;
    }

    private AdStatus getTimeBasedStatus(RentalAd rentalAd) {
        LocalDate today = LocalDate.now();
        LocalDate rentalAdStartDay;
        LocalDate rentalAdEndDay;

        for (TimeSlot timeSlot : rentalAd.getTimeSlotList()) {
            rentalAdStartDay = timeSlot.getStartDate();
            rentalAdEndDay = timeSlot.getEndDate();

            if ((rentalAdEndDay == null && (today.isAfter(rentalAdStartDay) || today.isEqual(rentalAdStartDay)))
                    || ((today.isAfter(rentalAdStartDay) || today.isEqual(rentalAdStartDay)) && (today.isBefore(rentalAdEndDay) || today.isEqual(rentalAdEndDay)))) {
                return AdStatus.ACTIVE;
            }
        }
        return AdStatus.EXPIRED;
    }

    private boolean checkVulgar(RentalAd rentalAd, String userEmail) {
        if (ProfanityFilter.getIsVulgar()) {
            rentalAd.setVisible(false);

            ModTicket modTicket = new ModTicket();
            modTicket.setAd(rentalAd);
            modTicket.setSubject(ModTicketSubject.OFFENDED);
            modTicket.setDescription("To ogłoszenie zawiera wulgaryzmy. Zgłoszenie wysłane automatycznie.");
            modTicketRepo.save(modTicket);
            emailSender.sendMessageWhenRentalAdGoToModerator(userEmail);
            return true;
        }

        return false;
    }

    public PaginatedDTO<RentalAdWithModTicketDTO> getMyRentalAdsToCorrect(int page, int howManyRecord, String authorization) {
        Long idUser = this.authorization.getIdUserFromToken(authorization);
        List<RentalAdWithModTicketDTO> result = new ArrayList<>();

        Page<AdModTicket> rentalAdPage = rentalAdRepo.findAdsByResult(idUser, ModTicketResult.TO_MODIFY.toString(), PageRequest.of(page, howManyRecord));

        for (AdModTicket adModTicket : rentalAdPage) {
            RentalAd rentalAd = AService.Get(RentalAd.class, rentalAdRepo, adModTicket.getAdId());
            ImageEntity firstImage = imageRepo.findFirstImageForRentalAd(rentalAd.getId());
            RentalAdWithModTicketDTO obj = mapper.map(rentalAd, RentalAdWithModTicketDTO.class);
            obj.setImagesIdList(Collections.singletonList(firstImage.getId()));
            obj.setThumbnailIdList(Collections.singletonList(firstImage.getImageThumbnail().getId()));
            obj.setModTicketId(adModTicket.getModTicketId());
            result.add(obj);
        }

        Long count = (long) result.size();
        return new PaginatedDTO<>(count, rentalAdPage.getTotalPages(), result);
    }

    public List<RentalAdResponseDTO> getRentalAdToFix(String authorization) {
        Long idUser = this.authorization.getIdUserFromToken(authorization);
        User user = Get(User.class, userRepo, idUser);
        List<RentalAdResponseDTO> list = new ArrayList<>();
        for (RentalAd rentalAd : rentalAdRepo.findAllByUserAndVisibleFalse(user.getId())) {
            RentalAdResponseDTO mapRentalAdToDTO = setIdImageAndThumbnailToRentalAdResponseDTO(mapper, rentalAd);
            list.add(mapRentalAdToDTO);


        }
        return list;


    }

}
