package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.Entities.Ad;
import com.example.szerowniabackend.Models.Entities.TimeSlot;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class AService {

    private static UserRepo userRepo;

    public AService(UserRepo userRepo) {
        AService.userRepo = userRepo;
    }

    static <T, ID> T Get(Class c, JpaRepository<T, ID> repository, ID id) {
        Optional<T> optional = repository.findById(id);
        if (optional.isEmpty()) throw new NotFoundException(c, id);
        return optional.get();
    }

    static User logged() {
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepo.findUserByEmail(email).orElseThrow(User.NotFound::new);
    }
}
