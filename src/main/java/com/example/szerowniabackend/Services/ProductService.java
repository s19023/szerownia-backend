package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.DTOs.Requests.*;
import com.example.szerowniabackend.Models.DTOs.Responses.CategoryReferenceDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ProductByIdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ProductGetMyProductsResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ProductListResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.*;
import com.example.szerowniabackend.Models.Builders.ProductBuilder;
import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.Entities.Enums.FeatureType;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Security.JwtTokenUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.example.szerowniabackend.Services.AService.*;


@Service
@Transactional
public class ProductService {

    final private ProductRepo productRepo;
    final private CategoryRepo categoryRepo;
    final private FeatureRepo featureRepo;
    final private FeatureOccurrenceRepo featureOccurrenceRepo;
    final private ModelMapper mapper;

    @Autowired
    public ProductService(ProductRepo productRepo, CategoryRepo categoryRepo, FeatureRepo featureRepo, FeatureOccurrenceRepo featureOccurrenceRepo) {
        this.productRepo = productRepo;
        this.categoryRepo = categoryRepo;
        this.featureRepo = featureRepo;
        this.featureOccurrenceRepo = featureOccurrenceRepo;
        this.mapper = new ModelMapper();
    }


    static boolean validateProductForModifications(List<Ad> adList) {

        for (Ad ad : adList) {
            for (TimeSlot timeSlot : ad.getTimeSlotList()) {

                LocalDate startDate = timeSlot.getStartDate();
                LocalDate endDate = timeSlot.getEndDate();
                LocalDate today = LocalDate.now();

                if (((today.isEqual(startDate)
                        || today.isAfter(startDate)) && endDate == null)
                        || (today.isEqual(startDate)
                        || today.isAfter(startDate)) && (today.isEqual(endDate)
                        || today.isBefore(endDate))) {
                    return false;
                }
            }
        }
        return true;
    }


    public List<ProductListResponseDTO> getProductsList() {
        List<ProductListResponseDTO> productsList = new ArrayList<>();

        for (Product product : productRepo.findAll()) {
            if (!product.isVisible()) {
                continue;
            }
            if (!product.isSearchProduct()) {
                CategoryReferenceDTO mappedCategoryReferenceDTO = mapper.map(product.getCategory(), CategoryReferenceDTO.class);
                ProductListResponseDTO mappedProductListResponseDTO = mapper.map(product, ProductListResponseDTO.class);
                mappedProductListResponseDTO.setCategoryReferenceDTO(mappedCategoryReferenceDTO);
                mappedProductListResponseDTO.setModifiable(validateProductForModifications(product.getIncludeAds()));
                productsList.add(mappedProductListResponseDTO);
            }
        }
        return productsList;
    }

    public ProductByIdResponseDTO getProductById(Long idProduct) {
        Product productById = Get(Product.class, productRepo, idProduct);

        if (!productById.isVisible()) {
            throw new NotFoundException("Nie znaleziono przedmiotu");
        }

        if (productById.isSearchProduct())
            throw new Product.IsASearchProduct();

        CategoryReferenceDTO mappedCategoryReferenceDTO = mapper.map(productById.getCategory(), CategoryReferenceDTO.class);
        ProductByIdResponseDTO mappedProductByIdResponseDTO = mapper.map(productById, ProductByIdResponseDTO.class);

        List<FeatureOccurrenceReferenceDTO> featureOccurrences = new ArrayList<>();
        for (FeatureOccurrence featureOccurrence : productById.getFeatureOccurrences()) {
            FeatureOccurrenceReferenceDTO mappedFeatureOccurrenceReferenceDTO = mapper.map(featureOccurrence, FeatureOccurrenceReferenceDTO.class);
            mappedFeatureOccurrenceReferenceDTO.setIdFeature(featureOccurrence.getFeature().getIdFeature());
            featureOccurrences.add(mappedFeatureOccurrenceReferenceDTO);
        }

        mappedProductByIdResponseDTO.setCategoryReferenceDTO(mappedCategoryReferenceDTO);
        mappedProductByIdResponseDTO.setFeatureOccurrences(featureOccurrences);
        mappedProductByIdResponseDTO.setModifiable(validateProductForModifications(productById.getIncludeAds()));

        return mappedProductByIdResponseDTO;
    }

    public void createProduct(ProductCreateRequestDTO productCreateRequestDTO) {
        ProductBuilder productBuilder = new ProductBuilder(productCreateRequestDTO.getName(), productCreateRequestDTO.getCondition(),
                productCreateRequestDTO.getEstimatedValue());

        if (productCreateRequestDTO.getMake() != null)
            productBuilder.withMake(productCreateRequestDTO.getMake());


        if (productCreateRequestDTO.getModel() != null)
            productBuilder.withModel(productCreateRequestDTO.getModel());


        Product productToCreate = productBuilder.build();

        Category productCategory = Get(Category.class, categoryRepo, productCreateRequestDTO.getIdCategory());

        if (productCategory.isAbstract())
            throw new Product.CannotBeAssignedToAbstractCategory();

        User user = logged();

        productToCreate.setCategory(productCategory);
        productToCreate.setUser(user);

        if (productCreateRequestDTO.isSearchProduct())
            productToCreate.setSearchProduct(true);

        productRepo.save(productToCreate);

        for (FeatureOccurrenceAssignProductRequestDTO productFeature : productCreateRequestDTO.getProductFeatures()) {
            validateProductFeature(productFeature);

            FeatureOccurrence featureOccurrence = null;
            Feature feature = Get(Feature.class, featureRepo, productFeature.getIdFeature());

            switch (feature.getType()) {
                case TEXT:
                    if (productFeature.getTextValue() == null)
                        throw new FeatureOccurrence.RequiresValue("tekstowej");

                    featureOccurrence = new FeatureOccurrence(productFeature.getTextValue(), feature, productToCreate);
                    break;
                case DOUBLE:
                    if (productFeature.getFloatingNumberValue() == null)
                        throw new FeatureOccurrence.RequiresValue("zmiennoprzecinkowej");


                    featureOccurrence = new FeatureOccurrence(productFeature.getFloatingNumberValue(), feature, productToCreate);
                    break;
                case INTEGER:
                    if (productFeature.getDecimalNumberValue() == null)
                        throw new FeatureOccurrence.RequiresValue("stałoprzecinkowej");

                    featureOccurrence = new FeatureOccurrence(productFeature.getDecimalNumberValue(), feature, productToCreate);
                    break;
                case BOOLEAN:
                    if (productFeature.getBooleanValue() == null)
                        throw new FeatureOccurrence.RequiresValue("logiczenj");

                    featureOccurrence = new FeatureOccurrence(productFeature.getBooleanValue(), feature, productToCreate);
                    break;
            }
            featureOccurrenceRepo.save(featureOccurrence);
        }
    }

    public void updateProduct(Long idProduct, ProductUpdateRequestDTO productUpdateRequestDTO) {

        Product productToUpdate = Get(Product.class, productRepo, idProduct);

        if (!validateProductForModifications(productToUpdate.getIncludeAds())) {
            throw new BadRequestException("Nie można zaktualizować przedmiotu przypisanego do aktulanie aktywnego ogłoszenia");
        }

        if (productUpdateRequestDTO.getName() != null)
            productToUpdate.setName(productUpdateRequestDTO.getName());

        if (productUpdateRequestDTO.getMake() != null)
            productToUpdate.setMake(productUpdateRequestDTO.getMake());

        if (productUpdateRequestDTO.getModel() != null)
            productToUpdate.setModel(productUpdateRequestDTO.getModel());

        if (productUpdateRequestDTO.getCondition() != null)
            productToUpdate.setCondition(productUpdateRequestDTO.getCondition());

        if (productUpdateRequestDTO.getEstimatedValue() != null)
            productToUpdate.setEstimatedValue(productUpdateRequestDTO.getEstimatedValue());

        if (productUpdateRequestDTO.getIdCategory() != null) {
            Category updatedCategory = Get(Category.class, categoryRepo, productUpdateRequestDTO.getIdCategory());

            if (updatedCategory.isAbstract())
                throw new Product.CannotBeAssignedToAbstractCategory();

            productToUpdate.setCategory(updatedCategory);
        }

        for (FeatureOccurrenceUpdateRequestDTO featureOccurrenceUpdateRequestDTO : productUpdateRequestDTO.getFeatureOccurrenceUpdateRequestDTO()) {
            FeatureOccurrence featureOccurrenceToUpdate = Get(FeatureOccurrence.class, featureOccurrenceRepo, featureOccurrenceUpdateRequestDTO.getId());
            FeatureType featureType = featureOccurrenceToUpdate.getFeature().getType();

            switch (featureType) {
                case TEXT:
                    if (featureOccurrenceUpdateRequestDTO.getTextValue() == null)
                        throw new FeatureOccurrence.RequiresValue("tekstowej");

                    featureOccurrenceToUpdate.setTextValue(featureOccurrenceUpdateRequestDTO.getTextValue());
                    break;
                case DOUBLE:
                    if (featureOccurrenceUpdateRequestDTO.getFloatingNumberValue() == null)
                        throw new FeatureOccurrence.RequiresValue("zmiennoprzecinkowej");


                    featureOccurrenceToUpdate.setFloatingNumberValue(featureOccurrenceUpdateRequestDTO.getFloatingNumberValue());
                    break;
                case INTEGER:
                    if (featureOccurrenceUpdateRequestDTO.getDecimalNumberValue() == null)
                        throw new FeatureOccurrence.RequiresValue("stałoprzecinkowej");

                    featureOccurrenceToUpdate.setDecimalNumberValue(featureOccurrenceUpdateRequestDTO.getDecimalNumberValue());
                    break;
                case BOOLEAN:
                    if (featureOccurrenceUpdateRequestDTO.getBooleanValue() == null)
                        throw new FeatureOccurrence.RequiresValue("logicznej");

                    featureOccurrenceToUpdate.setBooleanValue(featureOccurrenceUpdateRequestDTO.getBooleanValue());
                    break;
            }
            featureOccurrenceRepo.save(featureOccurrenceToUpdate);
        }
        productRepo.save(productToUpdate);
    }

    public void deleteProduct(Long idProduct) {
        Product product = Get(Product.class, productRepo, idProduct);

        if (!validateProductForModifications(product.getIncludeAds())) {
            throw new BadRequestException("Nie można usunąć przedmiotu przypisanego do aktulanie aktywnego ogłoszenia");
        }

        product.setVisible(false);
    }

    private void validateProductFeature(FeatureOccurrenceAssignProductRequestDTO productFeature) {
        int setFieldCount = 0;

        if (productFeature.getTextValue() != null)
            setFieldCount += 1;

        if (productFeature.getDecimalNumberValue() != null)
            setFieldCount += 1;

        if (productFeature.getFloatingNumberValue() != null)
            setFieldCount += 1;

        if (productFeature.getBooleanValue() != null)
            setFieldCount += 1;

        if (setFieldCount != 1)
            throw new FeatureOccurrence.OnlyOneValueAllowed();
    }

    public List<ProductGetMyProductsResponseDTO> getMyProducts(boolean showSearch) {
        Long id = logged().getId();
        List<Product> productsList = productRepo.findAllByUserIdAndIsSearchProduct(id, showSearch);
        List<ProductGetMyProductsResponseDTO> productsResponseDTOS = new ArrayList<>();

        for (Product product : productsList) {
            if (product.isVisible()) {
                ProductGetMyProductsResponseDTO mapProduct = mapper.map(product, ProductGetMyProductsResponseDTO.class);
                productsResponseDTOS.add(mapProduct);
            }
        }
        return productsResponseDTOS;
    }

    public List<SuggestProductCategoryResponseDTO> suggestProductCategory(String name) {
        return productRepo.suggestProductCategory(name);
    }

}
