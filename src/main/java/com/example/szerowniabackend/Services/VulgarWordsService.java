package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.VulgarWordResponseDTO;
import com.example.szerowniabackend.Models.Entities.VulgarWords;
import com.example.szerowniabackend.Models.ProfanityFilter.ProfanityFilter;
import com.example.szerowniabackend.Models.ProfanityFilter.ProfanityFilterHelper;
import com.example.szerowniabackend.Models.Repositories.VulgarWordsRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class VulgarWordsService {

    private final VulgarWordsRepo vulgarWordsRepo;
    private final ProfanityFilter profanityFilter;
    
    @Autowired
    public VulgarWordsService(VulgarWordsRepo vulgarWordsRepo, ProfanityFilter profanityFilter) {
        this.vulgarWordsRepo = vulgarWordsRepo;
        this.profanityFilter = profanityFilter;
    }

    public PaginatedDTO<VulgarWords> getListOfVulgarWords(Integer page, Integer howManyRecord,
                                                          Boolean sortAscending, String vulgarWord) {
        var sort = Sort.Direction.ASC;
        if (!sortAscending)
            sort = Sort.Direction.DESC;

        Long count;
        if (vulgarWord == null)
            count = vulgarWordsRepo.count();
        else
            count = vulgarWordsRepo.countByVulgarWordContaining(vulgarWord);

        List<VulgarWords> vulgarWords = vulgarWordsRepo.findAllVulgarWordsPageable(
                PageRequest.of(page, howManyRecord, sort, "VULGAR_WORD"), vulgarWord);

        return new PaginatedDTO<>(count, vulgarWords);
    }

    public VulgarWordResponseDTO getVulgarWordById(Long id) {
        VulgarWords vulgarWords = AService.Get(VulgarWords.class, vulgarWordsRepo, id);

        ModelMapper mapper = new ModelMapper();
        return mapper.map(vulgarWords, VulgarWordResponseDTO.class);
    }

    public VulgarWords getVulgarWordByWord(String vulgarWord) {
        Optional<VulgarWords> vulgarWordsOptional = vulgarWordsRepo.findByVulgarWord(vulgarWord);

        if (vulgarWordsOptional.isEmpty())
            throw new VulgarWords.NotFound(vulgarWord);

        return vulgarWordsOptional.get();
    }

    public void createVulgarWord(VulgarWordResponseDTO vulgarWords) {
        Optional<VulgarWords> vulgarWordsOptional = vulgarWordsRepo.findByVulgarWord(vulgarWords.getVulgarWord());

        if (vulgarWordsOptional.isPresent())
            throw new VulgarWords.VulgarWordExist();

        VulgarWords vulgarWordsNew = new VulgarWords();
        vulgarWordsNew.setVulgarWord(vulgarWords.getVulgarWord());

        vulgarWordsRepo.save(vulgarWordsNew);
        ProfanityFilterHelper.loadBadWordsToMapFromDatabase();
    }

    public void updateVulgarWord(Long vulgarWordId, VulgarWordResponseDTO vulgarWordResponseDTO) {
        VulgarWords vulgarWords = AService.Get(VulgarWords.class, vulgarWordsRepo, vulgarWordId);
        
        checkVulgar(vulgarWordResponseDTO, Optional.of(vulgarWords));
    }


    public void updateVulgarWordByWord(String word, VulgarWordResponseDTO vulgarWordResponseDTO) {
        Optional<VulgarWords> vulgarWordsOptional = vulgarWordsRepo.findByVulgarWord(word);

        if (vulgarWordsOptional.isEmpty()) 
            throw new VulgarWords.NotFound(word);
        
        checkVulgar(vulgarWordResponseDTO, vulgarWordsOptional);
    }

    public void deleteVulgarWordById(Long id) {
        vulgarWordsRepo.delete(AService.Get(VulgarWords.class, vulgarWordsRepo, id));
        ProfanityFilterHelper.loadBadWordsToMapFromDatabase();
    }

    public void deleteVulgarWordByVulgarWord(String word) {
        Optional<VulgarWords> vulgarWordsOptional = vulgarWordsRepo.findByVulgarWord(word);

        if (vulgarWordsOptional.isEmpty()) 
            throw new VulgarWords.NotFound(word);
        
        vulgarWordsRepo.delete(vulgarWordsOptional.get());
        ProfanityFilterHelper.loadBadWordsToMapFromDatabase();
    }

    private void checkVulgar(VulgarWordResponseDTO vulgarWordResponseDTO, Optional<VulgarWords> vulgarWordsOptional) {
        Optional<VulgarWords> vulgarWordGivenOptional = vulgarWordsRepo.findByVulgarWord(vulgarWordResponseDTO.getVulgarWord());

        if (vulgarWordGivenOptional.isPresent())
            throw new VulgarWords.VulgarWordExist();

        VulgarWords vulgarWords = vulgarWordsOptional.get();
        vulgarWords.setVulgarWord(vulgarWordResponseDTO.getVulgarWord());

        vulgarWordsRepo.save(vulgarWords);
        ProfanityFilterHelper.loadBadWordsToMapFromDatabase();
    }
}























