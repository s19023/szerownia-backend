package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Models.DTOs.Responses.StatisticsDTO;
import com.example.szerowniabackend.Models.Entities.Enums.EventType;
import com.example.szerowniabackend.Models.Entities.RentalAd;
import com.example.szerowniabackend.Models.Entities.StatsEvent;
import com.example.szerowniabackend.Models.Repositories.RentalAdRepo;
import com.example.szerowniabackend.Models.Repositories.StatisticsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Service
public class StatisticsService {

    private final StatisticsRepo statisticsRepo;
    private final RentalAdRepo rentalAdRepo;
    private LocalDate sinceWhen = LocalDate.now().minusDays(7);

    @Autowired
    public StatisticsService(StatisticsRepo statisticsRepo, RentalAdRepo rentalAdRepo) {
        this.statisticsRepo = statisticsRepo;
        this.rentalAdRepo = rentalAdRepo;
    }

    public StatisticsDTO getCreatedRentalAdStats() {
        List<LocalDate> dateList = countEvents(EventType.AD_CREATION, sinceWhen);

        StatisticsDTO rentalAdCreationDTO = getStatisticsDTO(dateList);

        return rentalAdCreationDTO;
    }

    public StatisticsDTO getRentStats() {
        List<LocalDate> dateList = countEvents(EventType.RENT, sinceWhen);

        StatisticsDTO rentStatsDTO = getStatisticsDTO(dateList);

        return rentStatsDTO;
    }

    public StatisticsDTO getCreatedUserStats() {
        List<LocalDate> dateList = countEvents(EventType.ACCOUNT_CREATION, sinceWhen);

        StatisticsDTO userStatsDTO = getStatisticsDTO(dateList);

        return userStatsDTO;
    }

    public Long getViewedRentalAdStats(Long idRentalAd) {
        RentalAd rentalAd = AService.Get(RentalAd.class, rentalAdRepo, idRentalAd);
        return countEvents(EventType.VIEWING_AD, rentalAd.getCreationDate(), idRentalAd);
    }


    public StatisticsDTO getTotalAdViews() {
        List<LocalDate> dateList = countEvents(EventType.VIEWING_AD, sinceWhen);
        StatisticsDTO totalCount = getStatisticsDTO(dateList);
        return totalCount;
    }

    private List<LocalDate> countEvents(EventType eventType, LocalDate sinceWhen) {
        return statisticsRepo.getRecentEventsOfType(eventType, sinceWhen);
    }

    private Long countEvents(EventType eventType, LocalDate sinceWhen, Long relatedItemId) {
        return statisticsRepo.countRecentEventsForItem(eventType, sinceWhen, relatedItemId);
    }

    private StatisticsDTO getStatisticsDTO(List<LocalDate> dateList) {

        Map<LocalDate, Long> countedByDay = dateList.stream().collect(Collectors.groupingBy(d -> d, TreeMap::new, Collectors.counting()));

        List<LocalDate> dates = new ArrayList<>();
        List<Long> counts = new ArrayList<>();

        for (LocalDate day = sinceWhen; day.isBefore(LocalDate.now().plusDays(1)); day = day.plusDays(1)) {
            Long count = countedByDay.getOrDefault(day, 0l);
            dates.add(day);
            counts.add(count);
        }

        StatisticsDTO statisticsDTO = new StatisticsDTO();
        statisticsDTO.setDates(dates.toArray(new LocalDate[dates.size()]));
        statisticsDTO.setCounts(counts.toArray(new Long[counts.size()]));

        return statisticsDTO;
    }

    public void recordEvent(EventType eventType, Long relatedItemId) {

        StatsEvent statsEvent = new StatsEvent();
        statsEvent.setEventType(eventType);
        statsEvent.setOccurredOn(LocalDate.now());
        statsEvent.setRelatedItemId(relatedItemId);

        statisticsRepo.save(statsEvent);
    }
}
