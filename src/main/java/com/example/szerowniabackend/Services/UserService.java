package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Email.EmailSender;
import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Exceptions.InvaildTokenException;
import com.example.szerowniabackend.Models.DTOs.Requests.*;
import com.example.szerowniabackend.Models.DTOs.Responses.*;
import com.example.szerowniabackend.Models.DTOs.Responses.GetSearchAd.SearchAdResponseDTO;
import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.Entities.Enums.EventType;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Security.Authorization;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.szerowniabackend.Services.AService.*;

@Transactional
@Service
public class UserService implements UserDetailsService {

    private final UserRepo userRepo;
    private final RTokenRepo rTokenRepo;
    private PasswordEncoder passwordEncoder;
    private final StatisticsService statisticsService;
    private final Authorization authorization;
    private final FileSystemRepo fileSystemRepo;
    private final ImageRepo imageRepo;
    private final EmailSender emailSender;
    final private ModelMapper mapper;
    private final RentalAdRepo rentalAdRepo;
    private final SearchAdRepo searchAdRepo;
    private final OpinionRepo opinionRepo;

    @Autowired
    public UserService(UserRepo userRepo, RTokenRepo rTokenRepo, PasswordEncoder passwordEncoder,
                       StatisticsService statisticsService, FileSystemRepo fileSystemRepo,
                       ImageRepo imageRepo, Authorization authorization, EmailSender emailSender,
                       RentalAdRepo rentalAdRepo, SearchAdRepo searchAdRepo, OpinionRepo opinionRepo) {
        this.userRepo = userRepo;
        this.rTokenRepo = rTokenRepo;
        this.passwordEncoder = passwordEncoder;
        this.statisticsService = statisticsService;
        this.fileSystemRepo = fileSystemRepo;
        this.imageRepo = imageRepo;
        this.authorization = authorization;
        this.emailSender = emailSender;
        this.rentalAdRepo = rentalAdRepo;
        this.searchAdRepo = searchAdRepo;
        this.opinionRepo = opinionRepo;
        mapper = new ModelMapper();
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        return userRepo.findUserByEmail(email).orElseThrow(() -> new User.NotFound());
    }

    public List<UserResponseDTO> getAllUsers() {
        List<UserResponseDTO> list = new ArrayList<>();

        for (User u : userRepo.findAll()) {
            UserResponseDTO userResponseDTO = mapper.map(u, UserResponseDTO.class);
            if (u.getUserImage() != null)
                userResponseDTO.setUserImageId(u.getUserImage().getId());

            list.add(userResponseDTO);
        }
        return list;
    }

    public UserRespDto getUser(Long id) {
        User user = Get(User.class, userRepo, id);
        UserRespDto response = mapper.map(user, UserRespDto.class);

        if (user.getUserImage() != null)
            response.setUserImageId(user.getUserImage().getId());

        return response;
    }

    public void createUser(UserRegisterRequestDTO req) {
        User user = new User(req.getEmail(), req.getFirstName(), req.getLastName(), req.getTelephoneNumber(),
                passwordEncoder.encode(req.getPassword()), "salt", Role.USER);
        userRepo.save(user);
        statisticsService.recordEvent(EventType.ACCOUNT_CREATION, user.getId());
        emailSender.sendMessageWhenUserRegistered(user.getEmail());
    }

    public void createSuperUser(SuperUserRegisterRequestDTO req) {
        User user = new User(req.getEmail(), req.getFirstName(), req.getLastName(), req.getTelephoneNumber(),
                passwordEncoder.encode(req.getPassword()), "salt", Role.valueOf(req.getRole()));
        userRepo.save(user);

    }

    public void updateUser(UserUpdateRequestDTO dto) {
        User user = logged();

        user.setEmail(dto.getEmail());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setTelephoneNumber(dto.getTelephoneNumber());

        userRepo.save(user);
    }

    public void deleteUser(Long id) {
        Get(User.class, userRepo, id).setBlocked(true);
    }

    public UserDetailsDTO getMyDetails() {
        User user = logged();
        UserDetailsDTO response = mapper.map(user, UserDetailsDTO.class);

        response.setAverageRate2Sharer(user.getSharerRate());
        response.setAverageRateBorrower(user.getBorrowerRate());

        if (user.getUserImage() != null)
            response.setUserImageId(user.getUserImage().getId());

        return response;
    }

    public List<UserResponseDTO> getInspectors() {
        List<User> users = userRepo.getAllByRole(Role.APPRAISER);
        List<UserResponseDTO> mappedUsers = new ArrayList<>();

        for (User u : users)
            mappedUsers.add(mapper.map(u, UserResponseDTO.class));

        return mappedUsers;
    }

    public String changeProfileImage(MultipartFile image) {
        User user = logged();

        if (user.getUserImage() != null) {
            ImageEntity imageToDelete = Get(ImageEntity.class, imageRepo, user.getUserImage().getId());

            imageRepo.deleteImage(imageToDelete.getId());
            fileSystemRepo.deleteFile(imageToDelete.getLocation());
        }

        byte[] bytes = new byte[0];
        try {
            bytes = image.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String imageName = image.getOriginalFilename();
        checkIsExtensionIsCorrect(imageName);

        String location = null;
        var imageNameWithDate = new Date().getTime() + "-" + imageName;
        try {
            location = fileSystemRepo.save(bytes, imageNameWithDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ImageEntity imageEntity = new ImageEntity(imageNameWithDate, location);
        imageEntity.setUser(user);
        imageRepo.save(imageEntity);

        user.setUserImage(imageEntity);
        userRepo.save(user);

        return imageNameWithDate;
    }

    public void changePremium() {
        User user = logged();
        user.setPremium(!user.getPremium());

        userRepo.save(user);
    }

    public void extendPremium() {
        User user = logged();
        LocalDate premiumTo = user.getPremiumTo();

        if (premiumTo == null) throw new ForbiddenException();

        if (premiumTo.isAfter(LocalDate.now()))
            user.setPremiumTo(premiumTo.plusMonths(1));

        userRepo.save(user);
    }

    public void getResetToken(String email) {
        if (userRepo.findUserByEmail(email).isEmpty()) throw new User.NotFound();
        ResetToken token = new ResetToken(email);

        rTokenRepo.save(token);
        emailSender.sendMessageWithResetPasswordToken(token);
    }

    public void resetPassword(ResetRequestDTO req) {
        String token = req.getToken();
        Optional<ResetToken> optionalrToken = rTokenRepo.findResetTokenByToken(token);
        if (optionalrToken.isEmpty()) throw new ResetToken.NotFound(token);

        ResetToken rt = optionalrToken.get();
        if (!rt.isValid()) throw new InvaildTokenException("Reset");

        String email = rt.getEmail();
        Optional<User> optionalUser = userRepo.findUserByEmail(email);
        if (optionalUser.isEmpty()) throw new User.NotFound();

        optionalUser.get().setPassword(passwordEncoder.encode(req.getNewpassword()));
    }

    public boolean checkIfExists(String email) {
        return userRepo.findUserByEmail(email).isPresent();
    }

    public PaginatedDTO<RentalAdWithDescriptionDTO> getUserRentalAds(Long userId, int page, int howManyRecord) {
        List<RentalAdWithDescriptionDTO> mappedRentalAds = new ArrayList<>();
        Page<RentalAd> rentalAdPage = rentalAdRepo.findAllByUserIdOrderDescByStartDate(userId, PageRequest.of(page, howManyRecord));

        for (RentalAd rentalAd : rentalAdPage) {
            RentalAdWithDescriptionDTO mapRentalAdToDTO = mapper.map(rentalAd, RentalAdWithDescriptionDTO.class);
            ImageEntity firstImageForRentalAd = imageRepo.findFirstImageForRentalAd(rentalAd.getId());
            if (firstImageForRentalAd != null) {
                mapRentalAdToDTO.setImagesIdList(Collections.singletonList(firstImageForRentalAd.getId()));
                mapRentalAdToDTO.setThumbnailIdList(Collections.singletonList(firstImageForRentalAd.getImageThumbnail().getId()));
            }
            mappedRentalAds.add(mapRentalAdToDTO);
        }

        return new PaginatedDTO<>((long) mappedRentalAds.size(), rentalAdPage.getTotalPages(), mappedRentalAds);
    }

    public PaginatedDTO<SearchAdResponseDTO> getUserSearchAds(Long userId, int page, int howManyRecord) {
        List<SearchAdResponseDTO> mappedSearchAds = new ArrayList<>();
        Page<SearchAd> searchAdPage = searchAdRepo.findUsersSearchAds(userId, PageRequest.of(page, howManyRecord));

        for (SearchAd searchAd : searchAdPage) {
            SearchAdResponseDTO mapSearchAdToDTO = mapper.map(searchAd, SearchAdResponseDTO.class);
            mappedSearchAds.add(mapSearchAdToDTO);
        }

        return new PaginatedDTO<>((long) mappedSearchAds.size(), searchAdPage.getTotalPages(), mappedSearchAds);
    }

    public PaginatedDTO<UserOpinionsResponseDTO> getUserOpinions(Long userId, int page, int howManyRecord) {

        AService.Get(User.class, userRepo, userId);
        Page<Opinion> opinionsPage = opinionRepo.findUserOpinions(userId, PageRequest.of(page, howManyRecord));

        List<UserOpinionsResponseDTO> result = new ArrayList<>();

        for (Opinion o : opinionsPage) {

            UserOpinionsResponseDTO opinionRes = new UserOpinionsResponseDTO();
            opinionRes.setIdOpinion(o.getId());
            opinionRes.setRating(o.getRating());
            opinionRes.setComment(o.getComment());
            opinionRes.setAboutSharer(o.isAboutSharer());
            opinionRes.setDate(o.getDate());

            Ad ad = o.getHire().getAd();
            opinionRes.setIdAd(ad.getId());
            opinionRes.setAdTitle(ad.getTitle());

            ImageEntity firstImageForAd = imageRepo.findFirstImageForRentalAd(ad.getId());
            if (firstImageForAd != null) {
                opinionRes.setIdAdThumbnail(firstImageForAd.getImageThumbnail().getId());
            }

            User author = o.getAuthor();
            opinionRes.setIdAuthor(author.getId());
            if (author.getUserImage() != null) {
                opinionRes.setIdAuthorAvatar(author.getUserImage().getImageThumbnail().getId());
            }
            opinionRes.setAuthorFirstName(author.getFirstName());
            opinionRes.setAuthorLastName(author.getLastName());

            result.add(opinionRes);

        }

        PaginatedDTO<UserOpinionsResponseDTO> paginatedDTO = new PaginatedDTO<>((long) result.size(), opinionsPage.getTotalPages(), result);

        return paginatedDTO;
    }

    private void checkIsExtensionIsCorrect(String imageName) {
        Pattern fileExtnPtrn = Pattern.compile("([^\\s]+(\\.(?i)(jpg|png))$)");
        Matcher mtch = fileExtnPtrn.matcher(imageName);
        if (!mtch.matches())
            throw new ImageEntity.BadTypeExtension();
    }

    public String validateUser(Long idUser, String authorization) {

        User userToValidate = AService.Get(User.class, userRepo, idUser);

        if (!this.authorization.isCurrentUser(userToValidate, authorization)) {
            throw new ForbiddenException("Nieprawidłowy token użytkownika");
        }

        return authorization;

    }
}
