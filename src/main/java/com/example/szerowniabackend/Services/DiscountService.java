package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.DTOs.Requests.DiscountRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.DiscountUpdateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetDiscount.DiscountResponseDTO;
import com.example.szerowniabackend.Models.Entities.Discount;
import com.example.szerowniabackend.Models.Repositories.DiscountRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DiscountService {

    private final ModelMapper mapper;
    private DiscountRepo discountRepo;

    @Autowired
    public DiscountService(DiscountRepo discountRepo) {
        this.discountRepo = discountRepo;
        mapper = new ModelMapper();
    }

    public List<DiscountResponseDTO> getAllDiscounts() {
        List<DiscountResponseDTO> result = new ArrayList<>();
        for (Discount singleDiscount : discountRepo.findAll()) {
            if (singleDiscount.isVisible()) {
                DiscountResponseDTO mappedDiscount = mapper.map(singleDiscount, DiscountResponseDTO.class);
                result.add(mappedDiscount);
            }
        }
        return result;
    }

    public DiscountResponseDTO getDiscount(Long id) {
        Discount discount = AService.Get(Discount.class, discountRepo, id);
        if (!discount.isVisible()) {
            throw new NotFoundException("Nie znaleziono zniżki");
        }
        DiscountResponseDTO mappedDiscount = mapper.map(discount, DiscountResponseDTO.class);
        return mappedDiscount;
    }

    public Long createDiscount(DiscountRequestDTO discount) {
        if (discount.getValue() >= 100)
            throw new Discount.InvalidDiscountValue(100);

        if (discount.getMinNumDays() < 1)
            throw new Discount.InvalidMinNumDaysValue(1);

        Discount newDiscount = mapper.map(discount, Discount.class);
        return discountRepo.save(newDiscount).getId();
    }

    public void updateDiscount(Long id, DiscountUpdateRequestDTO discountDto) {
        Discount discount = AService.Get(Discount.class, discountRepo, id);

        Discount tmp = mapper.map(discountDto, Discount.class);
        if (tmp.getValue() != null)
            discount.setValue(tmp.getValue());

        if (tmp.getMinNumDays() != null)
            discount.setMinNumDays(tmp.getMinNumDays());
    }

    public void deleteDiscount(Long id) {
        Discount discount = AService.Get(Discount.class, discountRepo, id);
        discount.setVisible(false);
    }
}
