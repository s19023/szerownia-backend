package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Models.DTOs.Requests.MessageExistingThreadDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.MessageNewThreadDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetMessage.*;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.Thread;
import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.ProfanityFilter.ProfanityFilter;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Security.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class MessageService {

    private MessageRepo messageRepo;
    private RentalAdRepo rentalAdRepo;
    private UserRepo userRepo;
    private ThreadRepo threadRepo;
    private ModTicketRepo modTicketRepo;
    private SearchAdRepo searchAdRepo;
    private final Authorization authorization;

    @Autowired
    public MessageService(MessageRepo messageRepo, RentalAdRepo rentalAdRepo, UserRepo userRepo, ThreadRepo threadRepo, ModTicketRepo modTicketRepo, SearchAdRepo searchAdRepo, Authorization authorization) {
        this.messageRepo = messageRepo;
        this.rentalAdRepo = rentalAdRepo;
        this.userRepo = userRepo;
        this.threadRepo = threadRepo;
        this.modTicketRepo = modTicketRepo;
        this.searchAdRepo = searchAdRepo;
        this.authorization = authorization;
    }

    public List<ThreadResponeDTO> getAllThreads(@RequestHeader String authorization) {

        Long userId = this.authorization.getIdUserFromToken(authorization);

        List<Thread> userThreads = messageRepo.findAll().stream()
                .filter(Message::isVisible)
                .filter(message -> message.getReceiver().getId().equals(userId) || message.getSender().getId().equals(userId))
                .map(Message::getThread)
                .distinct()
                .collect(Collectors.toList());

        List<ThreadResponeDTO> result = new ArrayList<>();

        for (Thread thread : userThreads) {
            //DON'T CHANGE ON MAPPER - TIME ISSUE
            ThreadResponeDTO messageResponseDTO = new ThreadResponeDTO();
            messageResponseDTO.setId(thread.getId());
            messageResponseDTO.setTopic(thread.getTopic());

            RentalAdResponseDTO rentalAdResponseDTO = new RentalAdResponseDTO();
            rentalAdResponseDTO.setId(thread.getAd().getId());
            rentalAdResponseDTO.setTitle(thread.getTopic());

            messageResponseDTO.setRentalAd(rentalAdResponseDTO);

            Message message = thread.getMessages().get(thread.getMessages().size() - 1);
            ReceiverDTO messageReceiverDTO = new ReceiverDTO();
            messageReceiverDTO.setFirstName(message.getReceiver().getFirstName());
            messageReceiverDTO.setLastName(message.getReceiver().getLastName());

            MessageResponseDTO lastmessage = new MessageResponseDTO();
            lastmessage.setId(message.getId());
            lastmessage.setContent(message.getContent());
            lastmessage.setReceiver(messageReceiverDTO);
            lastmessage.setSentDate(message.getSentDate());
            lastmessage.setReceiverId(message.getReceiver().getId());
            lastmessage.setSenderId(message.getSender().getId());

            messageResponseDTO.setLastMessage(lastmessage);

            ThreadUserDTO creatorDTO = new ThreadUserDTO();
            creatorDTO.setId(thread.getMessages().get(0).getSender().getId());
            creatorDTO.setFirstName(thread.getMessages().get(0).getSender().getFirstName());
            creatorDTO.setLastName(thread.getMessages().get(0).getSender().getLastName());
            messageResponseDTO.setSender(creatorDTO);

            ThreadUserDTO receiverDTO = new ThreadUserDTO();
            receiverDTO.setId(thread.getMessages().get(0).getReceiver().getId());
            receiverDTO.setFirstName(thread.getMessages().get(0).getReceiver().getFirstName());
            receiverDTO.setLastName(thread.getMessages().get(0).getReceiver().getLastName());
            messageResponseDTO.setReceiver(receiverDTO);

            result.add(messageResponseDTO);
        }


        return result;
    }

    public List<MessageResponseDTO> getAllMessagesFromThread(String token, Long threadId) {

        Long userId = authorization.getIdUserFromToken(token);

        User user = AService.Get(User.class, userRepo, userId);

        Thread thread = AService.Get(Thread.class, threadRepo, threadId);

        User threadSender = thread.getMessages().get(0).getSender();
        User threadReceiver = thread.getMessages().get(0).getReceiver();

        if (!threadSender.equals(user) && !threadReceiver.equals(user))
            throw new Message.ThreadHaveNoRelationWithLoggedUser();

        List<MessageResponseDTO> result = new ArrayList<>();
        for (Message message : thread.getMessages()) {

            if (message.getReadDate() == null && message.getReceiver().equals(user)) {
                message.setReadDate(LocalDateTime.now());
            }
            //DON'T CHANGE ON MAPPER - TIME ISSUE
            MessageResponseDTO messageResponseDTO = new MessageResponseDTO();
            messageResponseDTO.setId(message.getId());
            messageResponseDTO.setSenderId(message.getSender().getId());
            messageResponseDTO.setReceiverId(message.getReceiver().getId());
            messageResponseDTO.setContent(message.getContent());
            messageResponseDTO.setSentDate(message.getSentDate());
            messageResponseDTO.setReadDate(message.getReadDate());

            ReceiverDTO receiverDTO = new ReceiverDTO();
            receiverDTO.setLastName(threadReceiver.getLastName());
            receiverDTO.setFirstName(threadReceiver.getFirstName());

            messageResponseDTO.setReceiver(receiverDTO);

            result.add(messageResponseDTO);

        }

        Collections.reverse(result);


        return result;
    }

    public void createMessageForExistingThread(MessageExistingThreadDTO message, String token) { //dodawanie wiadomosci do watku
        Ad ad = null;
        ModTicket modTicket = null;

        Long senderId = authorization.getIdUserFromToken(token);
        User sender = AService.Get(User.class, userRepo, senderId);
        Thread thread = AService.Get(Thread.class, threadRepo, message.getThread());

        if (thread.getAd() != null)
            try {
                ad = AService.Get(RentalAd.class, rentalAdRepo, thread.getAd().getId());
            } catch (Exception e) {
                ad = AService.Get(SearchAd.class, searchAdRepo, thread.getAd().getId());
            }
        else if (thread.getModTicket() != null)
            modTicket = AService.Get(ModTicket.class, modTicketRepo, thread.getModTicket().getId());

        Message newMessage = new Message();
        newMessage.setContent(ProfanityFilter.getCensoredText(message.getContent()));
        newMessage.setSentDate(LocalDateTime.now());
        newMessage.setThread(thread);
        newMessage.setSender(sender);

        if (ad != null) {
            if (!senderId.equals(ad.getUser().getId()))
                newMessage.setReceiver(ad.getUser());
            else
                newMessage.setReceiver(thread.getMessages().get(0).getSender());

        } else //noinspection ConstantConditions      <- dont touch this!
            if (modTicket != null) {
                if (!sender.getRole().equals(Role.MODERATOR))
                    newMessage.setReceiver(thread.getMessages().get(0).getSender());
                else
                    newMessage.setReceiver(thread.getMessages().get(0).getReceiver());
            }
        messageRepo.save(newMessage);
    }

    public void createMessageForNewThread(MessageNewThreadDTO message, String token) {
        Long senderId = authorization.getIdUserFromToken(token);
        User sender = AService.Get(User.class, userRepo, senderId);

        if (message.getAdId() == null && message.getModTicketId() == null)
            throw new Message.AdAndModTicketCannotBeBothNull();

        if (message.getAdId() != null && message.getModTicketId() != null)
            throw new Message.AdAndModTicketCannotBeBothNull();

        Message newMessage = new Message();
        if (message.getAdId() != null) {
            Ad ad = null;
            try {
                ad = AService.Get(RentalAd.class, rentalAdRepo, message.getAdId());
            } catch (Exception e) {
                ad = AService.Get(SearchAd.class, searchAdRepo, message.getAdId());
            }

            if (sender.equals(ad.getUser())) {
                throw new BadRequestException("Nie można wysyłać wiadomości do siebie");
            }

            Optional<Long> existingThreadId = messageRepo.isExistThreadBetweenUsers(senderId, ad.getId());

            if (existingThreadId.isEmpty()) {
                Thread newThread = new Thread();
                newThread.setAd(ad);
                threadRepo.save(newThread);

                newMessage.setThread(newThread);
                newMessage.setReceiver(ad.getUser());

                newMessage.setContent(ProfanityFilter.getCensoredText(message.getContent()));
                newMessage.setSentDate(LocalDateTime.now());
                newMessage.setSender(sender);

                messageRepo.save(newMessage);
            } else {
                MessageExistingThreadDTO dto = new MessageExistingThreadDTO();
                dto.setContent(message.getContent());
                dto.setThread(existingThreadId.get());
                createMessageForExistingThread(dto, token);
            }

        } else if (message.getModTicketId() != null) {
            if (!sender.getRole().equals(Role.MODERATOR))
                throw new Message.OnlyModeratorCanCreateNewThreadRelatedToModTicket();

            ModTicket modTicket = AService.Get(ModTicket.class, modTicketRepo, message.getModTicketId());

            Thread newThread = new Thread();
            newThread.setModTicket(modTicket);
            newThread.setAd(modTicket.getAd());
            newThread.setTopic("Moderacja: " + modTicket.getAd().getTitle());
            threadRepo.save(newThread);

            newMessage.setThread(newThread);
            newMessage.setReceiver(modTicket.getNotifier());

            newMessage.setContent(ProfanityFilter.getCensoredText(message.getContent()));
            newMessage.setSentDate(LocalDateTime.now());
            newMessage.setSender(sender);

            messageRepo.save(newMessage);
        }
    }
}
