package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.InvaildTokenException;
import com.example.szerowniabackend.Models.DTOs.Responses.RefreshTokenDTO;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import com.example.szerowniabackend.Security.Authorization;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RefreshTokenService {

    private final UserRepo userRepo;
    private final Authorization authorization;

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    public RefreshTokenService(UserRepo userRepo, Authorization authorization) {
        this.userRepo = userRepo;
        this.authorization = authorization;
    }

    public RefreshTokenDTO refreshToken(RefreshTokenDTO refreshTokenDTO) {
        String actualToken = refreshTokenDTO.getToken();
        String refreshToken = refreshTokenDTO.getRefreshToken();

        String emailFromToken;
        try {
            emailFromToken = authorization.getEmailFromToken(actualToken);
        } catch (ExpiredJwtException e) {
            emailFromToken = e.getClaims().getSubject();
        }

        Optional<User> userOptional = userRepo.findUserByEmail(emailFromToken);
        if (userOptional.isEmpty()) throw new User.NotFound();
        User user = userOptional.get();

        if (!user.getRefreshToken().equals(refreshToken))
            throw new InvaildTokenException("Refresh");

        String refreshTokenGenerated = authorization.generateRefreshToken();

        user.setRefreshToken(refreshTokenGenerated);
        userRepo.save(user);

        String token = authorization.generateToken(user.getEmail(), user.getId(), user.getRole(), user.getPremium());

        RefreshTokenDTO newRefreshToken = new RefreshTokenDTO();
        newRefreshToken.setToken(token);
        newRefreshToken.setRefreshToken(refreshTokenGenerated);

        return newRefreshToken;
    }
}
