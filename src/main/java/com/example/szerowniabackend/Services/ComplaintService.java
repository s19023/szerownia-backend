package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Exceptions.NotAcceptableException;
import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.DTOs.Requests.ChangeComplainStatusDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.ComplainRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ComplaintDTO;
import com.example.szerowniabackend.Models.Entities.Complaint;
import com.example.szerowniabackend.Models.Entities.Enums.ComplainStatus;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.Hire;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.Repositories.ComplaintRepo;
import com.example.szerowniabackend.Models.Repositories.HireRepo;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import com.example.szerowniabackend.Security.Authorization;
import com.example.szerowniabackend.Security.JwtTokenUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ComplaintService {

    private final Authorization authorization;
    private ComplaintRepo complaintRepo;
    private UserRepo userRepo;
    private HireRepo hireRepo;


    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public ComplaintService(Authorization authorization, ComplaintRepo complaintRepo, UserRepo userRepo, HireRepo hireRepo) {
        this.authorization = authorization;
        this.complaintRepo = complaintRepo;
        this.userRepo = userRepo;
        this.hireRepo = hireRepo;
    }

    public PaginatedDTO<ComplaintDTO> getComplaints(int page, int howManyRecord) {
        List<ComplaintDTO> allMapped = new ArrayList<>();
        Page<Complaint> complaintPage = complaintRepo.findAllComplaints(PageRequest.of(page, howManyRecord));
        if (complaintPage == null) {
            return new PaginatedDTO<>(0L, 0, Collections.emptyList());
        }
        for (Complaint complaint : complaintPage) {
            ComplaintDTO map = modelMapper.map(complaint, ComplaintDTO.class);
            map.setId(complaint.getId());
            allMapped.add(map);
        }

        return new PaginatedDTO<>((long) allMapped.size(), complaintPage.getTotalPages(), allMapped);
    }

    public PaginatedDTO<ComplaintDTO> getMyComplaints(String authorization, int page, int howManyRecord) {
        Long idUser = JwtTokenUtil.getIdFromToken(authorization);
        List<ComplaintDTO> result = new ArrayList<>();
        Page<Complaint> complaintPage = complaintRepo.findMyComplaints(idUser, PageRequest.of(page, howManyRecord));
        if (complaintPage == null) {
            return new PaginatedDTO<>(0L, 0, Collections.emptyList());
        }

        for (Complaint complaint : complaintPage) {
            ComplaintDTO mapped = modelMapper.map(complaint, ComplaintDTO.class);
            result.add(mapped);
        }
        return new PaginatedDTO<>((long) result.size(), complaintPage.getTotalPages(), result);
    }

    public ComplaintDTO getComplaint(Long id) {
        return modelMapper.map(AService.Get(Complaint.class, complaintRepo, id), ComplaintDTO.class);
    }

    public void createComplaint(ComplainRequestDTO dto, String token) {
        Long idUser = JwtTokenUtil.getIdFromToken(token);
        Hire hire = AService.Get(Hire.class, hireRepo, dto.getHireId());

        if (isNotSharerAndBorrower(idUser, hire)) throw new Complaint.NotYourHire();

        Complaint c = new Complaint(idUser, dto.getReason(), dto.getDetails(), hire, ComplainStatus.SUBMITTED);

        complaintRepo.save(c);
    }

    private boolean isNotSharerAndBorrower(Long idUser, Hire hire) {
        return hire.getBorrower().getId() != idUser && hire.getSharer().getId() != idUser;
    }

    public void changeComplaintStatus(String tokenWithBearer, ChangeComplainStatusDTO changeComplainStatusDTO) {

        if (changeComplainStatusDTO.getComplaintId() == null || changeComplainStatusDTO.getStatus() == null) {
            throw new NotAcceptableException("Brakuje paramte");
        }

        Long idUser = authorization.getIdUserFromToken(tokenWithBearer);
        User user = AService.Get(User.class, userRepo, idUser);
        Role role = user.getRole();

        if (!role.equals(Role.MODERATOR)) {
            throw new ForbiddenException("Nie masz uprawnień do tej czynności");
        }

        Optional<Complaint> complaintOptional = complaintRepo.findById(changeComplainStatusDTO.getComplaintId());

        if (complaintOptional.isEmpty()) {
            throw new NotFoundException("Brak reklmacji o id: " + changeComplainStatusDTO.getComplaintId());
        }

        Complaint complaint = complaintOptional.get();
        complaint.setStatus(changeComplainStatusDTO.getStatus());
        complaintRepo.save(complaint);

    }
}
