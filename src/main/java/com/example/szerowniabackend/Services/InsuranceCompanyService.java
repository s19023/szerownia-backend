package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.DTOs.Requests.InsuranceCompanyRequestDto;
import com.example.szerowniabackend.Models.DTOs.Responses.GetRentalAdById.LocationDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.InsuranceCompanyResponseDto;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.InsuranceCompany;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.Repositories.InsuranceCompanyRepo;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import com.example.szerowniabackend.Security.Authorization;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class InsuranceCompanyService {

    private final InsuranceCompanyRepo insuranceCompanyRepo;
    private final UserRepo userRepo;
    private final Authorization authorization;
    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    public InsuranceCompanyService(InsuranceCompanyRepo insuranceCompanyRepo, UserRepo userRepo, Authorization authorization) {
        this.insuranceCompanyRepo = insuranceCompanyRepo;
        this.userRepo = userRepo;
        this.authorization = authorization;
    }

    public List<InsuranceCompanyResponseDto> getInsuranceCompanies(String authorization) {
        Long id = this.authorization.getIdUserFromToken(authorization);
        User user = AService.Get(User.class, userRepo, id);

        if (!(user.getRole() == Role.ADMIN || user.getRole() == Role.USER)) {
            throw new ForbiddenException();
        }

        List<InsuranceCompanyResponseDto> result = new ArrayList<>();
        for (InsuranceCompany insuranceCompany : insuranceCompanyRepo.findAll()) {
            if (insuranceCompany.isVisible()) {
                InsuranceCompanyResponseDto dto = mapper.map(insuranceCompany, InsuranceCompanyResponseDto.class);
                result.add(dto);
            }
        }
        return result;
    }

    public InsuranceCompanyResponseDto getInsuranceCompany(String authorization, Long insuranceCompanyId) {
        Long id = this.authorization.getIdUserFromToken(authorization);
        User user = AService.Get(User.class, userRepo, id);

        if (!(user.getRole() == Role.ADMIN || user.getRole() == Role.USER)) {
            throw new ForbiddenException();
        }

        InsuranceCompany insuranceCompany = AService.Get(InsuranceCompany.class, insuranceCompanyRepo, insuranceCompanyId);
        if (!insuranceCompany.isVisible()) {
            throw new NotFoundException("Nie znaleziono takiej firmy ubezpieczeniowej");
        }

        InsuranceCompanyResponseDto dto = mapper.map(insuranceCompany, InsuranceCompanyResponseDto.class);
        return dto;
    }

    public void createInsuranceCompany(String authorization, InsuranceCompanyRequestDto dto) {
        Long id = this.authorization.getIdUserFromToken(authorization);
        User user = AService.Get(User.class, userRepo, id);

        if (user.getRole() != Role.ADMIN) {
            throw new ForbiddenException();
        }

        if (dto.getCommissionRate() < 0) {
            throw new BadRequestException("Niepoprawna wartość prowizji");
        }

        if (dto.getLocation().getLatitude() < 0 || dto.getLocation().getLongitude() < 0) {
            throw new BadRequestException("Niepoprawne współrzędne lokalizacji");
        }

        if (dto.getLocation().getName().isEmpty()) {
            throw new BadRequestException("Nie podano nazwy lokalizacji");
        }

        if (dto.getName().isEmpty() || dto.getNip().isEmpty() || dto.getEmail().isEmpty()) {
            throw new BadRequestException("Wszystkie pola są wymagane");
        }

        InsuranceCompany insuranceCompany = mapper.map(dto, InsuranceCompany.class);
        insuranceCompanyRepo.save(insuranceCompany);
    }

    public void updateInsuranceCompany(String authorization, InsuranceCompanyRequestDto dto, Long insuranceCompanyId) {
        Long id = this.authorization.getIdUserFromToken(authorization);
        User user = AService.Get(User.class, userRepo, id);
        if (user.getRole() != Role.ADMIN) {
            throw new ForbiddenException();
        }

        if (dto.getCommissionRate() < 0) {
            throw new BadRequestException("Niepoprawna wartość prowizji");
        }

        if (dto.getLocation().getLatitude() < 0 || dto.getLocation().getLongitude() < 0) {
            throw new BadRequestException("Niepoprawne współrzędne lokalizacji");
        }

        if (dto.getLocation().getName().isEmpty()) {
            throw new BadRequestException("Nie podano nazwy lokalizacji");
        }

        if (dto.getName().isEmpty() || dto.getNip().isEmpty() || dto.getEmail().isEmpty()) {
            throw new BadRequestException("Wszystkie pola są wymagane");
        }

        InsuranceCompany insuranceCompany = AService.Get(InsuranceCompany.class, insuranceCompanyRepo, insuranceCompanyId);

        if (!insuranceCompany.isVisible()) {
            throw new NotFoundException("Nie znaleziono firmy ubezpieczeniowej");
        }

        insuranceCompany.setName(dto.getName());
        insuranceCompany.setNip(dto.getNip());
        insuranceCompany.setEmail(dto.getEmail());
        insuranceCompany.setCommissionRate(dto.getCommissionRate());
        insuranceCompany.getLocation().setName(dto.getLocation().getName());
        insuranceCompany.getLocation().setLatitude(dto.getLocation().getLatitude());
        insuranceCompany.getLocation().setLongitude(dto.getLocation().getLongitude());

    }

    public void deleteInsuranceCompany(String authorization, Long insuranceCompanyId) {
        Long id = this.authorization.getIdUserFromToken(authorization);
        User user = AService.Get(User.class, userRepo, id);

        if (user.getRole() != Role.ADMIN) {
            throw new ForbiddenException();
        }

        InsuranceCompany insuranceCompany = AService.Get(InsuranceCompany.class, insuranceCompanyRepo, insuranceCompanyId);
        insuranceCompany.setVisible(false);
    }
}
