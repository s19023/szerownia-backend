package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.ForbiddenException;
import com.example.szerowniabackend.Models.DTOs.Requests.ClaimInspectionRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetClaims.ClaimAppraiserResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetClaims.UserAppraiserDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.UserRespDto;
import com.example.szerowniabackend.Models.Entities.Claim;
import com.example.szerowniabackend.Models.Entities.ClaimInspection;
import com.example.szerowniabackend.Models.Entities.Enums.ClaimStatus;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.ImageEntity;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.Repositories.ClaimInspectionRepo;
import com.example.szerowniabackend.Models.Repositories.ClaimRepo;
import com.example.szerowniabackend.Models.Repositories.ImageRepo;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import com.example.szerowniabackend.Security.Authorization;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClaimInspectionService {

    private final ClaimInspectionRepo claimInspectionRepo;
    private final UserRepo userRepo;
    private final ClaimRepo claimRepo;
    private final Authorization authorization;
    private final ImageRepo imageRepo;
    private final ModelMapper modelMapper;

    public ClaimInspectionService(ClaimInspectionRepo claimInspectionRepo, UserRepo userRepo, ClaimRepo claimRepo, Authorization authorization, ImageRepo imageRepo) {
        this.claimInspectionRepo = claimInspectionRepo;
        this.userRepo = userRepo;
        this.claimRepo = claimRepo;
        this.authorization = authorization;
        this.imageRepo = imageRepo;
        this.modelMapper = new ModelMapper();
    }

    public void createClaimInspection(ClaimInspectionRequestDTO dto, String authorization) {
        Long userId = this.authorization.getIdUserFromToken(authorization);
        User user = AService.Get(User.class, userRepo, userId);
        Claim claim = AService.Get(Claim.class, claimRepo, dto.getClaimId());

        Optional<User> tempAppraiser = Optional.ofNullable(claim.getTempAppraiser());
        if (tempAppraiser.isPresent()) {
            if (!user.equals(tempAppraiser.get())) {
                throw new ForbiddenException("Nie zajmujesz się tą szkodą");
            }
        } else {
            if (!user.equals(claim.getClaimInspections().get(0).getAppraiser())) {
                throw new ForbiddenException("Nie zajmujesz się tą szkodą");
            }
        }

        if (!claim.getStatus().equals(ClaimStatus.IN_PROGRESS_BEFORE_INSPECTION)) {
            throw new Claim.InvalidWorkflow("Nieprawidłowy przepływ rozwiązywania szkody. Szkoda musi być najpierw zlecona do oględzin przez likwidatora");
        }

        ClaimInspection claimInspection = new ClaimInspection();
        if (dto.getInspectionDate().isBefore(claim.getClaimDate()))
            throw new ClaimInspection.InvalidDateValue();

        if (!claim.getClaimInspections().isEmpty()) {
            List<ClaimInspection> inspections = claim.getClaimInspections();
            LocalDate mostLatestExistInspectionRelatedToClaim = inspections.get(inspections.size() - 1).getInspectionDate();
            if (dto.getInspectionDate().isBefore(mostLatestExistInspectionRelatedToClaim)) {
                throw new ClaimInspection.InvalidDateValue();
            }
        }

        if (dto.getDamageAssessment() > claim.getPolicy().getInsuranceTotal())
            throw new ClaimInspection.InvalidDamageAssessmentValue();

        claimInspection.setInspectionDate(dto.getInspectionDate());
        claimInspection.setDescription(dto.getDescription());
        claimInspection.setDamageAssessment(dto.getDamageAssessment());
        claimInspection.setAppraiser(user);
        claimInspection.setClaim(claim);


        if (dto.getImagesId() != null) {
            List<ImageEntity> images = new ArrayList<>();
            for (Long imageId : dto.getImagesId()) {
                ImageEntity image = AService.Get(ImageEntity.class, imageRepo, imageId);
                images.add(image);
            }
            claimInspection.setImages(images);
        }

        claim.setStatus(ClaimStatus.IN_PROGRESS_AFTER_INSPECTION);


        if (claim.getTempAppraiser() != null) {
            claim.setTempAppraiser(null);
        }

        claimInspectionRepo.save(claimInspection);
    }

    public List<ClaimAppraiserResponseDTO> getClaimsWaitingForInspection(String authorization) {

        if (this.authorization.getRoleFromToken(authorization) != Role.APPRAISER) {
            throw new ForbiddenException("Nie jesteś rzeczoznawcą");
        }

        Long idUser = this.authorization.getIdUserFromToken(authorization);

        List<ClaimAppraiserResponseDTO> results = new ArrayList<>();

        for (Claim claim : claimRepo.findClaimsWaitingForInspection(idUser)) {

            ClaimAppraiserResponseDTO claimAppraiserResponseDTO = modelMapper.map(claim, ClaimAppraiserResponseDTO.class);
            claimAppraiserResponseDTO.setTitle(claim.getPolicy().getHire().getAd().getTitle());
            claimAppraiserResponseDTO.setBorrower(modelMapper.map(claim.getPolicy().getHire().getBorrower(), UserRespDto.class));
            claimAppraiserResponseDTO.setAdjuster(modelMapper.map(claim.getInsuranceAdjuster(), UserAppraiserDTO.class));

            results.add(claimAppraiserResponseDTO);
        }

        return results;
    }
}
