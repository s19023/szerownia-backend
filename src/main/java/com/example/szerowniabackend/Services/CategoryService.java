package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.NotAcceptableException;
import com.example.szerowniabackend.Models.DTOs.Requests.CategoryCreateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.CategoryUpdateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.FeatureForCategoryRequestDto;
import com.example.szerowniabackend.Models.DTOs.Responses.CategoryByIdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.CategoryReferenceDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.CategoryListResponseDTO;
import com.example.szerowniabackend.Models.Entities.Category;
import com.example.szerowniabackend.Models.Entities.Feature;
import com.example.szerowniabackend.Models.Entities.FeatureCategory;
import com.example.szerowniabackend.Models.Repositories.CategoryRepo;
import com.example.szerowniabackend.Models.Repositories.FeatureCategoryRepo;
import com.example.szerowniabackend.Models.Repositories.FeatureRepo;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CategoryService {

    private final CategoryRepo categoryRepo;
    private final FeatureRepo featureRepo;
    private final FeatureCategoryRepo featureCategoryRepo;
    private final ModelMapper mapper;

    public CategoryService(CategoryRepo categoryRepo, FeatureRepo featureRepo, FeatureCategoryRepo featureCategoryRepo) {
        this.categoryRepo = categoryRepo;
        this.featureRepo = featureRepo;
        this.featureCategoryRepo = featureCategoryRepo;
        this.mapper = new ModelMapper();
    }

    public List<CategoryListResponseDTO> getCategoryList() {
        List<CategoryListResponseDTO> categoryList = new ArrayList<>();

        for (Category category : categoryRepo.findAll()) {

            CategoryListResponseDTO mappedCategoryListResponseDTO = mapper.map(category, CategoryListResponseDTO.class);
            if (category.getParentCategory() != null) {
                CategoryReferenceDTO mappedParentDTO = mapper.map(category.getParentCategory(), CategoryReferenceDTO.class);
                mappedCategoryListResponseDTO.setParentCategory(mappedParentDTO);
            }

            List<CategoryReferenceDTO> subCategoriesDTO = getCategoryReferenceDTOs(category);
            mappedCategoryListResponseDTO.setSubCategories(subCategoriesDTO);
            categoryList.add(mappedCategoryListResponseDTO);
        }
        return categoryList;
    }

    public CategoryByIdResponseDTO getCategoryById(Long idCategory) {
        Category categoryById = AService.Get(Category.class, categoryRepo, idCategory);
        List<CategoryReferenceDTO> subCategoriesDTO = getCategoryReferenceDTOs(categoryById);

        CategoryByIdResponseDTO mappedCategoryByIdResponseDTO = mapper.map(categoryById, CategoryByIdResponseDTO.class);
        mappedCategoryByIdResponseDTO.setSubCategories(subCategoriesDTO);

        return mappedCategoryByIdResponseDTO;
    }

    public void createCategory(CategoryCreateRequestDTO categoryCreateRequestDTO) {
        Category categoryToCreate;

        if (categoryCreateRequestDTO.getIdParentCategory() == null) {
            categoryToCreate = new Category(categoryCreateRequestDTO.getName(), false, null);

        } else {
            Category parentCategory = AService.Get(Category.class, categoryRepo, categoryCreateRequestDTO.getIdParentCategory());

            parentCategory.setAbstract(true);
            categoryRepo.save(parentCategory);
            categoryToCreate = new Category(categoryCreateRequestDTO.getName(), false, parentCategory);

            for (FeatureForCategoryRequestDto featureReqDto : categoryCreateRequestDTO.getFeatures()) {
                Feature feature = AService.Get(Feature.class, featureRepo, featureReqDto.getFeatureId());
                FeatureCategory featureCategory = new FeatureCategory();
                featureCategory.setFeature(feature);
                featureCategory.setCategory(categoryToCreate);
                featureCategory.setFeatureRequired(featureReqDto.getFeatureRequired());
                featureCategoryRepo.save(featureCategory);
            }
        }
        categoryRepo.save(categoryToCreate);
    }

    public void updateCategory(Long idCategory, CategoryUpdateRequestDTO categoryUpdateRequestDTO) {
        Category categoryToUpdate = AService.Get(Category.class, categoryRepo, idCategory);

        System.out.println(categoryToUpdate.getProducts().size());

        if (categoryToUpdate.getProducts().size() != 0)
            throw new NotAcceptableException("Nie można edytować. Kategoria posiada już przypisane produkty.");

        if (categoryUpdateRequestDTO.getName() != null)
            if (!categoryUpdateRequestDTO.getName().isBlank()) {
                categoryToUpdate.setName(categoryUpdateRequestDTO.getName());
            }

        Long idParentCategory = categoryUpdateRequestDTO.getIdParentCategory();

        if (idParentCategory != null) {
            Category parentCategory = AService.Get(Category.class, categoryRepo, idParentCategory);

            if (!parentCategory.getProducts().isEmpty())
                throw new Category.SubcategoryCannotHaveProducts(parentCategory.getName());

            if (!parentCategory.isAbstract()) {
                parentCategory.setAbstract(true);
                categoryRepo.save(parentCategory);
            }
            categoryToUpdate.setParentCategory(parentCategory);
        }

        categoryRepo.removeFeaturesFromCategory(idCategory);

        categoryUpdateRequestDTO.getFeatures().forEach(featureCategoryDTO -> {
            FeatureCategory featureCategory = new FeatureCategory(featureCategoryDTO.getIsRequired());
            featureCategory.setFeature(AService.Get(Feature.class, featureRepo, featureCategoryDTO.getIdFeature()));
            featureCategory.setCategory(categoryToUpdate);
            featureCategoryRepo.save(featureCategory);
        });

        categoryRepo.save(categoryToUpdate);
    }

    public void deleteCategory(Long idCategory) throws Category.ValidSubcategories {
        Category categoryToDelete = AService.Get(Category.class, categoryRepo,idCategory);

        if (!categoryToDelete.getSubCategories().isEmpty())
            throw new Category.ValidSubcategories(categoryToDelete.getName());

        if (!categoryToDelete.getProducts().isEmpty())
            throw new Category.CannotDeleteCategoryWithProducts(categoryToDelete.getName());

        categoryRepo.delete(categoryToDelete);
    }

    private List<CategoryReferenceDTO> getCategoryReferenceDTOs(Category category) {
        List<CategoryReferenceDTO> subCategoriesDTO = new ArrayList<>();

        for (Category subCategory : category.getSubCategories()) {
            CategoryReferenceDTO mappedSubCategoryDTO = mapper.map(subCategory, CategoryReferenceDTO.class);

            subCategoriesDTO.add(mappedSubCategoryDTO);
        }
        return subCategoriesDTO;
    }
}
