package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Models.DTOs.Responses.ImageDTO;
import com.example.szerowniabackend.Models.Entities.ImageEntity;
import com.example.szerowniabackend.Models.Entities.ImageThumbnail;
import com.example.szerowniabackend.Models.Repositories.FileSystemRepo;
import com.example.szerowniabackend.Models.Repositories.ImageRepo;
import com.example.szerowniabackend.Models.Repositories.ImageThumbnailRepo;
import com.example.szerowniabackend.Security.JwtTokenUtil;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ImagesService {

    private final FileSystemRepo fileSystemRepo;
    private final ImageRepo imageRepo;
    private final ImageThumbnailRepo imageThumbnailRepo;

    @Autowired
    public ImagesService(FileSystemRepo fileSystemRepo, ImageRepo imageRepo, ImageThumbnailRepo imageThumbnailRepo) {
        this.fileSystemRepo = fileSystemRepo;
        this.imageRepo = imageRepo;
        this.imageThumbnailRepo = imageThumbnailRepo;
    }

    public List<ImageDTO> save(List<MultipartFile> imageList, String token) {
        Long idUser = JwtTokenUtil.getIdFromToken(token);
        List<ImageDTO> listOfImagesId = new ArrayList<>();

        for (MultipartFile image : imageList) {
            byte[] bytes = new byte[0];
            try {
                bytes = image.getBytes();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String imageName = image.getOriginalFilename();
            checkIsExtensionIsCorrect(imageName);

            imageName.toLowerCase();

            String location = null;
            MessageDigest md = null;
            var nowDate = new Date().getTime();
            try {
                String toHash = nowDate + imageName;
                md = MessageDigest.getInstance("MD5");
                md.update(toHash.getBytes(StandardCharsets.UTF_8));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            byte[] digest = md.digest();
            String imageHash = DatatypeConverter
                    .printHexBinary(digest).toUpperCase();

            String finalNameOfImage = imageHash + "." + imageName.substring(imageName.length() - 3);

            try {

                location = fileSystemRepo.save(bytes, finalNameOfImage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ImageEntity imageEntity = new ImageEntity(finalNameOfImage, location);
            ImageThumbnail imageThumbnail = addThumbnailForExistingImage(imageEntity);
            imageThumbnail.setImageEntity(imageEntity);
            imageEntity.setImageThumbnail(imageThumbnail);
            imageRepo.save(imageEntity);
            listOfImagesId.add(new ImageDTO(imageEntity.getId(), imageThumbnail.getId()));
        }
        return listOfImagesId;
    }

    @Async
    public ImageThumbnail addThumbnailForExistingImage(ImageEntity imageDownload) {
        FileSystemResource image = fileSystemRepo.findInFileSystem(imageDownload.getLocation());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        BufferedImage bufferedImage = null;

        try {
            bufferedImage = ImageIO.read(image.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Thumbnails.of(bufferedImage)
                    .size(640, 360)
                    .outputFormat("JPEG")
                    .outputQuality(1)
                    .toOutputStream(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bytes = outputStream.toByteArray();
        String imageName = imageDownload.getName();

        return addThumbnailToDb(bytes, imageName);
    }

    private ImageThumbnail addThumbnailToDb(byte[] bytes, String imageName) {
        checkIsExtensionIsCorrect(imageName);

        imageName.toLowerCase();

        String location = null;
        MessageDigest md = null;
        var nowDate = new Date().getTime();
        try {
            String toHash = nowDate + imageName + "thumbNail";
            md = MessageDigest.getInstance("MD5");
            md.update(toHash.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] digest = md.digest();
        String imageHash = DatatypeConverter
                .printHexBinary(digest).toUpperCase();

        String finalNameOfImage = imageHash + "." + imageName.substring(imageName.length() - 3);

        try {
            location = fileSystemRepo.save(bytes, finalNameOfImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ImageThumbnail imageThumbnail = new ImageThumbnail(finalNameOfImage, location);
        imageThumbnailRepo.save(imageThumbnail);

        return imageThumbnail;
    }

    public FileSystemResource find(Long imageId) {
        ImageEntity image = AService.Get(ImageEntity.class, imageRepo, imageId);
        return fileSystemRepo.findInFileSystem(image.getLocation());
    }

    public FileSystemResource findThumbnail(Long imageId) {
        ImageThumbnail thumbnail = AService.Get(ImageThumbnail.class, imageThumbnailRepo, imageId);
        return fileSystemRepo.findInFileSystem(thumbnail.getLocation());
    }

    public void deleteImage(Long imageId, String token) {
        Long idUser = JwtTokenUtil.getIdFromToken(token);

        ImageEntity image = AService.Get(ImageEntity.class, imageRepo, imageId);
        imageRepo.delete(image);
        fileSystemRepo.deleteFile(image.getLocation());
        fileSystemRepo.deleteFile(image.getImageThumbnail().getLocation());
    }

    private void checkIsExtensionIsCorrect(String imageName) {
        Pattern fileExtnPtrn = Pattern.compile("([^\\s]+(\\.(?i)(jpg|png))$)");
        Matcher mtch = fileExtnPtrn.matcher(imageName);
        if (!mtch.matches())
            throw new ImageEntity.BadTypeExtension();
    }
}