package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Email.EmailSender;
import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.DTOs.Requests.CalculateHireCostRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.HireOutgoingRequestDto;
import com.example.szerowniabackend.Models.DTOs.Requests.HireRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.CalculateHireCostDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.DiscountDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetHire.HireResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetHire.RentalAdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetHire.UserDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.GetHireMyBorrowsSharers.HireMyBorrowsResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.OpinionResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.UserRespDto;
import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.Entities.Enums.EventType;
import com.example.szerowniabackend.Models.Entities.Enums.HireStatus;
import com.example.szerowniabackend.Models.Entities.Enums.NotificationContent;
import com.example.szerowniabackend.Models.Entities.Enums.NotificationStatus;
import com.example.szerowniabackend.Models.ProfanityFilter.ProfanityFilter;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Security.Authorization;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class HireService {

    final private HireRepo hireRepo;
    final private RentalAdRepo rentalAdRepo;
    final private UserRepo userRepo;
    final private DiscountRepo discountRepo;
    final private StatisticsService statisticsService;
    private final Authorization authorization;
    private final EmailSender emailSender;
    private final ModelMapper mapper;
    private final PolicyRepo policyRepo;
    private final InsuranceCompanyRepo insuranceCompanyRepo;
    private final ImageRepo imageRepo;
    private final ProductRepo productRepo;
    private final NotificationRepo notificationRepo;


    @Autowired
    public HireService(HireRepo hireRepo, RentalAdRepo rentalAdRepo, UserRepo userRepo, DiscountRepo discountRepo, StatisticsService statisticsService, Authorization authorization, EmailSender emailSender, PolicyRepo policyRepo, InsuranceCompanyRepo insuranceCompanyRepo, ImageRepo imageRepo, ProductRepo productRepo, NotificationRepo notificationRepo) {
        this.hireRepo = hireRepo;
        this.rentalAdRepo = rentalAdRepo;
        this.userRepo = userRepo;
        this.discountRepo = discountRepo;
        this.statisticsService = statisticsService;
        this.authorization = authorization;
        this.emailSender = emailSender;
        this.imageRepo = imageRepo;
        this.productRepo = productRepo;
        this.notificationRepo = notificationRepo;
        mapper = new ModelMapper();
        this.policyRepo = policyRepo;
        this.insuranceCompanyRepo = insuranceCompanyRepo;
    }

    public List<HireResponseDTO> getAllHires() {
        List<HireResponseDTO> result = new ArrayList<>();
        for (Hire hire : hireRepo.findAll()) {
            HireResponseDTO mapHireResponseDTO = mapper.map(hire, HireResponseDTO.class);
            result.add(mapHireResponseDTO);
        }
        return result;
    }

    public HireResponseDTO getHire(Long id) {
        Hire hire = AService.Get(Hire.class, hireRepo, id);

        HireResponseDTO hireResponseDTO = mapper.map(hire, HireResponseDTO.class);
        UserDTO borrower = mapper.map(hire.getBorrower(), UserDTO.class);
        UserDTO sharer = mapper.map(hire.getSharer(), UserDTO.class);
        RentalAdResponseDTO ad = mapper.map(hire.getAd(), RentalAdResponseDTO.class);
        hireResponseDTO.setBorrower(borrower);
        hireResponseDTO.setSharer(sharer);
        hireResponseDTO.setAd(ad);

        return hireResponseDTO;
    }

    public CalculateHireCostDTO calculateHireCost(CalculateHireCostRequestDTO request) {
        RentalAd ad = AService.Get(RentalAd.class, rentalAdRepo, request.getRentalAdId());

        if (request.getStartDate() == null || request.getEndDate() == null)
            throw new Hire.MissingDates();

        if (request.getEndDate().isBefore(request.getStartDate()))
            throw new Hire.InvalidDate("Data zakończenie nie może być przed datą rozpoczęcia");

        Integer numberDaysElapsed = (int) ChronoUnit.DAYS.between(request.getStartDate(), request.getEndDate()) + 1;
        Optional<Integer> discountValue = discountRepo.getBestAvailableDiscount(ad.getId(), numberDaysElapsed);
        CalculateHireCostDTO response = new CalculateHireCostDTO();

        if (discountValue.isPresent()) {
            Integer discountVal = discountValue.get();
            Discount discount = discountRepo.getFirstByValueAndRentalAdId(discountVal, ad.getId());
            DiscountDTO discountDTO = mapper.map(discount, DiscountDTO.class);
            response.setUsedDiscount(discountDTO);
        }

        Double deposit = ad.getDepositAmount();
        Double totalCost = calculate(ad, discountValue, numberDaysElapsed);

        response.setPeriod(numberDaysElapsed);
        response.setCost(totalCost);
        response.setDepositAmount(deposit);
        response.setPricePerDay(ad.getPricePerDay());
        response.setCommission(commission(totalCost));
        return response;
    }

    public void createHire(HireRequestDTO hireRequest, String token) {
        Long borrowerId = authorization.getIdUserFromToken(token);

        RentalAd rentalAd = AService.Get(RentalAd.class, rentalAdRepo, hireRequest.getAdId());
        User sharer = AService.Get(User.class, userRepo, rentalAd.getUser().getId());
        User borrower = AService.Get(User.class, userRepo, borrowerId);


        if (sharer.getId() == borrower.getId())
            throw new Hire.InvalidUser("Nie możesz wypożyczyć swojego przedmiotu");

        if (rentalAd.getAverageOpinion() != null) {
            if (rentalAd.getAverageOpinion() > borrower.getBorrowerRate())
                throw new Hire.ViolatedRequirement(borrowerId + "średniej opinii");
        }

        if (rentalAd.getAverageRentalPrice() != null) {
            if (rentalAd.getAverageRentalPrice() > borrower.getBorrowerAverageRentalPrice())
                throw new Hire.ViolatedRequirement(borrowerId + "średniej ceny wypożyczeń");
        }

        if (rentalAd.getMinRentalHistory() != null) {
            if (rentalAd.getMinRentalHistory() > borrower.getBorrowerHistoryCount())
                throw new Hire.ViolatedRequirement(borrowerId + "minimalnej ilości wypożyczeń");
        }

        Hire hire = mapper.map(hireRequest, Hire.class);
        hire.setAd(rentalAd);
        hire.setSharer(sharer);
        hire.setBorrower(borrower);

        if (!hire.getDateFrom().isBefore(hire.getDateToPlanned()))
            throw new Hire.InvalidDate("Planowana data końca wypożyczenia nie może być przed datą rozpoczęcia wypożyczenia");

        if (sharer.getId().equals(borrowerId))
            throw new Hire.InvalidUser("Użytkownik nie może być zarówno użyczającym jak i wypożyczającym");

        Integer numberDaysElapsed = (int) ChronoUnit.DAYS.between(hire.getDateToPlanned(), hire.getDateToPlanned()) + 1;
        Optional<Integer> discountValue = discountRepo.getBestAvailableDiscount(rentalAd.getId(), numberDaysElapsed);
        Double depositAmount = rentalAd.getDepositAmount();
        Double totalCost = calculate(rentalAd, discountValue, numberDaysElapsed);
        Double commission = commission(totalCost);
        hire.setCost(totalCost + depositAmount + commission);
        hire.setCommission(commission);
        hireRepo.save(hire);

        statisticsService.recordEvent(EventType.RENT, hire.getId());

        var setProduct = hire.getAd().getProducts();
        List<String> productNameList = new ArrayList<>();

        for (Product product : setProduct) productNameList.add(product.getName());

        Notification notificationForBorrower = new Notification();
        notificationForBorrower.setNotificationStatus(NotificationStatus.UNREAD);
        notificationForBorrower.setContent(NotificationContent.BORROWED_HIRE.getPolishMessages());
        notificationForBorrower.setCurrentUser(hire.getBorrower());
        notificationForBorrower.setOtherUser(hire.getSharer());

        Notification notificationForSharer = new Notification();
        notificationForSharer.setNotificationStatus(NotificationStatus.UNREAD);
        notificationForSharer.setContent(NotificationContent.BORROWED_HIRE.getPolishMessages());
        notificationForSharer.setCurrentUser(hire.getSharer());
        notificationForBorrower.setOtherUser(hire.getBorrower());


        notificationRepo.saveAll(List.of(notificationForSharer, notificationForBorrower));

        emailSender.sendMessageToSomeoneWhenBorrowProduct(sharer.getEmail(), borrower.getEmail(), productNameList.toString());
        emailSender.sendMessageToYourSelfWhenBorrowProduct(borrower.getEmail(), sharer.getEmail(), productNameList.toString());
    }

    public void updateHire(@PathVariable Long id, @RequestBody LocalDate dateToActual) {
        Hire hire = AService.Get(Hire.class, hireRepo, id);

        if (dateToActual.isBefore(hire.getDateFrom()))
            throw new Hire.InvalidDate("Niepoprawna faktyczna data zwrotu");

        Integer numberDaysElapsed = Period.between(hire.getDateFrom(), hire.getDateToPlanned()).getDays();
        Optional<Integer> discountValue = discountRepo.getBestAvailableDiscount(hire.getAd().getId(), numberDaysElapsed);
        Double depositAmount = hire.getAd().getDepositAmount();

        hire.setDateToActual(dateToActual);
        if (dateToActual.isAfter(hire.getDateToPlanned()))
            hire.setCost(hire.getCost() + penaltyFee(hire));
        else
            hire.setCost(calculateActualCost(hire.getAd(), hire, discountValue) + depositAmount + hire.getCommission());
    }

    public void deleteHire(@PathVariable Long id) {
        if (hireRepo.findById(id).isEmpty()) throw new NotFoundException(Hire.class, id);
        hireRepo.deleteHire(id);
    }

    private Double calculateActualCost(RentalAd ad, Hire hire, Optional<Integer> discountValue) {
        Integer numberDaysElapsed = Period.between(hire.getDateFrom(), hire.getDateToActual()).getDays();
        return calculate(ad, discountValue, numberDaysElapsed);
    }

    private Double penaltyFee(Hire hire) {
        Integer delayedDays = Period.between(hire.getDateToPlanned(), hire.getDateToActual()).getDays();
        Double result = delayedDays * hire.getAd().getPenaltyForEachDayOfDelayInReturns();
        return result;
    }

    private Double commission(Double totalCost) {
        if (totalCost <= 250)
            return 0.1 * totalCost;

        if (totalCost <= 500)
            return 0.05 * totalCost;

        if (totalCost <= 1000)
            return 0.03 * totalCost;

        return 0.02 * totalCost;
    }

    public List<HireMyBorrowsResponseDTO> getMyBorrows(String tokenWithBearer, HireStatus hireStatus) {
        Long idUser = authorization.getIdUserFromToken(tokenWithBearer);
        List<HireMyBorrowsResponseDTO> result = new ArrayList<>();
        List<Hire> hireList = (hireStatus == null) ?
                hireRepo.findAllByBorrowerId(idUser) : hireRepo.findAllByBorrowerIdAndHireStatus(idUser, hireStatus);

        for (Hire hire : hireList) {
            HireMyBorrowsResponseDTO mapHireResponseDTO = mapper.map(hire, HireMyBorrowsResponseDTO.class);
            User sharer = AService.Get(User.class, userRepo, hire.getSharer().getId());
            User borrower = AService.Get(User.class, userRepo, hire.getBorrower().getId());
            UserRespDto borrowerDTO = mapper.map(borrower, UserRespDto.class);
            UserRespDto sharerDTO = mapper.map(sharer, UserRespDto.class);

            mapHireResponseDTO.setBorrower(borrowerDTO);
            mapHireResponseDTO.setSharer(sharerDTO);

            List<OpinionResponseDTO> opinions = new ArrayList<>();
            for (Opinion o : hire.getOpinions()) {
                OpinionResponseDTO opinion = mapper.map(o, OpinionResponseDTO.class);
                opinions.add(opinion);
            }
            mapHireResponseDTO.setOpinions(opinions);
            mapHireResponseDTO.getAd().setAdThumbnailId(imageRepo.findFirstImageForRentalAd(hire.getAd().getId()).getId());

            result.add(mapHireResponseDTO);
        }
        return result;
    }

    public List<HireMyBorrowsResponseDTO> getMyShares(String tokenWithBearer, HireStatus hireStatus) {
        Long idUser = authorization.getIdUserFromToken(tokenWithBearer);
        List<HireMyBorrowsResponseDTO> result = new ArrayList<>();

        List<Hire> hireList = (hireStatus == null) ?
                hireRepo.findAllBySharerId(idUser) : hireRepo.findAllBySharerIdAndHireStatus(idUser, hireStatus);

        for (Hire hire : hireList) {
            HireMyBorrowsResponseDTO mapHireResponseDTO = mapper.map(hire, HireMyBorrowsResponseDTO.class);
            User borrower = AService.Get(User.class, userRepo, hire.getBorrower().getId());
            User sharer = AService.Get(User.class, userRepo, hire.getSharer().getId());
            UserRespDto borrowerDTO = mapper.map(borrower, UserRespDto.class);
            UserRespDto sharerDTO = mapper.map(sharer, UserRespDto.class);

            mapHireResponseDTO.setBorrower(borrowerDTO);
            mapHireResponseDTO.setSharer(sharerDTO);


            mapHireResponseDTO.getAd().setAdThumbnailId(imageRepo.findFirstImageForRentalAd(hire.getAd().getId()).getId());
            if (hire.getPolicy() != null) {
                mapHireResponseDTO.setPolicyId(hire.getPolicy().getId());
            }
            result.add(mapHireResponseDTO);
        }
        return result;
    }

    private Double calculate(RentalAd ad, Optional<Integer> discountValue, Integer numberDaysElapsed) {
        Double result;
        Double rate = ad.getPricePerDay();
        result = discountValue
                .map(integer -> numberDaysElapsed * rate * (100 - integer) / 100)
                .orElseGet(() -> numberDaysElapsed * rate);
        return result;
    }


    public void addOutgoingShipmentNumber(Long id, String tokenWithBearer, HireOutgoingRequestDto request) {
        Long idUser = authorization.getIdUserFromToken(tokenWithBearer);
        Hire hire = AService.Get(Hire.class, hireRepo, id);

        if (hire.getSharer().getId() != idUser)
            throw new Hire.InvalidUser("Nie jesteś użyczającym");

        String outgoingShipmentNumber = ProfanityFilter.getCensoredText(request.getOutgoingShipmentNumber());

        if (outgoingShipmentNumber != null) {
            hire.setOutgoingShipmentNumber(outgoingShipmentNumber);
            hire.setHireStatus(HireStatus.SHIPPED);

            Notification notification = new Notification();
            notification.setNotificationStatus(NotificationStatus.UNREAD);
            notification.setContent(NotificationContent.HIRE_SHIPPED.getPolishMessages());
            notification.setCurrentUser(hire.getBorrower());
            notification.setOtherUser(hire.getSharer());
            notificationRepo.save(notification);

        }

        System.out.println("aaaaa\t" + request.getInsure());
        System.out.println("bbbbb\t" + request.getInsuranceCompanyId());
        if (request.getInsure()) {
            System.out.println("Hello");
            createPolicy(hire, request.getInsuranceCompanyId());
        }


        emailSender.sendMessageWithShipmentNumber(authorization.getEmailFromToken(tokenWithBearer), outgoingShipmentNumber, hire.getAd().getTitle());
        emailSender.sendMessageWithShipmentNumber(hire.getBorrower().getEmail(), outgoingShipmentNumber, hire.getAd().getTitle());
        System.out.println("END ---------");
    }

    private void createPolicy(Hire hire, Long insuranceCompanyId) {
        Policy policy = new Policy();
        policy.setHire(hire);
        int estimateProductsValueTotal = 0;
        for (Product product : hire.getAd().getProducts()) {
            estimateProductsValueTotal += product.getEstimatedValue();
        }
        policy.setInsuranceTotal(0.7 * estimateProductsValueTotal);

        System.out.println("HELLO\t" + estimateProductsValueTotal);
        InsuranceCompany insuranceCompany = AService.Get(InsuranceCompany.class, insuranceCompanyRepo, insuranceCompanyId);
        policy.setInsuranceCompany(insuranceCompany);
        policyRepo.save(policy);
    }

    public void addIncomingShipmentNumber(Long id, String tokenWithBearer, String incomingShipmentNumber) {
        Long idUser = authorization.getIdUserFromToken(tokenWithBearer);
        Hire hire = AService.Get(Hire.class, hireRepo, id);

        if (hire.getBorrower().getId() != idUser)
            throw new Hire.InvalidUser("Nie jesteś wypożyczającym");

        if (incomingShipmentNumber != null) {
            incomingShipmentNumber = ProfanityFilter.getCensoredText(incomingShipmentNumber);
            hire.setIncomingShipmentNumber(incomingShipmentNumber);
            hire.setHireStatus(HireStatus.SHIPPED_BACK);

            Notification notification = new Notification();
            notification.setNotificationStatus(NotificationStatus.UNREAD);
            notification.setContent(NotificationContent.HIRE_SHIPPED.getPolishMessages());
            notification.setCurrentUser(hire.getSharer());
            notification.setOtherUser(hire.getBorrower());
            notificationRepo.save(notification);
        }

        emailSender.sendMessageWithShipmentNumber(authorization.getEmailFromToken(tokenWithBearer), incomingShipmentNumber, hire.getAd().getTitle());
        emailSender.sendMessageWithShipmentNumber(hire.getSharer().getEmail(), incomingShipmentNumber, hire.getAd().getTitle());
    }

    public void confirmReceivingShipment(Long id, String tokenWithBearer) {
        Long idUser = authorization.getIdUserFromToken(tokenWithBearer);
        Hire hire = AService.Get(Hire.class, hireRepo, id);

        if (hire.getSharer().getId().equals(idUser)) {
            hire.setDateToActual(LocalDate.now());
            hire.setCommission(calculateCommission(hire.getDateToPlanned(), hire.getDateToActual(), hire.getAd().getPenaltyForEachDayOfDelayInReturns()));
            hire.setReturned(true);
            hire.setHireStatus(HireStatus.RETURNED);
        } else if (hire.getBorrower().getId().equals(idUser)) {
            hire.setDelivered(true);
            hire.setHireStatus(HireStatus.PENDING_HIRE);
        } else {
            throw new Hire.InvalidUser("Nie jesteś ani użyczającym ani wypożyczającym");
        }
    }

    private Double calculateCommission(LocalDate plannedDateTo, LocalDate actualDateTo, double penaltyRate) {

        if (actualDateTo.isBefore(plannedDateTo))
            plannedDateTo = actualDateTo;

        long differenceInDays = ChronoUnit.DAYS.between(plannedDateTo, actualDateTo);

        double commission = differenceInDays * penaltyRate;

        return commission;
    }

    public List<HireResponseDTO> productHistory(Long productId, String tokenWithBearer) {

        Long idUser = this.authorization.getIdUserFromToken(tokenWithBearer);

        Product product = AService.Get(Product.class, productRepo, productId);
        if (!product.getUser().getId().equals(idUser)) {
            throw new User.PermissionDenied();
        }

        List<HireResponseDTO> hireResponseDTOS = new ArrayList<>();

        for (Ad includeAd : product.getIncludeAds()) {
            for (Hire hire : includeAd.getHire()) {
                HireResponseDTO hireResponseDTO = mapper.map(hire, HireResponseDTO.class);
                hireResponseDTOS.add(hireResponseDTO);
            }

        }
        return hireResponseDTOS;
    }

    public Double calculateCostOfInsurance(Long id) {

        Hire hire = AService.Get(Hire.class, hireRepo, id);

        long numberOfDatsOnLoan = ChronoUnit.DAYS.between(hire.getDateFrom(), hire.getDateToPlanned()) + 1;
        Double itemCost = hire.getCost();

        double contributionAmount = 0d;

        if (itemCost <= 300) {
            contributionAmount = 15d;
        } else if (itemCost >= 301 && itemCost <= 500) {
            contributionAmount = 20d;
        } else if (itemCost >= 501 && itemCost <= 1000) {
            contributionAmount = 50d;
        } else if (itemCost >= 1001 && itemCost <= 2500) {
            contributionAmount = 90d;
        } else if (itemCost >= 2501) {
            contributionAmount = 130d;
        }

        if (numberOfDatsOnLoan >= 8 && numberOfDatsOnLoan <= 14) {
            contributionAmount = contributionAmount * 2;
        } else if (numberOfDatsOnLoan >= 15 && numberOfDatsOnLoan <= 30) {
            contributionAmount = contributionAmount * 2.5;
        } else if (numberOfDatsOnLoan > 30) {
            contributionAmount = contributionAmount * 3;
        }

        return contributionAmount * numberOfDatsOnLoan;

    }

    public PaginatedDTO<HireMyBorrowsResponseDTO> getUserBorrows(Long idUser, String authorization, int page, int howManyRecord) {

        if (!this.authorization.isSuperUser(authorization)) {
            throw new BadRequestException("Nie posiadzasz odpowiednich uprawnień");
        }

        Page<Hire> hirePage = hireRepo.findAllHireBorrowsByIdUser(idUser, PageRequest.of(page, howManyRecord));

        return getPaginatedHireMyBorrowsResponseDTO(hirePage);
    }

    public PaginatedDTO<HireMyBorrowsResponseDTO> getUserShares(Long idUser, String authorization, int page, int howManyRecord) {

        if (!this.authorization.isSuperUser(authorization)) {
            throw new BadRequestException("Nie posiadzasz odpowiednich uprawnień");
        }

        Page<Hire> hirePage = hireRepo.findAllHireSharesByIdUser(idUser, PageRequest.of(page, howManyRecord));

        return getPaginatedHireMyBorrowsResponseDTO(hirePage);

    }

    private PaginatedDTO<HireMyBorrowsResponseDTO> getPaginatedHireMyBorrowsResponseDTO(Page<Hire> hirePage) {
        List<HireMyBorrowsResponseDTO> mappedHireList = new ArrayList<>();

        for (Hire hireToMap : hirePage) {
            HireMyBorrowsResponseDTO hireMyBorrowsResponseDTO = mapper.map(hireToMap, HireMyBorrowsResponseDTO.class);
            hireMyBorrowsResponseDTO.getAd().setAdThumbnailId(imageRepo.findFirstImageForRentalAd(hireToMap.getAd().getId()).getId());
            mappedHireList.add(hireMyBorrowsResponseDTO);
        }

        PaginatedDTO<HireMyBorrowsResponseDTO> paginatedDTO = new PaginatedDTO<>((long) mappedHireList.size(), hirePage.getTotalPages(), mappedHireList);

        return paginatedDTO;
    }

}
