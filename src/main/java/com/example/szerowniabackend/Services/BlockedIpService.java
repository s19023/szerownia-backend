package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Models.DTOs.Requests.BlockedIpRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.PaginatedDTO;
import com.example.szerowniabackend.Models.Entities.BlockedIp;
import com.example.szerowniabackend.Models.Repositories.BlockedIpRepo;
import com.example.szerowniabackend.Security.Ip.BlockedIpClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class BlockedIpService {

    private final BlockedIpRepo blockedIpRepo;

    @Autowired
    public BlockedIpService(BlockedIpRepo blockedIpRepo) {
        this.blockedIpRepo = blockedIpRepo;
    }


    public PaginatedDTO<BlockedIp> getAllBlockedIp(Integer page, Integer howManyRecord,
                                                   Boolean sortAscending, String ip) {
        Long count;
        List<BlockedIp> ips;
        var sort = Sort.Direction.ASC;

        if (!sortAscending)
            sort = Sort.Direction.DESC;

        if (ip == null)
            count = blockedIpRepo.count();
        else
            count = blockedIpRepo.countByIpContaining(ip);

        ips = blockedIpRepo.findAllBlockedIp(PageRequest.of(page, howManyRecord, sort, "IP"), ip);
        return new PaginatedDTO<>(count, ips);
    }

    public void addIpToBlock(BlockedIpRequestDTO blockedIp) {

        if (blockedIp == null)
            throw new BlockedIp.IpCannotBeNull();

        BlockedIp blockedIpNew = new BlockedIp();
        blockedIpNew.setIp(blockedIp.getIp());

        blockedIpRepo.save(blockedIpNew);
        addBlockedIdFromDbToStaticBlockedList();
    }

    public BlockedIp getBlockedIpById(Long id) {
        return AService.Get(BlockedIp.class, blockedIpRepo, id);
    }

    public void updateBlockedIp(Long id, BlockedIpRequestDTO blockedIp) {

        BlockedIp optionalBlockedIp = AService.Get(BlockedIp.class, blockedIpRepo, id);
        Optional<BlockedIp> optionalBlockedIpIsExists = blockedIpRepo.findByIp(blockedIp.getIp());

        if (optionalBlockedIpIsExists.isPresent())
            throw new BlockedIp.IpExists();

        optionalBlockedIp.setIp(blockedIp.getIp());

        blockedIpRepo.save(optionalBlockedIp);
        addBlockedIdFromDbToStaticBlockedList();
    }

    public void deleteIpFromBlackList(String ipToUnlock) {

        if (ipToUnlock == null)
            throw new BlockedIp.IpCannotBeNull();

        if (blockedIpRepo.findByIp(ipToUnlock).isEmpty())
            throw new BlockedIp.NotFound();

        blockedIpRepo.deleteByIp(ipToUnlock);
        addBlockedIdFromDbToStaticBlockedList();
    }

    public void deleteIpFromBlackListById(Long id) {

        if (blockedIpRepo.findById(id).isEmpty())
            throw new BlockedIp.NotFound();

        blockedIpRepo.deleteById(id);
        addBlockedIdFromDbToStaticBlockedList();
    }

    private void addBlockedIdFromDbToStaticBlockedList() {
        BlockedIpClass.clearStaticListAndSynchronizeStaticBlockedListWithDbBlockedList();
    }
}
