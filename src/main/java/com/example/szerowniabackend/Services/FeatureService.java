package com.example.szerowniabackend.Services;


import com.example.szerowniabackend.Exceptions.BadRequestException;
import com.example.szerowniabackend.Exceptions.NotAcceptableException;
import com.example.szerowniabackend.Models.DTOs.Requests.FeatureCreateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Requests.FeatureUpdateRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.FeatureByIdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.FeatureListDoubleResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.FeatureListIntegerResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.FeatureListResponseDTO;
import com.example.szerowniabackend.Models.Entities.Category;
import com.example.szerowniabackend.Models.Entities.Enums.FeatureType;
import com.example.szerowniabackend.Models.Entities.Feature;
import com.example.szerowniabackend.Models.Entities.FeatureCategory;
import com.example.szerowniabackend.Models.Repositories.CategoryRepo;
import com.example.szerowniabackend.Models.Repositories.FeatureCategoryRepo;
import com.example.szerowniabackend.Models.Repositories.FeatureRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FeatureService {

    final private FeatureRepo featureRepo;
    final private CategoryRepo categoryRepo;
    final private ModelMapper mapper;
    final private FeatureCategoryRepo featureCategoryRepo;

    @Autowired
    public FeatureService(FeatureRepo featureRepo, CategoryRepo categoryRepo, FeatureCategoryRepo featureCategoryRepo) {
        this.featureRepo = featureRepo;
        this.categoryRepo = categoryRepo;
        this.featureCategoryRepo = featureCategoryRepo;
        this.mapper = new ModelMapper();
    }

    public List<FeatureListResponseDTO> getAllFeatures() {
        List<Feature> allFeatures = featureRepo.findAll();
        List<FeatureListResponseDTO> mappedFeatureListResponseDTO = new ArrayList<>();

        for (Feature allFeature : allFeatures) {
            mappedFeatureListResponseDTO.add(mapper.map(allFeature, FeatureListResponseDTO.class));
        }

        return mappedFeatureListResponseDTO;
    }

    public List<FeatureListResponseDTO> getFeatureList(Long idCategory) {
        Category category = AService.Get(Category.class, categoryRepo, idCategory);

        if (category.isAbstract())
            throw new Category.AbstractCategoryHasNoFeatures(category.getName());

        List<FeatureListResponseDTO> featureList = new ArrayList<>();

        for (FeatureCategory featureCategory : category.getFeatureCategories()) {
            if (featureCategory.getFeature().getType().equals(FeatureType.TEXT) ||
                    featureCategory.getFeature().getType().equals(FeatureType.BOOLEAN)) {
                FeatureListResponseDTO mappedFeatureListResponseDTO = mapper.map(featureCategory, FeatureListResponseDTO.class);
                mappedFeatureListResponseDTO.setName(featureCategory.getFeature().getName());
                mappedFeatureListResponseDTO.setType(featureCategory.getFeature().getType());
                featureList.add(mappedFeatureListResponseDTO);
            } else if (featureCategory.getFeature().getType().equals(FeatureType.INTEGER)) {
                FeatureListIntegerResponseDTO mappedFeatureListNumericDTO = mapper.map(featureCategory, FeatureListIntegerResponseDTO.class);
                mappedFeatureListNumericDTO.setName(featureCategory.getFeature().getName());
                mappedFeatureListNumericDTO.setType(featureCategory.getFeature().getType());
                Optional<Integer> minInteger = featureRepo.getMinValueNumeric(idCategory, featureCategory.getFeature().getIdFeature());
                Optional<Integer> maxInteger = featureRepo.getMinValueNumeric(idCategory, featureCategory.getFeature().getIdFeature());
                minInteger.ifPresent(mappedFeatureListNumericDTO::setMin);
                maxInteger.ifPresent(mappedFeatureListNumericDTO::setMax);
                featureList.add(mappedFeatureListNumericDTO);
            } else if (featureCategory.getFeature().getType().equals(FeatureType.DOUBLE)) {
                FeatureListDoubleResponseDTO mappedFeatureListNumericDTO = mapper.map(featureCategory, FeatureListDoubleResponseDTO.class);
                mappedFeatureListNumericDTO.setName(featureCategory.getFeature().getName());
                mappedFeatureListNumericDTO.setType(featureCategory.getFeature().getType());
                Optional<Double> minFloat = featureRepo.getMinValueFloat(idCategory, featureCategory.getFeature().getIdFeature());
                Optional<Double> maxFloat = featureRepo.getMaxValueFloat(idCategory, featureCategory.getFeature().getIdFeature());
                minFloat.ifPresent(mappedFeatureListNumericDTO::setMin);
                maxFloat.ifPresent(mappedFeatureListNumericDTO::setMax);
                featureList.add(mappedFeatureListNumericDTO);
            }
        }
        return featureList;
    }

    public FeatureByIdResponseDTO getFeatureById(Long idFeature) {
        Feature featureById = AService.Get(Feature.class, featureRepo, idFeature);
        return mapper.map(featureById, FeatureByIdResponseDTO.class);
    }

    public void createFeature(FeatureCreateRequestDTO featureCreateRequestDTO) {
        Feature featureToCreate = new Feature(featureCreateRequestDTO.getName(), featureCreateRequestDTO.getType());
        featureRepo.save(featureToCreate);
    }

    public void updateFeature(Long idFeature, FeatureUpdateRequestDTO featureUpdateRequestDTO) {
        Feature featureToUpdate = AService.Get(Feature.class, featureRepo, idFeature);

        if(!featureToUpdate.getFeatureOccurrences().isEmpty()) {
            throw new NotAcceptableException("Nie można edytować. Podana cecha ma już przypisany produkt");
        }

        if (featureUpdateRequestDTO.getName() != null) {
            if(!featureUpdateRequestDTO.getName().isBlank()) {
                featureToUpdate.setName(featureUpdateRequestDTO.getName());
            }
        }

        if(featureUpdateRequestDTO.getType() != null) {
            if(!featureUpdateRequestDTO.getType().isBlank()) {
                FeatureType newType;
                switch (featureUpdateRequestDTO.getType()) {
                    case "TEXT":
                        newType = FeatureType.TEXT;
                        break;
                    case "BOOLEAN":
                        newType = FeatureType.BOOLEAN;
                        break;
                    case "INTEGER":
                        newType = FeatureType.INTEGER;
                        break;
                    case "DOUBLE":
                        newType = FeatureType.DOUBLE;
                        break;
                    default:
                        throw new BadRequestException("Niepoprawny typ cechy");
                }
                featureToUpdate.setType(newType);
            }
        }

        featureRepo.save(featureToUpdate);
    }

    public void deleteFeature(Long idFeature) {
        featureRepo.delete(AService.Get(Feature.class, featureRepo, idFeature));
    }
}
