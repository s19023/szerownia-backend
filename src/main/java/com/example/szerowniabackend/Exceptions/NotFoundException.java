package com.example.szerowniabackend.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    public NotFoundException() { }

    public NotFoundException(String message) {
        super(message);
    }

    public <ID> NotFoundException(Class c, ID id) { super(c.getSimpleName() + " with id: " + id + " not exists."); }
}
