package com.example.szerowniabackend.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvaildTokenException extends RuntimeException {
    public InvaildTokenException(String tokenType) { super( tokenType + " not valid."); }
}
