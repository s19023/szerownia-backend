package com.example.szerowniabackend.Acpects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;


@Aspect
@Component
public class LoggingAspect {
    private static final Logger LOGGER = LogManager.getLogger(LoggingAspect.class);

    @Pointcut("within(@org.springframework.stereotype.Repository *) || within(@org.springframework.stereotype.Service *)"
            + " || within(@org.springframework.web.bind.annotation.RestController *)")
    public void springBeanPointcut() { }

    @Pointcut("within(com.example.szerowniabackend.Services.*)")
    public void applicationPackagePointcut() { }

    @Around("execution(* com.example.szerowniabackend.Services..*(..)))")
    public Object timingAllMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

        String className = methodSignature.getDeclaringType().getSimpleName();
        String methodName = methodSignature.getName();

        final StopWatch stopWatch = new StopWatch();

        stopWatch.start();
        Object result = proceedingJoinPoint.proceed();
        stopWatch.stop();

        LOGGER.info("Execution time of {}.{} :: {} ms {}", className, methodName, stopWatch.getTotalTimeMillis(),
                        SecurityContextHolder.getContext().getAuthentication() != null
                        ? "by " + SecurityContextHolder.getContext().getAuthentication().getName() : "");

        return result;
    }

    @AfterThrowing(pointcut = "applicationPackagePointcut() && springBeanPointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        String errorName = e.getClass().getSimpleName();
        String err = errorName.endsWith("Exception") ? errorName : errorName + "Exception";

        LOGGER.error("{} in {}() with cause: {}", err, joinPoint.getSignature().getName(), e.getMessage());
    }


}
