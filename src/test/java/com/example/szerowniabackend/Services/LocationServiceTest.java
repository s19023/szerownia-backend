package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Models.DTOs.Requests.LocalizeRequestDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.DistrictResponseDTO;
import com.example.szerowniabackend.Models.Entities.District;
import com.example.szerowniabackend.Models.Entities.Region;
import com.example.szerowniabackend.Models.Repositories.DistrictRepo;
import com.example.szerowniabackend.Models.Repositories.LocationRepo;
import com.example.szerowniabackend.Models.Repositories.RegionRepo;
import net.bytebuddy.asm.Advice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.ManyToOne;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LocationServiceTest {

    private LocationService underTest;

    @Mock
    private LocationRepo locationRepo;

    @Mock
    private DistrictRepo districtRepo;

    @Mock
    private RegionRepo regionRepo;


    @BeforeEach
    void setUp() {
        underTest = new LocationService(locationRepo, regionRepo, districtRepo);

        Region re = new Region();
        District d = new District("1815","żyrardowski",52.00254242231065,20.430579188210288,re);
        District d1 = new District("1819","obornicki",52.70994769781617,16.842518127227056,re);
        District d2 = new District("1811","leszczyński",51.872091419719006,16.568198445648495,re);
        District d3 = new District("1810","Siedlce",52.16504570986749,22.27106942100644,re);

        when(districtRepo.findAll()).thenReturn(List.of(d,d1,d2,d3));
        when(districtRepo.findById("1815")).thenReturn(Optional.of(d));
    }

    @Test
    void getClosestDistrict() {
        LocalizeRequestDTO closestTo1815 = new LocalizeRequestDTO(51.74634525,20.4562472);

        DistrictResponseDTO closest = underTest.getClosestDistrict(closestTo1815);

        assertEquals("1815",closest.getId());
        assertEquals("żyrardowski",closest.getName());
    }
}