package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.DTOs.Requests.ComplainRequestDTO;
import com.example.szerowniabackend.Models.Entities.Complaint;
import com.example.szerowniabackend.Models.Entities.Enums.ComplainStatus;
import com.example.szerowniabackend.Models.Entities.Enums.ComplaintReason;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.Hire;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.Repositories.ComplaintRepo;
import com.example.szerowniabackend.Models.Repositories.HireRepo;
import com.example.szerowniabackend.Models.Repositories.UserRepo;
import com.example.szerowniabackend.Security.Authorization;
import com.example.szerowniabackend.Security.JwtTokenUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ComplaintServiceTest {


    private ComplaintService underTest;

    @Mock
    private ComplaintRepo complaintRepo;

    @Mock
    private UserRepo userRepo;

    @Mock
    private HireRepo hireRepo;

    @Mock
    private Authorization authorization;

    private List<Complaint> complaints;

    @BeforeEach
    void setUp() {

        underTest = new ComplaintService(authorization, complaintRepo, userRepo, hireRepo);

    }


    private void setUp_getAll() {
        Hire hire1 = new Hire(LocalDate.now(), LocalDate.now().plusDays(5), 60.60, PickupMethod.PERSONAL_PICKUP, 30.0);
        Hire hire2 = new Hire(LocalDate.now(), LocalDate.now().plusDays(5), 60.60, PickupMethod.COURIER, 30.0);
        Hire hire3 = new Hire(LocalDate.now(), LocalDate.now().plusDays(5), 60.60, PickupMethod.PERSONAL_PICKUP, 30.0);

        Complaint comp1 = new Complaint((long) 1, ComplaintReason.INCOMPLETE, "", hire1, ComplainStatus.ACCEPTED);
        Complaint comp2 = new Complaint((long) 1, ComplaintReason.NOT_AS_DESCRIBED, "", hire2, ComplainStatus.REJECTED);
        Complaint comp3 = new Complaint((long) 2, ComplaintReason.DEFECTIVE, "", hire3, ComplainStatus.SUBMITTED);

        complaints = List.of(comp1, comp2, comp3);

        when(complaintRepo.findAll()).thenReturn(complaints);
    }

//    @Test
//    void getComplaints_returnsAllComplaints() {
//        setUp_getAll();
//
//        List<ComplaintDTO> result = underTest.getComplaints();
//        assertNotNull(result);
//
//        for (int i = 0; i < result.size(); i++) {
//            assertEquals(complaints.get(i).getReason(), result.get(i).getReason());
//            assertEquals(complaints.get(i).getHire().getSelectedPickupMethod(), result.get(i).getHire().getSelectedPickupMethod());
//        }
//
//    }

//    @Test
//    void getMyComplaints_returnsOnlyMyComplaints() {
//        setUp_getAll();
//        JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();
//        jwtTokenUtil.setSecretStatic("Lol");
//
//        String token = JwtTokenUtil.generateToken("test@gmail.com", 1L, Role.USER, false);
//        String token2 = JwtTokenUtil.generateToken("test@gmail.com", 2L, Role.USER, false);
//
//        List<ComplaintDTO> myComplaints = underTest.getMyComplaints("Bearer " + token);
//        List<ComplaintDTO> myComplaints2 = underTest.getMyComplaints("Bearer " + token2);
//
//        assertEquals(2, myComplaints.size());
//        assertEquals(1, myComplaints2.size());
//    }

    @Test
    void getComplaint_NotExist_ThrowsNotFoundException() {

        Hire hire2 = new Hire(LocalDate.now(), LocalDate.now().plusDays(5), 60.60, PickupMethod.COURIER, 30.0);
        Complaint comp2 = new Complaint((long) 1, ComplaintReason.NOT_AS_DESCRIBED, "", hire2, ComplainStatus.SUBMITTED);
        complaintRepo.save(comp2);

        assertThrows(NotFoundException.class, () -> underTest.getComplaint(7L));

    }

    @Test
    void createFakeComplaint_ThrowsNotYourHireException() {
        Long authorID = 1L;
        Long borrowerID = 7L;
        Long shererID = 2L;
        User borrower = new User();
        User sherer = new User();
        borrower.setId(borrowerID);
        sherer.setId(shererID);

        JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();
        jwtTokenUtil.setSecretStatic("Lol");
        String token = JwtTokenUtil.generateToken("test@gmail.com", authorID, Role.USER, false);

        Hire hire = new Hire(LocalDate.now(), LocalDate.now().plusDays(5), 60.60, PickupMethod.COURIER, 30.0);

        hire.setSharer(sherer);
        hire.setBorrower(borrower);

        ComplainRequestDTO complaintRequest = new ComplainRequestDTO();
        complaintRequest.setHireId(1L);

        when(hireRepo.findById(1L)).thenReturn(java.util.Optional.of(hire));

        assertThrows(Complaint.NotYourHire.class,() -> underTest.createComplaint(complaintRequest, "Bearer " + token));
    }

}