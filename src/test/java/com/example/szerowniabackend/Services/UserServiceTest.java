package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Email.EmailSender;
import com.example.szerowniabackend.Models.DTOs.Requests.UserRegisterRequestDTO;
import com.example.szerowniabackend.Models.Entities.User;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Security.Authorization;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepo userRepo;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private RTokenRepo rTokenRepo;
    @Mock
    private StatisticsService statisticsService;
    @Mock
    private FileSystemRepo fileSystemRepo;
    @Mock
    private ImageRepo imageRepo;
    @Mock
    private Authorization authorization;
    @Mock
    private EmailSender emailSender;

    private UserService underTest;

    @Mock
    private RentalAdRepo rentalAdRepo;

    @Mock
    private SearchAdRepo searchAdRepo;

    @Mock
    private OpinionRepo opinionRepo;

    @BeforeEach
    void setUp() {
        underTest = new UserService(userRepo, rTokenRepo, passwordEncoder, statisticsService, fileSystemRepo, imageRepo, authorization, emailSender, rentalAdRepo, searchAdRepo, opinionRepo);
    }

    @Test
    void createUser_Test_Success() {
        // given
        UserRegisterRequestDTO user = new UserRegisterRequestDTO();
        user.setEmail("random@gmail.com");
        user.setFirstName("Jan");
        user.setLastName("Kowalski");
        user.setPassword(passwordEncoder.encode("pass1"));
        user.setTelephoneNumber("123456789");

        // when
        underTest.createUser(user);

        // then
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        ModelMapper mapper = new ModelMapper();
        verify(userRepo).save(userArgumentCaptor.capture());
        UserRegisterRequestDTO userRegisterRequestDTO = mapper.map(userArgumentCaptor.getValue(), UserRegisterRequestDTO.class);

        assertThat(user.getEmail()).isSameAs(userRegisterRequestDTO.getEmail());
        assertThat(user.getPassword()).isSameAs(userRegisterRequestDTO.getPassword());


    }


}