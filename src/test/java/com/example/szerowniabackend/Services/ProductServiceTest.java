package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Models.DTOs.Responses.ProductByIdResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ProductGetMyProductsResponseDTO;
import com.example.szerowniabackend.Models.DTOs.Responses.ProductListResponseDTO;
import com.example.szerowniabackend.Models.Entities.Category;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.Product;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Security.JwtTokenUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    private ProductService productService;

    @Mock
    private ProductRepo productRepo;

    @Mock
    private CategoryRepo categoryRepo;

    @Mock
    private UserRepo userRepo;

    @Mock
    private FeatureRepo featureRepo;

    @Mock
    private FeatureOccurrenceRepo featureOccurrenceRepo;

    @BeforeEach
    void setUp() {

        productService = new ProductService(productRepo, categoryRepo, featureRepo, featureOccurrenceRepo);

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getProductsList() {

        Category category = new Category("testCategory", false, null);

        Product product1 = new Product("testName1", "testMake1", "testModel1", 5, 222);
        Product product2 = new Product("testName2", "testMake2", "testModel2", 5, 222);
        Product product3 = new Product("testName3", "testMake3", "testModel3", 5, 222);

        product1.setCategory(category);
        product2.setCategory(category);
        product3.setCategory(category);

        List<Product> productsList = List.of(product1, product2, product3);

        when(productRepo.findAll()).thenReturn(productsList);

        List<ProductListResponseDTO> productListResponseDTO = productService.getProductsList();

        assertNotNull(productListResponseDTO);
        assertEquals(3, productListResponseDTO.size());

        for (int i = 0; i < 3; i++) {
            assertEquals(productsList.get(i).getName(), productListResponseDTO.get(i).getName());
            assertEquals(productsList.get(i).getMake(), productListResponseDTO.get(i).getMake());
            assertEquals(productsList.get(i).getModel(), productListResponseDTO.get(i).getModel());
            assertEquals(productsList.get(i).getCondition(), productListResponseDTO.get(i).getCondition());
            assertEquals(productsList.get(i).getEstimatedValue(), productListResponseDTO.get(i).getEstimatedValue());
        }

    }

    @Test
    void getProductById() {

        Category category = new Category("testCategory", false, null);

        Product product1 = new Product("testName1", "testMake1", "testModel1", 5, 222);
        product1.setId(1L);

        product1.setCategory(category);

        when(productRepo.findById(1L)).thenReturn(java.util.Optional.of(product1));

        ProductByIdResponseDTO productByIdResponseDTO = productService.getProductById(1L);

        assertNotNull(productByIdResponseDTO);

        assertEquals(product1.getName(), productByIdResponseDTO.getName());
        assertEquals(product1.getMake(), productByIdResponseDTO.getMake());
        assertEquals(product1.getModel(), productByIdResponseDTO.getModel());
        assertEquals(product1.getCondition(), productByIdResponseDTO.getCondition());
        assertEquals(product1.getEstimatedValue(), productByIdResponseDTO.getEstimatedValue());

    }

    @Test
    void getMyProducts() {

        JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();
        jwtTokenUtil.setSecretStatic("Lol");
        String token = JwtTokenUtil.generateToken("test@gmail.com", 1L, Role.USER, false);

        Product product1 = new Product("testName1", "testMake1", "testModel1", 5, 222);
        Product product2 = new Product("testName2", "testMake2", "testModel2", 5, 222);

        List<Product> productsList = List.of(product1, product2);

//        when(productRepo.findAllByUserIdAndIsSearchProduct(1L, true)).thenReturn(productsList);
//        when(AService.logged().getId()).thenReturn(1L);

//        List<ProductGetMyProductsResponseDTO> myProductsResponseDTOs = productService.getMyProducts(true);
//
//        assertNotNull(myProductsResponseDTOs);
//        assertEquals(2, myProductsResponseDTOs.size());
//
//        for (int i = 0; i < 2; i++) {
//            assertEquals(productsList.get(i).getName(), myProductsResponseDTOs.get(i).getName());
//            assertEquals(productsList.get(i).getMake(), myProductsResponseDTOs.get(i).getMake());
//            assertEquals(productsList.get(i).getModel(), myProductsResponseDTOs.get(i).getModel());
//            assertEquals(productsList.get(i).getCondition(), myProductsResponseDTOs.get(i).getCondition());
//            assertEquals(productsList.get(i).getEstimatedValue(), myProductsResponseDTOs.get(i).getEstimatedValue());
//        }

    }
}