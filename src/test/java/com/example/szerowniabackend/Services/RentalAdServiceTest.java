package com.example.szerowniabackend.Services;

import com.example.szerowniabackend.Email.EmailSender;
import com.example.szerowniabackend.Exceptions.NotFoundException;
import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Models.Repositories.*;
import com.example.szerowniabackend.Security.Authorization;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class RentalAdServiceTest {

    @Mock
    private RentalAdRepo rentalAdRepo;
    @Mock
    private UserRepo userRepo;
    @Mock
    private ProductRepo productRepo;
    @Mock
    private CategoryRepo categoryRepo;
    @Mock
    private FeatureOccurrenceRepo featureOccurrenceRepo;
    @Mock
    private ModTicketRepo modTicketRepo;
    @Mock
    private LocationRepo locationRepo;
    @Mock
    private HireRepo hireRepo;
    @Mock
    private StatisticsService statisticsService;
    @Mock
    private MessageRepo messageRepo;
    @Mock
    private MessageService messageService;
    @Mock
    private SearchAdRepo searchAdRepo;
    @Mock
    private EntityManager entityManager;
    @Mock
    FileSystemRepo fileSystemRepo;
    @Mock
    ImageRepo imageRepo;
    @Mock
    ImagesService imagesService;
    @Mock
    Authorization authorization;
    @Mock
    EmailSender emailSender;


    private RentalAdService underTest;


    @BeforeEach
    void setUp() {
        underTest = new RentalAdService(rentalAdRepo, productRepo,
                categoryRepo, featureOccurrenceRepo,
                modTicketRepo, locationRepo, hireRepo,
                statisticsService, messageRepo, messageService, searchAdRepo, imageRepo
                , entityManager, authorization, emailSender, userRepo);
    }


//    @Test
//    void getListOfRentalAd() {
//        //given
//        //when
//        underTest.getListOfRentalAd();
//        //then
//        verify(rentalAdRepo).findAll();
//
//
//    }

    @Test
    @Disabled
    void getRentalAdById() {
        //given
        //when
//        underTest.getRentalAdById();
//        //then
//        verify(rentalAdRepo).findAll();
    }

    @Test
    @Disabled
    void getRentalAdByKeyWord() {
    }

    @Test
    @Disabled
    void getAllAvailableDatesOfHire() {
        Long idRentalAd = 1L;
        //given
        List<PickupMethod> pickupMethodList = List.of(PickupMethod.COURIER, PickupMethod.PARCEL_LOCKER);

        List<TimeSlot> timeSlotList = List.of(new TimeSlot(null, LocalDate.now().plusDays(10)));

        RentalAd rentalAd = new RentalAd();
        rentalAd.setTitle("cos");
        rentalAd.setTimeSlotList(timeSlotList);
        rentalAd.setLocation(new Location(56.0, 47.4, "Warszawa"));
        rentalAd.setPickupMethod(List.of(PickupMethod.PERSONAL_PICKUP));
        rentalAd.setDescription("Telewizor");
        rentalAd.setVisible(true);
        rentalAd.setPricePerDay(56.7);
        rentalAd.setDepositAmount(156.0);
        rentalAd.setPenaltyForEachDayOfDelayInReturns(200.0);

        given(rentalAdRepo.findById(rentalAd.getId())).willReturn(Optional.of(rentalAd));
        //when
        int year = 2020;
        int month = 5;
        //underTest.getAllAvailableDatesOfHire(rentalAd.getId(), year, month);
        //then
        ArgumentCaptor<RentalAd> argumentCaptor = ArgumentCaptor.forClass(RentalAd.class);
        verify(rentalAdRepo).findById(argumentCaptor.capture().getId());

        RentalAd value = argumentCaptor.getValue();
//        assertThat(value).isEqualTo()


    }

    @Test
    void getAllAvailableDatesOfHire_allValuesOK_resultListOfFreeDayInHire() {

        //given
        int year = LocalDate.now().getYear();
        int month = LocalDate.now().getMonthValue();


        Hire hire1 = new Hire(LocalDate.now().minusDays(6), LocalDate.now().plusDays(4), 34.0,
                PickupMethod.PERSONAL_PICKUP, 4.0);

        Hire hire2 = new Hire(LocalDate.now().minusDays(34), LocalDate.now().minusDays(26), 34.0,
                PickupMethod.PERSONAL_PICKUP, 4.0);

        List<Hire> hireList = List.of(hire1, hire2);

        List<LocalDate> dayList = LocalDate.of(year, month, 1)
                .datesUntil(LocalDate.of(year, month, 1).plusMonths(1)).collect(Collectors.toList());

        List<List<LocalDate>> collect = new ArrayList<>();


        //when
        for (Hire hire : hireList) {
            collect.add(hire.getDateFrom().datesUntil(hire.getDateToPlanned().plusDays(1)).collect(Collectors.toList()));
            dayList.removeAll(hire.getDateFrom().datesUntil(hire.getDateToPlanned().plusDays(1)).collect(Collectors.toList()));
        }

        //then
        assertThat(dayList)
                .isNotEmpty()
                .doesNotContain(collect.get(0).get(0))
                .doesNotContain(collect.get(1).get(1))
                .isSorted();

    }

    @Test
    void getAllAvailableDatesOfHire_idRentalAdNotExists_throwNotFoundException() {

        //given
        int year = LocalDate.now().getYear();
        int month = LocalDate.now().getMonthValue();

        List<TimeSlot> timeSlotList = List.of(new TimeSlot(null, LocalDate.now().plusDays(10)));

        RentalAd rentalAd = new RentalAd();
                rentalAd.setTitle("cos");
                rentalAd.setTimeSlotList(timeSlotList);
                rentalAd.setLocation(new Location(56.0, 47.4, "Warszawa"));
                rentalAd.setPickupMethod(List.of(PickupMethod.PERSONAL_PICKUP));
                rentalAd.setDescription("Telewizor");
                rentalAd.setVisible(true);
                rentalAd.setPricePerDay(56.7);
                rentalAd.setDepositAmount(156.0);
                rentalAd.setPenaltyForEachDayOfDelayInReturns(200.0);


        given(rentalAdRepo.findById(rentalAd.getId())).willReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> underTest.getAllAvailableDatesOfHire(rentalAd.getId()))
                .isInstanceOf(NotFoundException.class)
                .hasMessage("RentalAd with id: " + rentalAd.getId() + " not exists.");

        verify(hireRepo, Mockito.never()).findAllByAdIdWhereYearIsOrderByDateFrom(anyLong(), anyInt());
    }

    @Test
    @Disabled
    void deleteRentalAd() {
    }

    @Test
    @Disabled
    void createRentalAd() {
    }

    @Test
    @Disabled
    void updateRentalAd() {
    }

    @Test
    @Disabled
    void getHistory() {
    }

    @Test
    @Disabled
    void getMyRentalAds() {
    }

    @Test
    @Disabled
    void getRentalAdWithByParams() {
    }

    @Test
    @Disabled
    void isInteger() {
    }
}