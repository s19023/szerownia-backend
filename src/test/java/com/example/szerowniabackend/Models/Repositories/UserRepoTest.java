package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.Enums.Role;
import com.example.szerowniabackend.Models.Entities.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
class UserRepoTest {

    @Autowired
    private UserRepo underTest;

    @Test
    void findUserByEmailTest_Exist() {
        // given
        String email = "testemail@gmail.com";
        User user = new User(
                email,
                "Jan",
                "Kowalski",
                "123456789",
                "pass1",
                "salt",
                Role.USER
                );

        underTest.save(user);

        // when
        Optional<User> expected = underTest.findUserByEmail(email);

        // then
        assertThat(expected).isPresent();
    }

    @Test
    void findUserByEmailTest_NotExist() {
        // given
        // when
        Optional<User> expected = underTest.findUserByEmail("asss");

        // then
        assertThat(expected).isEmpty();
    }
}