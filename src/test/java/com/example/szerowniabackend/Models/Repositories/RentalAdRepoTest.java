package com.example.szerowniabackend.Models.Repositories;

import com.example.szerowniabackend.Models.Entities.*;
import com.example.szerowniabackend.Models.Entities.Enums.PickupMethod;
import com.example.szerowniabackend.Models.Entities.Enums.Role;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.*;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


@DataJpaTest
class RentalAdRepoTest {
    private final String SUB_CATEGORY_NAME = "Telewizory";
    private final String KEY_WORD = "Telewizory";


    @Autowired
    private RentalAdRepo underTest;

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private CategoryRepo categoryRepo;

    @Autowired
    private UserRepo userRepo;


    @AfterEach
    void tearDown() {
        underTest.deleteAll();
        productRepo.deleteAll();
        categoryRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    void findRentalAdByCategoryAndProduct_allValuesOK_resultListWithOneElement() {

        //given
        List<PickupMethod> pickupMethodList = List.of(PickupMethod.COURIER, PickupMethod.PARCEL_LOCKER);

        List<TimeSlot> timeSlotList = List.of(new TimeSlot(null, LocalDate.now().plusDays(10)));
        RentalAd rentalAd = new RentalAd();
        rentalAd.setTitle(KEY_WORD);
        rentalAd.setTimeSlotList(timeSlotList);
        rentalAd.setLocation(new Location(56.0, 47.4, "Warszawa"));
        rentalAd.setPickupMethod(List.of(PickupMethod.PERSONAL_PICKUP));
        rentalAd.setDescription("Telewizor");
        rentalAd.setVisible(true);
        rentalAd.setPricePerDay(56.7);
        rentalAd.setDepositAmount(156.0);
        rentalAd.setPenaltyForEachDayOfDelayInReturns(200.0);


        Product product = new Product("Produkt", "Marka",
                "5000", 5, 25);

        Category categoryMain = new Category("Elektronika", true, null);

        categoryRepo.save(categoryMain);

        Category category = new Category(SUB_CATEGORY_NAME, false, categoryMain);

        categoryRepo.save(category);

        product.setCategory(category);
        product.setIncludeAds(List.of(rentalAd));

        productRepo.save(product);

        Set<Product> productSet = new HashSet<>();
        productSet.add(product);

        rentalAd.setProducts(productSet);
        underTest.save(rentalAd);


        //when
        List<RentalAd> expectedList = underTest.findRentalAdByCategoryAndProduct(KEY_WORD, SUB_CATEGORY_NAME);


        //then
        assertThat(expectedList).isNotEmpty().hasSize(1);
        assertThat(expectedList.get(0).getTitle()).contains(rentalAd.getTitle());
        assertThat(expectedList.get(0).getDescription()).contains(rentalAd.getDescription());
    }


    @Test
    void findRentalAdByCategoryAndProduct_badCategoryNameAndBadKeyWord_resultEmptyList() {

        //given
        List<PickupMethod> pickupMethodList = List.of(PickupMethod.COURIER);

        List<TimeSlot> timeSlotList = List.of(new TimeSlot(null, LocalDate.now().plusDays(10)));

        RentalAd rentalAd = new RentalAd();
        rentalAd.setTitle(KEY_WORD);
        rentalAd.setTimeSlotList(timeSlotList);
        rentalAd.setLocation(new Location(56.0, 47.4, "Warszawa"));
        rentalAd.setPickupMethod(List.of(PickupMethod.PERSONAL_PICKUP));
        rentalAd.setDescription("Telewizor");
        rentalAd.setVisible(true);
        rentalAd.setPricePerDay(56.7);
        rentalAd.setDepositAmount(156.0);
        rentalAd.setPenaltyForEachDayOfDelayInReturns(200.0);

        Product product = new Product("Produkt", "Marka",
                "5000", 5, 25);

        Category categoryMain = new Category("Elektronika", true, null);

        categoryRepo.save(categoryMain);

        Category category = new Category(SUB_CATEGORY_NAME, false, categoryMain);

        categoryRepo.save(category);

        product.setCategory(category);
        product.setIncludeAds(List.of(rentalAd));

        productRepo.save(product);

        Set<Product> productSet = new HashSet<>();
        productSet.add(product);

        rentalAd.setProducts(productSet);
        underTest.save(rentalAd);

        //when
        List<RentalAd> expectedList = underTest.findRentalAdByCategoryAndProduct("tra", "tra");

        //then
        assertThat(expectedList).isEmpty();
    }

    @Test
    void findAllByUserIdOrderByPublicationDateTopFiveRecords_correctData_resultSortedListOfLastFiveRentalAdThatBelongToTheUser() {
        //given
        List<PickupMethod> pickupMethodList = List.of(PickupMethod.COURIER);

        User user = new User("xxx@gmial.com", "Jan", "Kowalski",
                 "666999666", "aaaaaa", "salt", Role.USER);

        userRepo.save(user);

        List<RentalAd> rentalAdList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {

            List<TimeSlot> timeSlotList = List.of(new TimeSlot(LocalDate.now().plusDays(i), LocalDate.now().plusDays(10)));

            RentalAd rentalAd = new RentalAd();
            rentalAd.setTitle("Jestem nr: " + i);
            rentalAd.setTimeSlotList(timeSlotList);
            rentalAd.setLocation(new Location(56.0, 47.4, "Warszawa"));
            rentalAd.setPickupMethod(List.of(PickupMethod.PERSONAL_PICKUP));
            rentalAd.setDescription("Telewizor nr: " + i);
            rentalAd.setVisible(true);
            rentalAd.setPricePerDay(56.7);
            rentalAd.setDepositAmount(156.0);
            rentalAd.setPenaltyForEachDayOfDelayInReturns(200.0);
            rentalAd.setUser(user);
            rentalAdList.add(rentalAd);
        }

        underTest.saveAll(rentalAdList);

        //when
        List<RentalAd> outputList = underTest.findAllByUserIdOrderDescByStartDateTopFiveRecords(user.getId());
        List<RentalAd> expectedList = new ArrayList<>();

        for (int i = 0; i < outputList.size(); i++) {
            //Reverse descending order for comparator to check
            expectedList.add(outputList.get(outputList.size() - 1 - i));
        }

        //then
        assertThat(expectedList)
                .isNotEmpty()
                .hasSize(5)
                .isSortedAccordingTo(Comparator.comparing(ad -> ad.getTimeSlotList().get(0).getStartDate()));
    }

}