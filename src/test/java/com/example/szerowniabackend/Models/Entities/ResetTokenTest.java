package com.example.szerowniabackend.Models.Entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ResetTokenTest {
    ResetToken token;

    @BeforeEach
    void setUp() {
        token = new ResetToken("xd");
    }

    @Test
    void afterCreated_tokenIsValid(){
        assertThat(token.isValid()).isTrue();
    }

    @Test
    void afterCreated_whenTokenExpired_tokenIsNotValid(){
        token.setValiDate(token.getValiDate().minusMinutes(45));

        assertThat(token.isValid()).isFalse();
    }

    @Test
    void afterCreated_TokenIsLength32(){
        assertThat(token.getToken().length()).isEqualTo(32);
    }
}