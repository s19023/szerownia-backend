FROM maven as build
WORKDIR /app
COPY . ./
RUN mvn clean package

FROM openjdk:11
WORKDIR /app
COPY --from=build /app/target/szerownia-backend-0.0.1-SNAPSHOT.jar ./
COPY . ./
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "szerownia-backend-0.0.1-SNAPSHOT.jar"]
